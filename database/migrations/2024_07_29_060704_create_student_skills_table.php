<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('student_skills', function (Blueprint $table) {
            $table->id();

            $table->foreignId('student_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->date('end_date');
            $table->integer('hours_passed');
            $table->text('academy');
            $table->string('type');
            $table->integer('course')->nullable();
            $table->foreignId('photo_id')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('seen')->default(0);
            $table->boolean('deleted')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('student_skills');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('creator_id');
            $table->text('description');
            $table->text('invitation')->nullable();
            $table->date('start_register_at');
            $table->date('end_register_at');
            $table->date('start_at');
            $table->date('end_at'); 
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('map_id')->nullable();
            $table->foreignId('vertical_banner_id')->nullable();
            $table->foreignId('horizontal_banner_id')->nullable();
            $table->foreignId('university_id');
            $table->foreignId('acceptor_id')->nullable();
            $table->boolean('status')->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};

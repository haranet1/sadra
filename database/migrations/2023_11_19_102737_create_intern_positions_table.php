<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('intern_positions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('event_id');
            $table->string('title');
            $table->text('description');
            $table->boolean('status')->nullable();
            $table->boolean('deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('intern_positions');
    }
};

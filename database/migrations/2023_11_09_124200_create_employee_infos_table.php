<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_infos', function (Blueprint $table) {
            $table->id();
            $table->string('employee_code')->nullable();
            $table->foreignId('university_id')->nullable();
            $table->string('university_position')->nullable();
            $table->text('about')->nullable();
            $table->boolean('status')->nullable();
            $table->foreignId('logo_id')->nullable();
            $table->foreignId('header_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_infos');
    }
};

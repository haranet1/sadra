<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_info_id');
            $table->string('user_info_type');
            $table->foreignId('paymentable_id');
            $table->string('paymentable_type');
            $table->integer('price');
            $table->text('payment_key')->nullable();
            $table->string('gateway')->nullable();
            $table->string('receipt')->nullable();
            $table->string('tracking_code')->nullable();
            $table->string('card_number')->nullable();
            $table->string('payer_bank')->nullable();
            $table->string('payment_date_time')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_infos', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('n_code');
            $table->string('registration_number');
            $table->string('co_phone')->unique();
            $table->string('activity_field');
            $table->date('year');
            $table->string('ceo');
            $table->string('website')->nullable();
            $table->string('co_email')->unique();
            $table->text('about');
            $table->text('address')->nullable();
            $table->boolean('status')->nullable();
            $table->foreignId('logo_id');
            $table->foreignId('header_id')->nullable();
            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company_infos');
    }
};

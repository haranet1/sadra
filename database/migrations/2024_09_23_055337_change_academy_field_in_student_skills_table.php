<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('student_skills', function (Blueprint $table) {
            $table->dropColumn('academy');
            $table->foreignId('academy_id')->after('hours_passed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('student_skills', function (Blueprint $table) {
            //
        });
    }
};

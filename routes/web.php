<?php

use App\Http\Controllers\Admin\AdminIndexController;
use App\Http\Controllers\Admin\ProvinceController;
use App\Http\Controllers\Admin\UnitController;
use App\Http\Controllers\Ajax\AuthAjaxController;
use App\Http\Controllers\Ajax\EventAjaxController;
use App\Http\Controllers\Auth\CompanyInfoController;
use App\Http\Controllers\Auth\StudentInfoController;
use App\Http\Controllers\Auth\EmployeeInfoController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Event\EventController;
use App\Http\Controllers\Event\InternRequestController;
use App\Http\Controllers\Event\InternShipController;
use App\Http\Controllers\Event\PlanController;
use App\Http\Controllers\Event\RegisterController as RegisterEventController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SinglePageController;
use App\Http\Controllers\StudentSkillController;
use App\Http\Controllers\User\InfoController;
use App\Http\Controllers\User\ListController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\UserManagementController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\IsEmployee;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('csp')->group(function () {
    // روتهای مرتبط با این Middleware
    Route::get('verify-mobile' , [UserController::class , 'verifyMobileShow'])->name('verify-mobile.show');
    Route::post('verify-mobile' , [UserController::class , 'verifyMobile'])->name('verify-mobile');

    Route::prefix('password')->group( function () {
        Route::get('request' , [ForgotPasswordController::class , 'showLinkRequestForm'])->name('request.password');
        Route::post('reset/sms', [ForgotPasswordController::class, 'sendVerifyCode'])->name('reset.sms.password');
        Route::post('reset/sms/verify', [ForgotPasswordController::class, 'VerifyCode'])->name('reset.sms.verify.password');
        Route::post('reset/sms/reset', [ForgotPasswordController::class, 'passwordReset'])->name('change.password');
    });


    Route::get('/' , [HomeController::class , 'index'])->name('home');
    Route::get('/opening' , [HomeController::class , 'index'])->name('opening');
    Route::get('news' , [HomeController::class , 'news'])->name('news');
    Route::get('events' , [HomeController::class , 'events'])->name('events');
    Route::get('service-incentives' , [HomeController::class , 'serviceIncentives'])->name('service.incentives');
    Route::prefix('single')->group( function () {
        Route::get('news/{id}' , [SinglePageController::class , 'news'])->name('news.signle');
        Route::get('event/{id}' , [SinglePageController::class , 'event'])->name('event.single');
    });
});
require __DIR__.'/auth.php';

Route::middleware('auth' ,'verifyMobile')->group( function () {

    Route::get('stdInfo' , [StudentInfoController::class , 'show'])->name('set.std.info');
    Route::post('stdInfo' , [StudentInfoController::class , 'store'])->name('student.store.info');

    Route::get('coInfo' , [CompanyInfoController::class , 'show'])->name('set.co.info');
    Route::post('coInfo' , [CompanyInfoController::class , 'store'])->name('company.store.info');

    Route::get('empInfo' , [EmployeeInfoController::class , 'show'])->name('set.emp.info');
    Route::post('empInfo' , [EmployeeInfoController::class , 'store'])->name('employee.store.info');


    Route::prefix('ajax')->group( function () {
        // get cities route
        Route::post('get-cities', [AuthAjaxController::class , 'getCities'])->name('auth.ajax.get-cities');
    });

    Route::middleware('CheckInfo')->group( function () {
        Route::prefix('panel')->group( function () {

            // dashboard
            Route::middleware('Dashboard')->group( function () {
                Route::middleware('EmpDashboard','IsCountry')->get('/' , [AdminIndexController::class , 'show'])->name('dashboard');
                Route::middleware('IsProvincial')->get('provincial' , [ProvinceController::class , 'show'])->name('provincial.dashboard');
                Route::middleware('IsUnit')->get('unit' , [UnitController::class , 'show'])->name('unit.dashboard');
            });

            // profile routes
            Route::prefix('single')->group( function () {
                Route::get('company/{id}' , [SinglePageController::class , 'company'])->name('company.single');
                Route::get('student/{id}' , [SinglePageController::class , 'student'])->name('student.single');
            });

            // user information
            Route::prefix('info')->group( function () {
                Route::prefix('show')->group(function(){
                    Route::get('co', [InfoController::class , 'showCo'])->name('co.show.info');
                    Route::get('std', [InfoController::class , 'showStd'])->name('std.show.info');
                    Route::get('emp/{id}', [InfoController::class , 'showEmp'])->name('emp.show.info');
                });
                Route::prefix('edit')->group(function(){
                    Route::get('co', [InfoController::class , 'editCo'])->name('co.edit.info');
                    Route::get('std', [InfoController::class , 'editStd'])->name('std.edit.info');
                    Route::get('emp/{id}', [InfoController::class , 'editEmp'])->name('emp.edit.info');
                });
                Route::prefix('delete')->group(function() {
                    Route::post('emp' , [InfoController::class , 'deleteEmp'])->name('emp.delete.info');
                });
                Route::post('co', [InfoController::class , 'updateCo'])->name('co.update.info');
                Route::post('std', [InfoController::class , 'updateStd'])->name('std.update.info');
                Route::post('emp', [InfoController::class , 'updateEmp'])->name('emp.update.info');
            });

            // events routes
            Route::prefix('events')->group( function () {
                Route::middleware('IsEmployee')->group(function(){
                    Route::get('/' , [EventController::class , 'index'])->name('index.event');
                    Route::get('single/{id}' , [EventController::class , 'single'])->name('single.event');
                    Route::get('create', [EventController::class, 'create'])->name('create.event');
                    Route::post('store', [EventController::class, 'store'])->name('store.event');
                    Route::get('edit/{id}' , [EventController::class , 'edit'])->name('edit.event');
                    Route::post('update' , [EventController::class , 'update'])->name('update.event');
                    Route::post('delete' , [EventController::class , 'delete'])->name('delete.event');
                    Route::get('unconfirmed' , [EventController::class , 'unConfirmed'])->name('unconfirmed.event');
                    Route::post('confirme' , [EventController::class , 'confirme'])->name('confirme.event');
                    /** route for province employee */
                    Route::get('confirmed' , [EventController::class , 'confirmed'])->name('confirmed.event');
                    Route::post('cancellation' , [EventController::class , 'cancellation'])->name('cancellation.event');

                });
                // plan
                Route::prefix('plan')->middleware('IsEmployee')->group(function(){
                    Route::get('index' , [PlanController::class, 'index'])->name('index.plan');
                    Route::get('list/{id}' , [PlanController::class , 'list'])->name('list.index.plan');
                    Route::get('create/{id}' , [PlanController::class , 'create'])->name('create.plan');
                    Route::post('store' , [PlanController::class , 'store'])->name('store.plan');
                    Route::get('edit/{id}' , [PlanController::class , 'edit'])->name('edit.plan');
                    Route::post('update' , [PlanController::class , 'update'])->name('update.plan');
                    Route::post('delete' , [PlanController::class , 'delete'])->name('delete.plan');
                    Route::get('delete/booth/{id}' , [PlanController::class , 'deleteBooth'])->name('delete.booth.plan');

                });
                // register
                Route::prefix('reg')->group(function(){
                    Route::middleware('IsCompany')->get('index' , [RegisterEventController::class, 'index'])->name('index.register.event');
                    Route::middleware('IsCompany')->post('create' , [RegisterEventController::class , 'create'])->name('create.register.event');
                    Route::middleware('IsCompany')->post('store' , [RegisterEventController::class , 'store'])->name('store.register.event');
                    Route::middleware('IsStudent')->get('index-std' , [RegisterEventController::class , 'studentIndex'])->name('index-std.event');
                    Route::middleware('IsStudent')->post('store-std' , [RegisterEventController::class , 'studentStore'])->name('store-std.register.event');

                    Route::middleware('IsEmployee')->get('show/{id}' , [RegisterEventController::class , 'show'])->name('show.register.event');
                    Route::middleware('IsEmployee')->get('done/{id}' , [RegisterEventController::class , 'done'])->name('done.register.event');
                    Route::middleware('IsEmployee')->get('student/{id}' , [RegisterEventController::class , 'showStudents'])->name('show-students.register.event');

                });
                // payment
                Route::prefix('pay')->group(function() {
                    Route::middleware('IsCompany')->group(function(){
                        Route::get('prepay/{id}' , [PaymentController::class , 'eventPrepay'])->name('prepay.pay.event');
                        Route::get('return/show/{id}' , [PaymentController::class , 'eventReturnShow'])->name('return.show.pay.event');
                        Route::get('result/show/{id}' , [PaymentController::class , 'eventResultShow'])->name('result.show.pay.event');
                        Route::get('invitation/{id}' , [RegisterEventController::class , 'invitation'])->name('create.invitation.event');
                    });
                    
                    Route::get('std-invitation/{id}' , [RegisterEventController::class , 'stdInvitation'])->name('std.invitation.event');
                });
                // intership
                Route::prefix('internship')->group(function(){
                    Route::get('index/{id}' , [InternShipController::class , 'eventIndex'])->name('index.internship.event');
                    Route::get('std-index/{id}' , [InternShipController::class , 'eventStdIndex'])->name('std.index.internship.event');
                    Route::post('store' , [InternShipController::class , 'store'])->name('store.internship.event');
                    Route::post('update' , [InternShipController::class , 'update'])->name('update.internship.event');
                    Route::post('delete' , [InternShipController::class , 'delete'])->name('delete.internship.event');
                });
                // news
                Route::prefix('news')->group(function(){
                    Route::get('index/{id}' , [NewsController::class , 'indexE'])->name('index.news.event');
                    Route::get('create/{id}' , [NewsController::class , 'createE'])->name('create.news.event');
                    Route::post('store' , [NewsController::class , 'storeE'])->name('store.news.event');
                });
                Route::middleware('IsCompany')->get('list' , [EventController::class , 'list'])->name('list.event');
                Route::middleware('IsStudent')->get('list-std' , [EventController::class , 'studentList'])->name('list-std.event');
            });

            // internship routes
            Route::prefix('internship')->group( function () {
                Route::get('index' , [InternShipController::class , 'index'])->name('index.internship');
                Route::get('std-index' , [InternShipController::class , 'StdIndex'])->name('std.index.internship');
                Route::middleware('IsEmployee')->get('list' , [InternShipController::class , 'list'])->name('list.internship');
                Route::middleware('IsEmployee')->get('confirme/{id}' , [InternShipController::class , 'confirme'])->name('confirme.intern.position');

                Route::prefix('request')->group(function(){
                    Route::get('std-index' , [InternRequestController::class , 'stdIndex'])->name('std-index.request.internship');
                    Route::post('store' , [InternRequestController::class , 'store'])->name('store.request.internship');
                    Route::get('co-index', [InternRequestController::class , 'coIndex'])->name('co-index.request.internship');
                    Route::post('accept' , [InternRequestController::class , 'accept'])->name('accept.request.internship');
                    Route::post('decline' , [InternRequestController::class , 'decline'])->name('decline.request.internship');
                    Route::get('emp-index' , [InternRequestController::class , 'empIndex'])->name('emp-index.request.internship');

                });
            });

            // news
            Route::prefix('news')->group( function () {
                Route::get('index' , [NewsController::class , 'index'])->name('index.news');
                Route::get('create' , [NewsController::class , 'create'])->name('create.news');
                Route::post('store' , [NewsController::class , 'store'])->name('store.news');
                Route::get('edit/{id}' , [NewsController::class , 'edit'])->name('edit.news');
                Route::post('update', [NewsController::class , 'update'])->name('update.news');
                Route::post('delete' , [NewsController::class , 'delete'])->name('delete.news');
                Route::post('confirme', [NewsController::class , 'confirme'])->name('confirme.news');
                Route::post('reject' , [NewsController::class , 'reject'])->name('reject.news');
                Route::get('unconfirmed' , [NewsController::class , 'unConfirmed'])->name('unconfirmed.news');
            });

            // list
            Route::middleware('IsEmployee')->prefix('list')->group( function () {
                Route::get('std' , [ListController::class , 'students'])->name('std.list');
                Route::get('un-std' , [ListController::class , 'unStudents'])->name('un-std.list');

                Route::get('universities' , [ListController::class , 'universities'])->name('universities.list');
            });

            // profile
            Route::prefix('profile')->group( function () {
                Route::get('edit' , [ProfileController::class , 'edit'])->name('edit.profile');
                Route::post('update' , [ProfileController::class , 'update'])->name('update.profile');
            });

            Route::prefix('skill')->group( function () {
                Route::get('index/{id}' , [StudentSkillController::class , 'show'])->name('show.skill');

                Route::get('list' , [StudentSkillController::class , 'uniList'])->name('uni.list.skill')->middleware('IsEmployee');
                Route::get('un-list/export', [ExportController::class , 'unConfirmListSkill'])->name('export.un.list.skill');

                Route::get('accepted' , [StudentSkillController::class , 'acceptedList'])->name('accepted.uni.list.skill');
                Route::get('accepted/export', [ExportController::class , 'acceptedListSkill'])->name('export.accepted.list.skill');
                
                Route::post('accept' , [StudentSkillController::class , 'accept'])->name('uni.accept.skill')->middleware('IsEmployee');
                
                Route::get('armed-list' , [StudentSkillController::class , 'armedList'])->name('armed.list.skill')->middleware('IsAdmin');
                Route::get('armed-list/export' , [ExportController::class , 'AllStudentHasSkill'])->name('export.armed-list.skill');
                Route::get('armed-search' , [StudentSkillController::class , 'armedSearch'])->name('armed.search.skill')->middleware('IsAdmin');

                Route::get('transactions/list' , [StudentSkillController::class , 'skillTransactionListForAdmin'])->name('list.of.transactions.skill')->middleware('IsAdmin');
                
                Route::middleware('IsStudent')->group( function () {
                    Route::get('create' , [StudentSkillController::class , 'create'])->name('create.skill');
                    Route::post('store' , [StudentSkillController::class , 'store'])->name('store.skill');
                    Route::get('edit/{id}' , [StudentSkillController::class , 'edit'])->name('edit.skill');
                    Route::post('update' , [StudentSkillController::class , 'update'])->name('update.skill');
                    Route::post('delete' , [StudentSkillController::class , 'delete'])->name('delete.skill');
                    Route::post('get-academy' , [StudentSkillController::class ,'getAcademyByText'])->name('get.academy.skill');

                    Route::get('pay-skill' , [PaymentController::class , 'studentPaySkills'])->name('student.pay.skill');
                    Route::get('return/show/{id}' , [PaymentController::class , 'studentSkillReturnShow'])->name('return.show.pay.skill');
                    Route::get('result/show/{id}' , [PaymentController::class , 'studentSkillResultShow'])->name('result.show.pay.skill');

                    Route::get('list-of-transactions' , [StudentSkillController::class , 'transactionsList'])->name('list.of.transactions.std');
                });
            });

            // users management route
            Route::prefix('user-management')->middleware('IsAdmin')->group( function () {
                Route::get('/' , [UserManagementController::class , 'index'])->name('index.user-management');
                Route::get('create' , [UserManagementController::class , 'create'])->name('create.user-management');
                Route::post('store' , [UserManagementController::class , 'storeEmployee'])->name('store.employee.user-managment');
                Route::get('un-confirmed' , [UserManagementController::class , 'unConfirmed'])->name('unconfirmed.user-management');
                
                Route::get('p-un-confirmed' , [UserManagementController::class , 'provincialUnConfirmed'])->name('p-unconfirmed.user-management');
                
                Route::get('create/armed-forces' , [UserManagementController::class , 'createArmedForces'])->name('create.armed-forces.user-management');
                Route::post('store/armed-forces' , [UserManagementController::class , 'storeArmedForces'])->name('store.armed-forces.user-management');

                Route::get('employee' , [UserManagementController::class , 'employee'])->name('employee.user-managment');
                Route::get('company' , [UserManagementController::class , 'company'])->name('company.user-managment');
                Route::get('student' , [UserManagementController::class , 'student'])->name('student.user-managment');
            });

            // user
            Route::prefix('user')->middleware( [IsEmployee::class] )->group( function () {
                Route::post('confirm' , [UserController::class , 'confirm'])->name('confirm.user');
            });

            // ajax
            Route::prefix('ajax')->group( function () {
                Route::post('add-event-booth' , [EventAjaxController::class , 'addBooth'])->name('add.event.booth');
                Route::post('get-booth-by-plan-id' , [EventAjaxController::class , 'getBoothByPlanId'])->name('get.event.booth-by-plan-id');
            });

            // search
            Route::prefix('search')->group( function () {
                Route::post('list/employee' , [SearchController::class , 'employee'])->name('employee.list.search');
                Route::post('list/company' , [SearchController::class , 'company'])->name('company.list.search');
                Route::post('list/student' , [SearchController::class , 'student'])->name('student.list.search');
            });

            // export
            Route::middleware( [IsEmployee::class] )->prefix('export')->group( function () {

                Route::prefix('all')->group(function(){
                    Route::get('events' , [ExportController::class , 'allEvents'])->name('event.all.export');
                    Route::get('companies' , [ExportController::class , 'allCompanies'])->name('companies.all.export');
                    Route::get('students' , [ExportController::class , 'allStudents'])->name('students.all.export');
                    Route::get('employee' , [ExportController::class , 'allEmployees'])->name('employee.all.export');
                    Route::get('intern-positions' , [ExportController::class , 'allInternPositions'])->name('intern-positions.all.export');
                });

                Route::prefix('p-uni')->group( function () {
                    Route::get('events' , [ExportController::class , 'provinceUniEvents'])->name('events.p-uni.export');
                    Route::get('intern-positions' , [ExportController::class , 'provinceUniInternPositions'])->name('intern-positions.p-uni.export');
                    Route::get('list' , [ExportController::class , 'provinceUniList'])->name('list.p-uni.export');
                    Route::prefix('sub-students')->group( function () {
                        Route::get('unconfirmed' , [ExportController::class , 'provinceUniUnConfirmedSubStudents'])->name('un.sub-students.p-uni.export');
                        Route::get('confirmed' , [ExportController::class , 'provinceUniConfirmedSubStudents'])->name('sub-students.p-uni.export');
                    });
                });

                Route::get('registred-companies-in-event/{event}' , [ExportController::class , 'registeredCompaniesInEvent'])->name('registred.companies.event.export');
                Route::get('registred-students-in-event/{event}' , [ExportController::class , 'registeredStudentsInEvent'])->name('registred.students.event.export');
            });

            // help
            Route::prefix('help')->group( function () {
                Route::get('unit' , function () { return view('panel.admin.help.unit'); });
                Route::get('province' , function () { return view('panel.admin.help.province'); });
                Route::get('country' , function () { return view('panel.admin.help.country'); });
                Route::get('company' , function () { return view('panel.admin.help.company'); });
                Route::get('student' , function () { return view('panel.admin.help.student'); });
            });

        });

    });

});

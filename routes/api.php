<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Controllers\Api\UserConteroller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group( function () {

    Route::post('store_company_full_process' , [CompanyController::class , 'storeCompanyFullProcess']);

    Route::post('store_student_full_process' , [StudentController::class , 'storeStudentFullProcess']);

    Route::prefix('user')->group( function () {

        Route::post('store' , [UserConteroller::class, 'store']);

        Route::post('getToken' , [UserConteroller::class, 'getToken']);

    });

    Route::middleware('auth:sanctum')->group( function () {

        Route::prefix('company')->group( function () {
            
            Route::post('get' , [CompanyController::class , 'getCompany']);

            Route::post('store' , [CompanyController::class , 'store']);

        });

        Route::prefix('student')->group( function () {

            Route::post('store' , [StudentController::class , 'store']);

            Route::post('student_event' , [StudentController::class , 'studentEvent']);

        });

        Route::prefix('event')->group( function () {

            Route::post('get_event_id' , [EventController::class , 'getEventId']);

            Route::post('company_event' , [EventController::class , 'companyEvent']);

            Route::post('company_pay' , [EventController::class , 'companyPay']);

        });

    });

});
@extends('frontend.layout.master')
@section('title' , 'اخبار')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/news.css')}}">
@endsection
@section('main')
    <main>
        <div class="NewsSingle">
            <section class="slideNews1 mb-5">
                <div class="container">
                    <div class="news__lists news-page mt-md-5">
                        <div class="row justify-content-center">
                            <div class="col-md-10 col-12 mt-5">
                                <div class="news-list nohover aspect-ratio-single">
                                    <a href="#">
                                    </a>
                                    <div class="items1">
                                        <a href="#">
                                            <div class="item-text font-all-2 pb-5">
                                                <h5>{{$news->title}}</h5>
                                                <hr class="d-sm-block d-none">
                                                <p>{{Str::limit($news->description,'150','...')}}</p>
                                            </div>
                                        </a>
                                        <div class="footer-text font-all-2 pt-2">

                                            <a>
                                                <i class="far fa-clock icon-style-inline-news"></i>
                                                {{verta($news->created_at)->formatDate()}}
                                            </a>
                                            @if (isset($news->event))
                                                <a class="a-news-slider" href="{{route('event.single',$news->event->id)}}">
                                                    <i class="fas fa-tag"></i>
                                                    رویداد {{$news->event->title}}
                                                </a>
                                            @else
                                                <a class="a-news-slider">
                                                    <i class="fas fa-tag icon-style-inline-news"></i>
                                                    دانشگاه {{$news->university->name}}
                                                </a>
                                            @endif
                                            <a>
                                                <i class="far fa-eye icon-style-inline-news"></i>
                                                {{$news->counter}}
                                            </a>
                                            {{-- <a>
                                                <i class="far fa-comment"></i>
                                                0
                                            </a> --}}
                                        </div>
                                        @if (isset($news->photo))
                                            <img class="img-position-single" src="{{asset($news->photo->path)}}" alt="img">
                                        @else
                                            <img class="img-position-single" src="{{asset('assets/frontend/img/sadra25.webp')}}" alt="img">
                                        @endif
                                        <i class="fa fa-arrow-down go-down i-single icon-style-arrow"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-10 col-12">
                                <div class="textarea-news">
                                    <h1 class="font-all-2">
                                        {{$news->title}}
                                    </h1>
                                    <p>{{$news->description}}</p>
                                    <div class="div-news-single font-all-2" class="tag-and-share">
                                        {{-- <div class="tags">
                                            برچسب ها:
                                            <span><a href="#"></a></span>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-10 col-12 mt-5">
                                <div class="textarea-news">
                                    <div class="container">
                                        <div class="innerbox scrollslider">
                                            <h4 style="margin-top:19px" class="font-all-2">
                                                <span class="spanComment">نظرات کاربران پیرامون این مطلب</span>
                                            </h4>
                                            <div class="row">
                                                <div class="col-md-12 w-100" style="padding-left:0 !important;">
                                                        <div class="rcommentsmarginbottom font-all">
                                                            <input id="TBX_Name" maxlength="30" class="rashinform-control rcommentstbx" placeholder="نام و نام خانوادگی">
                                                        </div>
                                                        <div id="TR_Email" class="rcommentsmarginbottom font-all">
                                                            <input id="TBX_Email" maxlength="100" class="rashinform-control rcommentstbx" placeholder="پست الکترونیک">
                                                        </div>
                                                        <div id="TR_Tell" class="rcommentsmarginbottom font-all">
                                                            <input id="TBX_tell" maxlength="15" class="rashinform-control rcommentstbx" placeholder="تلفن تماس">
                                                        </div>
                                                        <div id="Div1" class="rcommentsmarginbottom font-all">
                                                            <textarea id="TBX_Mesg" class="rashinform-control rcommentstbxmsg" placeholder="متن نظر"></textarea>
                                                        </div>
                                                        <div class="rcommentsclearfix8"></div>
                                                        <div>
                                                            <div class="rcommentspullleft font-all">
                                                                <input name="BTN" type="button" id="BTN" class="usercomments_90518_button css_" value="ثبت نظر" onclick="InsertComment()">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div> --}}

                        </div>
                    </div>
            </section>
        </div>


    </main>
@endsection


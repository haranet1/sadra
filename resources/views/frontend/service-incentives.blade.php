@extends('frontend.layout.master')
@section('title','مشوق های خدمتی')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/news.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/service-incentives.css')}}">
@endsection
@section('bodyClass','News')
@section('main')
    <div class="mb-10">

        <section class="mb-5 slideNews">
            <div class="title">
                <h1 class="text-white">مشوق های خدمتی</h1>
            </div>
            <!-- slider-->
            <div class="col-lg-12 col-md-12 col-12 h-200">
                <div class="container">
                    <div class="row">
                        <div class="card rounded-2rem bg-gray">
                            <div class="py-5 card-body d-flex justify-content-center">
                               {{-- <button></button> --}}
                               <a href="{{ asset('assets/files/protocol-1.pdf') }}" target="_blank" class="px-4 mx-3 rounded btn btn-warning">
                                    دانلود شیوه نامه
                                </a>
                                @auth
                                @else
                                    <a href="{{ route('login') }}" class="px-4 mx-3 rounded btn btn-warning">
                                        ثبت اطلاعات
                                    </a>
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--slider end-->
        </section>

        <section class="area-txt-incentive ">
            <div class="container">
                <div class="row">
                    <div class="col-7">

                        <div class="text-personology">
                            <div class="section-title section-title-incentive">
                                <h2> مشوق های خدمتی برای مهارت آموزان دانشگاهی </h2>
                                <div class="bar"></div>
                            </div>
                            <p>
                                بر اساس پیگیری‌های صورت گرفته طی سال‌های گذشته توسط معاونت پژوهشی وزارت علوم، خوشبختانه شیوه نامه مشوق‌های خدمتی برای دانشجویان و دانش‌آموختگان دانشگاهی تدوین و توسط ستاد کل نیروهای مسلح ابلاغ گردید.
                                بر اساس این شیوه نامه از این پس دانشجویان و دانش‌آموختگان دانشگاهی با کسب مهارت‌های تخصصی و عمومی می‌توانند از مشوق‌های خدمتی همچون کسری خدمت، مرخصی تشویقی، تعیین سازمان محل خدمت، بخشش اضافه خدمت سنواتی، تعویق اعزام و اعطای یک درجه بالاتر در صورت دارا بودن صلاحیت‌های لازم و متناسب با سوابق مرتبط، بهره‌مند شوند.
                                سوابق و اطلاعات دوره های مهارت آموزی برگزار شده در دانشگاه ها و مراکز آموزش عالی کشور باید تا دو ماه قبل از تاریخ اعزام توسط دانشجویان آماده شده و توسط دانشگاه ها مورد تایید قرار گیرد. در این راستا شیوه نامه و سامانه مربوطه برای دانشگاه ها ارسال شده و روش اجرایی آن از طریق سامانه صدرا دانشگاه ها خواهد بود.
                                این نخستین بار است که مهارت آموزی تاثیر مستقیم در فرایند خدمت سربازی دانشجویان داشته و نیاز است دانشگاه‌ها همکاری و هماهنگی لازم را با دفتر ارتباط با جامعه و صنعت وزارت علوم، تحقیقات و فناوری جهت اجرای این شیوه نامه داشته باشند.
                            </p>
                            <h4>
                                چه دانشجویانی میتوانند از این مشوق های خدمتی استفاده کنند :
                            </h4>
                            <p>
                                <span>-	فارغ التحصیلان دانشگاهی شامل دانشگاه های سراسری، آزاد ، غیر انتفاعی ، پیام نور </span>
                                <span>- فارغ التحصیلان دانشگاهی دانشگاه های علمی کاربردی و فنی حرفه ایی ( لازم به ذکر است در صورتی که این افراد دیپلم فنی و کاردانش دارند باید مستقیما از سامانه سخا درخواست مشوق خود را ثبت کنند و شامل حداکثر مشوق ها میشوند و نیازی نیست در سامانه صدرا ثبت نام کنند.)</span>
                            </p>

                        </div>

                        <div class="col-lg-12">
                            <h4 class="mt-2">
                                چه مدارکی در سامانه صدرا قابل قبول است :
                            </h4>
                            <p>
                                -	مدارک ارائه شده توسط دانشجویان باید مورد تایید دانشگاه بوده در صورتی که دوره در سازمان خارج از دانشگاه برگزار شده است نباید شامل موارد زیر باشد. موارد زیر به صورت جداگانه در سامانه سرباز ماهر یا سخا  توسط خود دانشجو ثبت شده و احراز آن توسط ستاد کل انجام خواهد شد.
                            </p>
                            <table class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th colspan="4" class="text-center">
                                    مراکز آموزش مورد تایید ستاد کل برای اجرای دوره های مهارت تخصصی
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>مراکز دارای مجوز از سازمان فنی و حرفه‌ای</td>
                                  <td>جهاد دانشگاهی</td>
                                  <td>وزارت آموزش و پرورش</td>
                                  <td>وزارت ارتباطات</td>
                                </tr>
                                <tr>
                                  <td>سازمان تبلیغات اسلامی</td>
                                  <td>وزارت جهاد کشاورزی</td>
                                  <td>وزارت فرهنگ و ارشاد اسلامی</td>
                                  <td>بنیاد ICDL</td>
                                </tr>
                                <tr>
                                  <td>وزارت بهداشت، درمان و آموزش پزشکی</td>
                                  <td>وزارت ورزش و جوانان</td>
                                  <td>وزارت گردشگری و میراث فرهنگی</td>
                                </tr>
                              </tbody>
                            </table>
                            <p class="text-danger">
                                -	نکته : طبق جدول بالا مدارک مراکز فنی و حرفه ای باید در سامانه سخا ثبت شود و اگر در سامانه صدرا ثبت شوند کل درخواست رد خواهد شد
                            </p>
                            <p>
                              -	تصمیم گیری در مورد تایید یا رد یک دوره و تایید اطلاعات درج شده توسط دانشجو به عهده مدیر ارتباط با جامعه و صنعت دانشگاه میباشد.
                            </p>
                            <h4>امتیاز بندی هر یک از مدارک ثبت شده چگونه است: </h4>
                            <p>
                                -	دانشجویان باید مطابق جدول زیر مدارک خود را در دسته بندی های مناسب بارگزاری کرده تا امتیاز مورد نیاز ذکر شده را کسب کنند.
                            </p>
                            <div class="overflow-hidden border rounded-3">
                                <img src="{{ asset('assets/frontend/image/skill-point.jpg') }}" alt="">
                            </div>
                            <h4 class="mt-4">میزان بهره مندی از مشوق ها چگونه است : </h4>
                            <p>-	بهره مندی از مشوق ها مطابق شیوه نامه ابلاغی ستاد کل بر اساس جدول زیر است:</p>
                            <div class="overflow-hidden border rounded-3">
                                <img src="{{ asset('assets/frontend/image/skill-point2.jpg') }}" alt="">
                            </div>
                            <!-- <ul class="list-of-items">
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                    ثبت نام در سامانه صدرا حداقل 2 ماه قبل از اعزام
                                </li>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                    دریافت تاییده امتیازات کسب شده از دانشگاه آخرین محل تحصیل
                                </li>
                                <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                    دریافت گواهی مهارتی از سازمان یا یگان مورد تایید
                                </li>
                                <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                    ثبت کسری در برگه سبز توسط پلیس+۱۰
                                </li>
                            </ul> -->
                            <!-- <h4>
                                نکات مهم برای اعزامی های 1403/05/01
                            </h4>
                            <p class="text-danger">
                                دانشجویان دانشگاه ها از تاریخ 1403/03/25لغایت 1403/04/12با مراجعه به سامانه کار امد در بخش مشوق های خدمتی باید ثبت نام کرده و از دانشگاه آخرین محل تحصیل پیگیری تایید کارشناسان مشخص شده باشند.
                            </p> -->
                        </div>

                    </div>

                    <div class="col-1">

                    </div>

                    <div class="col-4">

                        <div class="mb-3 img-soldier">
                            <img src="{{ asset('assets/frontend/image/sarbazi.jpg') }}" alt="">
                        </div>

                        <div class="overflow-hidden border rounded-3">
                            <img src="{{ asset('assets/frontend/image/incentive-t.png') }}" alt="">
                        </div>

                        <h4 class="mt-4 text-center">ویدیو آموزشی</h4>

                        {{-- <div class="mt-4 h_iframe-aparat_embed_frame">
                            <span style="display: block;padding-top: 57%">
                            </span>
                            <iframe src="{{ asset('assets/frontend/video/moshavegh-720p.mp4') }}" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true">
                            </iframe>
                        </div> --}}

                        {{-- <div id="43997408601"><script type="text/JavaScript" src="https://www.aparat.com/embed/cpvl7gv?data[rnddiv]=43997408601&data[responsive]=yes&titleShow=true"></script></div> --}}

                        <style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class="h_iframe-aparat_embed_frame"><span style="display: block;padding-top: 57%"></span><iframe src="https://www.aparat.com/video/video/embed/videohash/cpvl7gv/vt/frame?titleShow=true"  allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe></div>

                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection

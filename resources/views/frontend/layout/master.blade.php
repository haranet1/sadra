<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta content="index,follow" name="robots">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/panel/images/logo/sadra-logo-min.svg')}}"/>
    <title>صدرا | @yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">
    @yield('css')
</head>
<body class="@yield('bodyClass')">
    @include('frontend.layout.header')

    <main>
        @yield('main')
    </main>

    @include('frontend.layout.footer')
    <script src="{{asset('assets/frontend/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/all.js')}}"></script>
    <script src="{{asset('assets/frontend/js/script.js')}}"></script>
    <script src="{{asset('assets/frontend/js/header.js')}}"></script>
    @yield('script')
</body>
</html>

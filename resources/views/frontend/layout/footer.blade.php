<?php
    use \App\Models\News;

    $News = News::Confirmed()->orderByDesc('created_at')->take(5)->get();
?>
<footer>
    <a href="#" class="go-top icon-act"><i class="fa fa-arrow-up icon-footer-arrow-up"></i></a>
    <div class="container-xxl row me-auto ms-auto gap-5 gap-sm-0">
        <div class="col-lg-3 col-sm-6 gooter__about-us">
            <div class="title font-all">
                <h5>پرتال رسمی رویداد صدرا </h5>
            </div>
            <div class="text font-all">
                <p>
                    دانشگاه آزاد اسلامی چندی است با برگزاری طرح پویش تلاش کرده تا گام جدی در مسیر برقراری ارتباط منسجم بردارد و با ایجاد زمینه حضور دانشجویان در صنایع، از یک‌سو مهارت دانشجویان را تقویت کند و از سوی دیگر نیازهای صنایع را پوشش دهد. برخی واحدهای دانشگاهی قدم‌های دیگری برای تکمیل این ارتباط برداشتند؛ مثل واحد جامع نجف‌آباد که با رویدادی به نام «صدرا» صنعت و دانشگاه را به هم نزدیک کرده است.
               </p>
            </div>
            <div class="social-icon font-all-2">
                <span>ما را در شبکه های اجتماعی دنبال کنید : </span>
                <a href="#" title="Instagram">
                    <i class="fab fa-instagram icon-footer-style"></i>
                </a>
                <a href="#" title="Aparat">
                    <svg class="svg-footer" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
                         id="IconChangeColor">
                        <path
                            d="M12.0014 1.5938C2.7317 1.5906-1.9119 12.7965 4.641 19.3515c2.975 2.976 7.4496 3.8669 11.3374
                             2.257 3.8877-1.61 6.4228-5.4036 6.4228-9.6116 0-5.7441-4.6555-10.4012-10.3997-10.4031zM6.11 6.783c.5011-2.5982 3.8927-3.2936 5.376-1.1028 1.4834 2.1907-.4216 5.0816-3.02 4.5822-1.6118-.3098-2.6668-1.868-2.356-3.4794zm4.322 8.9882c-.5045 2.5971-3.8965 3.288-5.377 1.0959-1.4807-2.1922.427-5.0807 3.0247-4.5789 1.612.3114 2.6655 1.8714 2.3524 3.483zm1.2605-2.405c-1.1528-.2231-1.4625-1.7273-.4917-2.3877.9708-.6604 2.256.18 2.0401 1.3343-.1347.7198-.8294 1.1924-1.5484 1.0533zm6.197 3.8375c-.501 2.5981-3.8927 3.2935-5.376 1.1028-1.4834-2.1908.4217-5.0817 3.0201-4.5822 1.6117.3097 2.6667 1.8679 2.356 3.4794zm-1.9662-5.5018c-2.5981-.501-3.2935-3.8962-1.1027-5.3795 2.1907-1.4834 5.0816.4216 4.5822 3.02-.3082 1.6132-1.8668 2.6701-3.4795 2.3595zm-2.3348 11.5618l2.2646.611c1.9827.5263 4.0167-.6542 4.5433-2.6368l.639-2.4016a11.3828 11.3828 0 0 1-7.4469 4.4274zM21.232 3.5985l-2.363-.6284a11.3757 11.3757 0 0 1 4.3538 7.619l.6495-2.4578c.5194-1.9804-.6615-4.0076-2.6403-4.5328zM.6713 13.8086l-.5407 2.04c-.5263 1.9826.6542 4.0166 2.6368 4.5432l2.1066.5618a11.3792 11.3792 0 0 1-4.2027-7.145zM10.3583.702L8.1498.1261C6.166-.4024 4.1296.7785 3.603 2.763l-.5512 2.082A11.3757 11.3757 0 0 1 10.3583.702Z"
                            id="mainIconPathAttribute"></path>
                    </svg>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 text-center footer__symbols">
            <div class="title text-md-center text-end font-all">
                <h5>نماد های اعتماد الکترونیکی</h5>
            </div>

            <img  alt="img" src="{{asset('assets/frontend/img/logosaman.png')}}">
        </div>
        <div class="col-lg-2 col-sm-6 footer__list">
            <div class="title font-all">
                <h5>لینک های مفید</h5>
            </div>
            <ul class="p-0 font-all-2">
                <li>
                    <a href="https://www.leader.ir/" target="blank">پایگاه دفتر مقام معظم رهبری</a>
                </li>

                <li>
                    <a href="https://www.president.ir/" target="blank">پایگاه دفتر  ریاست جمهوری</a>
                </li>

                <li>
                    <a href="http://mmtehranchi.ir/" target="blank">سایت دکتر طهرانچی </a>
                </li>

                <li>
                    <a href="https://iau.ir/fa" target="blank">سازمان مرکزی دانشگاه آزاد اسلامی</a>
                </li>

                <li>
                    <a href="https://sanjesh.iau.ir/fa" target="blank">مرکز سنجش و پذیرش دانشگاه آزاد اسلامی </a>
                </li>

            </ul>
        </div>
        <div class="col-lg-2 col-sm-6 footer__list">
            <div class="title font-all">
                <h5>اخبار های داغ</h5>
            </div>
            <ul>
                @foreach ($News as $news)
                    <li>
                        <a href="{{ route('news.signle',$news->id) }}">{{ Str::limit($news->title,'50','...')}}</a>
                    </li>
                @endforeach
            </ul>

        </div>
        <div class="col-lg-3 col-sm-6 footer__contact-us">
            <div class="title font-all">
                <h5>ارتباط با ما</h5>
            </div>
            <div class="text font-all-2">
                <p>
                    نشانی:
                    <br>
                    تهران، ولنجک، انتهای بلوار دانشجو، خیابان البرز یکم، سازمان سما
                </p>
                <p>تلفن: 30-22174327</p>
                <p>کد پستی: 1984136573</p>
                <p>ساعات پاسخگویی:
                    <span>
                        7:30 الی 14:50
                    </span>
                </p>
            </div>
            <!--<div class="title">
                <h5 class="font-all">گواهینامه های اخذ شده </h5>
            </div>
            <div class="aways_symbul d-flex flex-wrap">
                <img src="assets/img/ufimember.jpg" alt="" width="45%" title="" style="margin-left:5px;">
                <img src="assets/img/mtc2.jpg" alt="" width="45%" title="" style="margin-left:5px;">
            </div>-->
        </div>
        <hr class="mt-5 opacity-25 text-secondary">
        <div class="col-12 text-center text-primary">
            معاونت علوم تربیتی و مهارتی دانشگاه آزاد اسلامی
        </div>
        <div class="col-12 m-0 p-0 d-flex div-footer-height align-items-center justify-content-center
        justify-content-xl-between flex-wrap flex-xl-row flex-column" >
            <p class="text-center text-xl-right font-all">
                حقوق مادی و معنوی این وب سایت متعلق به دانشگاه آزاد اسلامی می باشد
            </p>
            <div class="copyright">
                <span class="span-footer">copyright © 2023 powered by
                    <a class="a-footer" href="https://haranet.ir" target="_blank">www.haranet.ir</a>
                </span>
            </div>
        </div>
    </div>
</footer>

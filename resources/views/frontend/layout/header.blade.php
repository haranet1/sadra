<header>
{{--    <div class="mobile-top-header d-lg-none container-fluid">
        <div class="info-lang justify-content-between align-items-center row flex-nowrap w-100 m-auto">
            <div class="infocall">

                <a  href="{{route('login')}}"> <i class="fa fa-user"></i>  ورود </a>
                <a  href="{{route('register')}}">  <i class=" me-2 fa fa-pen-to-square"></i>ثبت نام</a>

            </div>
            --}}{{--<div class="langs" style="justify-content: flex-start;">
                <select onchange="document.location.href=this.value">
                    <option value="http://isfahanfair.ir/">FA</option>
                    <option value="http://english.isfahanfair.ir/">EN</option>
                    <option value="http://arabic.isfahanfair.ir/">AR</option>
                    <option value="http://arabic.isfahanfair.ir/">RU</option>
                </select>
            </div>--}}{{--
        </div>
    </div>--}}
    @if(\Illuminate\Support\Facades\Route::is('opening'))
        <div class="d-flex">
            <div id="i1" class="intro1">
                <img src="{{asset('assets/frontend/image/bc1.jpeg')}}" alt="">
                <div class="open-intro">
                    <img src="{{asset('assets/frontend/image/nn.png')}}" alt="">

                </div>
            </div>
            <div id="i2" class="intro2">
                <img src="{{asset('assets/frontend/image/bc1.jpeg')}}" alt="">
            </div>
            <div class="text-intro">
                <span>خوش آمدید</span>
                <p class="p-intro">آیین رو نمایی از سامانه مدیریت رویدادهای صدرا</p>
            </div>

        </div>
    @endif
    <div class="header-container w-100 container-fluid container-lg">
        <div class="menu d-lg-none d-flex align-items-center justify-content-between">
            <div class="hamburger-menu">
              <span class="menuIcon" id="toggle"></span>
            </div>

            <img class=" logo-mobile-header-style  logo-mobile d-lg-none bg-white p-2 rounded-circle float-end ms-3" src="{{asset('assets/frontend/img/logoRR.png')}}" alt="img" >
        </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="nav col-lg-8 col-12">
                        <ul class="parent-nav-responsive d-lg-none ">
                            <li class="{{Route::is('home')?'active':''}}">
                                <a href="{{route('home')}}">صفحه اصلی</a>
                            </li>
                            <li class="{{Route::is('news')?'active':''}}">
                                <a href="{{route('news')}}">اخبار</a>
                            </li>
                            <li class="{{Route::is('events')?'active':''}}">
                                <a href="{{route('events')}}">رویداد ها</a>
                            </li>
                            <li class="{{ Route::is('service.incentives') ? 'active' : '' }}">
                                <a href="{{ route('service.incentives') }}">مشوق های خدمتی</a>
                            </li>
                            {{-- <li class="have-child1">
                                <a href="javascript:void(0);">تقویم</a>
                                <div class="div-icon">
                                </div>
                                <ul class="child-nav1">
                                    <li><a href="javascript:void(0);">رویداد داخلی 1402</a></li>
                                    <li><a href="javascript:void(0);">رویداد خارجی 1402</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="">
                                <a href="javascript:void(0);">بلیط بازدید</a>
                            </li> --}}
                            {{-- <li class="have-child1">
                                <a href="javascript:void(0);">رسانه ها</a>
                                <div class="div-icon">
                                </div>
                                <ul class="child-nav1 c1">
                                    <li><a href="javascript:void(0);">گالری تصاویر</a></li>
                                    <li><a href="javascript:void(0);">آرشیو کلیپ</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child1">
                                <a href="javascript:void(0);">تور مجازی</a>
                                <div class="div-icon">
                                </div>
                                <ul class="child-nav1">
                                    <li><a href="javascript:void(0);">رویداد های 1402</a></li>
                                    <li><a href="javascript:void(0);">محل برگزاری</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child1">
                                <a href="javascript:void(0);">خدمات ما</a>
                                <div class="div-icon">
                                </div>
                                <ul class="child-nav1">
                                    <li><a href="javascript:void(0);">قوانین و مقررات کلی رویداد</a></li>

                                    <li><a href="javascript:void(0);">خدمات رویدادی</a></li>
                                    <li><a href="javascript:void(0);">ثبت شکایات</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child1 menu1">
                                <a href="javascript:void(0);" >شرایط و مقررات اجرایی</a>
                                <div class="div-icon">
                                </div>
                                <ul class="child-nav1">
                                    <li>
                                        <a href="javascript:void(0);">کشاورزی 1402</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">سنگ و معدن 1402</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">تاسیسات 1402</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">مبل خانگی 1402</a>
                                    </li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child1 menu1">
                                <a href="javascript:void(0);" >رویداد های 1401</a>
                                <div class="div-icon">
                                </div>                                        <ul class="child-nav1">
                                <li>
                                    <a href="javascript:void(0);">لوازم خانگی</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">فرش دست بافت</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">جامع کشاورزی</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">سرمایشی و گرمایشی</a>
                                </li>
                            </ul>
                            </li> --}}
                        </ul>

                        <ul class="parent-nav d-lg-flex  d-sm-none d-none">
                            <li class="{{Route::is('home')?'active':''}}">
                                <a href="{{route('home')}}">صفحه اصلی</a>
                            </li>
                            <li class="{{Route::is('news')?'active':''}}">
                                <a href="{{route('news')}}">اخبار</a>
                            </li>
                            <li class="{{Route::is('events')?'active':''}}">
                                <a href="{{route('events')}}">رویداد ها</a>
                            </li>
                            <li class="{{ Route::is('service.incentives') ? 'active' : '' }}">
                                <a href="{{ route('service.incentives') }}">مشوق های خدمتی</a>
                            </li>
                            {{-- <li class="have-child">
                                <a href="#">تقویم</a>
                                <i class="  mt-lg-1 fa fa-angle-left ps-2 pe-lg-2" style="margin-top:3px !important;"></i>
                                        <ul class="child-nav">
                                            <li><a href="#">رویداد داخلی 1402</a></li>
                                            <li><a href="#">رویداد خارجی 1402</a></li>
                                        </ul>
                            </li> --}}
                            {{-- <li class=""><a href="#">بلیط بازدید</a></li> --}}
                            {{-- <li class="have-child"><a href="#">رسانه ها</a>
                                <i class=" mt-lg-1 fa fa-angle-left ps-2 pe-lg-2" style="margin-top:3px !important;"></i>
                                <ul class="child-nav">
                                    <li><a href="#">گالری تصاویر</a></li>
                                    <li><a href="#">آرشیو کلیپ</a></li>
                                    <li class="have-child">
                                        <a href="#" >پلان رویداد</a>
                                        <i class="float-lg-start mt-lg-1 fa fa-angle-left  pe-lg-2" style="margin-top:-24px !important;"></i>
                                        <ul class="ul-child-2">
                                            <li>
                                                <a href="#">پلان رویداد خودرو</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child"><a href="#">خدمات ما</a>
                                <i class=" mt-lg-1 fa fa-angle-left ps-2 pe-lg-2" style="margin-top:3px !important;"></i>
                                <ul class="child-nav">
                                    <li><a href="#">قوانین و مقررات کلی رویداد</a></li>
                                    <li class="have-child">
                                        <a href="#" >شرایط و مقررات اجرایی</a>
                                        <i class="float-lg-start mt-lg-1 fa fa-angle-left" style="margin-top:-24px !important;"></i>
                                        <ul class="ul-child-2  font-all-2">
                                            <li>
                                                <a href="#">کشاورزی 1402</a>
                                            </li>
                                            <li>
                                                <a href="#">سنگ و معدن 1402</a>
                                            </li>
                                            <li>
                                                <a href="#">تاسیسات 1402</a>
                                            </li>
                                            <li>
                                                <a href="#">مبل خانگی 1402</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">خدمات رویدادی</a></li>
                                    <li><a href="#">ثبت شکایات</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="have-child"><a href="#">تور مجازی</a>
                                <i class=" mt-lg-1 fa fa-angle-left ps-2 pe-lg-2" style="margin-top:3px !important;"></i>
                                <ul class="child-nav font-all-2">
                                    <li><a href="#">رویداد های 1402</a></li>
                                    <li class="have-child">
                                        <a href="#" >رویداد های 1401</a>
                                        <i class="float-lg-start mt-lg-1 fa fa-angle-left  pe-lg-2" style="margin-top:-24px !important;"></i>
                                        <ul class="ul-child-2">
                                            <li>
                                                <a href="#">لوازم خانگی</a>
                                            </li>
                                            <li>
                                                <a href="#">فرش دست بافت</a>
                                            </li>
                                            <li>
                                                <a href="#">جامع کشاورزی</a>
                                            </li>
                                            <li>
                                                <a href="#">سرمایشی و گرمایشی</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">محل برگزاری</a></li>
                                </ul>
                            </li> --}}
                        </ul>
                    </div>
                    <div class="col-lg-4  d-lg-flex info-lang align-items-center position-relative">
                        <div class="infocall">
                            @auth
                                <a href="{{route('dashboard')}}"><i class="fa fa-user icon-header-style"></i> {{Auth::user()->name}} {{Auth::user()->family}}</a>
                                <a href="{{route('logout')}}"><i class="fa-solid fa-right-from-bracket icon-header-style"></i> خروج </a>
                            @else
                                <a  href="{{route('login')}}"> <i class="fa fa-user icon-header-style"></i>  ورود </a>
                                <a  href="{{route('register')}}"> <i class=" me-2 fa fa-pen-to-square icon-header-style"></i>ثبت نام</a>
                            @endauth
                        </div>
                        {{--<div class="langs" style="justify-content: flex-start;">
                            <a href="#" class="active">FA</a>
                            <a href="http://english.isfahanfair.ir/">EN</a>
                            <a href="#">AR</a>
                        </div>--}}
                    </div>
                    <img class="logo d-lg-block d-none image-floid logo-header-logo-style" src="{{asset('assets/frontend/img/logoRR.png')}}" alt="img" >

                </div>

            </div>
    </div>
</header>

<section class="news-slider">
    <div class="container">
        <div class="row news-title pb-3 pb-sm-0">
            <div class="align-items-center gap-2">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link nav-custom font-all-2 active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">جدیدترین اخبار</button>
                    </li>
                    {{-- <li class="nav-item" role="presentation">
                        <button class="nav-link nav-custom font-all" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">محبوب ترین اخبار</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link nav-custom font-all" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">بهترین اخبار</button>
                    </li> --}}
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    @if (count($News)>0)
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="owl-carousel OwlSlider1 owl-theme" id="tabOwl">
                                @foreach ($News as $news)
                                    <div class="">
                                        <div aria-label="1 / 9"
                                            class="swiper-slide p1 swiper-slide-news swiper-slide-visible swiper-slide-next"
                                            data-swiper-slide-index="0" role="group">
                                            <!--  data-tilt="" for parallax banner -->
                                            <a href="{{route('news.signle',$news->id)}}">
                                                <div class="items">
                                                    <div class="item-text font-all-2">
                                                        <h5>{{$news->title}}</h5>
                                                        <hr>
                                                        <p>{{Str::limit($news->description,'150','...')}}</p>
                                                    </div>
                                                    <div class="footer-text font-all-2">
                                                        {{-- <a href="#">
                                                        </a> --}}
                                                        <a href="#">
                                                            <i class="far fa-clock icon-style-inline-news"></i>
                                                            {{verta($news->created_at)->formatDate()}}
                                                        </a>
                                                            @if (isset($news->event))
                                                                <a class="a-news-slider" href="{{route('event.single',$news->event->id)}}">
                                                                    <i class="fas fa-tag icon-style-inline-news"></i>
                                                                    رویداد {{$news->event->title}}
                                                                </a>
                                                            @else
                                                                <a class="a-news-slider">
                                                                    <i class="fas fa-tag icon-style-inline-news"></i>
                                                                    دانشگاه {{$news->university->name}}
                                                                </a>
                                                            @endif
                                                            <a href="#">
                                                                <i class="far fa-eye icon-style-inline-news"></i>
                                                                {{$news->counter}}
                                                            </a>
                                                            {{-- <a href="#">
                                                                <i class="far fa-comment"></i>
                                                                0
                                                            </a> --}}
                                                    </div>
                                                    @if (isset($news->photo))
                                                        <img class="image-news-slider" src="{{asset($news->photo->path)}}" alt="img">
                                                    @else
                                                        <img src="{{asset('assets/frontend/img/sadra25.webp')}}" alt="img ">
                                                    @endif
                                                    <i class="fa fa-arrow-down icon-style-arrow"></i>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    @endif

                    {{-- <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div> --}}
                    {{-- <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div> --}}
                </div>

            </div>
        </div>
    </div>
</section>

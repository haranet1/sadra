<section class="email-tel">
    <div class="container">
        <div class="row justify-content-center row-gap-5">
            <div class="col-sm-8 col-6 order-1 order-sm-2">
                <div class="email_form_footer">
                    <a class="a-email-tel">عضویت</a>
                    <div class="text">
                        <input id="subscribe-mobile" type="text" placeholder="تلفن همراه خود را وارد نمایید">
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-10 order-sm-2">
                <img src="{{asset('assets/frontend/img/Information.webp')}}" alt="img" class="w-100">
            </div>
        </div>
    </div>
</section>

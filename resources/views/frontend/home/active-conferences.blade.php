<section class="Exhibitions-conferences mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 exhibition-title mb-5 d-flex">
                <div class="col-12 d-flex justify-content-between align-items-center ps-xxl-3">
                    <h5>رویداد های فعال</h5>
                    {{-- <a href="{{}}" class="rounded-pill">مشاهده همه</a> --}}
                </div>
            </div>
            @if (count($activeEvents) > 0)
                @foreach ($activeEvents as $event)
                    <div class="col-lg-6 col-12   domestic-exhibitions">
                        <a href="{{route('event.single',$event->id)}}">
                            <div class="banner d-flex">
                                <img src="{{asset($event->photo->path)}}" alt="img">
                                <div class="description">
                                    <div class="body-text font-all-2">
                                        <h1>{{$event->title}}</h1>
                                        <p class="w-75 p-about">مجری: استان {{$event->university->province->name}} دانشگاه {{$event->university->name}}</p>
                                        @if (!is_null($event->creator))
                                            <p>مدیر برگزاری: {{$event->creator->user->name}} {{$event->creator->user->family}}</p>
                                        @endif
                                        <p class="w-75 p-about">{{Str::limit(strip_tags($event->description),'70','...')}}</p>
                                    </div>
                                    <div class="date rounded-pill font-all-2">
                                        <span>{{verta($event->start_at)->format('%d')}}</span>
                                        <span>{{verta($event->start_at)->format('%B')}}</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="d-flex justify-content-center my-5">
                    <h3><strong>"در حال حاضر رویداد فعالی وجود ندارد"</strong></h3>
                </div>
            @endif
        </div>
    </div>
</section>








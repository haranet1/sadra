@extends('frontend.layout.master')
@section('title' , 'خانه')
@section('css')
    @if(\Illuminate\Support\Facades\Route::is('opening'))
        <link rel="stylesheet" href="{{asset('assets/frontend/css/opening.css')}}">
    @endif
@endsection
@section('main')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">



    <div class="mb-4">
        @include('frontend.home.logo-banner')
        @include('frontend.home.slider')
        @include('frontend.home.news-slider')
        @include('frontend.home.countreUp')
        @include('frontend.home.active-conferences')
        @include('frontend.home.about-us')
        @include('frontend.home.public-chart')
        @include('frontend.home.future-Exhibitions')
        {{--@include('frontend.home.banner')--}}
        {{--@include('frontend.home.email-tel')--}}

        <input type="hidden" id="myData" value="{{json_encode($publicChart)}}">
        <input type="hidden" id="currentDate" value="فروردین تا اسفند {{ verta()->year }}">

    </div>

@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/exporting.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/export-data.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/accessibility.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/index.js') }}"></script>
    {{-- @if(\Illuminate\Support\Facades\Route::is('opening'))
        <script src="{{asset('assets/frontend/js/opening.js')}}"></script>
    @endif --}}
@endsection

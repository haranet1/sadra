<section class="counterUp">
    <div class="container">
        <div class="row">
            <div class="col-12 exhibition-title d-flex">
                <div class="col-12 d-flex  align-items-center ps-xxl-3">
                    <h5>آمار سامانه صدرا</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="counterup-items  mt-4 mb-5">
        <div class="container">
            <div class="row justify-content row-gap-5">
                <div class="col-6 col-lg-3 col-sm-3">
                    <div class="counter-box">
                        <i class="fa-solid fa-user-graduate icon-countreUp-style"></i>
                        <p class="counter" data-target="{{$students}}"></p>
                        <p>دانشجو</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-sm-3">
                    <div class="counter-box">
                        <i class="fa-solid fa-building icon-countreUp-style"></i>
                        <p class="counter" data-target="{{$coms}}"></p>
                        <p>شرکت</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-sm-3">
                    <div class="counter-box">
                        <i class="fa-solid fa-calendar-check icon-countreUp-style"></i>
                        <p class="counter" data-target="{{$eventsCount}}"></p>
                        <p>رویداد صدرا</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-sm-3">
                    <div class="counter-box">
                        <i class="fa-solid fa-user-check icon-countreUp-style"></i>
                        <p class="counter" data-target="{{$pooyeshCount}}"></p>
                        <p>درخواست پویش</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="banner-section">
    <div class="container">
        <div class="row justify-content-center row-gap-4">
            <div class="col-md-4 banner-slides slide-section">
                <a href="#">
                    <img src="assets/img/se8.jpg" class="w-100" alt="img">
                </a><div class="title"><a href="#">
                <h1>لیست مشارکت کنندگان رویداد کشاورزی 1402</h1>
            </a>
                <a href="#" class="rounded-pill">جهت مشاهده لیست مشارکت کنندگان رویداد کشاورزی 1402 کلیک کنید</a>
            </div>

            </div>
            <div class="col-md-4 col-6  banner-slides">
                <a href="#">
                    <img src="assets/img/se8.1.jpg" class="w-100" alt="img">
                </a>
                <div class="title"><a href="#">
                    <h1>فراخوان واگذاری رویدادهای سال 1403</h1>
                </a>
                    <a href="#" class="rounded-pill">جهت مشاهده فراخوان واگذاری رویدادهای سال 1403 کلیک کنید</a>
                </div>

            </div>
            <div class="col-md-4 col-6  banner-slides d-section">
                <a href="#">
                    <img src="assets/img/se8.2.jpg" class="w-100" alt="img">
                </a>
                <div class="title"><a href="#">
                    <h1>DIN EN ISO 9001:2015</h1>
                </a><a href="#" class="rounded-pill">جهت مشاهده DIN EN ISO 9001:2015 کلیک کنید</a>
                </div>

            </div>
        </div>
    </div>
</section>

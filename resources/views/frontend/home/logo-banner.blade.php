<section class="logo-banner">
    <a href="#" class="go-down icon-act">
        <i class="fa fa-arrow-down icon-style-logo-banner"></i>
    </a>
    <div class="logo-banner-content container d-flex align-items-center justify-content-center flex-column w-100 h-25 gap-5">
        <div class="text-title text-center">
            {{-- <h6 class="font-all-2" style="text-shadow: 0 0 25px #ffffff;">معاونت علوم تربیتی و مهارتی دانشگاه آزاد اسلامی</h6> --}}
        </div>
        </div>
        <div class="logo-logo-banner text-center">
            <img class="mt-1 img-logo-banner" src="{{asset('assets/frontend/img/logo-slider-sadra-min.png')}}" alt="img">

        </div>

</section>




<section class="slider" >
    <div class="title">
        <h1 class="font-all-2">آرشیو رویداد ها</h1>
    </div>
    <!-- slider-->

    <div class="d-none d-md-block col-lg-12 col-md-12 col-12">
        <div class="owl-carousel OwlSliderIndex owl-theme" id="webdesign">
            @foreach ($events as $event)
                @if ($event->horizontalBanner)
                    <div class="container-fluid">
                        <img src="{{asset($event->horizontalBanner->path)}}" class="w-100 d-block img-fluid justify-content-center " alt="img">
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="d-md-none col-lg-12 col-md-12 col-12">
        <div class="owl-carousel OwlSliderIndex owl-theme" id="webdesign">
            @foreach ($events as $event)
                @if ($event->photo)
                    <div class="container-fluid">
                        <img src="{{asset($event->photo->path)}}" class="w-100 d-block img-fluid justify-content-center " alt="img">
                    </div>
                @endif
            @endforeach
        </div>
    </div>


    <!--slider end-->

</section>

{{--
<div class="owl-carousel OwlSliderIndex owl-theme">
    <div class="col-12">
        <img src="assets/image/slidersader1png.png" class="d-block img-fluid justify-content-center " alt="...">
    </div>
</div> --}}

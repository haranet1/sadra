<section class="virtual-tour-Exhibitions">
    <section class="Exhibitions-conferences mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 exhibition-title mb-5 d-flex">
                    <div class="col-12 d-flex justify-content-between align-items-center ps-xxl-3">
                        <h5>رویداد های آتی</h5>
                        <a href="{{route('events')}}" class="rounded-pill">مشاهده همه</a>
                    </div>
                </div>
                @if (count($futureEvents) > 1)
                    @foreach ($futureEvents as $event)
                        <div class="col-lg-6 col-12   domestic-exhibitions">
                            <a href="{{route('event.single',$event->id)}}">
                                <div class="banner d-flex">
                                    <img src="{{asset($event->photo->path)}}" alt="img">
                                    <div class="description">
                                        <div class="body-text font-all-2">
                                            <h1>{{$event->title}}</h1>
                                            <p class="w-75 p-about">مجری: استان {{$event->university->province->name}} دانشگاه {{$event->university->name}}</p>
                                            @if (!is_null($event->creator))
                                                <p>مدیر برگزاری: {{$event->creator->user->name}} {{$event->creator->user->family}}</p>
                                            @endif
                                            <p class="w-75 p-about" >{{Str::limit(strip_tags($event->description),'70','...')}}</p>
                                        </div>
                                        <div class="date rounded-pill font-all-2">
                                            <span>{{verta($event->start_at)->format('%d')}}</span>
                                            <span>{{verta($event->start_at)->format('%B')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @elseif (count($events) == 1)
                    @foreach ($events as $event)
                        <div class="col-lg-6 col-12   domestic-exhibitions">
                            <a href="{{route('event.single',$event->id)}}">
                                <div class="banner d-flex">
                                    <img src="{{asset($event->photo->path)}}" alt="img">
                                    <div class="description">
                                        <div class="body-text font-all-2">
                                            <h1>{{$event->title}}</h1>
                                            <p class="w-75 p-about">مجری: استان {{$event->university->province->name}} دانشگاه {{$event->university->name}}</p>
                                            <p>مدیر برگزاری: {{$event->creator->user->name}} {{$event->creator->user->family}}</p>
                                            <p class="w-75 p-about" >{{Str::limit($event->description,'70','...')}}</p>
                                        </div>
                                        <div class="date rounded-pill font-all-2">
                                            <span>{{verta($event->start_at)->format('%d')}}</span>
                                            <span>{{verta($event->start_at)->format('%B')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="col-lg-6 col-12 domestic-exhibitions">
                        <a href="">
                            <div class="banner d-flex">
                                <img src="{{asset('assets/frontend/image/sadra10.jpg')}}" alt="img">
                                <div class="description d-flex align-items-center justify-content-center">
                                    <div class="body-text">
                                        <h1>در انتظار تعریف رویداد</h1>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @else
                    <div class="col-lg-6 col-12 domestic-exhibitions">
                        <a href="">
                            <div class="banner d-flex">
                                <img src="{{asset('assets/frontend/image/sadra10.jpg')}}" alt="img">
                                <div class="description d-flex align-items-center justify-content-center">
                                    <div class="body-text">
                                        <h1>در انتظار تعریف رویداد</h1>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-12 domestic-exhibitions">
                        <a href="">
                            <div class="banner d-flex">
                                <img src="{{asset('assets/frontend/image/sadra10.jpg')}}" alt="img">
                                <div class="description d-flex align-items-center justify-content-center">
                                    <div class="body-text">
                                        <h1>در انتظار تعریف رویداد</h1>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>
    {{-- <section class="Exhibitions-conferences mt-2">
        <div class="container">
            <div class="row">
                @if (count($activeEvents)  > 0)
                    <div class="col-12 exhibition-title responsive d-flex">
                        <div class="col-12 font-all-2 d-flex justify-content-between align-items-center ps-xxl-3">
                            <a href="#" class="rounded-pill">مشاهده همه</a>
                            <h5>رویداد های فعال</h5>
                        </div>
                        @foreach ($activeEvents as $event)
                            <div class="col-lg-6 col-12  domestic-exhibitions">
                                <a href="{{route('event.single',$event->id)}}">
                                    <div class="banner d-flex">
                                        <img src="{{asset($event->photo->path)}}" alt="">
                                        <div class="description">
                                            <div class="body-text font-all-2">
                                                <h1>{{$event->title}}</h1>
                                                <p>مجری: دانشگاه {{$event->university->name}}</p>
                                                <p>مدیر برگزاری: {{$event->creator->user->name}} {{$event->creator->user->family}}</p>
                                                <p>{{$event->description}}</p>
                                            </div>
                                            <div class="date rounded-pill ">
                                                <span>{{verta($event->start_at)->format('%d')}}</span>
                                                <span>{{verta($event->start_at)->format('%B')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert-warning" role="alert">رویداد فعالی وجود ندارد</div>
                @endif
            </div>
        </div>
    </section> --}}

</section>





{{-- <div class="row ">
    <div class="col-12 exhibition-title responsive d-flex">
        <div class=" col-12 font-all-2 d-flex justify-content-between align-items-center ps-xxl-3">
            <a class="rounded-pill" href="#">مشاهده همه</a>
            <h5>رویداد های فعال خارجی</h5>
        </div>
    </div>
    <div class="col-lg-6 col-12  domestic-exhibitions">
        <a href="#">
            <div class="banner d-flex">
                <img alt="" src="assets/image/sadra6.jpg">
                <div class="description">
                    <div class="body-text font-all-2">
                        <h1> دومین رویداد </h1>
                        <p>مجری: شرکت مدیریت رویداد و رویداد </p>
                        <p>شماره تماس : 0913000 / 12-31</p>
                        <p>وب سایت: www.iaun.ic.ir</p>
                        <p>مدیر برگزاری: آقای </p>
                    </div>
                    <div class="date rounded-pill font-all-2">
                        <span>23</span>
                        <span>آبان</span>
                    </div>

                </div>
            </div>
        </a>
    </div>

</div> --}}

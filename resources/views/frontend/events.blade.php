@extends('frontend.layout.master')
@section('title','رویداد ها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/news.css')}}">
@endsection
{{-- @section('bodyClass') --}}
@section('main')
    <main>
        <div>
            <section class="slideNews mb-5">
                <div class="title">
                    <h1 class="text-white">رویداد ها</h1>
                </div>
                <!-- slider-->
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="owl-carousel OwlSliderNews owl-theme" id="webdesign">
                        @foreach ($events as $eve)
                            @if ($eve->horizontalBanner)
                                <div class="col-12">
                                    <img src="{{asset($eve->horizontalBanner->path)}}" class="d-block img-fluid justify-content-center " alt="img">

                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <!--slider end-->
            </section>
            <section class="Exhibitions-conferences mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12 exhibition-title mb-5 d-flex">
                            <div class="col-12 d-flex justify-content-between align-items-center ps-xxl-3">
                                <h5>همه رویداد ها</h5>
                            </div>
                        </div>
                        @if (count($events) > 1)
                            @foreach ($events as $event)
                                <div class="col-lg-6 col-12   domestic-exhibitions">
                                    <a href="{{route('event.single',$event->id)}}">
                                        <div class="banner d-flex">
                                            <img src="{{asset($event->photo->path)}}" alt="img">
                                            <div class="description">
                                                <div class="body-text font-all-2">
                                                    <h1>{{$event->title}}</h1>
                                                    <p>مجری: استان {{$event->university->province->name}} دانشگاه {{$event->university->name}}</p>
                                                    @if ($event->creator)
                                                        <p>مدیر برگزاری: {{$event->creator->user->name}} {{$event->creator->user->family}}</p>
                                                    @endif
                                                    <p class="w-75 p-about" >
                                                        @php
                                                            echo Str::limit(strip_tags($event->description), 70, '...')
                                                        @endphp
                                                    </p>
                                                </div>
                                                <div class="date rounded-pill font-all-2">
                                                    <span>{{verta($event->start_at)->format('%d')}}</span>
                                                    <span>{{verta($event->start_at)->format('%B')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="col-lg-6 col-12 domestic-exhibitions">
                                <a href="">
                                    <div class="banner d-flex">
                                        <img src="{{asset('assets/frontend/image/sadra10.jpg')}}" alt="img">
                                        <div class="description d-flex align-items-center justify-content-center">
                                            <div class="body-text">
                                                <h1>در انتظار تعریف رویداد</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-6 col-12 domestic-exhibitions">
                                <a href="">
                                    <div class="banner d-flex">
                                        <img src="{{asset('assets/frontend/image/sadra10.jpg')}}" alt="img">
                                        <div class="description d-flex align-items-center justify-content-center">
                                            <div class="body-text">
                                                <h1>در انتظار تعریف رویداد</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="d-flex mb-5 justify-content-center align-items-center">
                {{$events->links('pagination.front')}}
            </div>
        </div>

    </main>
@endsection

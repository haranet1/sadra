@extends('frontend.layout.master')
@section('title','اخبار')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/news.css')}}">
@endsection
@section('bodyClass','News')
@section('main')
    <main>
        <div>
            <section class="slideNews mb-5">
                <div class="title">
                    <h1 class="text-white">اخبار و رويدادها</h1>
                </div>
                <!-- slider-->
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="owl-carousel OwlSliderNews owl-theme" id="webdesign">
                        @foreach ($events as $eve)
                            @if ($eve->horizontalBanner)
                                <div class="col-12">
                                    <img src="{{asset($eve->horizontalBanner->path)}}" class="d-block img-fluid justify-content-center " alt="img">

                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <!--slider end-->
            </section>

            <section class="news">
                <div class="container">
                    <div class="news__lists">
                        <!-- Additional required wrapper -->
                        <div class="row">
                            @if (count($News)>0)
                                @foreach ($News as $news)
                                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                                        <div class="w-100">
                                            <div class="news-list event-single">
                                                <div class="items event-single" >
                                                    <a href="{{route('news.signle',$news->id)}}">
                                                        <div class="item-text font-all-2">
                                                            <h5>{{$news->title}}</h5>
                                                            <hr>
                                                            <p>{{Str::limit($news->description,'100','...')}}</p>
                                                        </div>
                                                        <div class="footer-text font-all-2 w-100">
                                                            {{-- <a href=""></a>     --}}
                                                            <a class="a-news-slider">
                                                                <i class="far fa-clock icon-style-inline-news"></i>
                                                                {{verta($news->created_at)->formatDate()}}
                                                            </a>
                                                            @if (isset($news->event))
                                                                <a class="a-news-slider" href="{{route('event.single',$news->event->id)}}">
                                                                    <i class="fas fa-tag icon-style-inline-news"></i>
                                                                    رویداد {{$news->event->title}}
                                                                </a>
                                                            @else
                                                                <a class="a-news-slider">
                                                                    <i class="fas fa-tag icon-style-inline-news"></i>
                                                                    دانشگاه {{$news->university->name}}
                                                                </a>
                                                            @endif
                                                            <a class="a-news-slider">
                                                                <i class="far fa-eye icon-style-inline-news"></i>
                                                                {{$news->counter}}
                                                            </a>
                                                            {{-- <a style="flex=0 0 25%;">
                                                                <i class="far fa-comment"></i>
                                                                0
                                                            </a> --}}
                                                        </div>
                                                        @if (isset($news->photo))
                                                            <img src="{{asset($news->photo->path)}}" alt="img ">
                                                        @else
                                                            <img src="{{asset('assets/frontend/img/sadra25.webp')}}" alt="img ">
                                                        @endif
                                                        <i class="fa fa-arrow-down icon-style-arrow"></i>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                @endforeach
                            @else
                                بی خبریم
                            @endif
                        </div>
                    </div>
                </div>


            </section>
            <div class="d-flex mb-5 justify-content-center align-items-center">
                {{$News->links('pagination.front')}}
            </div>
        </div>

    </main>
@endsection

@extends('frontend.layout.master')
@section('title' , 'رویداد ها')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/plugins/swiper/css/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/singlePost.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/news.css')}}">
@endsection
@section('bodyClass' , 'News')
@section('main')
<div class="singlePost">
    <section class="slider-single-post">
        <!-- slider-->

        <div class="col-lg-12 col-md-12 col-12">
            <div class="owl-carousel OwlSliderIndex owl-theme" id="webdesign">
                @foreach ($events as $eve)
                    @if ($eve->horizontalBanner)
                        <div class="col-12">
                            <img src="{{asset($eve->horizontalBanner->path)}}" class="d-block img-fluid justify-content-center " alt="img">

                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <!--slider end-->

    </section>

    <section class="exhibition-list">
        <div class="container container-fluid">
            <div class="d-flex mb-5 justify-content-center">
                <h1 class="font-all-2">
                   رویداد {{$event->title}}
                </h1>
            </div>
            <div class="row exhibition-information justify-content-center align-items-center">
                <div class="  col-sm-6 col-12 inf-list font-all" data-title="اطلاعات مجری رویداد">
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fas fa-user-tie icon-style-inline-news mb-2"></i>
                        <span class="text-primary">نام دانشگاه:</span>
                        <span>
                            <strong>{{$event->university->name}}</strong>
                        </span>
                    </div>
                    @if (!is_null($event->creator))
                        <div class="font-all-2 my-3 fs-5">
                            <i class="fas fa-user-tie icon-style-inline-news mb-2"></i>
                            <span class="text-primary">نام برگزار کننده:</span>
                            <span>
                                <strong>{{$event->creator->user->name}} {{$event->creator->user->family}}</strong>
                            </span>
                        </div>
                    @endif
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fa fa-calendar-days icon-style-inline-news mb-2"></i>
                        <span class="text-primary">تاریخ ثبت نام:</span>
                        <span dir="rtl">
                            <strong>{{verta($event->start_register_at)->formatJalaliDate()}} تا {{verta($event->end_register_at)->formatJalaliDate()}}</strong>
                        </span>
                    </div>
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fa fa-calendar-days icon-style-inline-news mb-2"></i>
                        <span class="text-primary">تاریخ برگزاری رویداد:</span>
                        <span dir="rtl">
                            <strong>{{verta($event->start_at)->formatJalaliDate()}} تا {{verta($event->end_at)->formatJalaliDate()}}</strong>
                        </span>
                    </div>
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fas fa-tty icon-style-inline-news mb-2"></i>
                        <span class="text-primary">تلفن ثابت:</span>
                        <span>
                            <strong class="font-all-2">{{$event->university->phone}}</strong>
                        </span>
                    </div>
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fa fa-map-marker-alt icon-style-inline-news mb-2"></i>
                        <span class="text-primary">آدرس: </span>
                        <span>
                            <strong>{{$event->university->address}}</strong>
                        </span>
                    </div>
                    <div class="font-all-2 my-3 fs-5">
                        <i class="fa fa-paragraph icon-style-inline-news mb-3"></i>
                        <span class="text-primary">توضیحات:</span>
                        <span>
                            <strong>
                                @php
                                    echo $event->description;
                                @endphp
                            </strong>
                        </span>
                    </div>
                </div>

            </div>
            @if ($event->university->id == 35)
                <div class="row mt-4 justify-content-center">
                    <div class="col-12 col-md-6 text-center">
                        <a href="https://jobist.ir/register" class="btn btn-lg text-center text-white button-event-single">ثبت نام در رویداد</a>
                    </div>
                </div>
            @else
                @auth
                    @if (verta(now())->lessThan(verta($event->start_register_at)))
                        <div class="row mt-4 justify-content-center">
                            <div class="col-12 col-md-6 text-center font-all-2">
                                <button type="submit" class="btn btn-lg text-center text-white disabled button-event-single-style"
                            >شروع ثبت نام {{verta($event->start_register_at)->diffDays()}} روز دیگر</button>
                            </div>
                        </div>
                    @elseif (verta(now())->lessThan(verta($event->end_register_at)))

                        @if (Auth::user()->isCompany())
                            <div class="row mt-4 justify-content-center">
                                <div class="col-12 col-md-6 text-center">
                                    <form action="{{route('create.register.event')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="event" value="{{$event->id}}">
                                        <button type="submit" class="btn btn-lg text-center text-white button-event-single">ثبت نام در رویداد</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->isStudent())
                            <div class="row mt-4 justify-content-center">
                                <div class="col-12 col-md-6 text-center">
                                    <form action="{{route('index-std.event')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="event" value="{{$event->id}}">
                                        <button type="submit" class="btn btn-lg text-center text-white button-event-single">ثبت نام در رویداد</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                    @else
                        <div class="row mt-4 justify-content-center">
                            <div class="col-12 col-md-6 text-center font-all-2">
                                <button type="submit" class="button-event-style-single btn btn-lg text-center text-white disabled"
                            >پایان مهلت ثبت نام</button>
                            </div>
                        </div>
                    @endif
                @else
                    <div class="row mt-4 justify-content-center">
                        <div class="col-12 col-md-6 text-center">
                            <a href="{{route('register')}}" class="btn btn-lg text-center text-white button-event-single">ثبت نام در رویداد</a>
                        </div>
                    </div>
                @endauth
            @endif

        </div>
    </section>


    <section class="control-items">
        <div class="container">
            <div class="row justify-content row-gap-5">
                <div class="col-6 col-lg-3 col-sm-3">
                    <a href="#">
                        <i class="fas fa-clipboard icon-style-inline-event"></i>
                        <span>شرایط و مقررات</span>
                    </a>
                </div>
                <div class="col-6 col-lg-3 col-sm-3">
                    <a href="#">
                        <i class="fa fa-envelope-open-text icon-style-inline-event-1"></i>
                        <span>ثبت انتقادات و شکایات</span>
                    </a>
                </div>
                <div class="col-6 col-lg-3 col-sm-3">
                    <a href="#">
                        <i class="fa fa-edit icon-style-inline-event-1"></i>
                        <span>نظر سنجی</span>
                    </a>
                </div>

                <div class="col-6 col-lg-3 col-sm-3">
                    <a href="#">
                        <i class="fa fa-laptop-house icon-style-inline-event-2"></i>
                        <span>رویداد مجازی</span>
                    </a>
                </div>


            </div>
        </div>
    </section>


    {{-- <section class="news ">
        <div class="container">
            <div class="news__lists">
                <!-- Additional required wrapper -->
                <div class="row">
                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                        <div class="w-100">
                            <div class="news-list" style="aspect-ratio:4/3;">
                                <a href="#">
                                </a>
                                <div class="items" style="aspect-ratio:4/3;">
                                    <a href="#">
                                        <div class="item-text font-all-2">
                                            <h5>رویداد دروازه مهم...</h5>
                                            <hr>
                                            <p>
                                                دومین رویداد صدرا صنایع مختلفی از قبیل شرکت پشتیبانی فولاد مبارکه، شرکت ذوب‌آهن اصفهان، هلدینگ کیمیا و شرکت‌های تابعه اسنوا حضور داشتند. سومین رویداد نیز دیروز در دانشگاه آزاد واحد مستقل نجف‌آباد همزمان با نشست شورای دانشگاه آغاز شد. 100 شرکت انتخاب‌شده قرار است حوزه اشتغال دانشجویان را پوشش دهند؛ یعنی همه رشته‌های دانشگاه در این رویداد حضور داشته باشند.
                                            </p>
                                        </div>
                                    </a><div class="footer-text font-all-2" style="width: 100%">
                                    <a href="#">
                                    </a><a href="#" style="flex=0 0 25%;">
                                    <i class="far fa-clock"></i>
                                    1402/8/30
                                </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="fas fa-tag"></i>
                                        برچسب ها
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-eye"></i>
                                        15
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-comment"></i>
                                        0
                                    </a>
                                </div>
                                    <img src="assets/img/sadra25.webp" alt="img ">
                                    <i class="fa fa-arrow-down"></i>
                                </div>

                            </div>
                        </div>

                    </div>


                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                        <div class="w-100">
                            <div class="news-list" style="aspect-ratio:4/3;">
                                <a href="#">
                                </a>
                                <div class="items" style="aspect-ratio:4/3;">
                                    <a href="#">
                                        <div class="item-text font-all-2">
                                            <h5>رویداد دروازه مهم...</h5>
                                            <hr>
                                            <p>
                                                دومین رویداد صدرا صنایع مختلفی از قبیل شرکت پشتیبانی فولاد مبارکه، شرکت ذوب‌آهن اصفهان، هلدینگ کیمیا و شرکت‌های تابعه اسنوا حضور داشتند. سومین رویداد نیز دیروز در دانشگاه آزاد واحد مستقل نجف‌آباد همزمان با نشست شورای دانشگاه آغاز شد. 100 شرکت انتخاب‌شده قرار است حوزه اشتغال دانشجویان را پوشش دهند؛ یعنی همه رشته‌های دانشگاه در این رویداد حضور داشته باشند.
                                            </p>
                                        </div>
                                    </a><div class="footer-text font-all-2" style="width: 100%">
                                    <a href="#">
                                    </a><a href="#" style="flex=0 0 25%;">
                                    <i class="far fa-clock"></i>
                                    1402/8/30
                                </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="fas fa-tag"></i>
                                        برچسب ها
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-eye"></i>
                                        15
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-comment"></i>
                                        0
                                    </a>
                                </div>
                                    <img src="assets/img/sadra25.webp" alt="img ">
                                    <i class="fa fa-arrow-down"></i>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                        <div class="w-100">
                            <div class="news-list" style="aspect-ratio:4/3;">
                                <a href="#">
                                </a>
                                <div class="items" style="aspect-ratio:4/3;">
                                    <a href="#">
                                        <div class="item-text font-all-2">
                                            <h5>رویداد دروازه مهم...</h5>
                                            <hr>
                                            <p>
                                                دومین رویداد صدرا صنایع مختلفی از قبیل شرکت پشتیبانی فولاد مبارکه، شرکت ذوب‌آهن اصفهان، هلدینگ کیمیا و شرکت‌های تابعه اسنوا حضور داشتند. سومین رویداد نیز دیروز در دانشگاه آزاد واحد مستقل نجف‌آباد همزمان با نشست شورای دانشگاه آغاز شد. 100 شرکت انتخاب‌شده قرار است حوزه اشتغال دانشجویان را پوشش دهند؛ یعنی همه رشته‌های دانشگاه در این رویداد حضور داشته باشند.
                                            </p>
                                        </div>
                                    </a><div class="footer-text font-all-2" style="width: 100%">
                                    <a href="#">
                                    </a><a href="#" style="flex=0 0 25%;">
                                    <i class="far fa-clock"></i>
                                    1402/8/30
                                </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="fas fa-tag"></i>
                                        برچسب ها
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-eye"></i>
                                        15
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-comment"></i>
                                        0
                                    </a>
                                </div>
                                    <img src="assets/img/sadra25.webp" alt="img ">
                                    <i class="fa fa-arrow-down"></i>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                        <div class="w-100">
                            <div class="news-list" style="aspect-ratio:4/3;">
                                <a href="#">
                                </a>
                                <div class="items" style="aspect-ratio:4/3;">
                                    <a href="#">
                                        <div class="item-text font-all-2">
                                            <h5>رویداد دروازه مهم...</h5>
                                            <hr>
                                            <p>
                                                دومین رویداد صدرا صنایع مختلفی از قبیل شرکت پشتیبانی فولاد مبارکه، شرکت ذوب‌آهن اصفهان، هلدینگ کیمیا و شرکت‌های تابعه اسنوا حضور داشتند. سومین رویداد نیز دیروز در دانشگاه آزاد واحد مستقل نجف‌آباد همزمان با نشست شورای دانشگاه آغاز شد. 100 شرکت انتخاب‌شده قرار است حوزه اشتغال دانشجویان را پوشش دهند؛ یعنی همه رشته‌های دانشگاه در این رویداد حضور داشته باشند.
                                            </p>
                                        </div>
                                    </a><div class="footer-text font-all-2" style="width: 100%">
                                    <a href="#">
                                    </a><a href="#" style="flex=0 0 25%;">
                                    <i class="far fa-clock"></i>
                                    1402/8/30
                                </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="fas fa-tag"></i>
                                        برچسب ها
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-eye"></i>
                                        15
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-comment"></i>
                                        0
                                    </a>
                                </div>
                                    <img src="assets/img/sadra25.webp" alt="img ">
                                    <i class="fa fa-arrow-down"></i>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                        <div class="w-100">
                            <div class="news-list" style="aspect-ratio:4/3;">
                                <a href="#">
                                </a>
                                <div class="items" style="aspect-ratio:4/3;">
                                    <a href="#">
                                        <div class="item-text font-all-2">
                                            <h5>رویداد دروازه مهم...</h5>
                                            <hr>
                                            <p>
                                                دومین رویداد صدرا صنایع مختلفی از قبیل شرکت پشتیبانی فولاد مبارکه، شرکت ذوب‌آهن اصفهان، هلدینگ کیمیا و شرکت‌های تابعه اسنوا حضور داشتند. سومین رویداد نیز دیروز در دانشگاه آزاد واحد مستقل نجف‌آباد همزمان با نشست شورای دانشگاه آغاز شد. 100 شرکت انتخاب‌شده قرار است حوزه اشتغال دانشجویان را پوشش دهند؛ یعنی همه رشته‌های دانشگاه در این رویداد حضور داشته باشند.
                                            </p>
                                        </div>
                                    </a><div class="footer-text font-all-2" style="width: 100%">
                                    <a href="#">
                                    </a><a href="#" style="flex=0 0 25%;">
                                    <i class="far fa-clock"></i>
                                    1402/8/30
                                </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="fas fa-tag"></i>
                                        برچسب ها
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-eye"></i>
                                        15
                                    </a>
                                    <a href="#" style="flex=0 0 25%;">
                                        <i class="far fa-comment"></i>
                                        0
                                    </a>
                                </div>
                                    <img src="assets/img/sadra25.webp" alt="img ">
                                    <i class="fa fa-arrow-down"></i>
                                </div>

                            </div>
                        </div>

                    </div>






                </div>
             </div>
        </div>
    </section> --}}


    <section class="news">
        <div class="container">
            <div class="news__lists">
                <div class="row">
                    <h3>اخبار این رویداد</h3>
                </div>
                <!-- Additional required wrapper -->
                <div class="row">
                    @if (count($event->news)>0)

                        @foreach ($event->news as $news)
                            @if (!is_null($news->status))
                                <div class="newsimportant col-lg-4 col-md-6 col-12 mt-5">
                                    <div class="w-100">
                                        <div class="news-list event-single">
                                            <div class="items event-single">
                                                <a href="{{route('news.signle',$news->id)}}">
                                                    <div class="item-text font-all-2">
                                                        <h5>{{$news->title}}</h5>
                                                        <hr>
                                                        <p>{{Str::limit($news->description,'100','...')}}</p>
                                                    </div>
                                                    <div class="footer-text font-all-2 w-100" >
                                                        {{-- <a href=""></a>     --}}
                                                        <a class="a-news-slider">
                                                            <i class="far fa-clock icon-style-inline-news"></i>
                                                            {{verta($news->created_at)->formatDate()}}
                                                        </a>
                                                        @if (isset($news->event))
                                                            <a class="a-news-slider" href="{{route('event.single',$news->event->id)}}" >
                                                                <i class="fas fa-tag icon-style-inline-news"></i>
                                                                رویداد {{$news->event->title}}
                                                            </a>
                                                        @else
                                                            <a class="a-news-slider">
                                                                <i class="fas fa-tag icon-style-inline-news"></i>
                                                                دانشگاه {{$news->university->name}}
                                                            </a>
                                                        @endif
                                                        <a class="a-news-slider">
                                                            <i class="far fa-eye icon-style-inline-news"></i>
                                                            {{$news->counter}}
                                                        </a>
                                                        {{-- <a style="flex=0 0 25%;">
                                                            <i class="far fa-comment"></i>
                                                            0
                                                        </a> --}}
                                                    </div>
                                                    @if (isset($news->photo))
                                                        <img src="{{asset($news->photo->path)}}" alt="img ">
                                                    @else
                                                        <img src="{{asset('assets/frontend/img/sadra25.webp')}}" alt="img ">
                                                    @endif
                                                    <i class="fa fa-arrow-down icon-style-arrow"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="d-flex justify-content-center my-5">
                            <h5><strong>"در حال حاضر برای این رویداد خبری ثبت نشده"</strong></h5>
                        </div>
                    @endif
                </div>
            </div>
        </div>


    </section>


</div>
@endsection
@section('script')
    <script src="{{ asset('assets/frontend/plugins/swiper/js/swiper-bundle.min.js') }}"></script>
@endsection

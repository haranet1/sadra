<section>
    <div class="row">
        <div class="col-12">
            <div class="card overflow-hidden">
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="container-mix"></div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>
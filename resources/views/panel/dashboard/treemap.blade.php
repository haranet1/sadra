<div class="row">
    <div class="col-12 col-md-3">
        <div class="card overflow-hidden">
            <div class="card-body pb-0 bg-recentorder">
                <h3 class="card-title text-white">نسبت همپوشانی شرکت ها</h3>
                <div class="chartjs-wrapper-demo">
                    <canvas id="recentorders2" class="chart-dropshadow"></canvas>
                </div>
            </div>
            <div id="flotback-chart2" class="flot-background"></div>
            <div class="card-body">
                <div class="d-flex mb-4 mt-3 align-items-center">
                    <div class="avatar avatar-md bg-secondary-transparent text-secondary bradius me-3">
                        <i class="fe fe-check"></i>
                    </div>
                    <h6 class="mb-1 fw-semibold">درصد همپوشانی</h6>
                    
                    <div class=" ms-auto">
                        <p class="fw-bold fs-20 mb-0"> {{Percent($hamposhani, count($companies))}} </p>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <div class="avatar avatar-md bg-pink-transparent text-pink bradius me-3">
                        <i class="fe fe-x"></i>
                    </div>
                    <h6 class="mb-1 fw-semibold">تعداد همپوشانی</h6>
                    
                    <div class=" ms-auto">
                        <p class="fw-bold fs-20 mb-0">{{$hamposhani}} از {{count($companies)}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title fw-semibold">برترین های کشور</h4>
            </div>
            <div class="card-body pb-0">
                <ul class="task-list">
                    <li class="d-sm-flex">
                        <div>
                            <i class="task-icon bg-success"></i>
                            <h6 class="fw-semibold">بیشترین رویداد<span class="text-muted fs-11 mx-2 fw-normal"></span>
                            </h6>
                            <p class="text-muted fs-12">دانشگاه <a href="javascript:void(0)" class="fw-semibold"> اصفهان-خوراسگان</a>
                                رویداد شما را تایید کرد.
                            </p>
                        </div>
                        <div class="ms-auto d-md-flex">
                            <a href="javascript:void(0)" class="text-muted" data-bs-toggle="tooltip" data-bs-placement="top" title="حذف" aria-label="حذف"><span class="fe fe-trash-2"></span></a>
                        </div>
                    </li>
                    <li class="d-sm-flex">
                        <div>
                            <i class="task-icon bg-warning"></i>
                            <h6 class="fw-semibold">بیشترین ثبت نام صنعت<span class="text-muted fs-11 mx-2 fw-normal">2 آذر 1402</span>
                            </h6>
                            <p class="text-muted mb-0 fs-12">توسط شرکت <a href="javascript:void(0)" class="fw-semibold"> حرانت</a>
                                ایجاد و منتظر تایید دانشگاه است.
                            </p>
                        </div>
                        <div class="ms-auto d-md-flex">
                            <a href="javascript:void(0)" class="text-muted me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="ویرایش" aria-label="ویرایش"><span class="fe fe-edit"></span></a>
                            <a href="javascript:void(0)" class="text-muted"><span class="fe fe-trash-2"></span></a>
                        </div>
                    </li>
                    <li class="d-sm-flex">
                        <div>
                            <i class="task-icon bg-primary"></i>
                            <h6 class="fw-semibold">بیشترین ثبت نام دانشجو<span class="text-muted fs-11 mx-2 fw-normal">2 آذر 1401</span>
                            </h6>
                            <p class="text-muted mb-0 fs-12">شرکت <a href="javascript:void(0)" class="fw-semibold"> گروه صنعتی همگام</a>
                                در رویداد صدرا 3 ثبت نام کرد
                            </p>
                        </div>
                        <div class="ms-auto d-md-flex">
                            <a href="javascript:void(0)" class="text-muted me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="ویرایش" aria-label="ویرایش"><span class="fe fe-edit"></span></a>
                            <a href="javascript:void(0)" class="text-muted"><span class="fe fe-trash-2"></span></a>
                        </div>
                    </li>
                    <li class="d-sm-flex">
                        <div>
                            <i class="task-icon bg-info"></i>
                            <h6 class="fw-semibold">بیتشرین موقعیت پویش<span class="text-muted fs-11 mx-2 fw-normal">2 آذر 1401</span>
                            </h6>
                            <p class="text-muted fs-12">دانشجو <a href="javascript:void(0)" class="fw-semibold"> محمد عباسیان</a>
                                در رویداد صدرا 3 ثبت نام کرد.
                            </p>
                        </div>
                        <div class="ms-auto d-md-flex">
                            <a href="javascript:void(0)" class="text-muted me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="ویرایش" aria-label="ویرایش"><span class="fe fe-edit"></span></a>
                            <a href="javascript:void(0)" class="text-muted"><span class="fe fe-trash-2"></span></a>
                        </div>
                    </li>
                    <li class="d-sm-flex">
                        <div>
                            <i class="task-icon bg-primary"></i>
                            <h6 class="fw-semibold">بیشترین پویش تایید شده<span class="text-muted fs-11 mx-2 fw-normal">2 آذر 1401</span>
                            </h6>
                            <p class="text-muted mb-0 fs-12">شرکت <a href="javascript:void(0)" class="fw-semibold"> پارتاک طب</a>
                                در رویداد صدرا 3 ثبت نام کرد
                            </p>
                        </div>
                        <div class="ms-auto d-md-flex">
                            <a href="javascript:void(0)" class="text-muted me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="ویرایش" aria-label="ویرایش"><span class="fe fe-edit"></span></a>
                            <a href="javascript:void(0)" class="text-muted"><span class="fe fe-trash-2"></span></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="col-12 col-md-3">
        <div class="card overflow-hidden">
            <div class="card-body pb-0 bg-recentorder">
                <h3 class="card-title text-white">نسبت درخواست های پویش</h3>
                <div class="chartjs-wrapper-demo">
                    <canvas id="recentorders" class="chart-dropshadow"></canvas>
                </div>
            </div>
            <div id="flotback-chart" class="flot-background"></div>
            <div class="card-body">
                <div class="d-flex mb-4 mt-3">
                    <div class="avatar avatar-md bg-secondary-transparent text-secondary bradius me-3">
                        <i class="fe fe-check"></i>
                    </div>
                    <div class="">
                        <h6 class="mb-1 fw-semibold">درخواست های قبول شده</h6>
                        <p class="fw-normal fs-12"> نسبت به کل <span class="text-success">{{Percent(count($internAcceptedRequest) , count($internRequests))}}</span>
                            </p>
                    </div>
                    <div class=" ms-auto my-auto">
                        <p class="fw-bold fs-20"> {{count($internAcceptedRequest)}} </p>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="avatar avatar-md bg-pink-transparent text-pink bradius me-3">
                        <i class="fe fe-x"></i>
                    </div>
                    <div class="">
                        <h6 class="mb-1 fw-semibold">درخواست های لغو شده</h6>
                        <p class="fw-normal fs-12"> نسبت به کل <span class="text-success">{{Percent(count($internDeclinedRequest) , count($internRequests))}}</span>
                            </p>
                    </div>
                    <div class=" ms-auto my-auto">
                        <p class="fw-bold fs-20 mb-0"> {{count($internDeclinedRequest)}} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card overflow-hidden">
            <div class="card-body">
              <figure class="highcharts-figure">
                <div id="container-future-event"></div>
              </figure>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card overflow-hidden">
            <div class="card-body">
              <figure class="highcharts-figure">
                <div id="container-p-price-event"></div>
              </figure>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card overflow-hidden">
            <div class="card-body">
              <figure class="highcharts-figure">
                <div id="container-p-free-event"></div>
              </figure>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex mb-1">
                            <div class="mt-2">
                                <h6 class="">رویداد ها</h6>
                                <h2 class="mb-0 number-font">{{$eventsCount}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="saleschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12"> <span class="text-secondary fs-16">{{Percent($eventsCount , $eventsAll)}}</span>
                        نسبت به کل رویداد ها</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-p-event"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex mb-1">
                            <div class="mt-1">
                                <h6 class="">شرکت ها</h6>
                                <h2 class="mb-0 number-font">{{$companiesCount}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="leadschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div> 
                        <span class="text-muted fs-12">در <span class="text-pink fs-16">{{Percent($companiesCount , $companiesAll)}}</span>
                        نسبت به کل شرکت ها</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-p-company"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex mb-1">
                            <div class="mt-2">
                                <h6 class="">دانشجویان</h6>
                                <h2 class="mb-0 number-font fs-5">{{$studentsCount}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="profitchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12"> <span class="text-green fs-16">{{Percent($studentsCount , $studentsAll)}}</span>
                        نسبت به کل دانشجو ها</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-p-student"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex mb-1">
                            <div class="mt-2">
                                <h6 class="">موقعیت ها</h6>
                                <h2 class="mb-0 number-font fs-5">{{$internPositionsCount}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="costchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12"> <span class="text-warning fs-16">{{Percent($internPositionsCount , $internPositionsAll)}}</span>
                        نسبت به کل موقعیت های پویش</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-p-pooyesh"></div>
                        </figure>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
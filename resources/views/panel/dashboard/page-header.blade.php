@php
  use App\Http\Controllers\Admin\AdminIndexController;
@endphp
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">رویداد ها</h6>
                                <h2 class="mb-0 number-font">{{AdminIndexController::$allEvents->count()}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="saleschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">در <span class="text-secondary fs-16">{{$universityEventCount}}</span>
                        دانشگاه</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-event"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex">
                            <div class="mt-1">
                                <h6 class="">شرکت ها</h6>
                                <h2 class="mb-0 number-font">{{count($companies)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="leadschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div> 
                        <span class="text-muted fs-12">در <span class="text-pink fs-16">{{$provinceCompanyCount}}</span>
                        استان ایران</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-company"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">دانشجویان</h6>
                                <h2 class="mb-0 number-font fs-5">{{count($students)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="profitchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">در <span class="text-green fs-16">{{$universityStudentCount}}</span>
                        دانشگاه</span> --}}

                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-student"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-6">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        {{-- <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">موقعیت ها</h6>
                                <h2 class="mb-0 number-font fs-5">{{count($internPositions)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="costchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">توسط <span class="text-warning fs-16">{{$companyInternPositionCount}}</span>
                        شرکت</span> --}}
                        <figure class="highcharts-figure">
                            <div class="chart-container" id="container-pooyesh"></div>
                        </figure>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
{{-- <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">رویداد های برگزار شده</h6>
                                <h2 class="mb-0 number-font">{{AdminIndexController::$doneEvents->count()}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="saleschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">در <span class="text-secondary fs-16">{{$universityEventCount}}</span>
                        دانشگاه</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="mt-1">
                                <h6 class="">شرکت ها</h6>
                                <h2 class="mb-0 number-font">{{count($companies)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="leadschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div> 
                        <span class="text-muted fs-12">در <span class="text-pink fs-16">{{$provinceCompanyCount}}</span>
                        استان ایران</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">دانشجویان</h6>
                                <h2 class="mb-0 number-font fs-5">{{count($students)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="profitchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">در <span class="text-green fs-16">{{$universityStudentCount}}</span>
                        دانشگاه</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="mt-2">
                                <h6 class="">موقعیت ها</h6>
                                <h2 class="mb-0 number-font fs-5">{{count($internPositions)}}</h2>
                            </div>
                            <div class="ms-auto">
                                <div class="chart-wrapper mt-1">
                                    <canvas id="costchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <span class="text-muted fs-12">توسط <span class="text-warning fs-16">{{$companyInternPositionCount}}</span>
                        شرکت</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> --}}
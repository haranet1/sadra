<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <a href="{{route('companies.all.export')}}">
                    <div class="card overflow-hidden bg-primary text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h4 class="">خروجی تمام شرکت ها</h6>
                                </div>
                                <div class="ms-auto">
                                    <img src="{{asset('assets/panel/images/icon/SVG/company-profile.svg')}}" alt="company">
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <a href="{{route('event.all.export')}}">
                    <div class="card overflow-hidden bg-info text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h4 class="">خروجی تمام رویداد ها</h4>
                                </div>
                                <div class="ms-auto">
    
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <a href="{{route('students.all.export')}}">
                    <div class="card overflow-hidden bg-green text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h4 class="">خروجی تمام دانشجویان</h4>
                                </div>
                                <div class="ms-auto">
    
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                <a href="{{route('intern-positions.all.export')}}">
                    <div class="card overflow-hidden bg-success text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h4 class="">خروجی موقعیت های پویش</h4>
                                </div>
                                <div class="ms-auto">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
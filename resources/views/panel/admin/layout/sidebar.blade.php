
<ul class="side-menu">
    <li class="sub-category">
        <h3>اصلی</h3>
    </li>
    <li class="slide">
        <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('dashboard')}}">
            <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/dashboard.svg')}}" alt="داشبورد">
            <span class="side-menu__label ms-3">داشبورد</span>
        </a>
        <a class="side-menu__item has-link" target="_blank" data-bs-toggle="slide" href="{{route('home')}}">
            <img class="img-layout-sidebar" src="{{asset('assets/panel/images/icon/SVG/home.svg')}}" alt="داشبورد">
            <span class="side-menu__label ms-3">صفحه اصلی سایت</span>
        </a>
    </li>

    <li class="sub-category">
        <h3> امکانات</h3>
    </li>

    @if(Auth::user()->IsAdmin() && Auth::user()->groupable->status == true)
        
        <li class="slide manage_user ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/users.svg')}}" alt="کاربران">
                <span class="side-menu__label ms-3">مدیریت کاربران</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">مدیریت کاربران</a></li>
                <li><a href="{{route('create.user-management')}}"  class="slide-item {{Route::is('create.user-management')?'active':''}}">افزودن کاربر</a></li>
                <li><a href="{{route('create.armed-forces.user-management')}}"  class="slide-item {{Route::is('create.armed-forces.user-management')?'active':''}}">افزودن نیرو مسلح</a></li>
                <li><a href="{{route('employee.user-managment')}}" class="slide-item {{Route::is('employee.user-managment')?'active':''}}">کارمندان دانشگاه</a></li>
                <li><a href="{{route('company.user-managment')}}" class="slide-item {{Route::is('company.user-managment')?'active':''}}">شرکت ها</a></li>
                <li><a href="{{route('student.user-managment')}}" class="slide-item {{Route::is('student.user-managment')?'active':''}}">دانشجویان</a></li>
                {{-- <li><a href="" class="slide-item">کاربران تایید شده</a></li> --}}
                {{-- <li><a href="{{route('employee.export.all.user.show')}}" class="slide-item {{Route::is('employee.export.all.user.show')?'active':''}}">خروجی تمام کاربران</a></li> --}}
            </ul>
        </li>
        <li class="slide manage_user ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/transaction.svg')}}" alt="تراکنش ها">
                <span class="side-menu__label ms-3">تراکنش ها</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">تراکنش ها</a></li>
                <li><a href="{{route('list.of.transactions.skill')}}"  class="slide-item {{Route::is('list.of.transactions.skill')?'active':''}}">مهارت ها</a></li>
                
                {{-- <li><a href="" class="slide-item">کاربران تایید شده</a></li> --}}
                {{-- <li><a href="{{route('employee.export.all.user.show')}}" class="slide-item {{Route::is('employee.export.all.user.show')?'active':''}}">خروجی تمام کاربران</a></li> --}}
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('armed.list.skill')}}">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/skill.svg')}}" alt="مهارت ها">
                <span class="side-menu__label ms-3">مهارت ها</span>
            </a>
        </li>
    @endif

    @if (UserIsUnitEmployee())
        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar" src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="رویداد">
                <span class="side-menu__label ms-3">رویداد صدرا</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">رویداد صدرا</a></li>
                <li><a href="{{route('create.event')}}" class="slide-item {{Route::is('create.event')?'active':''}}">ثبت رویداد</a></li>
                <li><a href="{{route('index.event')}}" class="slide-item {{Route::is('index.event')?'active':''}}">لیست رویدادها</a></li>
                <li><a href="{{route('index.plan')}}" class="slide-item {{Route::is('index.plan')?'active':''}}">لیست پلن های ثبت نام </a></li>
                <li><a href="{{route('list.internship')}}" class="slide-item {{Route::is('list.internship')?'active':''}}">موقعیت های پویش</a></li>
            </ul>
        </li>
        <li class="slide public ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar-width"  src="{{asset('assets/panel/images/icon/SVG/uni.svg')}}" alt="دانشجویان">
                <span class="side-menu__label ms-3">دانشجویان</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">دانشجویان</a></li>
                <li><a href="{{route('emp-index.request.internship')}}" class="slide-item {{Route::is('emp-index.request.internship')?'active':''}}">درخواست های پویش</a></li>
                <li><a href="{{route('std.list')}}" class="slide-item {{Route::is('std.list')?'active':''}}">لیست دانشجویان</a></li>
                <li><a href="{{route('uni.list.skill')}}" class="slide-item {{Route::is('uni.list.skill')?'active':''}}">لیست مهارت  ها</a></li>
            </ul>
        </li>
    @endif

    @if (UserIsProvincialEmployee())
        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="رویداد">
                <span class="side-menu__label ms-3">رویداد صدرا</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">رویداد صدرا</a></li>
                <li><a href="{{route('create.event')}}" class="slide-item {{Route::is('create.event')?'active':''}}">ثبت رویداد</a></li>
                <li><a href="{{route('index.event')}}" class="slide-item {{Route::is('index.event')?'active':''}}">لیست  رویدادها</a></li>
                <li><a href="{{route('index.plan')}}" class="slide-item {{Route::is('index.plan')?'active':''}}">لیست پلن های ثبت نام </a></li>
                <li><a href="{{route('list.internship')}}" class="slide-item {{Route::is('list.internship')?'active':''}}">موقعیت های پویش</a></li>
            </ul>
        </li>

        <li class="slide news ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar" src="{{asset('assets/panel/images/icon/SVG/news.svg')}}" alt="اخبار">
                <span class="side-menu__label ms-3">اخبار</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li><a href="{{route('create.news')}}" class="slide-item {{Route::is('create.news')?'active':''}}">افزدون خبر</a></li>
                <li><a href="{{route('index.news')}}" class="slide-item {{Route::is('index.news')?'active':''}}">لیست اخبار</a></li>
            </ul>
        </li>

        <li class="slide public ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar-width"  src="{{asset('assets/panel/images/icon/SVG/uni.svg')}}" alt="واحدها">
                <span class="side-menu__label ms-3">واحدهای زیر مجموعه</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">واحد های دانشگاهی</a></li>
                <li><a href="{{ route('universities.list') }}" class="slide-item {{Route::is('universities.list')?'active':''}}">لیست دانشگاه ها</a></li>
                <li><a href="{{route('unconfirmed.event')}}" class="slide-item {{Route::is('unconfirmed.event')?'active':''}}">رویداد های تایید نشده</a></li>
                <li><a href="{{route('confirmed.event')}}" class="slide-item {{Route::is('confirmed.event')?'active':''}}">رویداد های تایید شده</a></li>
                <li><a href="{{route('unconfirmed.news')}}" class="slide-item {{Route::is('unconfirmed.news')?'active':''}}">اخبار تایید نشده</a></li>
            </ul>
        </li>
        <li class="slide public ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/profile.svg')}}" alt="دانشجویان">
                <span class="side-menu__label ms-3">دانشجویان</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">دانشجویان</a></li>
                <li><a href="{{route('emp-index.request.internship')}}" class="slide-item {{Route::is('emp-index.request.internship')?'active':''}}">درخواست های پویش</a></li>
                <li><a href="{{route('std.list')}}" class="slide-item {{Route::is('std.list')?'active':''}}">لیست دانشجویان</a></li>
                <li><a href="{{route('uni.list.skill')}}" class="slide-item {{Route::is('uni.list.skill')?'active':''}}">لیست مهارت  ها</a></li>
            </ul>
        </li>
    @endif

    @if (UserIsCountryEmployee())
        {{-- <li class="slide pooyesh-menu">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <i class="side-menu__icon fe fe-slack"></i>
                <span class="side-menu__label">طرح پویش <span style="font-size: 10px">(اشتغال همراه با تحصیل)</span></span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پویش</a></li>
                <li><a href="{{route('std-index.request.internship')}}" class="slide-item {{Route::is('std-index.request.internship')?'active':''}}"> درخواست های ثبت شده</a></li>


            </ul>
        </li> --}}
    @endif

    @if (Auth::user()->IsCompany())
        <li class="slide company">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/company-profile.svg')}}" alt="پروفایل شرکت">
                <span class="side-menu__label ms-3">پروفایل شرکت</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پروفایل شرکت</a></li>
                <li><a href="{{route('company.single',Auth::user()->groupable_id)}}" class="slide-item {{Route::is('company.single',Auth::user()->groupable_id)?'active':''}}">صفحه اختصاصی شرکت </a></li>
                <li><a href="{{route('co.show.info')}}" class="slide-item {{Route::is('co.show.info')?'active':''}}">اطلاعات شرکت</a></li>
            </ul>
        </li>

        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="رویداد">
                <span class="side-menu__label ms-3">رویداد صدرا</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">رویداد صدرا</a></li>
                <li><a href="{{route('index.register.event')}}" class="slide-item {{Route::is('index.register.event')?'active':''}}">رویداد ها</a></li>
                <li><a href="{{route('list.event')}}" class="slide-item {{Route::is('list.event')?'active':''}}">رویداد های جدید</a></li>
                <li><a href="{{route('index.internship')}}" class="slide-item {{Route::is('index.internship')?'active':''}}">موقعیت های پویش</a></li>
            </ul>
        </li>

        <li class="slide public ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/dile2.svg')}}" alt="استخدام و سرمایه گذاری">
                <span class="side-menu__label ms-3">عمومی</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">عمومی</a></li>
                <li><a href="{{route('co-index.request.internship')}}" class="slide-item {{Route::is('co-index.request.internship')?'active':''}}"> درخواست های پویش</a></li>
        </li>
    @endif

    @if (Auth::user()->IsStudent())
        <li class="slide company">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
                <img class="img-layout-sidebar"  src="{{asset('assets/panel/images/icon/SVG/profile.svg')}}" alt="پروفایل دانشجو">
                <span class="side-menu__label ms-3">پروفایل دانشجو</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پروفایل دانشجو</a></li>
                <li><a href="{{route('student.single',\Illuminate\Support\Facades\Auth::user()->groupable_id)}}" class="slide-item {{Route::is('student.single',\Illuminate\Support\Facades\Auth::user()->groupable_id)?'active':''}}">صفحه اختصاصی دانشجو </a></li>
                <li><a href="{{route('std.show.info')}}" class="slide-item {{Route::is('std.show.info')?'active':''}}">اطلاعات دانشجو</a></li>
                <li><a href="{{route('list.of.transactions.std')}}" class="slide-item {{Route::is('list.of.transactions.std')?'active':''}}">تراکنش ها</a></li>
            </ul>
        </li>

        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="img-layout-sidebar" src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="استخدام و سرمایه گذاری">
                <span class="side-menu__label ms-3">رویداد صدرا</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">رویداد صدرا</a></li>
                <li><a href="{{route('index-std.event')}}" class="slide-item {{Route::is('index-std.event')?'active':''}}">رویداد ها</a></li>
                <li><a href="{{route('list-std.event')}}" class="slide-item {{Route::is('list-std.event')?'active':''}}">رویداد های جدید</a></li>
                <li><a href="{{route('std-index.request.internship')}}" class="slide-item {{Route::is('std-index.request.internship')?'active':''}}">درخواست های پویش</a></li>
            </ul>
        </li>


    @endif




    {{-- <li class="slide public ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            <i class="side-menu__icon fe fe-slack"></i>
            <span class="side-menu__label">عمومی</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">

            <li class="side-menu-label1"><a href="javascript:void(0)">عمومی</a></li>
            <li><a href="{{route('employee.unconfirmed-stds')}}" class="slide-item {{Route::is('employee.unconfirmed-stds')?'active':''}}">دانشجویان تایید نشده</a></li>
            <li><a href="{{route('employee.unconfirmed-companies')}}" class="slide-item {{Route::is('employee.unconfirmed-companies')?'active':''}}">شرکت های تایید نشده</a></li>
            <li><a href="{{route('employee.co.list')}}" class="slide-item {{Route::is('employee.co.list')?'active':''}}">لیست شرکت ها</a></li>
            <li><a href="{{route('employee.std.list')}}" class="slide-item {{Route::is('employee.std.list')?'active':''}}">لیست دانشجویان</a></li>
            <li><a href="{{route('emp.graders.list')}}" class="slide-item {{Route::is('emp.graders.list')?'active':''}}">لیست دانش آموزان</a></li>
            <li><a href="{{route('employee.personality-tests.result-show')}}" class="slide-item {{Route::is('employee.personality-tests.result-show')?'active':''}}">نتایج تست های شخصیتی</a></li>
            <li><a href="#" class="slide-item {{Route::is('employee.unconfirmed-job-positions')?'active':''}}">موقعیت های شغلی تایید نشده</a></li>
            <li><a href="#" class="slide-item {{Route::is('employee.pooyesh-reqs')?'active':''}}"> درخواست های پویش</a></li>
            <li><a href="#" class="slide-item {{Route::is('employee.hire-reqs')?'active':''}}"> درخواست های استخدام</a></li>
            <li><a href="#" class="slide-item {{Route::is('employee.std-resume-list.show')?'active':''}}"> رزومه های ارسال شده</a></li>
            <li><a href="#" class="slide-item {{Route::is('files.index')?'active':''}}"> فرم ها و آیین نامه ها</a></li>
            <li><a href="#" class="slide-item {{Route::is('emp.announce.list')?'active':''}}"> اطلاعیه ها</a></li>
            <li><a href="#" class="slide-item {{Route::is('emp.signature.create')?'active':''}}"> افزودن امضا </a></li>
        </ul>
    </li>

    <li class="slide sadra ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            <i class="side-menu__icon fe fe-slack"></i>
            <span class="side-menu__label">رویداد صدرا</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">رویداد صدرا</a></li>
            <li><a href="{{route('create.event')}}" class="slide-item {{Route::is('create.event')?'active':''}}">ثبت رویداد</a></li>
            <li><a href="{{route('index.event')}}" class="slide-item {{Route::is('index.event')?'active':''}}">لیست  رویدادها</a></li>
            <li><a href="{{route('list.event')}}" class="slide-item {{Route::is('list.event')?'active':''}}">لیست  رویدادها</a></li>
            <li><a href="{{route('index.plan')}}" class="slide-item {{Route::is('index.plan')?'active':''}}">لیست پلن های ثبت نام </a></li>
        </ul>
    </li> --}}



    {{-- @can('support')
        <li class="slide supp ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <i class="side-menu__icon fe fe-slack"></i>
                <span class="side-menu__label">پشتیبانی</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پشتیبانی</a></li>
                <li><a href="{{route('employee.tickets.list')}}" class="slide-item {{Route::is('employee.tickets.list')?'active':''}}"> لیست درخواست ها</a></li>
            </ul>
        </li>
    @endcan --}}

    {{-- @can('readLog')
        <li class="slide other ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <i class="side-menu__icon fe fe-slack"></i>
                <span class="side-menu__label">سایر</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پشتیبانی</a></li>
                <li><a href="{{route('employee.co-log-activity.list')}}" class="slide-item {{Route::is('employee.co-log-activity.list')?'active':''}}"> گزارش فعالیت شرکت ها</a></li>
                <li><a href="{{route('employee.std-log-activity.list')}}" class="slide-item {{Route::is('employee.std-log-activity.list')?'active':''}}"> گزارش فعالیت کارجویان</a></li>
                <li><a href="{{route('employee.emp-log-activity.list')}}" class="slide-item {{Route::is('employee.emp-log-activity.list')?'active':''}}"> گزارش فعالیت کارکنان</a></li>
            </ul>
        </li>
    @endcan --}}

    {{-- @can('managePermission')
        <li class="slide role-permission ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <i class="side-menu__icon fe fe-slack"></i>
                <span class="side-menu__label">نقش ها و دسترسی ها</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">نقش ها و درسترسی ها</a></li>
                <li><a href="{{route('employee.role-create')}}" class="slide-item {{Route::is('employee.role-create')?'active':''}}">تعریف نقش</a></li>
                <li><a href="{{route('employee.show.role.assign')}}" class="slide-item {{Route::is('employee.show.role.assign')?'active':''}}">انتصاب نقش به کاربر</a></li>
            </ul>
        </li>
    @endcan --}}
</ul>

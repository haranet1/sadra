<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="صدرا">
    <meta name="author" content="haranet.ir">
    <meta name="_token" content="{{csrf_token()}}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">



    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/panel/images/logo/sadra-logo-min.svg')}}"/>

    <title>صدرا | @yield('title')</title>

    <link id="style"  href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.rtl.min.css')}}" rel="stylesheet"/>
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.rtl.min.css" integrity="sha384-gXt9imSW0VcJVHezoNQsP+TNrjYXoGcrqBZJpry9zJt8PCQjobwmhMGaDHTASo9N" crossorigin="anonymous"> --}}


    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/css/dark-style.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/panel/css/skin-modes.css')}}" rel="stylesheet"/>

    <link href="{{asset('assets/panel/css/icons.css')}}" rel="stylesheet"/>

    <link  rel="stylesheet" type="text/css" media="all" href="{{asset('assets/panel/colors/color1.css')}}"/>
    <link href="{{asset('assets/panel/switcher/css/switcher.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/switcher/demo.css')}}" rel="stylesheet"/>

    <link rel="stylesheet"  href="{{asset('assets/panel/fonts/styles-fa-num/iran-yekan.css')}}">
    <link href="{{asset('assets/panel/css/rtl.css')}}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{asset('assets/panel/css/persianDatepicker.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/panel/css/layout-header-in-style.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/panel/css/layout-sidebar-in-style.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/panel/css/master-in-style.css')}}"/>


    @yield('css')
</head>
<body class="app sidebar-mini rtl light-mode">


<div id="global-loader">
    <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
</div>


<div class="page">
    <div class="page-main">

        @include('panel.admin.layout.header')


        <div class="sticky">
            <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
            <div class="app-sidebar">
                <div class="side-header">
                    <a class="header-brand1" href="{{ route('home') }}">
                        <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}" class="img-master-in-style header-brand-img desktop-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/logo/sadra-logo-min-w.svg')}}"  class="img-master-in-style header-brand-img toggle-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/logo/sadra-logo-min.svg')}}"  class="img-master-in-style header-brand-img light-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/logo/sadra-logo.svg')}}"  class="img-master-in-style header-brand-img light-logo1" alt="logo">
                    </a>

                </div>
                <div class="main-sidemenu">
                    <div class="slide-left disabled" id="slide-left">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-master-in-style"
                             viewBox="0 0 24 24">
                            <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"/>
                        </svg>
                    </div>
                    @include('panel.admin.layout.sidebar')
                    <div class="slide-right" id="slide-right">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-master-in-style"
                             viewBox="0 0 24 24">
                            <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"/>
                        </svg>
                    </div>
                </div>
            </div>

        </div>

        <div class="main-content app-content mt-0">
            <div class="side-app">

                <div class="main-container container-fluid">

                    @yield('main')
                </div>
            </div>
        </div>
    </div>


    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-md-12 col-sm-12 text-center">
                    حرانت ©
                </div>
            </div>
        </div>
    </footer>

</div>

<a href="#top" id="back-to-top" class="d-print-none">
    <i class="fa fa-angle-up">

    </i></a>

<input type="hidden" id="CSRF" value="{{csrf_token()}}">
@yield('inputs')

{{--<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>--}}
<script src="{{asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/panel/js/jquery.sparkline.min.js')}}"></script>

<script src="{{asset('assets/panel/js/sticky.js')}}"></script>

<script src="{{asset('assets/panel/js/circle-progress.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/peitychart/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/peitychart/peitychart.init.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidebar/sidebar.js')}}"></script>

{{-- <script src="{{asset('assets/panel/plugins/p-scroll/perfect-scrollbar.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll-1.js')}}"></script> --}}

<script src="{{asset('assets/panel/plugins/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/rounded-barchart.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/utils.js')}}"></script>

<script src="{{asset('assets/panel/plugins/select2/select2.full.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('assets/panel/js/apexcharts.js')}}"></script>
<script src="{{asset('assets/panel/plugins/apexchart/irregular-data-series.js')}}"></script>

<script src="{{asset('assets/panel/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/chart.flot.sampledata.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/dashboard.sampledata.js')}}"></script>

<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidemenu/sidemenu.js')}}"></script>

{{-- <script src="{{asset('assets/panel/plugins/bootstrap5-typehead/autocomplete.js')}}"></script> --}}
{{-- <script src="{{asset('assets/panel/js/typehead.js')}}"></script> --}}

<script src="{{asset('assets/panel/js/index1.js')}}"></script>

<script src="{{asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{asset('assets/panel/js/custom.js')}}"></script>
<script src="{{asset('assets/panel/js/custom1.js')}}"></script>

<script src="{{asset('assets/panel/plugins/persianDatepicker/js/persianDatepicker.min.js')}}"></script>



<script src="{{asset('assets/panel/plugins/swal2/swal2.all.min.js')}}"></script>

@yield('script')
</body>
</html>

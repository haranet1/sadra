@extends('panel.admin.layout.master')
@section('title' , 'درخواست های پویش')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/sent-resume-styles.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">درخواست های همکاری</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">درخواست های همکاری</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body px-1 px-md-3">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="nav panel-tabs overflow-auto d-flex flex-nowrap">
                                    <li><a id="head-tab28" href="#tab1" class="active li-co-index-style"  data-bs-toggle="tab">درخواست های جدید</a></li>
                                    <li><a id="head-tab25" href="#tab2" class="li-co-index-style" data-bs-toggle="tab">درخواست های پذیرفته شده</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    @if(count($requests) > 0)
                                        <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                            @foreach ($requests as $request)
                                                <div class="col-12 col-md-4  mb-3">
                                                    <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                        <div class="card-body">

                                                            <!-- Profile picture and short information -->
                                                            <div class="d-flex align-items-center position-relative pb-3">
                                                                <div class="flex-shrink-0">
                                                                    <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="badge bg-primary">کارآموزی</span>
                                                                    <span>{{$request->internPosition->title}}</span>
                                                                    <div class="mt-2">
                                                                        <h4>{{$request->studentInfo->user->name." ".$request->studentInfo->user->family}}</h4>
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <h6 class="text-gray">دانشگاه : {{$request->studentInfo->university->name}}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- END : Profile picture and short information -->

                                                            <!-- Options buttons -->
                                                            <div class="mt-3 pt-2 text-center border-top">
                                                                <div class="d-flex justify-content-center gap-3">
                                                                    <a href="{{route('student.single',$request->studentInfo->id)}}" target="_blank" class="btn btn-info">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                    </a>
                                                                    <input type="hidden" id="request_id" value="{{$request->id}}">
                                                                    <a href="javascript:void(0)" id="accept" class="btn btn-success" type="button">
                                                                        <i class="fa fa-check" aria-hidden="true"></i> تایید
                                                                    </a>
                                                                    <a href="javascript:void(0)" id="decline" class="btn btn-danger " type="button">
                                                                        <i class="fa fa-close" aria-hidden="true"></i> رد
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- END : Options buttons -->

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="alert alert-warning text-center">
                                            اطلاعاتی جهت نمایش وجود ندارد
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$requests->links('pagination.panel')}}
                                    </div>
                                </div>

                                <div class="tab-pane " id="tab2">
                                    @if(count($confirmedRequests) > 0)
                                        <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                            @foreach ($confirmedRequests as $request)
                                                <div class="col-12 col-md-4  mb-3">
                                                    <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                        <div class="card-body">

                                                            <!-- Profile picture and short information -->
                                                            <div class="d-flex align-items-center position-relative pb-3">
                                                                <div class="flex-shrink-0">
                                                                    <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="badge bg-primary">کارآموزی</span>
                                                                    <span>{{$request->internPosition->title}}</span>
                                                                    <div class="mt-2">
                                                                        <h4>{{$request->studentInfo->user->name." ".$request->studentInfo->user->family}}</h4>
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <h6 class="text-gray">دانشگاه : {{$request->studentInfo->university->name}}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- END : Profile picture and short information -->

                                                            <!-- Options buttons -->
                                                            <div class="mt-3 pt-2 text-center border-top">
                                                                <div class="d-flex justify-content-center gap-3">
                                                                    <a href="{{route('student.single',$request->studentInfo->id)}}" target="_blank" class="btn btn-info">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- END : Options buttons -->

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="alert alert-warning text-center">
                                            اطلاعاتی جهت نمایش وجود ندارد
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$confirmedRequests->links('pagination.panel')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="acceptRequestInternship" value="{{ route('accept.request.internship') }}">
        <input type="hidden" id="declineRequestInternship" value="{{ route('decline.request.internship') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/events/js/pooyesh.js') }}"></script>
@endsection

{{-- js is fixed --}}
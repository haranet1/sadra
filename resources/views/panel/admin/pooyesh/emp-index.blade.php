@extends('panel.admin.layout.master')
@section('title' , 'درخواست های پویش')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست درخواست های پویش</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد کارمند</a></li>
                <li class="breadcrumb-item active" aria-current="page">لیست درخواست های پویش</li>
            </ol> 
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                </div>
                @if(count($requests) > 0)
                    <div class="e-table px-5 pb-5">
                        <div class="table-responsive table-lg">
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th>تاریخ ارسال</th>
                                        <th>دانشجو</th>
                                        <th>شرکت</th>
                                        <th>رویداد</th>
                                        <th>موقعیت پویش</th>
                                        <th>وضعیت</th>
                                        <th class="text-center">عملکردها</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($requests as $request)
                                        <tr>
                                            <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                            <td class="text-nowrap align-middle">{{$request->studentInfo->user->name}} {{$request->studentInfo->user->family}}</td>
                                            <td class="text-nowrap align-middle">{{$request->companyInfo->name}}</td>
                                            <td class="text-nowrap align-middle">{{$request->internPosition->event->title}}</td>
                                            <td>
                                                @if($request->internPosition)
                                                    {{$request->internPosition->title}}
                                                @endif
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                @if(is_null($request->status))
                                                    <span class="badge bg-primary"> در انتظار بررسی توسط شرکت</span>
                                                @else
                                                    @if($request->status == 1)
                                                        <span class="badge bg-success">قبول شده</span>
                                                    @elseif($request->status == 0)
                                                        <span class="badge bg-danger"> رد شده </span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-center align-middle">
                                                <div class=" align-top">
                                                    <a href="{{route('company.single',$request->company_id)}}"
                                                    class="btn btn-sm btn-primary badge mb-1" data-target="#user-form-modal"
                                                    data-bs-toggle="" type="button">پروفایل شرکت</a>
                                                    <a href="{{route('student.single' , $request->student_id)}}"
                                                    class="btn btn-sm btn-info badge mb-1" data-target="#user-form-modal"
                                                    data-bs-toggle="" type="button">پروفایل دانشجو</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            
                        </div>
                    </div>
                @else
                    <div class="row m-3 justify-content-center">
                        <div class="alert alert-warning text-center">
                            اطلاعاتی جهت نمایش وجود ندارد
                        </div>
                    </div>
                @endif
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
@extends('panel.admin.layout.master')
@section('title','لیست کارمندان دانشگاه')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/manage-users-company-in-style.css')}}">

@endsection
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست کارمندان دانشگاه</h3>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            <div class="input-group">
                                <input type="text" id="name" class="form-control" placeholder="نام :">
                                <input type="text" id="family" class="form-control input-user-company-style" placeholder="نام خانوادگی :" >
                                <input type="text" id="mobile" class="form-control input-user-company-style" placeholder="تلفن همراه :">
                                <input type="text" id="uni" class="form-control input-user-company-style" placeholder="نام دانشگاه :">
                                <div class="input-group-prepend">
                                    <div onclick="empSearch()" class="btn btn-info form-control div-border-user-company-style" >جستجو</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 me-2">
                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button> --}}
                        </div>
                        <div class="col-1">
                            <a href="{{route('employee.all.export')}}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$employees->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($employees)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام دانشگاه</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>شماره تلفن همراه</th>
                                        <th>نوع کاربری</th>
                                        <th>سمت دانشگاهی</th>
                                        <th class="text-center">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="emp_table">
                                    @php
                                        $counter = (($employees->currentPage() -1) * $employees->perPage()) + 1;
                                    @endphp
                                    @foreach ($employees as $employee)
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$counter++}}</td>
                                            @if ($employee->university)
                                                <td class="text-nowrap align-middle">{{$employee->university->name}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif
                                            @if ($employee->user)
                                                <td class="text-nowrap align-middle">{{$employee->user->name}} {{$employee->user->family}}</td>
                                                <td class="text-nowrap align-middle">{{$employee->user->mobile}}</td>
                                                <td class="text-nowrap align-middle">{{getRoleNamesFromUser($employee->user->getRoleNames())}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif
                                            @if ($employee->university_position)
                                                <td class="text-nowrap align-middle">{{$employee->university_position}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="javascript:void(0)" onclick="deleteInfoWithUser({{$employee->id}})"  class="btn btn-sm btn-danger badge" type="button">حذف</a>
                                                    <a href="{{route('emp.edit.info',$employee->id)}}" class="btn btn-sm btn-primary badge" type="button">ویرایش اطلاعات</a>
                                                    {{-- <a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button">تایید </a> --}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$employees->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="employeeSearchUrl" value="{{ route('employee.list.search') }}">
        <input type="hidden" id="editEmployeeInfoUrl" value="{{ route('emp.edit.info' , ':id' ) }}">
        <input type="hidden" id="deleteInfoWithUserUrl" value="{{ route('emp.delete.info') }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/search/js/employee.js')}}"></script>
@endsection

{{-- js is fixed --}}
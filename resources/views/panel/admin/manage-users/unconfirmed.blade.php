@extends('panel.admin.layout.master')
@section('title','کاربران تایید نشده')
@section('css')

    <link rel="stylesheet" href="{{asset('assets/panel/css/manage-users-company-in-style.css')}}">

@endsection
@section('main')

    <div class="row row-cards mt-4">
        <div class="col-lg-12 col-xl-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست کاربران تایید نشده</h3>
                </div>
                <div class="card-body">
                    <div class="card-pay">
                        <ul class="tabs-menu nav">
                            <li class=""><a href="#tab18" class="active" data-bs-toggle="tab">کارمندان</a></li>
                            <li class=""><a href="#tab20" class="" data-bs-toggle="tab">شرکت ها</a></li>
                            <li><a href="#tab22" data-bs-toggle="tab" class="">دانشجویان</a></li>
                        </ul>
                        <div class="tab-content">
                            {{-- employee list --}}
                            <div class="tab-pane active show" id="tab18">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-9">
                                            <div class="input-group">
                                                <input type="text" id="un_co_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="un_co_family_search" class="form-control input-user-company-style" placeholder="نام خانوادگی :">
                                                <input type="text" id="un_co_mobile_search" class="form-control input-user-company-style" placeholder="تلفن همراه :">
                                                <input type="text" id="un_co_province_search" class="form-control input-user-company-style" placeholder="استان :">
                                                <input type="text" id="un_co_coname_search" class="form-control input-user-company-style" placeholder="اسم شرکت :">
                                                <input type="text" id="un_co_workfield_search" class="form-control input-user-company-style" placeholder="حوضه کاری :">
                                                <div class="input-group-prepend">
                                                    <div onclick="unConfirmedCompanySearch()" id="un_co_btn_search" class="btn btn-info form-control div-border-user-company-style" >جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 me-2">
                                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button> --}}
                                        </div>
                                        <div class="col-1">
                                            <a href="" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$companies->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($companies)>0)
                                        <table class="table border-top table-bordered mb-0 table-striped">
                                            <thead>
                                            <tr>
                                                {{-- <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th> --}}
                                                <th>ردیف</th>
                                                <th>لوگو شرکت</th>
                                                <th>نام شرکت</th>
                                                <th>نام ثبت کننده شرکت در سامانه</th>
                                                <th>تلفن همراه</th>
                                                <th>مدیر عامل</th>
                                                <th>حوزه کاری</th>
                                                <th class="text-center">عملکردها</th>
                                            </tr>
                                            </thead>
                                            <tbody id="un-company-table-body">
                                            @php
                                                $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                            @endphp
                                            @foreach($companies as $company)
                                            <tr>
                                                {{-- <td><input type="checkbox" id="{{$company->id}}" class="checkbox" onclick="checkUncheck(this)"></td> --}}
                                                <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                <td class="align-middle text-center">
                                                    @if($company->groupable->logo)
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($company->groupable->logo->path)}}">
                                                    @else
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/images/defult-logo/'.$coimg.'.png')}}">
                                                    @endif
                                                </td>
                                                @if ($company->groupable)
                                                    <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">نام شرکت ثبت نشده</td>
                                                @endif
                                                <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->mobile}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>

                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="{{route('company.single',$company->groupable->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser({{$company->id}})">تایید </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                اطلاعاتی جهت نمایش وجود ندارد
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{-- company list --}}
                            <div class="tab-pane active show" id="tab20">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-9">
                                            <div class="input-group">
                                                <input type="text" id="un_co_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="un_co_family_search" class="form-control input-user-company-style" placeholder="نام خانوادگی :">
                                                <input type="text" id="un_co_mobile_search" class="form-control input-user-company-style" placeholder="تلفن همراه :">
                                                <input type="text" id="un_co_province_search" class="form-control input-user-company-style" placeholder="استان :">
                                                <input type="text" id="un_co_coname_search" class="form-control input-user-company-style" placeholder="اسم شرکت :">
                                                <input type="text" id="un_co_workfield_search" class="form-control input-user-company-style" placeholder="حوضه کاری :">
                                                <div class="input-group-prepend">
                                                    <div onclick="unConfirmedCompanySearch()" id="un_co_btn_search" class="btn btn-info form-control div-border-user-company-style">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 me-2">
                                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button> --}}
                                        </div>
                                        <div class="col-1">
                                            <a href="" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$companies->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($companies)>0)
                                        <table class="table border-top table-bordered mb-0 table-striped">
                                            <thead>
                                            <tr>
                                                {{-- <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th> --}}
                                                <th>ردیف</th>
                                                <th>لوگو شرکت</th>
                                                <th>نام شرکت</th>
                                                <th>نام ثبت کننده شرکت در سامانه</th>
                                                <th>تلفن همراه</th>
                                                <th>مدیر عامل</th>
                                                <th>حوزه کاری</th>
                                                <th class="text-center">عملکردها</th>
                                            </tr>
                                            </thead>
                                            <tbody id="un-company-table-body">
                                            @php
                                                $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                            @endphp
                                            @foreach($companies as $company)
                                            <tr>
                                                {{-- <td><input type="checkbox" id="{{$company->id}}" class="checkbox" onclick="checkUncheck(this)"></td> --}}
                                                <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                <td class="align-middle text-center">
                                                    @if($company->groupable->logo)
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($company->groupable->logo->path)}}">
                                                    @else
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/images/defult-logo/'.$coimg.'.png')}}">
                                                    @endif
                                                </td>
                                                @if ($company->groupable)
                                                    <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">نام شرکت ثبت نشده</td>
                                                @endif
                                                <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->mobile}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                                <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>

                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="{{route('company.single',$company->groupable->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser({{$company->id}})">تایید </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                اطلاعاتی جهت نمایش وجود ندارد
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{-- student list --}}
                            <div class="tab-pane" id="tab22">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-9">
                                            <div class="input-group">
                                                <input type="text" id="un_std_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="un_std_family_search" class="form-control input-user-company-style" placeholder="نام خانوادگی :">
                                                <input type="text" id="un_std_mobile_search" class="form-control input-user-company-style" placeholder="تلفن همراه :" >
                                                <select id="un_std_sex_search" class="form-control input-user-company-style">
                                                    <option value="0">جنسیت :</option>
                                                    <option value="male">مرد</option>
                                                    <option value="female">زن</option>
                                                </select>
                                                <input type="text" id="un_std_province_search" class="form-control input-user-company-style" placeholder="استان :">
                                                <input type="text" id="un_std_uni_search" class="form-control input-user-company-style" placeholder="دانشگاه :">
                                                <input type="text" id="un_std_major_search" class="form-control input-user-company-style" placeholder="رشته تحصیلی :">
                                                <div class="input-group-prepend">
                                                    <div onclick="unConfirmedStudentSearch()" id="un_std_btn_search" class="btn btn-info form-control div-border-user-company-style">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 me-2">
                                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید دانشجویان انتخاب شده</button> --}}
                                        </div>
                                        <div class="col-1">
                                            <a href="" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$students->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($students) > 0)
                                        <table class="table border-top table-bordered mb-0 table-striped">
                                            <thead>
                                            <tr>
                                                {{-- <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th> --}}
                                                <th>ردیف</th>
                                                <th>نام و نام خانوادگی </th>
                                                <th>تلفن همراه</th>
                                                <th>تاریخ تولد</th>
                                                <th>جنسیت</th>
                                                <th>استان</th>
                                                <th>دانشگاه</th>
                                                <th>رشته</th>
                                                <th>شهر</th>
                                                <th class="text-center">عملکردها</th>
                                            </tr>
                                            </thead>
                                            <tbody id="un-student-table-body">
                                            @php
                                                $counter = (($students->currentPage() -1) * $students->perPage()) + 1;
                                            @endphp
                                            @foreach($students as $std)
                                            <tr>
                                                {{-- <td><input type="checkbox" id="{{$std->id}}" class="checkbox" onclick="checkUncheck(this)"></td> --}}
                                                <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                <td class="text-nowrap align-middle">{{$std->name." ".$std->family}}</td>
                                                <td class="text-nowrap align-middle">{{$std->mobile}}</td>
                                                <td class="text-nowrap align-middle">{{verta($std->birth)->formatJalaliDate()}}</td>
                                                @if ($std->sex)
                                                    <td class="text-nowrap align-middle">{{change_sex_to_fa($std->sex)}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">جنسیت ثبت نشده</td>
                                                @endif
                                                @if ($std->province)
                                                    <td class="text-nowrap align-middle">{{$std->province->name}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">استان ثبت نشده</td>
                                                @endif
                                                @if ($std->city)
                                                    <td class="text-nowrap align-middle">{{$std->city->name}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">شهر ثبت نشده</td>
                                                @endif
                                                @if ($std->groupable->university)
                                                    <td class="text-nowrap align-middle">{{$std->groupable->university->name}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">دانشگاه ثبت نشده</td>
                                                @endif
                                                @if ($std->groupable->major)
                                                    <td class="text-nowrap align-middle">{{$std->groupable->major}}</td>
                                                @else
                                                    <td class="text-nowrap align-middle">رشته ثبت نشده</td>
                                                @endif
                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="{{route('student.single',$std->groupable->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                                        <a href="javascript:void(0)" onclick="confirmUser({{$std->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                اطلاعاتی جهت نمایش وجود ندارد
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                    {{$students->links('pagination.panel')}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="confirmUserUrl" value="{{ route('confirm.user') }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/auth/js/user.js')}}"></script>
@endsection

{{-- js is fixed --}}




@extends('panel.admin.layout.master')
@section('title' , 'تعریف نیرو مسلح')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/create-users.css')}}">
    <link rel="stylesheet" href="{{asset('assets/panel/plugins/persianDatePicker/css/persianDatepicker-default.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">تعریف نیرو مسلح جدید</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد کارکنان</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success text-center">
            {{Session::pull('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger text-center">
            {{Session::pull('error')}}
        </div>
    @endif
    {{-- @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="alert alert-danger alert-dismissible">
                    <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                    @foreach($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            </div>
        </div>
    @endif --}}
    <div class="row row-cards justify-content-center ">
        <div class="col-lg-10 col-xl-10 px-0 px-md-2">
            <div class="card p-5 px-0 px-md-2">
                <div class="panel panel-primary">
                    <div class="tab-menu-heading tab-menu-heading-boxed">
                        <div class="tabs-menu-boxed">
                            <ul class="nav panel-tabs">
                                <li><a href="#tab1" class="active" data-bs-toggle="tab">نیرو مسلح</a></li>
                                {{-- <li><a href="#tab2" data-bs-toggle="tab" class="">شرکت</a></li> --}}
                                {{-- <li><a href="#tab3" data-bs-toggle="tab" class="">دانشجو</a></li> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="wrap-login100 p-2 p-md-6">
                                    <form action="{{route('store.armed-forces.user-management')}}" method="POST" class="login100-form validate-form">
                                        @csrf
                                        <div class="row">
                                            <input name="role" value="armed_forces" type="hidden">


                                            <div class="col-12 col-md-6">
                                                {{-- name --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="name" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder="نام">
                                                </div>
                                                @error('name') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- family --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="family" class="input100 border-start-0 ms-0 form-control" type="text"
                                                           placeholder="نام خانوادگی">
                                                </div>
                                                @error('family') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- n_code --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="n_code" value="{{old('n_code')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder=" کد ملی">
                                                </div>
                                                @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- sex --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <select name="sex" class="form-control form-select select2" >
                                                        <option label="جنسیت" value="">جنسیت</option>
                                                        <option value="male">مرد</option>
                                                        <option value="female">زن</option>
                                                    </select>
                                                </div>
                                                @error('sex') <small class="text-danger ">{{$message}}</small> @enderror

                                            </div>
                                            <div class="col-12 col-md-6">
                                                {{-- birth --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="birth" id="birth" value="{{old('birth')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder=" تاریخ تولد">
                                                </div>
                                                @error('birth') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- email --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                        placeholder=" ایمیل">
                                                </div>
                                                @error('email') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- mobile --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="mobile" class="input100 border-start-0 ms-0 form-control" type="text"
                                                           placeholder=" موبایل">
                                                </div>
                                                @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                                            </div>
                                        </div>

                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">ثبت</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/plugins/persianDatepicker/js/persianDatepicker.min.js')}}"></script>
    <script src="{{ asset('assets/manage-user/js/uesr-manage.js') }}"></script>
@endsection

{{-- js is fixed --}}
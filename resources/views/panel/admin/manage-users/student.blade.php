@extends('panel.admin.layout.master')
@section('title','لیست دانشجویان')
@section('css')

    <link rel="stylesheet" href="{{asset('assets/panel/css/manage-users-company-in-style.css')}}">

@endsection
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست دانشجویان</h3>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            <div class="input-group">
                                <input type="text" id="name" class="form-control" placeholder="نام :">
                                <input type="text" id="family" class="form-control input-user-company-style" placeholder="نام خانوادگی :">
                                <input type="text" id="mobile" class="form-control input-user-company-style" placeholder="تلفن همراه :">
                                <input type="text" id="grade" class="form-control input-user-company-style" placeholder="مقطع :">
                                <input type="text" id="major" class="form-control input-user-company-style" placeholder="رشته :">
                                <div class="input-group-prepend">
                                    <div onclick="coSearch()" class="btn btn-info form-control div-border-user-company-style" >جستجو</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 me-2">
                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button> --}}
                        </div>
                        <div class="col-1">
                            <a href="{{route('students.all.export')}}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$students->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($students)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>شماره تلفن همراه</th>
                                        <th>مقطع </th>
                                        <th>رشته</th>
                                        <th class="text-center">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="std_table">
                                    @php
                                        $counter = (($students->currentPage() -1) * $students->perPage()) + 1;
                                    @endphp
                                    @foreach ($students as $student)
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$counter++}}</td>
                                            @if ($student->user)
                                                <td class="text-nowrap align-middle">{{$student->user->name}} {{$student->user->family}}</td>
                                                <td class="text-nowrap align-middle">{{$student->user->mobile}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            <td class="text-nowrap align-middle">{{$student->grade}}</td>

                                            <td class="text-nowrap align-middle">{{$student->major}}</td>

                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="{{route('student.single',$student->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده اطلاعات</a>
                                                    @if (is_null($student->user->status))
                                                        <a href="javascript:void(0)" onclick="confirmUser({{$student->user->id}})" class="btn btn-sm btn-success badge" type="button">تایید </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$students->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="studentSearchUrl" value="{{ route('student.list.search') }}">
        <input type="hidden" id="confirmUserUrl" value="{{ route('confirm.user') }}">
        <input type="hidden" id="showStudentSingleUrl" value="{{ route('student.single' , ':id' ) }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/auth/js/user.js')}}"></script>
    <script src="{{asset('assets/search/js/student.js')}}"></script>
@endsection

{{-- js is fixed --}}
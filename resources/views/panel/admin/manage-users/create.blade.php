@extends('panel.admin.layout.master')
@section('title' , 'تعریف کاربر')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/create-users.css')}}">
    <link rel="stylesheet" href="{{asset('assets/panel/plugins/persianDatePicker/css/persianDatepicker-default.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">تعریف کاربر جدید</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success text-center">
            {{Session::pull('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger text-center">
            {{Session::pull('error')}}
        </div>
    @endif
    {{-- @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="alert alert-danger alert-dismissible">
                    <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                    @foreach($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            </div>
        </div>
    @endif --}}
    <div class="row row-cards justify-content-center ">
        <div class="col-lg-10 col-xl-10 px-0 px-md-2">
            <div class="card p-5 px-0 px-md-2">
                <div class="panel panel-primary">
                    <div class="tab-menu-heading tab-menu-heading-boxed">
                        <div class="tabs-menu-boxed">
                            <ul class="nav panel-tabs">
                                <li><a href="#tab1" class="active" data-bs-toggle="tab">کارمند</a></li>
                                {{-- <li><a href="#tab2" data-bs-toggle="tab" class="">شرکت</a></li> --}}
                                {{-- <li><a href="#tab3" data-bs-toggle="tab" class="">دانشجو</a></li> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="wrap-login100 p-2 p-md-6">
                                    <form action="{{route('store.employee.user-managment')}}" method="POST" class="login100-form validate-form">
                                        @csrf
                                        <div class="row">
                                            <input name="groupable_type" value="employee" type="hidden">


                                            <div class="col-12 col-md-6">
                                                {{-- name --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="name" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder="نام">
                                                </div>
                                                @error('name') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- family --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="family" class="input100 border-start-0 ms-0 form-control" type="text"
                                                           placeholder="نام خانوادگی">
                                                </div>
                                                @error('family') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- n_code --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="n_code" value="{{old('n_code')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder=" کد ملی">
                                                </div>
                                                @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- sex --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                                    </a>
                                                    <select name="sex" class="form-control form-select select2" >
                                                        <option label="جنسیت" value="">جنسیت</option>
                                                        <option value="male">مرد</option>
                                                        <option value="female">زن</option>
                                                    </select>
                                                </div>
                                                @error('sex') <small class="text-danger ">{{$message}}</small> @enderror

                                            </div>
                                            <div class="col-12 col-md-6">
                                                {{-- birth --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="birth" id="birth" value="{{old('birth')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                        placeholder=" تاریخ تولد">
                                                </div>
                                                @error('birth') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- email --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </span>
                                                    <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                        placeholder=" ایمیل">
                                                </div>
                                                @error('email') <small class="text-danger ">{{$message}}</small> @enderror
                                                {{-- mobile --}}
                                                <div class="wrap-input100 validate-input input-group">
                                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-phone" aria-hidden="true"></i>
                                                    </a>
                                                    <input name="mobile" class="input100 border-start-0 ms-0 form-control" type="text"
                                                           placeholder=" موبایل">
                                                </div>
                                                @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            {{-- university --}}
                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-city-alt" aria-hidden="true"></i>
                                                </span>
                                                <select id="university_id" name="university_id" class="select-employee-edit form-control form-select select2" >
                                                    <option value="" label="انتخاب واحد دانشگاهی">انتخاب واحد دانشگاهی</option>
                                                    @foreach($universities as $uni)
                                                        <option value="{{$uni->id}}"
                                                            @if(old('university_id'))
                                                                {{old('university_id') == $uni->id ? 'selected' : ''}}
                                                            @endif
                                                            >{{$uni->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('university_id') <small class="text-danger ">{{$message}}</small> @enderror
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-12 text-center">
                                                <span>نوع کاربری: </span>
                                            </div>
                                            <div class="col-12">
                                                <ul class="js-product-variants">
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="unit" name="role" class="variant-selector">
                                                            <span class="ui-variant--check">دسترسی واحدی</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-building"></i>
                                                            </span>
                                                            <input type="radio" value="provincial" name="role" class="variant-selector">
                                                            <span class="ui-variant--check">دسترسی استانی</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="country" name="role" class="variant-selector">
                                                            <span class="ui-variant--check">دسترسی کشوری</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            @error('role') <small class="text-danger ">{{$message}}</small> @enderror
                                        </div>

                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">ثبت</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            {{-- <div class="tab-pane" id="tab2">
                                <div class="col-12  ">
                                    <div class="card text-white bg-success mb-3">
                                        <div class="card-header">راهنما</div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <h5 class="text-white"> توضیحات</h5>
                                                    <p>
                                                        فایل نمونه را دانلود کرده و خانه های آن را طبق نمونه کامل
                                                        نمایید، سپس فایل خود را در محل مشخص شده آپلود نمایید و دکمه
                                                        ثبت  را بزنید.
                                                    </p>
                                                    <p>
                                                        دقت داشته باشید که فایل شما باید سر ستونهایی دقیقا مشابه
                                                        فایل نمونه داشته باشد.
                                                    </p>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <h5 class="text-white"> راهنمای سر ستون ها</h5>
                                                    <ul class="list-unstyled text-center ">
                                                        <li>name: نام کاربر را در این ستون وارد کنید</li>
                                                        <li>family: نام خانوادگی کاربر را در این ستون وارد کنید</li>
                                                        <li>mobile: شماره موبایل کاربر را در این ستون وارد کنید</li>
                                                        <li>password: کلمه عبور کاربر را در این ستون وارد کنید</li>
                                                    </ul>
                                                    <a class="btn btn-light my-2"
                                                       href="{{asset('files/users.xlsx')}}" download>دانلود
                                                        فایل نمونه</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <form class="form-group text-center mt-3"
                                          action="{{route('employee.store.user.excel')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="wrap-input100 validate-input input-group">
                                            <div class="row product-variants w-100 aligh-items-center">
                                                <div class="col-12 d-flex justify-content-start">
                                                    <span class="align-self-center">نوع کاربران: </span>
                                                    <ul class="js-product-variants">
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-building"></i>
                                                                </span>
                                                                <input type="radio" value="company" name="role"
                                                                    class="variant-selector">
                                                                <span class="ui-variant--check">شرکت</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="radio" value="student" name="role"class="variant-selector">
                                                                <span class="ui-variant--check">کارجو</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="radio" value="student" name="role"class="variant-selector">
                                                                <span class="ui-variant--check">دانشجو</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 d-flex justify-content-start">
                                                    <span class="align-self-center">ارسال پیامک اطلاع رسانی (SMS):</span>
                                                    <ul class="js-product-variants">
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                                <input type="radio" value="1" name="sms"
                                                                    class="variant-selector">
                                                                <span class="ui-variant--check">ارسال شود</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                                <input type="radio" value="0" name="sms"class="variant-selector">
                                                                <span class="ui-variant--check">ارسال نشود</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="file" name="users" class="form-control" required
                                               oninvalid="this.setCustomValidity('لطفا یک فایل انتخاب کنید')"
                                               oninput="setCustomValidity('')">
                                        <br>
                                        <button class="btn btn-success">ثبت </button>
                                    </form>

                                </div>
                            </div> --}}

                            {{-- <div class="tab-pane" id="tab3">

                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/plugins/persianDatepicker/js/persianDatepicker.min.js')}}"></script>
    <script src="{{ asset('assets/manage-user/js/uesr-manage.js') }}"></script>
@endsection

{{-- js is fixed --}}
@extends('panel.admin.layout.master')
@section('title','لیست شرکت ها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/manage-users-company-in-style.css')}}">

@endsection

@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست شرکت ها</h3>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            <div class="input-group">
                                <input type="text" id="co_name" class="form-control" placeholder="نام شرکت :" class="input-user-company-style">
                                <input type="text" id="name" class="form-control" placeholder="نام :">
                                <input type="text" id="family" class="form-control" placeholder="نام خانوادگی :"  class="input-user-company-style">
                                <input type="text" id="mobile" class="form-control" placeholder="تلفن همراه :"  class="input-user-company-style">
                                <div class="input-group-prepend">
                                    <div onclick="coSearch()" class="btn btn-info form-control div-border-user-company-style">جستجو</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 me-2">
                            {{-- <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button> --}}
                        </div>
                        <div class="col-1">
                            <a href="{{route('companies.all.export')}}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$companies->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($companies)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام شرکت</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>شماره تلفن همراه</th>
                                        <th>نام مدیرعامل</th>
                                        <th>حوزه کاری</th>
                                        <th class="text-center">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                    @endphp
                                    @foreach ($companies as $company)
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$counter++}}</td>
                                            @if ($company->name)
                                                <td class="text-nowrap align-middle">{{$company->name}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif
                                            @if ($company->user)
                                                <td class="text-nowrap align-middle">{{$company->user->name}} {{$company->user->family}}</td>
                                                <td class="text-nowrap align-middle">{{$company->user->mobile}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            <td class="text-nowrap align-middle">{{$company->ceo}}</td>

                                            @if ($company->activity_field)
                                                <td class="text-nowrap align-middle">{{$company->activity_field}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده اطلاعات</a>
                                                    {{-- <a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button">تایید </a> --}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$companies->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="companySearchUrl" value="{{ route('company.list.search') }}">
        <input type="hidden" id="showCompanySingleUrl" value="{{ route('company.single' , ':id' ) }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/search/js/company.js')}}"></script>
@endsection

{{-- js is fixed --}}
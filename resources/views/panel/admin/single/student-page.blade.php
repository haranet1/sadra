@extends('panel.admin.layout.master')
@section('title' , 'مشخصات دانشجو')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/single-student-page-in-style.css')}}">
@endsection
@section('main')


    <div class="mt-2 row">
        <div class="col-lg-12">
            @if (Auth::user()->groupable_type == App\Models\StudentInfo::class && count($unpaidSkills) > 0)
                <div class="mt-2">
                    <div class="bg-white rounded-2 border-warning">
                        <div class="alert alert-warning d-flex justify-content-between align-items-center">
                            <div class="d-flex align-items-center">
                                <span class=""><svg xmlns="http://www.w3.org/2000/svg" height="40" width="40" viewBox="0 0 24 24"><path fill="#fad383" d="M15.728,22H8.272a1.00014,1.00014,0,0,1-.707-.293l-5.272-5.272A1.00014,1.00014,0,0,1,2,15.728V8.272a1.00014,1.00014,0,0,1,.293-.707l5.272-5.272A1.00014,1.00014,0,0,1,8.272,2H15.728a1.00014,1.00014,0,0,1,.707.293l5.272,5.272A1.00014,1.00014,0,0,1,22,8.272V15.728a1.00014,1.00014,0,0,1-.293.707l-5.272,5.272A1.00014,1.00014,0,0,1,15.728,22Z"></path><circle cx="12" cy="16" r="1" fill="#f7b731"></circle><path fill="#f7b731" d="M12,13a1,1,0,0,1-1-1V8a1,1,0,0,1,2,0v4A1,1,0,0,1,12,13Z"></path></svg></span>
                                <h5 class="m-0 ms-2">شما {{ count($unpaidSkills) }} مهارت در انتظار پرداخت دارید. برای بررسی و تایید آن‌ها مبلغ <span class="c-ltr-text"> <strong>{{ 50000 * count($unpaidSkills) }}</strong> (50000 x {{ count($unpaidSkills) }})</span> تومان از طریق دکمه روبه‌رو پرداخت نمایید.</h5>
                            </div>
                            <a href="{{ route('student.pay.skill') }}" class="mt-1 mb-1 btn btn-warning me-5"><span></span> <span>پرداخت</span></a>
                        </div>
                    </div>
                </div>

                {{-- <div class="mt-2 alert alert-warning d-flex justify-content-between">
                    <h5>شما {{ count($unpaidSkills) }} عدد مهارت در انتظار پرداخت دارید. برای بررسی و تایید آن‌ها مبلغ {{ count($unpaidSkills) * 10000 }} تومان از طریق دکمه روبه‌رو پرداخت نمایید</h5>
                </div> --}}
            @endif
            <div class="mt-4 card">
                <div class="card-body">
                    <div class="mb-2 wideget-user">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    @if(Session::has('success'))
                                        <div class="mt-2 text-center alert alert-success">
                                            <h5>{{Session::pull('success')}}</h5>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="mt-2 text-center alert alert-danger">
                                            <h5>{{Session::pull('error')}}</h5>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="panel profile-cover">
                                        <div class="c-profile-cover__action bg-img"></div>
                                        <div class="profile-cover__img">
                                            <div class="profile-img-1">
                                                <img src="{{asset('assets/images/std-defult-logo/'.$img.'.png')}}" alt="img">
                                            </div>
                                            <div class="profile-img-content text-dark text-start">
                                                <div class="text-dark">
                                                    <h3 class="mb-2 h3">{{$student->user->name}} {{$student->user->family}}</h3>
                                                    <h5 class="text-muted">{{$student->university->name}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-profile">
                                            <a href="{{ route('create.skill') }}" class="mt-1 mb-1 btn btn-primary"><span>ثبت مهارت جدید</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="px-0 px-sm-4">
                                        <div class="mt-5 social social-profile-buttons float-end">
                                            <div class="mt-3">
                                                <a class="social-icon text-primary" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                                                <a class="social-icon text-primary" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                                                <a class="social-icon text-primary" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                                                <a class="social-icon text-primary" href="javascript:void(0)"><i class="fa fa-rss"></i></a>
                                                <a class="social-icon text-primary" href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                <a class="social-icon text-primary" href="https://myaccount.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if (Auth::user()->IsStudent())
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden card">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">تعداد شرکت ها</h6>
                                                <h2 class="mb-0 number-font">{{$companyCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="mt-1 chart-wrapper">
                                                    <canvas id="saleschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-secondary"><i class="fe fe-arrow-up-circle text-secondary"></i> 5%</span>
                                        هفته گذشته</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden card">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">تعداد رویداد ها</h6>
                                                <h2 class="mb-0 number-font">{{$eventCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="mt-1 chart-wrapper">
                                                    <canvas id="leadschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-pink"><i class="fe fe-arrow-down-circle text-pink"></i> 0.75%</span>
                                        شش روز اخیر</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden card">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">موقعیت های پویش</h6>
                                                <h2 class="mb-0 number-font">{{$internCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="mt-1 chart-wrapper">
                                                    <canvas id="profitchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-green"><i class="fe fe-arrow-up-circle text-green"></i> 0.9%</span>
                                        نه روز گذشته</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden card">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">درخواست های پویش</h6>
                                                <h2 class="mb-0 number-font fs-5">{{$requestCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="mt-1 chart-wrapper">
                                                    <canvas id="costchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-warning"><i class="fe fe-arrow-up-circle text-warning"></i> 0.6%</span>
                                        سال گذشته</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">

                @if ($student->skills->count() > 0)
                    <div class="col-xl-9">
                        <h3>مهارت های دانشجو </h3>
                        @foreach ($student->skills as $skill)
                            {{-- @php
                                $showSkill = false;
                                if(Auth::user()->groupable_type == StudentInfo::class || Auth::user()->groupable_type == EmployeeInfo::class){
                                    $showSkill = true;
                                }elseif (Auth::user()->groupable_type == ) {
                                    # code...
                                }
                            @endphp --}}
                            {{-- @if ($skill->status) --}}
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div class="d-flex justify-content-between">
                                                        <h4 class="">{{ $skill->title }}</h4>
                                                        <p class="p-1 text-white bg-info rounded-2">تاریخ اخذ مدرک : <span>{{ verta($skill->end_date)->formatJalaliDate() }}</span></p>
                                                </div>
                                                @if ($skill->academy)
                                                        <p class=""> محل آموزش : <strong>{{ $skill->academy->name }}</strong></p>
                                                @endif
                                                <p class="">ساعت گذرانده : <strong>{{ $skill->hours_passed }}</strong> ساعت</p>
                                                <p class="">
                                                        نوع مهارت : <strong>{{ $skill->type }}</strong>
                                                        @if ($skill->course)
                                                            با تعداد <strong>{{ $skill->course }}</strong> واحد
                                                        @endif
                                                </p>
                                                @if ($skill->description)

                                                    <p class="cjustify">
                                                        توضیحات : {{ $skill->description }}
                                                    </p>
                                                @endif

                                                @if (Auth::id() == $student->user->id)
                                                    <div class="mt-3 d-flex justify-content-between">
                                                        @php
                                                            echo get_student_skill_status($skill->status);
                                                        @endphp
                                                        @if ($skill->status == 0)
                                                            <div>
                                                                <a href="{{ route('edit.skill' , $skill->id) }}" class="btn btn-success">ویرایش</a>
                                                                <a href="javascript:void(0)" onclick="deleteSkill({{ $skill->id }})" class="btn btn-danger">حذف</a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-2 align-self-start">
                                                <a href="{{ asset($skill->photo->path) }}" target="_blank">
                                                    <img src="{{ asset($skill->photo->path) }}" style="max-height: 300px" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{-- @endif --}}
                        @endforeach
                    </div>
                @endif

                <div class="col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row justify-content-start">
                                @if ($student->province)
                                    <div class="card-title">استان : {{$student->province->name}}</div>
                                @endif
                                @if ($student->city)
                                    <div class="card-title">شهر : {{$student->city->name}}</div>
                                @endif
                                @if ($student->grade)
                                    <div class="card-title">مقطع تحصیلی : {{$student->grade}}</div>
                                @endif
                                @if ($student->major)
                                    <div class="card-title">رشته تحصیلی : {{$student->major}}</div>
                                @endif
                                @if ($student->end_date)
                                    <div class="card-title">تاریخ اخذ مدرک : {{verta($student->end_date)->format('Y/m/d')}}</div>
                                @endif
                                @if ($student->events)
                                    <div class="card-title">حضور در {{count($student->events)}} رویداد</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-9">
                    @if ($student->about)
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">درباره دانشجو</div>
                            </div>
                            <div class="card-body">
                                <p class="h3">
                                    @php
                                        echo $student->about;
                                    @endphp
                                </p>
                            </div>
                        </div>
                    @endif

                    {{-- @if (count($internPositions)>0)
                        <h2 class="mb-4 h2">موقیت های پویش</h2>
                        <div class="row">
                            @foreach ($internPositions as $internPosition)
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="text-white card-header bg-info">
                                            <div class="card-title">عنوان : {{$internPosition->title}} | رویداد : {{$internPosition->event->title}}</div>
                                        </div>
                                        <div class="card-body">
                                            <p class="">توضیحات : {{$internPosition->description}}</p>
                                        </div>
                                        @if (Auth::check() && Auth::user()->isStudent())
                                            <div class="card-footer">
                                                <form action="{{route('store.request.internship')}}" class="d-inline" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="internPosition" value="{{$internPosition->id}}">
                                                    <input type="hidden" name="company" value="{{$company->id}}">
                                                    <button type="submit" class="btn btn-success">درخواست پویش</button>
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="mb-5">
                                {{$internPositions->links('pagination.panel')}}
                            </div>
                        </div>
                    @endif --}}
                </div>
            </div>

            @if (Auth::user()->IsStudent())
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden text-white card bg-primary">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h4 class="">لیست شرکت ها</h6>
                                            </div>
                                            <div class="ms-auto">
                                                <img src="{{asset('assets/panel/images/icon/SVG/company-profile.svg')}}" alt="company">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden text-white card bg-info">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h4 class="">لیست رویداد ها</h4>
                                            </div>
                                            <div class="ms-auto">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden text-white card bg-success">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h4 class="">لیست موقعیت های پویش</h4>
                                            </div>
                                            <div class="ms-auto">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="overflow-hidden text-white card bg-green">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h4 class="">لیست درخواست های پویش</h4>
                                            </div>
                                            <div class="ms-auto">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <input type="hidden" id="deleteSkillUrl" value="{{ route('delete.skill') }}">
    <input type="hidden" id="CSRF" value="{{csrf_token()}}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/student-skill.js') }}"></script>
@endsection


@extends('panel.admin.layout.master')
@section('title' , 'مشخصات شرکت'.' '. $company->name)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/single-company-page-in-style.css')}}">
@endsection
@section('main')


    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="wideget-user mb-2">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="panel profile-cover">
                                        @if (isset($company->header))
                                            <div id="bg-single-company-img" class="bg-single-company-style c-profile-cover__action bg-img" style="background: url() no-repeat !important;"></div>
                                            <input id="bgCompanyImgUrl" type="hidden" value="{{asset($company->header->path)}}">
                                        @else
                                            <div class="c-profile-cover__action bg-img"></div>
                                        @endif
                                        <div class="profile-cover__img">
                                            @if ($company->has('logo'))
                                                <div class="profile-img-1">
                                                    <img class="img-single-company-style" src="{{asset($company->logo->path)}}" alt="img">
                                                </div>
                                            @else
                                                <div class="profile-img-1">
                                                    <img src="{{asset('assets/images/defult-logo/'.$img.'.png')}}" alt="img">
                                                </div>
                                            @endif
                                            <div class="profile-img-content text-dark text-start">
                                                <div class="text-dark">
                                                    <h3 class="h3 mb-2">{{$company->name}}</h3>
                                                    <h5 class="text-muted">{{$company->activity_field}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-profile">
                                            <button class="btn btn-primary mt-1 mb-1"> <i class="fa fa-rss"></i> <span>دنبال کنید</span></button>
                                            <button class="btn btn-secondary mt-1 mb-1"> <i class="fa fa-envelope"></i> <span>پیام</span></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="px-0 px-sm-4">
                                        <div class="social social-profile-buttons mt-5 float-end">
                                            <div class="mt-3">
                                                <a class="social-icon text-primary" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                                                <a class="social-icon text-primary" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                                                <a class="social-icon text-primary" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                                                <a class="social-icon text-primary" href="javascript:void(0)"><i class="fa fa-rss"></i></a>
                                                <a class="social-icon text-primary" href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                <a class="social-icon text-primary" href="https://myaccount.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (Auth::user()->IsCompany())
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="card overflow-hidden">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">تعداد دانشجویان</h6>
                                                <h2 class="mb-0 number-font">{{$studentCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="chart-wrapper mt-1">
                                                    <canvas id="saleschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-secondary"><i class="fe fe-arrow-up-circle  text-secondary"></i> 5%</span>
                                        هفته گذشته</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="card overflow-hidden">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">تعداد رویداد ها</h6>
                                                <h2 class="mb-0 number-font">{{$eventCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="chart-wrapper mt-1">
                                                    <canvas id="leadschart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-pink"><i class="fe fe-arrow-down-circle text-pink"></i> 0.75%</span>
                                        شش روز اخیر</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="card overflow-hidden">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">موقعیت های پویش</h6>
                                                <h2 class="mb-0 number-font">{{$internCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="chart-wrapper mt-1">
                                                    <canvas id="profitchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-green"><i class="fe fe-arrow-up-circle text-green"></i> 0.9%</span>
                                        نه روز گذشته</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                                <div class="card overflow-hidden">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mt-2">
                                                <h6 class="">درخواست های دریافتی</h6>
                                                <h2 class="mb-0 number-font fs-5">{{$requestCount}}</h2>
                                            </div>
                                            <div class="ms-auto">
                                                <div class="chart-wrapper mt-1">
                                                    <canvas id="costchart" class="h-8 w-9 chart-dropshadow"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-muted fs-12"><span class="text-warning"><i class="fe fe-arrow-up-circle text-warning"></i> 0.6%</span>
                                        سال گذشته</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xl-9">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">درباره شرکت</div>
                        </div>
                        <div class="card-body">
                            <p class="h3">
                                @php
                                    echo $company->about;
                                @endphp
                            </p>
                        </div>
                    </div>
                    @php
                        $positionExist = false;
                        $studenSentRequest = false;
                    @endphp
                    @if (count($internPositions)>0)
                        <div class="row">
                            @foreach ($internPositions as $internPosition)
                                @if (verta(now())->lessThan(verta($internPosition->event->end_at)))
                                    @php
                                        $positionExist = true;
                                        foreach ($internRequests as $req) {
                                            if($req->intern_position_id == $internPosition->id && $req->student_id == Auth::user()->groupable_id){
                                                $studenSentRequest = true;
                                            }
                                        }
                                    @endphp
                                    @if ($positionExist == true)
                                        <h2 class="h2 mb-4">موقیت های پویش</h2>
                                    @endif
                                    <div class="col-12 col-md-6">
                                        <div class="card">
                                            <div class="card-header bg-info text-white">
                                                <div class="card-title">عنوان : {{$internPosition->title}} | رویداد : {{$internPosition->event->title}}</div>
                                            </div>
                                            <div class="card-body">
                                                <p class="">توضیحات : {{$internPosition->description}}</p>
                                            </div>
                                            @if (Auth::check() && Auth::user()->isStudent())
                                                @if ($studenSentRequest == false)
                                                    <div class="card-footer">
                                                        <form action="{{route('store.request.internship')}}" class="d-inline" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="internPosition" value="{{$internPosition->id}}">
                                                            <input type="hidden" name="company" value="{{$company->id}}">
                                                            <button type="submit" class="btn btn-success">درخواست پویش</button>
                                                        </form>
                                                    </div>
                                                @else
                                                    <div class="card-footer">
                                                        <button disabled class="btn btn-dark">درخواست ارسال شده</button>
                                                    </div>
                                                @endif

                                            @endif
                                        </div>
                                    </div>
                                @else
                                    @php
                                        $positionExist = false;
                                    @endphp
                                @endif
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="mb-5">
                                {{$internPositions->links('pagination.panel')}}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row justify-content-start">
                                <div class="card-title">سال تاسیس : {{verta($company->year)->formatJalaliDate()}}</div>
                                @if ($company->province)
                                    <div class="card-title">استان : {{$company->province->name}}</div>
                                @endif
                                @if ($company->city)
                                    <div class="card-title">شهر : {{$company->city->name}}</div>
                                @endif
                                <div class="card-title">تلفن شرکت : {{$company->co_phone}}</div>
                                @if ($company->ceo)
                                    <div class="card-title">مدیر عامل : {{$company->ceo}}</div>
                                @endif
                                @if ($company->website)
                                    <div class="card-title">وبسایت : {{$company->website}}</div>
                                @endif
                                @if ($company->events)
                                    <div class="card-title">حضور در {{count($company->events)}} رویداد</div>
                                @endif
                                <div class="card-title">کد QR : {!! QrCode::size(100)->generate(route('company.single' , $company->id)) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/single/js/company.js') }}"></script>

@endsection

@php
    use App\Http\Controllers\Admin\UnitController as Index;
@endphp
@extends('panel.admin.layout.master')
@section('title' , 'داشبورد')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/panel/plugins/highcharts/css/style.css') }}">
@endsection
@section('main')

    <div class="page-header">
        <h1 class="page-title">داشبورد ( دانشگاه {{ Auth::user()->groupable->university->name }})</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحه اصلی</a></li>
                <li class="breadcrumb-item active" aria-current="page">داشبورد</li>
            </ol>
        </div>
    </div>

    @include('panel.dashboard.u-page-header')
    @include('panel.dashboard.u-events-chart')
    @include('panel.dashboard.u-treemap')
    @include('panel.dashboard.mixed-chart')
@endsection
@section('inputs')
    <input type="hidden" id="freeEvents" value="{{ Index::$freeEvents->count() }}">
    <input type="hidden" id="priceEvents" value="{{ Index::$priceEvents->count() }}">
    <input type="hidden" id="freeDoneEvents" value="{{ Index::$freeDoneEvents->count() }}">
    <input type="hidden" id="priceDoneEvents" value="{{ Index::$priceDoneEvents->count() }}">
    <input type="hidden" id="freeUnDoneEvents" value="{{ Index::$freeUnDoneEvents->count() }}">
    <input type="hidden" id="priceUnDoneEvents" value="{{ Index::$priceUnDoneEvents->count() }}">
    <input type="hidden" id="u_allCompanies" value="{{ Index::$allCompanies->count() }}">
    <input type="hidden" id="u_companyHasEvent" value="{{ Index::$companyHasEvent->count() }}">
    <input type="hidden" id="u_allStudents" value="{{ Index::$allStudents->count() }}">
    <input type="hidden" id="u_studentsHasEvent" value="{{ Index::$studentsHasEvent->count() }}">
    <input type="hidden" id="u_acceptInternRequests" value="{{ Index::$acceptInternRequests->count() }}">
    <input type="hidden" id="u_declinedInternRequests" value="{{ Index::$declinedInternRequests->count() }}">
    <input type="hidden" id="u_pendingInternRequests" value="{{ Index::$pendingInternRequests->count() }}">
    <input type="hidden" id="price_events" value="{{ json_encode($price_events) }}">
    <input type="hidden" id="free_events" value="{{ json_encode($free_events) }}">
    <input type="hidden" id="MixCompany" value="{{ json_encode(Index::$MixCompany) }}">
    <input type="hidden" id="MixEvent" value="{{ json_encode(Index::$MixEvent) }}">
    <input type="hidden" id="MixStudent" value="{{ json_encode(Index::$MixStudent) }}">

    <input type="hidden" id="currentDate" value="فروردین تا اسفند {{ verta()->year }}">
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/highcharts-more.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/exporting.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/export-data.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/accessibility.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/data.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/drilldown.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/variable-pie.js') }}"></script>
    <script src="{{ asset('assets/dashboard/js/unit.js') }}"></script>
@endsection
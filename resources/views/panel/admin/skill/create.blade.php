@extends('panel.admin.layout.master')

@section('title' , 'ثبت مهارت')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/panel/css/student-skill.css') }}">
@endsection

@section('main')
    
    <div class="page-header">
        <h1 class="page-title">ثبت مهارت </h1>
        <div>
            <ol class="breadcrumb">
                {{-- <li class="breadcrumb-item"><a href="javascript:void(0)">ویرایش اطلاعات</a></li> --}}
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center mt-5">
        <div class="col-xl-8 col-lg-12">
            <div class="card">

                <div class="card-body">

                    <form action="{{ route('store.skill') }}" class="form-horizontal row" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="col-12 col-md-6 px-0 mb-4">
                            <div class="mx-1">

                                <label for="title" class="form-lable">
                                    <span>عنوان دوره مهارتی</span>
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="title" type="text" class="form-control mb-4" id="title"
                                    @if (old('title'))
                                        value="{{ old('title') }}"
                                    @endif
                                    >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror

                                <label for="end_date" class="form-lable">
                                    <span>تاریخ اخذ مدرک</span>
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="end_date" type="text" class="form-control mb-4" id="end_date"
                                    @if (old('end_date'))
                                        value="{{ old('end_date') }}"
                                    @endif
                                    >
                                @error('end_date') <span class="text-danger">{{$message}}</span> @enderror

                                <label for="hours_passed" class="form-lable">
                                    <span>ساعت گذرانده</span>
                                    <span class="text-danger">*</span>
                                    <small class="text-info">(به صورت عددی وارد کنید)</small>
                                </label>
                                <input name="hours_passed" type="text" class="form-control mb-4" id="hours_passed"
                                    @if (old('hours_passed'))
                                        value="{{ old('hours_passed') }}"
                                    @endif
                                    >
                                @error('hours_passed') <span class="text-danger">{{$message}}</span> @enderror

                            </div>
                        </div>

                        <div class="col-12 col-md-6 px-0 mb-4">
                           <div class="mx-1">
                                <label for="academy" class="form-lable">
                                    <span>محل آموزش</span>
                                    <span class="text-danger">*</span>
                                </label>
                                {{-- <input name="academy" oninput="getAcademies(this)" type="text" class="form-control mb-4" id="academy"
                                    @if (old('academy'))
                                        value="{{ old('academy') }}"
                                    @endif
                                    >
                                <div id="academy-dropdown" class="dropdown-menu pb-3 mw-dropdown overflow-y-scroll" style="height: 60vh;"></div> --}}
                                <div class="dropdown w-100 mb-4">
                                    <button class="btn btn-light dropdown-toggle w-100" 
                                    data-bs-toggle="dropdown"
                                    type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
                                        انتخاب کنید
                                    </button>
                                    <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                                        <input type="text" class="form-control" oninput="getAcademies(this)" id="academySearch" placeholder="جستجو...">
                                        <div id="academy-options" class="dropdown-item-list"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="academy" id="selectedAcademy">
                                @error('academy') <span class="text-danger">{{$message}}</span> @enderror

                                <label for="type" class="form-lable">
                                    نوع مهارت
                                    <span class="text-danger">*</span>
                                </label>
                                <select name="type" id="type" class="form-control form-select mb-4">
                                    <option value="">انتخاب...</option>
                                    <option value="مهارت های تخصصی">مهارت های تخصصی</option>
                                    <option value="مهارت های شغلی عمومی">مهارت های شغلی عمومی</option>
                                    <option value="سابقه دستیار پژوهشی">سابقه دستیار پژوهشی</option>
                                    <option value="سابقه دستیار آزمایشگاه، کارآموزی، انجام امور کارگاهی، کارورزی">سابقه دستیار آزمایشگاه، کارآموزی، انجام امور کارگاهی، کارورزی</option>
                                    <option value="سابقه تدریس دروس دانشگاهی و دوره های تخصصی">سابقه تدریس دروس دانشگاهی و دوره های تخصصی</option>
                                    <option value="دروس عملی و آزمایشگاهی و کارگاهی هررشته">دروس عملی و آزمایشگاهی و کارگاهی هررشته</option>
                                </select>
                                @error('type') <span class="text-danger">{{$message}}</span> @enderror

                                {{-- <label for="course" class="form-lable">
                                    تعداد واحد درسی
                                    <small class="text-info">(درصورت انتخاب گزینه دروس علمی و آزمایشگاهی وکارگاهی)</small>
                                </label>
                                <input name="course" type="text" class="form-control mb-4" id="course"
                                    @if (old('course'))
                                        value="{{ old('course') }}"
                                    @endif
                                    >
                                @error('course') <span class="text-danger">{{$message}}</span> @enderror --}}

                                <label for="photo" class="form-lable">
                                    تصویر مدرک
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="photo" type="file" class="form-control mb-4" id="photo"
                                    @if (old('photo'))
                                        value="{{ old('photo') }}"
                                    @endif
                                    >
                                @error('photo') <span class="text-danger">{{$message}}</span> @enderror

                           </div>
                        </div>

                        <label for="photo" class="form-lable">توضیحات</label>

                        <textarea name="description" id="description" cols="30" rows="5" class="form-control mb-4">
                            @if (old('description'))
                                {{ old('description') }}
                            @endif
                        </textarea>
                        @error('description') <span class="text-danger">{{$message}}</span> @enderror

                        
                        <div class="row mb-4 justify-content-center">
                            <button class="col-3 btn btn-success" type="submit">
                                ثبت
                            </button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="getAcademySkillUrl" value="{{ route('get.academy.skill') }}">

@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/student-skill.js') }}"></script>
@endsection
@extends('panel.admin.layout.master')
@section('title','دانشجویان تایید نشده')
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست دانشجویان دارای مهارت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد کارکنان</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="{{ route('armed.search.skill') }}" method="GET">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6 col-md-3">
                                <input type="text" name="n_code" id="n_code" class="form-control mb-2" placeholder="کد ملی :"
                                @isset($oldData['n_code'])
                                    @if(! is_null($oldData['n_code']) && $oldData['n_code'] != '')
                                        value="{{ $oldData['n_code'] }}"
                                    @endif
                                @endisset
                                >

                                <select name="type" id="type" class="form-control form-select mb-4">
                                    @isset($oldData['type'])
                                        @if (! is_null($oldData['type']) && $oldData['type'] != '')
                                            <option value="{{ $oldData['type'] }}">{{ $oldData['type'] }}</option>
                                        @endif
                                    @endisset
                                    <option value="">نوع مهارت</option>
                                    <option value="مهارت های تخصصی">مهارت های تخصصی</option>
                                    <option value="مهارت های شغلی عمومی">مهارت های شغلی عمومی</option>
                                    <option value="سابقه دستیار پژوهشی">سابقه دستیار پژوهشی</option>
                                    <option value="سابقه دستیار آزمایشگاه، کارآموزی، انجام امور کارگاهی، کارورزی">سابقه دستیار آزمایشگاه، کارآموزی، انجام امور کارگاهی، کارورزی</option>
                                    <option value="سابقه تدریس دروس دانشگاهی و دوره های تخصصی">سابقه تدریس دروس دانشگاهی و دوره های تخصصی</option>
                                    <option value="دروس عملی و آزمایشگاهی و کارگاهی هررشته">دروس عملی و آزمایشگاهی و کارگاهی هررشته</option>
                                </select>
                            </div>
                            <div class="col-6 col-md-3">
                                <input type="text" name="mobile" id="mobile" class="form-control mb-2" placeholder="تلفن همراه :">

                                <select name="province" oninput="getCities()" id="province" class="form-control form-select">
                                    @isset($oldData['province'])
                                        @if (! is_null($oldData['province']) && $oldData['province'] != '')
                                            <option value="{{ $oldData['province'] }}">
                                                @foreach ($provinces as $province)
                                                    @if ($province->id == $oldData['province'])
                                                        {{ $province->name }}
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endif
                                    @endisset
                                    <option value="">انتخاب استان</option>
                                    @foreach ($provinces as $province)
                                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6 col-md-3">
                                <input type="text" name="start_date" id="start_date" class="form-control mb-2" placeholder="اخذ مدرک از :">

                                <select name="city" id="city" class="form-control form-select">
                                    @isset($oldData['city'])
                                        @if (! is_null($oldData['city']) && $oldData['city'] != '')
                                            <option value="{{ $oldData['city'] }}">{{ App\Models\City::find($oldData['city'])->name }}</option>
                                        @endif
                                    @endisset
                                    <option value="">انتخاب شهر</option>
                                </select>
                            </div>
                            <div class="col-6 col-md-3">
                                <input type="text" name="end_date" id="end_date" class="form-control mb-2" placeholder="اخذ مدرک تا :">
                                
                                <button type="submit" class="btn btn-info form-control" >جستجو</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-end">
                   
                    <div class="">
                        <a href="{{ route('export.armed-list.skill') }}" class="btn btn-success" >خروجی اکسل</a>

                        {{-- <select class="form-control select2 w-100">
                            <option value="asc">آخرین</option>
                            <option value="desc">قدیمی ترین</option>
                        </select> --}}
                    </div>
                    
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($studentSkills) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead class="bg-info">
                            <tr>
                                <th class="text-white text-center">نام و نام خانوادگی </th>
                                <th class="text-white text-center">تلفن</th>
                                <th class="text-white text-center">سن</th>
                                <th class="text-white text-center">دانشگاه</th>
                                <th class="text-white text-center">رشته</th>
                                <th class="text-white text-center">استان | شهر</th>
                                <th class="text-white text-center">تعداد مهارت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($studentSkills as $std)
                            <tr>
                                <td class="text-nowrap align-middle">
                                    <a href="{{ route('student.single' , $std->id) }}">
                                        {{$std->user->name." ".$std->user->family}}
                                    </a>
                                </td>
                                <td class="text-nowrap align-middle">{{$std->user->mobile}}</td>
                                <td class="text-nowrap align-middle">{{verta($std->user->birth)->formatJalaliDate()}}</td>
                                <td class="text-nowrap align-middle">{{$std->university->name}}</td>
                                <td class="text-nowrap align-middle">{{$std->major}}</td>
                                @if ($std->user->city)
                                    <td class="text-nowrap align-middle">{{ $std->user->province->name }} | {{$std->user->city->name}}</td>
                                @endif
                                <td class="text-center align-middle">
                                    @php
                                        $skillCount = 0;
                                        foreach ($std->skills as $skill) {
                                            if($skill->status == 2){
                                                $skillCount++;
                                            }
                                        }
                                    @endphp
                                    {{ $skillCount }}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$studentSkills->links('pagination.panel')}}
            </div>
        </div>
    </div>
    <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/student-skill.js') }}"></script>
    <script src="{{ asset('assets/auth/js/info.js') }}"></script>
@endsection

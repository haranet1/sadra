@extends('panel.admin.layout.master')
@section('title','رویدادها')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/result-in-style.css')}}">
@endsection

@section('main').

    <div class="row row-cards justify-content-center">
        <div class="col-lg-12 col-xl-6">
            <div class="input-group mb-5">
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                @if ($payment->receipt && $payment->tracking_code)
                    <div class="card-header d-flex justify-content-center">
                        <h3 class="text-success mb-0">{{$payment->status}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <div class="row justify-content-center div-result-in-style border border-info mb-3">
                                    <div class="col-6 text-center">
                                        <span class="item-span">هزینه :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->price}}</span>
                                        <span class="item-span">تومان</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center div-result-in-style border border-info mb-3" >
                                    <div class="col-6 text-center">
                                        <span class="item-span">تاریخ / زمان :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->payment_date_time}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border div-result-in-style border-info mb-3" >
                                    <div class="col-6 text-center">
                                        <span class="item-span">شماره پیگیری :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->tracking_code}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border div-result-in-style border-info mb-3">
                                    <div class="col-6 text-center">
                                        <span class="item-span">بانک صادر کننده :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->payer_bank}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border div-result-in-style border-info mb-3">
                                    <div class="col-12 text-center">
                                        <span class="item-span">{{ $payment->paymentable->quantity }} عدد مهارت پرداخت شده :</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border div-result-in-style border-info mb-3">
                                    @foreach ($payment->paymentable->skills as $skill)
                                        <div class="col-6 text-center">
                                            <span class="badge bg-info fs-14 m-1">{{ $skill->title }}</span>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row justify-content-center">
                                    <a href="{{route('student.single', $payment->user_info_id)}}" class="btn btn-success item-span">بازگشت به صفحه اصلی</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="mb-5">
                {{-- {{$events->links('pagination.panel')}} --}}
            </div>
        </div>
    </div>
@endsection

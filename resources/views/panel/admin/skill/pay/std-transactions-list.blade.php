@extends('panel.admin.layout.master')
@section('title','لیست تراکنش ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست تراکنش های دانشجو</h1>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="mb-5 input-group">
            </div>
            <div class="card">
                {{-- <div class="card-header border-bottom-0 justify-content-end">

                    <a href="{{ route('export.accepted.list.skill') }}" class="mx-1 btn btn-success" >خروجی اکسل</a>
                    <a href="{{ route('uni.list.skill') }}" class="mx-1 btn btn-info" >مهارت های تایید نشده</a>


                </div> --}}
                <div class="card-body">
                    <div class="px-5 pb-5 e-table">
                        <div class="table-responsive table-lg">
                            @if(count($studentSkills) > 0)
                            <table class="table mb-0 border-top table-bordered">
                                <thead class="bg-info">
                                <tr class="text-center">
                                    <th class="text-white">ردیف</th>
                                    <th class="text-white">قیمت </th>
                                    <th class="text-white">تاریخ پرداخت</th>
                                    <th class="text-white">رسید تراکنش</th>
                                    <th class="text-white">وضعیت پرداخت</th>
                                    <th class="text-white">عنوان مهارت</th>
                                    <th class="text-white">تصویر مدرک</th>
                                    {{-- <th class="text-white">محل آموزش | ساعت گذرانده</th>
                                    <th class="text-white">نوع مهارت</th> --}}
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $counter = (($studentSkills->currentPage() -1) * $studentSkills->perPage()) + 1;
                                @endphp
                                @foreach($studentSkills as $skill)
                                <tr>
                                    <td class="text-center align-middle text-nowrap">{{ $counter++ }}</td>
                                    <td class="align-middle text-nowrap">{{$skill->amount}} تومان</td>
                                    @if ($skill->payment)
                                        <td class="align-middle text-nowrap">{{verta($skill->payment->created_at)->formatJalaliDate()}}</td>
                                    @else
                                        <td class="align-middle text-nowrap">- - -</td>
                                    @endif
                                    @if ($skill->receipt)
                                        <td class="align-middle text-nowrap">{{ $skill->receipt }}</td>
                                    @else
                                        <td class="align-middle text-nowrap">- - -</td>
                                    @endif
                                    @if (! is_null($skill->payment) && ! is_null($skill->payment->status))
                                        <td class="align-middle text-nowrap text-info">{{ $skill->payment->status }}</td>
                                    @else
                                        <td class="align-middle text-nowrap">پرداخت با مشکل مواجه شده است.</td>
                                    @endif
                                    <td class="align-middle text-nowrap">
                                        <table class="table mb-0 table-bordered">
                                            <tbody>
                                                @if ($skill->skills)
                                                    @foreach ($skill->skills as $skli)
                                                        <tr>
                                                            <td class="align-middle text-nowrap">
                                                                {{ $skli->title }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="align-middle text-nowrap">
                                        <table class="table mb-0 table-bordered">
                                            <tbody>
                                                @if ($skill->skills)
                                                    @foreach ($skill->skills as $skl)
                                                        <tr>
                                                            <td class="align-middle text-nowrap">
                                                                @if ($skl->photo)
                                                                    <a href="{{ asset($skl->photo->path) }}" target="_blank">
                                                                        نمایش عکس
                                                                    </a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                                <div class="text-center alert alert-warning">
                                    اطلاعاتی جهت نمایش وجود ندارد
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$studentSkills->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="acceptSkillUrl" value="{{ route('uni.accept.skill') }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/student-skill.js')}}"></script>
@endsection

{{-- js is fixed --}}

@extends('panel.admin.layout.master')
@section('title','لیست مهارت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست مهارت های تایید نشده</h1>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            {{-- search todo --}}
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-end">

                    <a href="{{ route('export.un.list.skill') }}" class="mx-1 btn btn-success" >خروجی اکسل</a>
                    <a href="{{ route('accepted.uni.list.skill') }}" class="mx-1 btn btn-info" >مهارت های تایید شده</a>


                </div>
                <div class="px-5 pb-5 e-table">
                    <div class="table-responsive table-lg">
                        @if(count($studentSkills) > 0)
                        <table class="table mb-0 border-top table-bordered">
                            <thead class="bg-info">
                            <tr class="text-center">
                                <th class="text-white">عنوان مهارت</th>
                                <th class="text-white">نام و نام خانوادگی </th>
                                <th class="text-white">تلفن</th>
                                <th class="text-white">محل آموزش | ساعت گذرانده</th>
                                <th class="text-white">تاریخ اخذ مدرک</th>
                                <th class="text-white">نوع مهارت</th>
                                <th class="text-white">تصویر مدرک</th>
                                <th class="text-center text-white">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($studentSkills as $skill)
                            <tr>
                                <td class="align-middle text-nowrap">{{$skill->title}}</td>
                                <td class="align-middle text-nowrap">
                                    <a href="{{ route('student.single' , $skill->student->id) }}" class="text-black">
                                        {{$skill->student->user->name." ".$skill->student->user->family}}
                                    </a>
                                </td>
                                <td class="align-middle text-nowrap">{{$skill->student->user->mobile}}</td>
                                <td class="align-middle text-nowrap">{{$skill->academy->name ?? '- -'}} | {{$skill->hours_passed}} ساعت</td>
                                <td class="align-middle text-nowrap">{{verta($skill->end_date)->formatJalaliDate()}}</td>
                                <td class="align-middle text-nowrap">{{$skill->type}}</td>
                                @if ($skill->photo)
                                    <td class="align-middle text-nowrap">
                                        <a href="{{ asset($skill->photo->path) }}" target="_blank">
                                            <img src="{{ asset($skill->photo->path) }}" class="img-fluid rounded-circle limited-size" alt="">
                                        </a>
                                    </td>
                                @endif
                                <td class="text-center align-middle">
                                    <div class="align-top btn-group">
                                        @if ($skill->status == 1)
                                            <a href="javascript:void(0)" onclick="acceptSkill({{$skill->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="text-center alert alert-warning">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$studentSkills->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="acceptSkillUrl" value="{{ route('uni.accept.skill') }}">
        <input type="hidden" id="CSRF" value="{{csrf_token()}}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/student-skill.js')}}"></script>
@endsection

{{-- js is fixed --}}

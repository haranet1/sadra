@extends('panel.admin.layout.master')
@section('title','ثبت خبر')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ویرایش خبر</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-7 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش خبر {{$news->title}}</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                    action="{{route('update.news')}}">
                        @csrf
                        {{-- news --}}
                        <input type="hidden" name="news" value="{{$news->id}}">
                        {{-- title --}} 
                        <div class="row mb-4">
                            <label for="title" class="col-md-3 form-lable"><span class="text-danger">*</span> عنوان خبر : </label>
                            <div class="col-md-9">
                                <input type="text" name="title" class="form-control" id="title" value="{{$news->title}}">
                                @error('title') <span class="text-danger">{{$massage}}</span> @enderror
                            </div>
                        </div>
                        {{-- photo --}}
                        <div class="row mb-4">
                            <label for="photo" class="col-md-3 form-lable"><span class="text-danger">*</span> تصویر :</label>
                            <div class="col-md-9">
                                <input type="file" name="photo" class="form-control" id="photo">
                                @error('photo') <span class="text-danger">{{$massage}}</span> @enderror
                            </div>
                        </div>
                        {{-- photo --}}
                        <div class="row mb-4">
                            <label for="description" class="col-md-3 form-lable"><span class="text-danger">*</span> شرح خبر :</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5">{{$news->description}}</textarea>
                                @error('description') <span class="text-danger">{{$massage}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
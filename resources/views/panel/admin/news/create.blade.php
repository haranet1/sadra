@extends('panel.admin.layout.master')
@section('title','ثبت خبر')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ایجاد خبر جدید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-7 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    @if (Route::is('create.news'))
                        <h4 class="card-title text-primary">ایجاد خبر عمومی</h4>
                    @elseif (Route::is('create.news.event'))
                        <h4 class="card-title text-primary"> ایجاد خبر برای رویداد {{$event->title}}</h4>
                    @endif
                </div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                    action="@if(Route::is('create.news')){{ route('store.news') }}@elseif(Route::is('create.news.event',$event->id)){{ route('store.news.event',$event->id) }}@endif">
                        @csrf
                        {{-- event --}}
                        @if (Route::is('create.news.event'))
                            <input type="hidden" name="event" value="{{$event->id}}">
                        @endif
                        {{-- title --}} 
                        <div class="row mb-4">
                            <label for="title" class="col-md-3 form-lable"><span class="text-danger">*</span> عنوان خبر : </label>
                            <div class="col-md-9">
                                <input type="text" name="title" class="form-control" id="title">
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        {{-- photo --}}
                        <div class="row mb-4">
                            <label for="photo" class="col-md-3 form-lable"><span class="text-danger">*</span> تصویر :</label>
                            <div class="col-md-9">
                                <input type="file" name="photo" class="form-control" id="photo">
                                @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        {{-- photo --}}
                        <div class="row mb-4">
                            <label for="description" class="col-md-3 form-lable"><span class="text-danger">*</span> شرح خبر :</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5"></textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
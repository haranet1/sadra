@extends('panel.admin.layout.master')
@section('title' , 'لیست اخبار')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست اخبار</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12 px-0 px-md-2">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-between">
                    @if (Route::is('index.news'))
                        <h4 class="card-title text-primary">لیست اخبار عمومی</h4>
                        <div class="">
                            <a href="{{route('create.news')}}" class="btn btn-success">افزودن خبر</a>
                        </div>
                    @elseif (Route::is('index.news.event'))
                        <h4 class="card-title text-primary">لیست اخبار رویداد {{$event->title}}</h4>
                        <div class="">
                            <a href="{{route('create.news.event',$event->id)}}" class="btn btn-success">افزودن خبر</a>
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    @if(count($News) > 0)
                        <div class="row">
                            @foreach ($News as $news)
                                <div class="col-12 col-md-4">
                                    <div class="card shadow-lg">
                                        <div class="card-header br-te-3 br-ts-3 bg-dark">
                                            <h4 class="card-title text-white">عنوان : {{$news->title}}</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-6 px-1">
                                                    <img alt="photo" class="" src="{{asset($news->photo->path)}}">
                                                </div>
                                            </div>
                                            <div class="row mt-3 px-2">
                                                {{Str::limit($news->description,'100','...')}}
                                            </div>
                                            <div class="row mt-2 px-2 align-items-center mb-2">
                                                <span class="col-5 col-md-4">وضعیت : </span>
                                                <div class="col-7 col-md-8">
                                                    @if (is_null($news->status))
                                                        <span class="badge rounded-pill bg-primary">در انتظار تایید</span> 
                                                    @else
                                                        @if ($news->status == 1)
                                                            <span class="badge rounded-pill bg-success">تایید شده</span>
                                                        @else
                                                            <span class="badge rounded-pill bg-danger">رد شده</span>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row justify-content-center">
                                                    <a class="col-5 col-md-3 mb-1 btn btn-sm btn-primary mx-1" href="{{route('edit.news',$news->id)}}">ویرایش</a>
                                                    <a class="col-5 col-md-3 mb-1 btn btn-sm btn-danger delete mx-1" onclick="deleteNews({{$news->id}})" href="javascript:void(0)">حذف</a>
                                                    @if(!userIsUnitEmployee() && $news->status != 1)
                                                        <a class="col-5 col-md-3 mb-1 btn btn-sm btn-success mx-1" onclick="confirmeNews({{$news->id}})" href="javascript:void(0)">تایید</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-5">
                {{$News->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="deleteNewsUrl" value="{{ route('delete.news') }}"> 
        <input type="hidden" id="confirmeNewsUrl" value="{{ route('confirme.news') }}"> 
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/events/js/news.js')}}"></script>
@endsection

{{-- js is fixed --}}
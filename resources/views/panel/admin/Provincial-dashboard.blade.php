@php
    use App\Http\Controllers\Admin\ProvinceController as Index;
@endphp
@extends('panel.admin.layout.master')
@section('title' , 'داشبورد استانی')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/panel/plugins/highcharts/css/style.css') }}">

@endsection
@section('main')

    <div class="page-header">
        <h1 class="page-title">داشبورد استانی  ( دانشگاه {{ Auth::user()->groupable->university->name }})</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحه اصلی</a></li>
                <li class="breadcrumb-item active" aria-current="page">داشبورد</li>
            </ol>
        </div>
    </div>

    @include('panel.dashboard.p-page-header')
    @include('panel.dashboard.p-event-chart')
    @include('panel.dashboard.p-treemap')
    @include('panel.dashboard.mixed-chart')
@endsection
@section('inputs')
    <input type="hidden" id="freeEvents" value="{{ Index::$freeEvents->count() }}">
    <input type="hidden" id="priceEvents" value="{{ Index::$priceEvents->count() }}">
    <input type="hidden" id="freeDoneEvents" value="{{ Index::$freeDoneEvents->count() }}">
    <input type="hidden" id="priceDoneEvents" value="{{ Index::$priceDoneEvents->count() }}">
    <input type="hidden" id="freeUnDoneEvents" value="{{ Index::$freeUnDoneEvents->count() }}">
    <input type="hidden" id="priceUnDoneEvents" value="{{ Index::$priceUnDoneEvents->count() }}">
    <input type="hidden" id="p_allCompanies" value="{{ Index::$allCompanies->count() }}">
    <input type="hidden" id="p_companyHasEvent" value="{{ Index::$companyHasEvent->count() }}">
    <input type="hidden" id="p_allStudents" value="{{ Index::$allStudents->count() }}">
    <input type="hidden" id="p_studentsHasEvent" value="{{ Index::$studentsHasEvent->count() }}">
    <input type="hidden" id="p_acceptInternRequests" value="{{ Index::$acceptInternRequests->count() }}">
    <input type="hidden" id="p_declinedInternRequests" value="{{ Index::$declinedInternRequests->count() }}">
    <input type="hidden" id="p_pendingInternRequests" value="{{ Index::$pendingInternRequests->count() }}">
    <input type="hidden" id="provinces" value="{{ json_encode($c_price_events) }}">
    <input type="hidden" id="p_thisPriceDrilldown" value="{{ json_encode($thisPriceDrilldown) }}">
    <input type="hidden" id="c_free_events" value="{{ json_encode($c_free_events) }}">
    <input type="hidden" id="p_thisFreeDrilldown" value="{{ json_encode($thisFreeDrilldown) }}">
    <input type="hidden" id="MixCompany" value="{{ json_encode(Index::$MixCompany) }}">
    <input type="hidden" id="MixEvent" value="{{ json_encode(Index::$MixEvent) }}">
    <input type="hidden" id="MixStudent" value="{{ json_encode(Index::$MixStudent) }}">
    
    <input type="hidden" id="currentDate" value="فروردین تا اسفند {{ verta()->year }}">
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/highcharts-more.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/exporting.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/export-data.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/accessibility.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/data.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/drilldown.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/highcharts/js/variable-pie.js') }}"></script>
    <script src="{{ asset('assets/dashboard/js/province.js') }}"></script>
@endsection

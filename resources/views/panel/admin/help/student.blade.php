@extends('panel.admin.layout.master')

@section('title' , 'راهنما دانشجو')

@section('css')    
    <link rel="stylesheet" href="https://cdn.plyr.io/3.6.2/plyr.css" />
@endsection


@section('main')

    <div class="page-header">
        <h1 class="page-title">راهنما</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">راهنما</a></li>
                <li class="breadcrumb-item active" aria-current="page">دانشجو</li>
            </ol>
        </div>
    </div>    

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>راهنما کلی درباره روند کار در سامانه</h4>
                </div>
                <div class="card-body " style="height:400px">
                    {{-- video --}}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border btn-primary mb-3">
                                <h1 class="text-center" style="margin-top: 3px">1</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">ایجاد رویداد صدرا جدید</h4>
                                <div class="mt-5 justify-content-center">
                                    <video id="player" controls width="420" poster="{{ asset('img/events/17032533782.jpg') }}">
                                        <source src="https://sadra.iau.ir/dl/sadra-unit-event-process.mp4" type="video/mp4">
                                    </video>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-success mb-3">
                                {{-- <i class="fa fa-building-o fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">2</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">اضافه کردن پلن ثبت نام</h4>
                                <div class="mt-5 justify-content-center" >
                                    <video id="player1" controls width="420" poster="{{ asset('img/events/17032533782.jpg') }}"style="border-radius:20px">
                                        <source src="https://sadra.iau.ir/dl/sadra-unit-event-process.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-pink mb-3">
                                {{-- <i class="fa fa-file-word-o fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">3</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">اضافه کردن غرفه به پلن ثبت نام</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-danger mb-3">
                                {{-- <i class="fa fa-camera fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">4</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">در انتظار تایید</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-purple mb-3">
                                {{-- <i class="fa fa-pencil-square-o fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">5</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">ثبت نام شرکت ها و دانشجویان</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-warning mb-3">
                                {{-- <i class="fa fa-eercast fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">6</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">برگزاری رویداد</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-info mb-3">
                                {{-- <i class="fa fa-eercast fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">7</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">انتشار اخبار و گزارشات</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 feature">
                            <div class="fa-stack fa-lg fa-1x border bg-green mb-3">
                                {{-- <i class="fa fa-eercast fa-stack-1x text-white"></i> --}}
                                <h1 class="text-center" style="margin-top: 3px">8</h1>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mt-1">
                                <h4 class="fw-semibold">انتشار اخبار و گزارشات</h4>
                                <p class="mb-0">این یک واقعیت ثابت شده است که خواننده هنگام تماشای صفحه‌آرایش با محتوای قابل خواندن آن حواسش پرت می‌شود. نکته استفاده از لورم ایپسوم این است که در مقایسه با استفاده از «محتوا در اینجا، محتوا در اینجا»، توزیع کم و بیش عادی حروف دارد، و آن را شبیه به انگلیسی خواندنی می‌کند.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.plyr.io/3.6.2/plyr.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const player = new Plyr('#player');
            
        });
        document.addEventListener('DOMContentLoaded', function () {
            const player = new Plyr('#player1');
            
        });
    </script>
  
@endsection
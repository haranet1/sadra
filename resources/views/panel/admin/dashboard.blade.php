@php
  use App\Http\Controllers\Admin\AdminIndexController as Index;
  use App\Http\Controllers\Frontend\HomeController as Home;
@endphp
@extends('panel.admin.layout.master')
@section('title' , 'داشبورد کشور')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/panel/plugins/highcharts/css/style.css') }}">
@endsection
@section('main')


    <div class="page-header">
        <h1 class="page-title">داشبورد کشور</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحه اصلی</a></li>
                <li class="breadcrumb-item active" aria-current="page">داشبورد</li>
            </ol>
        </div>
    </div>

    @include('panel.dashboard.page-header')
    @include('panel.dashboard.events-chart')
    @include('panel.dashboard.treemap')
    @include('panel.dashboard.mixed-chart')
    @include('panel.dashboard.export-button')
    @include('panel.dashboard.all-events')

@endsection
@section('inputs')
    <input type="hidden" id="freeEvents" value="{{ Index::$freeEvents->count() }}">
    <input type="hidden" id="priceEvents" value="{{ Index::$priceEvents->count() }}">
    <input type="hidden" id="freeDoneEvents" value="{{ Index::$freeDoneEvents->count() }}">
    <input type="hidden" id="priceDoneEvents" value="{{ Index::$priceDoneEvents->count() }}">
    <input type="hidden" id="freeUnDoneEvents" value="{{ Index::$freeUnDoneEvents->count() }}">
    <input type="hidden" id="priceUnDoneEvents" value="{{ Index::$priceUnDoneEvents->count() }}">
    <input type="hidden" id="allCompanies" value="{{ Index::$allCompanies->count() }}">
    <input type="hidden" id="companyHasEvent" value="{{ Index::$companyHasEvent->count() }}">
    <input type="hidden" id="allStudents" value="{{ Index::$allStudents->count() }}">
    <input type="hidden" id="studentsHasEvent" value="{{ Index::$studentsHasEvent->count() }}">
    <input type="hidden" id="acceptInternRequests" value="{{ Index::$acceptInternRequests->count() }}">
    <input type="hidden" id="declinedInternRequests" value="{{ Index::$declinedInternRequests->count() }}">
    <input type="hidden" id="pendingInternRequests" value="{{ Index::$pendingInternRequests->count() }}">

    <input type="hidden" id="c_provincesEvents" value="{{ json_encode($c_provincesEvents) }}">
    <input type="hidden" id="price_drilldown" value="{{ json_encode($price_drilldown) }}">

    <input type="hidden" id="c_free_events" value="{{ json_encode($c_free_events) }}">
    <input type="hidden" id="free_drilldown" value="{{ json_encode($free_drilldown) }}">
    <input type="hidden" id="future_price_events" value="{{ json_encode($future_price_events) }}">
    <input type="hidden" id="future_free_events" value="{{ json_encode($future_free_events) }}">
    <input type="hidden" id="future_dilldown" value="{{ json_encode($future_dilldown) }}">
    <input type="hidden" id="c_MixCompany" value="{{ json_encode(Index::$c_MixCompany) }}">
    <input type="hidden" id="c_MixEvent" value="{{ json_encode(Index::$c_MixEvent) }}">
    <input type="hidden" id="c_MixStudent" value="{{ json_encode(Index::$c_MixStudent) }}">

    <input type="hidden" id="c_allEvents" value="{{ json_encode($c_allEvents) }}">
    <input type="hidden" id="allEventDrilldown" value="{{ json_encode($allEventDrilldown) }}">
    <input type="hidden" id="currentDate" value="فروردین تا اسفند {{ verta()->year }}">
@endsection
@section('script')
    <script src="{{asset('assets/panel/plugins/highcharts/js/highcharts.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/highcharts-more.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/exporting.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/export-data.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/accessibility.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/data.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/drilldown.js')}}"></script>
    <script src="{{asset('assets/panel/plugins/highcharts/js/variable-pie.js')}}"></script>
    <script src="{{ asset('assets/dashboard/js/country.js') }}"></script>
@endsection

@extends('panel.admin.layout.master')
@section('title','رزومه')
@section('main')
    <div class="page-header">
        <h1 class="page-title">رزومه دانشجو </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">رزومه دانشجو</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card"> 
                <div class="card-header justify-content-between">
                    <h3 class="card-title">اطلاعات شما</h3>
                    <a href="{{route('std.edit.info')}}" class="btn btn-primary">ویرایش اطلاعات</a>
                </div>
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="nav panel-tabs">
                                    <li><a href="#tab28" class="active" data-bs-toggle="tab"> شخصی</a></li>
                                    <li><a href="#tab25"  data-bs-toggle="tab">آکادمیک</a></li>
                                    <li><a href="#tab26" data-bs-toggle="tab">تماس</a></li>
                                    {{-- <li><a href="#tab27" data-bs-toggle="tab">مهارت ها</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab28">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td class="fw-bold">نام</td>
                                                <td>
                                                   {{\Illuminate\Support\Facades\Auth::user()->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">نام خانوادگی</td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->family}}

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">استان </td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->province->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">شهر </td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->city->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">سن  </td>
                                                <td>
                                                    {{verta(\Illuminate\Support\Facades\Auth::user()->birth)->formatDate()}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane " id="tab25">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td class="fw-bold">دانشگاه محل تحصیل</td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->groupable->university->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">مقطع</td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->groupable->grade}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">رشته </td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->groupable->major}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab26">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td class="fw-bold">تلفن</td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->mobile}}

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fw-bold">ایمیل</td>
                                                <td>
                                                    {{\Illuminate\Support\Facades\Auth::user()->email}}

                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>

                                {{-- <div class="tab-pane" id="tab27">
                                    <ol>
                                       @foreach(\Illuminate\Support\Facades\Auth::user()->skills as $skill)
                                           <li>{{$skill->name}}</li>
                                        @endforeach
                                    </ol>
                                </div> --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


@endsection

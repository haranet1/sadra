@extends('panel.admin.layout.master')
@section('title','ویرایش اطلاعات')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/student-edit-in-style.css')}}">

@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">رزومه دانشجو </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ویرایش اطلاعات</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between border-bottom-0">
                    <h2 class="card-title">اطلاعات شما</h2>
                </div>
                <div class="card-body pt-1">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab28">
                                    <div class="table-responsive">
                                        <form action="{{route('std.update.info')}}" method="POST">
                                            @csrf
                                            <table class="table table-bordered">
                                                    <tr>
                                                        <td class="fw-bold">نام</td>
                                                        <td>
                                                            <input type="text" name="name" class="form-control" value="{{$info->user->name}}">
                                                            @error('name')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">نام خانوادگی</td>
                                                        <td>
                                                            <input type="text" name="family" class="form-control" value="{{$info->user->family}}">
                                                            @error('family')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">استان </td>
                                                        <td>
                                                            <select name="province" class="form-control form-select select2" id="province" onchange="getCities()">
                                                                <option value="{{$info->user->province->id}}">{{$info->user->province->name}}</option>
                                                                @foreach ($provincies as $province)
                                                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('province')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">شهر </td>
                                                        <td>
                                                            <select id="city" name="city" class="student-edit-style form-control form-select select2" >
                                                                <option value="{{$info->user->city->id}}">{{$info->user->city->name}}</option>

                                                            </select>
                                                            @error('city')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">تاریخ تولد</td>
                                                        <td>
                                                            <input type="text" name="birth" id="birth" class="form-control" value="{{verta($info->user->birth)->formatDate()}}">
                                                            @error('birth')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    {{-- <tr>
                                                        <td class="fw-bold">دانشگاه محل تحصیل</td>
                                                        <td>
                                                            <select name="university" class="form-control form-select select2">
                                                                <option value="{{$info->university->id}}">{{$info->university->name}}</option>
                                                                @foreach ($universities as $university)
                                                                    <option value="{{$university->id}}">{{$university->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('university')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr> --}}
                                                    <tr>
                                                        <td class="fw-bold">مقطع</td>
                                                        <td>
                                                            <select name="grade" id="grade" class="student-edit-style form-control form-select select2">
                                                                <option selected value="{{$info->grade}}">{{$info->grade}}</option>
                                                                <option value="دکترا">دکترا</option>
                                                                <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                                                                <option value="کارشناسی">کارشناسی</option>
                                                                <option value="کاردانی">کاردانی</option>
                                                                <option value="دیپلم">دیپلم</option>
                                                            </select>
                                                            @error('grade')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">رشته </td>
                                                        <td>
                                                            <input type="text" name="major" class="form-control" value="{{$info->major}}">
                                                            @error('major')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">ایمیل</td>
                                                        <td>
                                                            <input type="text" name="email" class="form-control" value="{{$info->user->email}}">
                                                            @error('email')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                            </table>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-success">ثبت اطلاعات</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">

@endsection
@section('script')
    <script src="{{asset('assets/auth/js/info.js')}}"></script>
@endsection

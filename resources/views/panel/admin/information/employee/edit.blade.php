@extends('panel.admin.layout.master')
@section('title','ویرایش اطلاعات')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/create-users.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">ویرایش اطلاعات </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد کارمندان</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between border-bottom-0">
                    <h2 class="card-title">اطلاعات {{$info->user->name}} {{$info->user->family}}</h2>
                </div>
                <div class="card-body pt-1">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab28">
                                    <div class="table-responsive">
                                        <form action="{{route('emp.update.info')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="user" value="{{$info->id}}">
                                            <table class="table table-bordered">
                                                    <tr>
                                                        <td class="fw-bold">نام</td>
                                                        <td>
                                                            <input type="text" name="name" class="form-control" value="{{$info->user->name}}">
                                                            @error('name')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">نام خانوادگی</td>
                                                        <td>
                                                            <input type="text" name="family" class="form-control" value="{{$info->user->family}}">
                                                            @error('family')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <tr>
                                                        <td class="fw-bold">تلفن همراه</td>
                                                        <td>
                                                            <p class="text-warning">با تغییر تلفن همراه، کاربر باید با شماره جدید اقدام به ورود کند.</p>
                                                            <input type="text" name="mobile" class="form-control" value="{{$info->user->mobile}}">
                                                            @error('mobile')<small class="text-danger">{{$mobile}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">رمز عبور</td>
                                                        <td>
                                                            <p class="text-warning">فقط در صورتی که میخواهید رمز عبور را تغییر دهید، مقدار جدید را وارد نمایید.</p>
                                                            <input type="text" name="password" class="form-control">
                                                            @error('password')<small class="text-danger">{{$errors->first('password')}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">استان </td>
                                                        <td>
                                                            <select name="province" class="form-control form-select select2" id="province" onchange="getCities()">
                                                                @if ($info->user->province)
                                                                    <option value="{{$info->user->province->id}}">{{$info->user->province->name}}</option>
                                                                @else
                                                                    <option>انتخاب استان</option>
                                                                @endif
                                                                @foreach ($provincies as $province)
                                                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('province')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fw-bold">شهر </td>
                                                        <td>
                                                            <select class="select-employee-edit" id="city" name="city" class="form-control form-select select2" >
                                                                @if ($info->user->city)
                                                                    <option value="{{$info->user->city->id}}">{{$info->user->city->name}}</option>
                                                                @else
                                                                    <option>انتخاب شهر</option>
                                                                @endif
                                                                @if ($info->user->city)
                                                                    <option value="{{$info->user->city->id}}">{{$info->user->city->name}}</option>
                                                                @endif
                                                            </select>
                                                            @error('city')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="fw-bold">نام دانشگاه </td>
                                                        <td>
                                                            <select name="university" class="form-control form-select select2">
                                                                <option value="{{$info->university->id}}">{{$info->university->name}}</option>
                                                                @foreach ($universities as $university)
                                                                    <option value="{{$university->id}}">{{$university->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('university')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="fw-bold">ایمیل</td>
                                                        <td>
                                                            <input type="text" name="email" class="form-control" value="{{$info->user->email}}">
                                                            @error('email')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="fw-bold">نوع کاربری </td>
                                                        <td>
                                                            <ul class="js-product-variants">
                                                                <li class="ui-variant">
                                                                    <label class="ui-variant ui-variant--color">
                                                                        <span class="ui-variant-shape">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                        <input
                                                                        @if ($info->user->getRoleNames()[0] == 'unit')
                                                                            @checked(true)
                                                                        @endif
                                                                        type="radio" value="unit" name="role"class="variant-selector">
                                                                        <span class="ui-variant--check">دسترسی واحدی</span>
                                                                    </label>
                                                                </li>
                                                                <li class="ui-variant">
                                                                    <label class="ui-variant ui-variant--color">
                                                                        <span class="ui-variant-shape">
                                                                            <i class="fa fa-building"></i>
                                                                        </span>
                                                                        <input
                                                                        @if ($info->user->getRoleNames()[0] == 'provincial')
                                                                            @checked(true)
                                                                        @endif
                                                                        type="radio" value="provincial" name="role"class="variant-selector">
                                                                        <span class="ui-variant--check">دسترسی استانی</span>
                                                                    </label>
                                                                </li>
                                                                <li class="ui-variant">
                                                                    <label class="ui-variant ui-variant--color">
                                                                        <span class="ui-variant-shape">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                        <input
                                                                        @if ($info->user->getRoleNames()[0] == 'country')
                                                                            @checked(true)
                                                                        @endif
                                                                        type="radio" value="country" name="role"class="variant-selector">
                                                                        <span class="ui-variant--check">دسترسی کشوری</span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                            @error('role')<small class="text-danger">{{$message}}</small>@enderror
                                                        </td>
                                                    </tr>
                                            </table>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-success">ثبت اطلاعات</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">
    
@endsection
@section('script')
    <script src="{{asset('assets/auth/js/info.js')}}"></script>
@endsection

@extends('panel.admin.layout.master')
@section('title' , 'اطلاعات شرکت')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/company-edit-in-style.css')}}">

@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">ویرایش اطلاعات شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row justify-content-center p-3">
        <div class="col-md-8 col-xl-8 col-sm-6">
            <div class="card">
                <div class="p-3">
                    <form class="form-control" action="{{route('co.update.info')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <h4 class="mb-5 mt-3 fw-bold">درباره شرکت :</h4>
                        <textarea class="form-control" placeholder="" name="about" id="" cols="30"
                                  rows="5">@php echo $info->about @endphp</textarea>

                        <h4 class="mb-5 mt-3 fw-bold">اطلاعات بیشتر :</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="fw-bold">نام شرکت</td>
                                    <td>
                                        <input class="form-control" name="name" type="text" value="{{$info->name}}">
                                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold"> وب سایت</td>
                                    <td>
                                        <input class="form-control" name="website" type="text"
                                               value="{{$info->website}}">
                                        @error('website') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">شماره تلفن</td>
                                    <td>
                                        <input class="form-control" name="co_phone" type="text" value="{{$info->co_phone}}">
                                        @error('co_phone') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold"> ایمیل</td>
                                    <td>
                                        <input class="form-control" name="co_email" type="email" value="{{$info->co_email}}">
                                        @error('co_email') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">آدرس </td>
                                    <td>
                                        <input class="form-control" name="address" type="text" value="{{$info->address}}">
                                        @error('address') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">لوگو شرکت</td>
                                    <td>
                                        <div class="row align-items-center">
                                            @if ($info->logo)
                                                <div class="col-2">
                                                    <div class="border border-edit-company-in-style">
                                                        <img src="{{asset($info->logo->path)}}" alt="company-logo" >
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-10">
                                                <input type="file" name="logo" class="form-control">
                                                @error('logo') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">هدر شرکت (سربرگ)</td>
                                    <td>
                                        <div class="row align-items-center">
                                            @if ($info->header)
                                                <div class="col-2">
                                                    <div class="border border-edit-company-in-style" >
                                                        <img src="{{asset($info->header->path)}}" alt="company-logo">
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-10">
                                                <input type="file" name="header" class="form-control">
                                                @error('header') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <button type="submit" class="btn btn-success">ذخیره اطلاعات</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@extends('panel.admin.layout.master')
@section('title' , 'اطلاعات شرکت')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/company-show-in-style')}}">

@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">اطلاعات شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row justify-content-center p-3">
        <div class="col-md-8 col-xl-8 col-sm-6">
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                <div class="p-3">
                    <h4 class="mb-5 mt-3 fw-bold">درباره شرکت :</h4>
                    <p class="mb-3 fs-15">@php echo $info->about @endphp</p>

                    <h4 class="mb-5 mt-3 fw-bold">اطلاعات بیشتر :</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td class="fw-bold">نام شرکت</td>
                                <td> {{$info->name}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold"> وب سایت</td>
                                <td><a href="#">{{$info->website}}</a></td>
                            </tr>
                            @if ($info->logo)
                                <tr>
                                    <td class="fw-bold"> لوگو شرکت</td>
                                    <td><a href="{{asset($info->logo->path)}}" target="blank">
                                        <img class="img-show-in-style" src="{{asset($info->logo->path)}}" alt="logo">
                                    </a></td>
                                </tr>
                            @endif
                            <tr>
                                <td class="fw-bold">شماره تلفن </td>
                                <td>{{$info->co_phone}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold"> ایمیل</td>
                                <td> {{$info->co_email}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold">آدرس </td>
                                <td>
                                    {{$info->address}}
                                </td>
                            </tr>
                        </table>
                        <a class="btn btn-primary" href="{{route('co.edit.info')}}">ویرایش اطلاعات</a>


                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

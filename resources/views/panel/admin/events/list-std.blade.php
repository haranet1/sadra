@extends('panel.admin.layout.master')
@section('title' , 'ثبت نام در رویداد')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/list-std-in-style')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد دانشجو</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت نام در رویداد</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
                {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    @if(count($events) > 0)
                        <div class="row">
                            @foreach($events as $event)
                                @php
                                    $eventExist = false;
                                @endphp
                                @foreach ($studentEvent as $stdEvent)
                                    @if ($event->id == $stdEvent->event_id && ($stdEvent->status == 1 || is_null($stdEvent->status)))
                                        @php
                                            $eventExist = true;
                                        @endphp
                                        @break
                                    @endif
                                @endforeach
                                <div class="col-md-6 col-xl-4 col-sm-6">
                                    <div class="card shadow-lg">
                                        <div class="product-grid6">
                                            <div class="product-image6 p-5 ">
                                                @if ($event->photo)
                                                    <a target="_blank" href="{{url($event->photo->path)}}">
                                                        <img class="img-fluid img-list-std-style"
                                                            src="{{asset($event->photo->path)}}"
                                                            alt="photo">
                                                    </a>
                                                @else
                                                    <img class="img-list-std-style" src="{{asset('assets/images/default-image.svg')}}" alt="img" >
                                                @endif
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="product-content text-center">
                                                    <h1 class="title fw-bold fs-20"><a href="#">{{$event->title}}</a></h1>
                                                    {{Str::limit($event->description,'50','...')}}

                                                    <div class="mb-2 text-warning">
                                                        @if($event->start_register_at)
                                                        <h6 class="text-nowrap my-1" dir="ltr">شروع ثبت نام از
                                                            تاریخ: {{verta($event->start_register_at)->format('Y/m/d')}}
                                                            تا {{verta($event->end_register_at)->format('Y/m/d')}}</h6>
                                                        @endif

                                                    </div>
                                                    <div class="price">
                                                        <h6 class="text-nowrap" dir="ltr">از
                                                            تاریخ: {{verta($event->start_at)->format('Y/m/d')}}
                                                            تا {{verta($event->end_at)->format('Y/m/d')}}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer text-center">
                                                @if ($event->university->id == 35)
                                                    <a href="https://jobist.ir/register" class="btn btn-lg btn-success">ثبت نام</a>
                                                @else
                                                    @if ($eventExist == true)
                                                        <span class="btn btn-lg btn-dark disabled">ثبت نام شده</span>
                                                    @elseif (verta(now())->lessThan(verta($event->start_register_at)))
                                                        <span class="btn btn-lg btn-dark disabled">شروع ثبت نام {{verta($event->start_register_at)->diffDays()}} روز دیگر</span>
                                                    @elseif(verta(now())->lessThan(verta($event->end_register_at)))
                                                        <form action="{{route('store-std.register.event')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="event" value="{{$event->id}}">
                                                            <button type="submit" class="btn btn-lg btn-success">ثبت نام</button>
                                                        </form>
                                                    @else
                                                        <a class="btn btn-lg disabled btn-warning"
                                                        href="javascript:void(0)">
                                                            اتمام مهلت ثبت نام</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning text-center">
                            اطلاعاتی جهت نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

            </div>
            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
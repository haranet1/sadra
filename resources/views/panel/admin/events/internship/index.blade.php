@extends('panel.admin.layout.master')
@section('title' , 'لیست موقعیت های پویش')
@section('css')
    <style>
        @media screen and (max-width:500px){
            .c-font{
                font-size: 1rem !important;
            }
        }
    </style>
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست موقعیت های پویش</a></li>
            </ol>
        </div>
    </div>
    @if (Route::is('index.internship.event'))
        <div class="row mb-3 px-4">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
        </div>
        <div class="row row-cards">
            <div class="col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header border-bottom-0">
                        <div class="w-100 d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 c-font">موقعیت های پویش در رویداد {{$event->title}}</h4>
                            @if (Auth::user()->IsCompany())
                                <div class="">
                                    <button id="myInput" type="button" class="btn btn-success me-3 mt-2" 
                                    data-bs-toggle="modal" data-bs-target="#input-modal" data-bs-whatever="@ mdo">افزودن موقعیت پویش</button>
                                </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="modal fade" id="input-modal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">افزودن موقعیت پویش</h6>
                                    <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <form action="{{route('store.internship.event')}}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="event" value="{{$event->id}}">
                                        <div class="mb-3">
                                            <label for="recipient-name" class="col-form-label">عنوان موقعیت پویش :</label>
                                            <input name="title" type="text" class="form-control" id="recipient-name">
                                        </div>
                                        @error('title') <small class="text-danger">{{$message}}</small> @enderror
                                        <div class="mb-3">
                                            <label for="message-text" class="col-form-label">توضیحات :</label>
                                            <textarea name="description" class="form-control" id="message-text"></textarea>
                                        </div>
                                        @error('description') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn ripple btn-success" type="submit">ذخیره تغییرات</button>
                                        <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if(count($internPositions)>0)
                            <div class="row">
                                @foreach ($internPositions as $internPosition)
                                    <div class="col-12 col-md-6">
                                        <div class="card shadow-lg">
                                            <div class="card-header bg-info text-white">
                                                <div class="card-title">عنوان : {{$internPosition->title}}</div>
                                            </div>
                                            <div class="card-body">
                                                <p class="">توضیحات : {{$internPosition->description}}</p>
                                                <button type="button" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$internPosition->id}}">ویرایش</button>
                                                <form class="d-inline" action="{{route('delete.internship.event')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="internPosition" value="{{$internPosition->id}}">
                                                    <button type="submit" class="btn btn-danger mt-2">حذف</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="edit-modal-{{$internPosition->id}}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title">افزودن موقعیت پویش</h6>
                                                    <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form action="{{route('update.internship.event')}}" method="POST">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <input type="hidden" name="internPosition" value="{{$internPosition->id}}">
                                                        <div class="mb-3">
                                                            <label for="recipient-name" class="col-form-label">عنوان موقعیت پویش :</label>
                                                            <input name="title" value="{{$internPosition->title}}" type="text" class="form-control" id="recipient-name">
                                                        </div>
                                                        @error('title') <small id="titleErr" class="text-danger">{{$message}}</small> @enderror
                                                        <div class="mb-3">
                                                            <label for="message-text" class="col-form-label">توضیحات :</label>
                                                            <textarea name="description" value="{{$internPosition->description}}" placeholder="{{$internPosition->description}}" class="form-control" id="message-text"></textarea>
                                                        </div>
                                                        @error('description') <small id="descriptionErr" class="text-danger">{{$message}}</small> @enderror
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn ripple btn-success" type="submit">ذخیره تغییرات</button>
                                                        <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="mb-5">
                    {{$internPositions->links('pagination.panel')}}
                </div>
            </div>

        </div>
    @endif
    @if (Route::is('index.internship'))
        <div class="row mb-3 px-4">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
        </div>
        <div class="row row-cards">
            <div class="col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-body">
                        @if(count($internPositions)>0)
                            <div class="row">
                                @foreach ($internPositions as $internPosition)
                                    <div class="col-12 col-md-6">
                                        <div class="card shadow-lg">
                                            <div class="card-header bg-info text-white justify-content-between">
                                                <div class="card-title">عنوان : {{$internPosition->title}}</div>
                                                <div class="card-title">رویداد : {{$internPosition->event->title}}</div>
                                            </div>
                                            <div class="card-body">
                                                <p class="">توضیحات : {{$internPosition->description}}</p>
                                            </div>
                                            @if (count($internPosition->internRequests)>0)
                                                <div class="card-footer">
                                                    <p class="">تعداد درخواست های پویش : {{count($internPosition->internRequests)}}</p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="mb-5">
                    {{$internPositions->links('pagination.panel')}}
                </div>
            </div>

        </div>
    @endif
@endsection
@section('script')
    <script src="{{ asset('assets/events/js/internship.js') }}"></script>
@endsection

{{-- js is fixed --}}
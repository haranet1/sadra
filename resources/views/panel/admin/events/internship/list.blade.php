{{-- this page for emplyee --}}
@extends('panel.admin.layout.master')
@section('title' , 'لیست موقعیت های پویش')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست موقعیت های پویش</a></li>
            </ol>
        </div>
    </div>

    <div class="row mb-3 px-4">
        @if(Session::has('success'))
            <div class="alert alert-success mt-2 text-center">
                <h5>{{Session::pull('success')}}</h5>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger mt-2 text-center">
                <h5>{{Session::pull('error')}}</h5>
            </div>
        @endif
    </div>
    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="mx-1">
                        <a href="{{route('intern-positions.p-uni.export')}}" class="btn btn-primary">خروجی اکسل</a>
                    </div>
                </div>
                <div class="card-body">
                    @if(count($internPositions)>0)
                        <div class="row">
                            @foreach ($internPositions as $internPosition)
                                <div class="col-12 col-md-6">
                                    <div class="card shadow-lg">
                                        <div class="card-header bg-info text-white d-flex justify-content-between">
                                            <div class="card-title mx-1">عنوان : {{$internPosition->title}}  </div>  
                                            <div class="card-title mx-1">رویداد {{$internPosition->event->title}}  </div>
                                            @if (is_null($internPosition->status))
                                                <a href="{{route('confirme.intern.position' , $internPosition->id )}}" class="btn btn-success">تایید</a>
                                            @else
                                                @if ($internPosition->status == 1)
                                                    <button type="submit" class="btn btn-dark" disabled>تایید شده</button>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p class="badge bg-light h5">شرکت {{$internPosition->companyInfo->name}}</p>
                                            <p class="">توضیحات : {{$internPosition->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning text-center">
                            اطلاعاتی جهت نمایش وجود ندارد.
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-5">
                {{$internPositions->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
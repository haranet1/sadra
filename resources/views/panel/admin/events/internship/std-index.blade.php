@extends('panel.admin.layout.master')
@section('title' , 'لیست موقعیت های پویش')
@section('css')
    <style>
        @media screen and (max-width:500px){
            .c-font{
                font-size: 1rem !important;
            }
        }
    </style>
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست موقعیت های پویش</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد دانشجو</a></li>
                <li class="breadcrumb-item active" aria-current="page">لیست موقعیت های پویش</li>
            </ol>
        </div>
    </div>

    <div class="row mb-3 px-4">
        @if(Session::has('success'))
            <div class="alert alert-success mt-2 text-center">
                <h5>{{Session::pull('success')}}</h5>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger mt-2 text-center">
                <h5>{{Session::pull('error')}}</h5>
            </div>
        @endif
    </div>
    @if (Route::is('std.index.internship.event'))
        <div class="row row-cards">
            <div class="col-12">
                <div class="input-group w-25 mb-5">

                </div>
                <div class="card">
                    <div class="card-header border-bottom-0 justify-content-between">
                        <h4 class="mb-0 c-font">موقعیت های پویش در رویداد {{$event->title}}</h4>
                    </div>
                    <div class="card-body">
                        @if (count($internPositions)>0)
                            <div class="row">
                                @foreach ($internPositions as $internPosition)
                                    @php
                                        $positionExist = false;
                                    @endphp
                                    @foreach ($internRequests as $request)
                                        @if ($internPosition->id == $request->intern_position_id && $request->student_id == Auth::user()->groupable_id)
                                            @php
                                                $positionExist = true;
                                            @endphp
                                        @endif
                                    @endforeach
                                    <div class="col-12 col-md-6">
                                        <div class="card shadow-lg">
                                            <div class="card-header bg-info text-white justify-content-between">
                                                <div class="card-title">عنوان : {{$internPosition->title}}</div>
                                                <div class="card-title">شرکت : {{$internPosition->companyInfo->name}}</div>
                                            </div>
                                            <div class="card-body">
                                                <p class="">توضیحات : {{$internPosition->description}}</p>
                                                @if ($positionExist == true)
                                                    <div class="d-flex justify-content-center">
                                                        <span class="btn btn-dark disabled">درخواست پویش ثبت شده</span>
                                                    </div>
                                                @else
                                                    <form class="d-inline" action="{{route('store.request.internship')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="internPosition" value="{{$internPosition->id}}">
                                                        <input type="hidden" name="company" value="{{$internPosition->company_id}}">
                                                        <div class="d-flex justify-content-center">
                                                            <button type="submit" class="btn btn-success align-center">درخواست</button>
                                                        </div>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="mb-5">
                    {{$internPositions->links('pagination.panel')}}
                </div>
            </div>
        </div>
    @endif
@endsection
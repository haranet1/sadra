@extends('panel.admin.layout.master')
@section('title' , 'ثبت رویداد')
@section('css')

@endsection
@section('main')
<div class="page-header">
    <h1 class="page-title">ثبت رویداد</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف رویداد جدید</a></li>
        </ol>
    </div>
</div>

<div class="row row-cards justify-content-center">
    <div class="col-xl-8 col-lg-12">
        <div class="card">
            <div class="card-header text-center justify-content-center">
                <h4 class="card-title text-primary">ثبت رویداد صدرا</h4>
            </div>

            <div class="card-body">
                <form class="form-horizontal" action="{{route('store.event')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class=" row mb-4">
                        <label class="col-md-3 form-label"><span class="text-danger">*</span> عنوان</label>
                        <div class="col-md-9">
                            <input name="title" type="text" class="form-control" >
                            @error('title') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class=" row mb-4">
                        <label class="col-md-3 form-label">برای شرکت در رویداد</label>
                        <div class="col-md-9">
                            <select name="type" id="type" class="form-control form-select select2">
                                <option value="price">از شرکت ها هزینه گرفته می‌شود</option>
                                <option value="free">از شرکت ها هزینه گرفته نمی‌شود</option>
                            </select>
                            @error('type') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> پوستر عمودی</label>
                        <div class="col-md-9">
                            <input name="photo" type="file" class="form-control" >
                            @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                            <div class="alert alert-warning" role="alert">
                                بنر افقی در اسلایدر صفحه اصلی سایت نمایش داده خواهد شد. <br>
                                * حداکثر حجم قابل قبول 1 مگابایت است. <br>
                                * پوستر عمودی را حتما در سایز کاغذ A5 <small>(عرض 1748 پیکسل و ارتفاع 2480 پیکسل)</small> بارگذاری نمایید.
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email">بنر عمودی</label>
                        <div class="col-md-9">
                            <input name="vertical_banner" type="file" class="form-control" >
                            @error('vertical_banner') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div> --}}
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> بنر افقی</label>
                        <div class="col-md-9">
                            <input name="horizontal_banner" type="file" class="form-control" >
                            @error('horizontal_banner') <span class="text-danger">{{$message}}</span> @enderror
                            <div class="alert alert-warning" role="alert">
                                بنر افقی در اسلایدر صفحه اصلی سایت نمایش داده خواهد شد. <br>
                                * حداکثر حجم قابل قبول 1 مگابایت است. <br>
                                * حتما بنر افقی را با ابعاد 1200 پیکسل (عرض) و 450 پیکسل (ارتفاع) بارگذاری نمایید.
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> نقشه غرفه ها</label>
                        <div class="col-md-9">
                            <input name="map" type="file" class="form-control" >
                            @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            <div class="alert alert-warning" role="alert">
                                * حداکثر حجم قابل قبول 2 مگابایت است.
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> تاریخ شروع رویداد</label>
                        <div class="col-md-9">
                            <input name="start" type="text" id="start" class="form-control" >
                            @error('start') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> تاریخ پایان رویداد</label>
                        <div class="col-md-9">
                            <input name="end" type="text" id="end" class="form-control" >
                            @error('end') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> تاریخ شروع ثبت نام</label>
                        <div class="col-md-9">
                            <input name="start_register" type="text" id="start_register" class="form-control" >
                            @error('start_register') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> تاریخ پایان ثبت نام</label>
                        <div class="col-md-9">
                            <input name="end_register" type="text" id="end_register" class="form-control" >
                            @error('end_register') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> توضیحات</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="description" id="editor" cols="30" rows="5"></textarea>
                            @error('description') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-md-3 form-label" for="example-email"><span class="text-danger">*</span> متن دعوتنامه</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="invitation" id="editor1" cols="30" rows="5"></textarea>
                            @error('invitation') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    

                    <div class=" row mb-0 justify-content-center ">
                        <div class="col justify-content-center text-center ">
                           <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>
@endsection

@section('script')
    <script src="{{ asset('assets/panel/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor/translations/fa.js') }}"></script>    
    <script src="{{ asset('assets/events/js/event.js') }}"></script>
@endsection

{{-- js is fixed --}}
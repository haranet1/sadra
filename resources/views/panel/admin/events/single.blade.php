@extends('panel.admin.layout.master')
@section('title' , 'رویداد'.' '.$event->title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/single-in-style.css')}}">

@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست رویداد ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12 px-0 px-md-2">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-xl-5 col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="product-carousel img-list-std-style">
                                        <div id="Slider" class="carousel slide border" data-bs-ride="false">
                                            <div class="carousel-inner">
                                                <div class="carousel-item pt-2 active">
                                                    <h4 class="mt-4">
                                                        پوستر
                                                    </h4>
                                                    <img src="{{asset($event->photo->path)}}" alt="img" class="img-fluid mx-auto d-block">

                                                </div>
                                                <div class="carousel-item pt-2">
                                                    <h4 class="mt-4">
                                                        بنر افقی
                                                    </h4>
                                                    <img src="{{asset($event->horizontalBanner->path)}}" alt="img" class="img-fluid mx-auto d-block">
                                                </div>
                                                <div class="carousel-item pt-2">
                                                    <h4 class="mt-4">
                                                        نقشه رویداد
                                                    </h4>
                                                    <img src="{{asset($event->map->path)}}" alt="img" class="img-fluid mx-auto d-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix carousel-slider">
                                        <div id="thumbcarousel" class="carousel slide" data-bs-interval="t">
                                            <div class="carousel-inner">
                                                <ul class="carousel-item active">
                                                    <li data-bs-target="#Slider" data-bs-slide-to="0" class="thumb active m-2"><img src="{{asset($event->photo->path)}}" alt="img"></li>
                                                    <li data-bs-target="#Slider" data-bs-slide-to="1" class="thumb m-2"><img src="{{asset($event->horizontalBanner->path)}}" alt="img"></li>
                                                    <li data-bs-target="#Slider" data-bs-slide-to="2" class="thumb m-2"><img src="{{asset($event->map->path)}}" alt="img"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details col-xl-7 col-lg-12 col-md-12 mt-4 mt-xl-0">
                            <div class="mt-4 mb-4">
                                <h3 class="mb-5 fw-semibold">رویداد {{$event->title}}</h3>
                                <div class="border border-info mt-2 align-items-center">
                                    <div class="row justify-content-center">
                                        <span class="col-7 col-md-4">شروع و پایان رویداد : </span>
                                        <span class="col-5 col-md-3">{{verta($event->start_at)->formatDate()}}</span>
                                        <span class="col-7 col-md-2">لغایت</span>
                                        <span class="col-5 col-md-3" dir="ltr">{{verta($event->end_at)->formatDate()}}</span>
                                    </div>
                                </div>
                                <div class="border border-warning mt-2 align-items-center">
                                    <div class="row justify-content-center">
                                        <span class="col-7 col-md-4">شروع و پایان ثبت نام : </span>
                                        @if(!is_null($event->start_register_at))
                                            <span class="col-5 col-md-3">{{verta($event->start_register_at)->formatDate()}}</span>
                                            <span class="col-7 col-md-2">لغایت</span>
                                            <span class="col-5 col-md-3" dir="ltr">{{verta($event->end_register_at)->formatDate()}}</span>
                                        @endif
                                    </div>
                                </div>
                                <h4 class="mt-4"><b> توضیحات</b></h4>
                                @php
                                    echo $event->description
                                @endphp
                                <div class="row mt-4 px-2 align-items-center">
                                    <span class="col-5 col-md-2">وضعیت : </span>
                                    <div class="col-7 col-md-10">
                                        @if (is_null($event->status))
                                            @if (count($event->plans) == 0)
                                                <span class="badge rounded-pill bg-primary">پلن مورد نیازتان را اضافه کنید</span>
                                            @else
                                                <span class="badge rounded-pill bg-primary">در انتظار تایید</span>
                                            @endif

                                        @else
                                            @if ($event->status == 1)
                                                <span class="badge rounded-pill bg-success">تایید شده</span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">رد شده</span>
                                            @endif
                                        @endif
                                        @if ($event->is_free == true)
                                            <span class="badge rounded-pill bg-warning text-dark">رایگان</span>
                                        @else
                                            <span class="badge rounded-pill bg-success">غیر رایگان</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row justify-content-center mt-5 align-items-end">
                                        @if ($event->held == false || verta(now())->lessThan(verta($event->end_register_at)->addWeek(1)) || $event->has_five_news == false)
                                            <a class="col-5 col-md-3 mb-1 btn btn-sm btn-primary mx-1" href="{{route('edit.event',$event->id)}}">ویرایش</a>
                                        @endif
                                        <a class="col-5 col-md-3 mb-1 btn btn-sm btn-danger delete mx-1" onclick="deleteEvent({{$event->id}})" href="javascript:void(0)">حذف</a>
                                        @if(!userIsUnitEmployee() && $event->status != 1 && count($event->plans) > 0)
                                            <a class="col-5 col-md-3 mb-1 btn btn-sm btn-success mx-1" onclick="confirmeEvent({{$event->id}})" href="javascript:void(0)">تایید</a>
                                        @endif
                                        @if ($event->status == 1)
                                            <a class="col-5 col-md-3 mb-1 btn btn-sm btn-green mx-1" href="{{route('index.news.event',$event->id)}}">اخبار</a>
                                        @endif
                                        <a class="col-5 col-md-3 mb-1 btn btn-sm btn-warning mx-1"  href="{{route('list.index.plan',$event->id)}}">پلن های ثبت نام</a>
                                        <a class="col-5 col-md-3 mb-1 btn btn-sm btn-info mx-1" href="{{route('show.register.event',$event->id)}}">شرکت ها</a>
                                        {{-- <a class="btn btn-sm btn-secondary mx-1" href="{{route('done.register.event',$event->id)}}">ثبت نام های نهایی</a> --}}
                                        <a class="col-5 col-md-3 mb-1 btn btn-sm btn-secondary mx-1" href="{{route('show-students.register.event',$event->id)}}">دانشجویان</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="deletEventUrl" value="{{ route('delete.event') }}">
    <input type="hidden" id="confirmeEventUrl" value="{{ route('confirme.event') }}">
@endsection
@section('script')
    <script src="{{asset('assets/events/js/event.js')}}"></script>
@endsection

{{-- js is fixedd --}}

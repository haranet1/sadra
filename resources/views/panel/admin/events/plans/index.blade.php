@extends('panel.admin.layout.master')
@section('title' , 'لیست پنل های ثبت نام')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست پلن ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="input-group mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-between">
                    @if (Route::is('list.index.plan'))
                        <div class="">
                            <h4>پلن های رویداد {{$event->title}}</h4>
                        </div>
                        <div class="">
                            @if (Auth::user()->groupable_id == $event->creator_id)
                                <a href="{{route('create.plan',$event->id)}}" class="btn btn-success">افزودن پلن ثبت نام</a>
                            @endif
                            <a href="{{route('index.event')}}" class="btn btn-primary">لیست رویداد ها</a>
                        </div>
                    @endif
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($plans)>0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>نقشه غرفه ها</th>
                                    <th>عنوان</th>
                                    <th>مربوط به رویداد</th>
                                    <th>قیمت</th>
                                    <th>ابعاد</th>
                                    <th>تعداد میز</th>
                                    <th>تعداد صندلی</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plans as $plan)
                                    <tr>
                                        <td>
                                            @if($plan->map)
                                                <a href="{{url($plan->map->path)}}"> <img alt="image"
                                                class="avatar avatar-md br-7"
                                                src="{{asset($plan->map->path)}}"></a>
                                            @else
                                                <span>بدون نقشه</span>
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle"> {{$plan->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->event->title}}</td>
                                        <td class="text-nowrap align-middle"> 
                                            @if ($plan->price != '0')
                                                {{number_format($plan->price)}}
                                            @else
                                                رایگان
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle"> {{$plan->dimensions}}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->tables_qty }}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->chairs_qty}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a class="btn btn-sm btn-primary" href="{{route('edit.plan',$plan->id)}}">ویرایش</a>
                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" onclick="deletePlan({{$plan->id}})">حذف</a>
                                                @if (Auth::user()->groupable_id == $plan->event->creator_id)
                                                    <a class="btn btn-sm btn-success" onclick="addEventBooth({{$plan->id}})"
                                                        href="javascript:void(0)">افزودن غرفه</a>
                                                @endif
                                                <a class="btn btn-sm btn-secondary"
                                                   data-bs-target="#modal-{{$plan->id}}"
                                                   data-bs-toggle="modal"
                                                   href="javascript:void(0)">
                                                    مشاهده غرفه ها</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="modal-{{$plan->id}}">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title">
                                                        لیست غرفه های تعریف شده برای پلن: {{$plan->name}}
                                                    </h6>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="card-body">
                                                        @if($booths=getBuyPlanBooths($plan->id))
                                                            @foreach($booths as $booth)
                                                                <div>
                                                                    <span>عنوان: </span>
                                                                    <span class="text-primary p-2">{{$booth->name}}</span>
                                                                    <a href="{{route('delete.booth.plan',$booth->id)}}"><span class="text-danger p-2">حذف</span></a>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="alert alert-danger">
                                                                <h4>هنوز برای این پلن، غرفه ای تعریف نکرده اید!</h4>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$plans->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="addBoothUrl" value="{{ route('add.event.booth') }}">
        <input type="hidden" id="deletePlanUrl" value="{{ route('delete.plan') }}">
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/events/js/indexPlans.js')}}"></script>
@endsection

{{-- js is fixed --}}
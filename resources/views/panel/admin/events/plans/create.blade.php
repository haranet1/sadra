@extends('panel.admin.layout.master')
@section('title' , 'ثبت پلن جدید')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف پلن جدید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ثبت پلن جدید</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('store.plan')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$event->id}}" name="event_id">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="name" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        @if ($event->is_free == false)
                            <div class="row mb-4">
                                <label class="col-md-3 form-label" for="example-email"> قیمت (تومان) </label>
                                <div class="col-md-9">
                                    <input name="price" type="text" class="form-control" >
                                    @error('price') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="price" value="0">
                        @endif
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نقشه غرفه ها</label>
                            <div class="col-md-9">
                                <input name="map" type="file" class="form-control" >
                                @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">متراژ اتاق</label>
                            <div class="col-md-9">
                                <input name="dimensions" type="text"  class="form-control" >
                                @error('dimensions') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> تعداد میز</label>
                            <div class="col-md-9">
                                <input name="tables_qty" type="text"  class="form-control" >
                                @error('tables_qty') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> تعداد صندلی</label>
                            <div class="col-md-9">
                                <input name="chairs_qty" type="text"  class="form-control" >
                                @error('chairs_qty') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
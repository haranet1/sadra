@extends('panel.admin.layout.master')
@section('title' , 'لیست درخواست های ثبت نام')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست دانشجویان ثبت نام شده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">
                    <a href="{{route('registred.students.event.export' , $id)}}" class="btn btn-success">خروجی اکسل</a>

                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>رویداد</th>
                                    <th>نام و نام خانوادگی</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>وضعیت</th>
                                    <th class="text-center">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    @if(verta(now())->lessThan(verta($request->event->end_register_at)->addWeeks(3)))
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$request->event->title}}</td>
                                            <td class="text-nowrap align-middle">{{$request->studentInfo->user->name." ".$request->studentInfo->user->family}} <br> {{$request->studentInfo->user->mobile}}</td>
                                            <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                            <td class="text-nowrap align-middle">
                                                - - -
                                                {{-- @if (is_null($request->status))
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @endif --}}
                                            </td>
                                            <td class="text-nowrap align-middle text-center">
                                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#list-modal-{{$request->id}}">درخواست های پویش ثبت شده</button>
                                            </td>
                                        </tr>
                                        
                                        <div class="modal fade" id="list-modal-{{$request->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-xl">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="exampleModalLabel">لیست درخواست های ثبت شده پویش دانشجو {{$request->studentInfo->user->name." ".$request->studentInfo->user->family}}</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        
                                                        {{-- <table>
                                                            <tr>
                                                                asd
                                                            </tr>
                                                        </table>
                                                         --}}
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">بستن</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
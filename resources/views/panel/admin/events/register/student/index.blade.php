@extends('panel.admin.layout.master')
@section('title' , 'لسیت رویداد ها ثبت نام شده')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد دانشجو</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادهای ثبت نام شده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">

                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table table-striped border-top table-bordered mb-0">
                                <thead class="table-info">
                                <tr>
                                    <th>رویداد</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>تاریخ شروع رویداد</th>
                                    <th>تاریخ پایان رویداد</th>
                                    {{-- <th>توضیحات</th> --}}
                                    <th class="text-center">توضیحات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$request->event->title}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->event->start_at)->formatDate()}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->event->end_at)->formatDate()}}</td>
                                        {{-- <td class=" text-center text-nowrap align-middle">
                                            
                                        </td> --}}
                                        <td class="text-center align-middle">
                                            <p class="mb-0">
                                            @php
                                                echo Str::limit(strip_tags($request->event->description), 70, '...')
                                            @endphp
                                            </p>
                                            <br>
                                            <div class="btn-group align-top">
                                                @if (verta(now())->lessThan(verta($request->event->end_at)))
                                                    <a href="{{route('std.index.internship.event' , $request->event->id)}}" class="btn btn-primary mx-1">موقعیت های پویش</a>
                                                @endif
                                                <a href="{{ route('std.invitation.event' , $request->id) }}" class="btn btn-success mx-1">کارت ورود به رویداد</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد.
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
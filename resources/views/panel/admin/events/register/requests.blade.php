@extends('panel.admin.layout.master')
@section('title' , 'لیست درخواست های ثبت نام')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست درخواست های ثبت نام رویداد</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0 justify-content-between">
                    <a href="{{route('registred.companies.event.export',$id)}}" class="btn btn-success">خروجی اکسل</a>
                    <a href="{{route('done.register.event',$id)}}" class="btn btn-info">ثبت نام های نهایی</a>
                    
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>رویداد</th>
                                    <th>شرکت</th>
                                    <th>نام و نام خانوادگی</th>
                                    <th>غرفه انتخابی</th>
                                    <th>هزینه ثبت نام (تومان)</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>وضعیت</th>
                                    <th class="text-center">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    @if(verta(now())->lessThan(verta($request->event->end_register_at)->addWeeks(3)))
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$request->event->title}}</td>
                                            <td class="text-nowrap align-middle">{{$request->companyInfo->name}}</td>
                                            <td class="text-nowrap align-middle">{{$request->companyInfo->user->name." ".$request->companyInfo->user->family}} <br> {{$request->companyInfo->user->mobile}}</td>
                                            <td class="text-nowrap align-middle">{{$request->booth->plan->name}} <br> {{"غرفه: ". $request->booth->name}}</td>
                                            <td class="text-nowrap align-middle">{{number_format($request->booth->plan->price)}}</td>
                                            <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                            <td class="text-nowrap align-middle">
                                                @if (is_null($request->status))
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @else
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle text-center">
                                                <a href="javascript:void(0)" onclick="checkPayment({{$request->id}})" class="btn btn-primary">بررسی پرداخت</a>
                                                <a href="javascript:void(0)" onclick="declineSadraReg({{$request->id}})" class="btn btn-danger">رد درخواست</a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
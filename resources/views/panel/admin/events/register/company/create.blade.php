@extends('panel.admin.layout.master')
@section('title' , 'ثبت نام در رویداد')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/company-create-in-style.css')}}">
@endsection

@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت نام در رویداد</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-7 col-lg-12">
            <div class="card">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img class="p-5" src="{{asset($event->photo->path)}}" class="card-img-left h-100" alt="img">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{$event->title}}</h5>
                            @if ($event->is_free == true)
                                <p class="badge rounded-pill bg-success">رایگان</p>
                            @endif
                            <p class="card-text" align="justify">
                                {{Str::limit(strip_tags($event->description),'80','...')}}
                            </p>
                            <p class="card-text mt-5">
                                <p class="text-muted">
                                    از
                                        تاریخ: {{verta($event->start_at)->format('Y/m/d')}}
                                        تا {{verta($event->end_at)->format('Y/m/d')}}
                                </p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">اطلاعات تکمیلی ثبت نام</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('store.register.event')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event" value="{{$event->id}}">
                        {{-- plan --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="plan">انتخاب پلن:</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="plan" id="plan">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($event->plans as $plan)
                                        @if($plan->deleted == 0)
                                            <option class="form-control" value="{{$plan->id}}">{{$plan->name}}</option>
                                            @if($plan->map)
                                                <a href="{{asset($plan->map->path)}}">نقشه</a>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                                <span id="verifyPlan" class="text-danger"></span>
                            </div>
                        </div>
                        {{-- booth --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="booth">انتخاب
                                غرفه:</label>
                            <div class="col-md-6">
                                <select class="form-select form-control" name="booth" id="booth">
                                    <option class="form-control" value="">ابتدا پلن را انتخاب کنید</option>
                                </select>
                                <span id="verifyBooth" class="text-danger"></span>
                            </div>
                        </div>
                        <div class=" row mt-3 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="submit" class="btn btn-success">ثبت نام</button>
                                {{-- <a href="javascript:void(0)" onclick="Check()" class="btn btn-success btn-lg px-5"> ثبت نام
                                </a> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12">
            <div class="card div-company-create-in-style">
                <div class="card-body">
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->name}}</span>
                        </div>
                    </div>
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">زمینه فعالیت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->activity_field}}</span>
                        </div>
                    </div>
                    {{-- ceo --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام مدیر عامل شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->ceo}}</span>
                        </div>
                    </div>

                    {{-- map --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نقشه:</label>
                        <div class="col-6">
                            <a name="map" href="javascript:void(0)" target="_blank">
                                <img  name="map" id="map" class="img-fluid border border-danger img-company-create-in-style"  src="">
                            </a>
                            <span class="d-block text-info">روی نقشه کلیک کنید</span>
                        </div>
                    </div>
                    {{-- price --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">هزینه ثبت نام
                            (تومان) :</label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span id="price"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="getBoothByPlanIdUrl" value="{{ route('get.event.booth-by-plan-id') }}">
    <input type="hidden" id="imageSrc" value="{{ asset(':url') }}">
@endsection
@section('script')
    <script src="{{asset('assets/events/js/indexRegister.js')}}"></script>
@endsection

{{-- js is fixed --}}
@extends('panel.admin.layout.master')
@section('title' , 'لسیت رویداد ها ثبت نام شده')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادهای ثبت نام شده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
                
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">

                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>نام دانشگاه</th>
                                    <th>رویداد</th>
                                    <th>غرفه انتخابی</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>قیمت</th>
                                    <th>وضعیت</th>
                                    <th class="text-center">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$request->event->university->name}}</td>
                                        <td class="text-nowrap align-middle">{{$request->event->title}}</td>
                                        <td class="text-nowrap align-middle">{{$request->booth->name}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                        <td class="text-nowrap align-middle">{{$request->booth->plan->price}}</td>
                                        <td class=" text-center text-nowrap align-middle">
                                            @if(verta(now())->lessThan(verta($request->event->end_register_at)))
                                                @if ($request->event->is_free == true)
                                                    <span class="badge bg-dark">پایان ثبت نام</span>
                                                @elseif(is_null($request->status))
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @elseif($request->status === 0)
                                                    <span class="badge bg-danger"> رد شده</span>
                                                @else
                                                    <span class="badge bg-success">پرداخت شده</span>
                                                @endif
                                            @else
                                                <span class="badge bg-dark">پایان ثبت نام</span>
                                            @endif
                                            @if(!verta(now())->lessThan(verta($request->event->end_at)))
                                                <span class="badge bg-dark">رویداد پایان یافت</span>
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                @if ($request->event->is_free == false && verta(now())->lessThan(verta($request->event->end_register_at)))
                                                    @if (is_null($request->status) && is_null($request->receipt_id))
                                                        <a  class="btn register btn-success"
                                                        href="{{route('prepay.pay.event',$request->id)}}"
                                                        href="javascript:void(0)">
                                                        پرداخت</a>
                                                    @endif
                                                @endif
                                                
                                                @if ($request->event->is_free == false)
                                                    @if ($request->status === 1 && $request->payment)
                                                        <a  class="btn register btn-primary"
                                                        href="{{route('result.show.pay.event',$request->payment->id)}}"
                                                        href="javascript:void(0)">
                                                        مشاهده رسید</a>
                                                        <a  class="btn register btn-success"
                                                        href="{{route('create.invitation.event',$request->id)}}"
                                                        href="javascript:void(0)">
                                                        مشاهده دعوتنامه</a>
                                                        @if(verta(now())->lessThan(verta($request->event->end_at)))
                                                            <a  class="btn register btn-info"
                                                            href="{{route('index.internship.event',$request->event->id)}}"
                                                            href="javascript:void(0)">
                                                            ساخت موقعیت پویش</a>
                                                        @endif
                                                    @endif
                                                @else
                                                    <a  class="btn register btn-success"
                                                    href="{{route('create.invitation.event',$request->id)}}"
                                                    href="javascript:void(0)">
                                                    مشاهده دعوتنامه</a>
                                                    @if(verta(now())->lessThan(verta($request->event->end_at)))
                                                        <a  class="btn register btn-info"
                                                        href="{{route('index.internship.event',$request->event->id)}}"
                                                        href="javascript:void(0)">
                                                        ساخت موقعیت پویش</a>
                                                    @endif
                                                @endif
                                                
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@extends('panel.admin.layout.master')
@section('title','درخواست های ثبت نام صدرا')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست درخواست های ثبت نام صدرا</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">
                    <div class="">
                        {{-- <a href="{{route('employee.done.sadra.export-excel', $sadra_id)}}" class="btn btn-success">خروجی</a> --}}
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>رویداد</th>
                                    <th>شرکت</th>
                                    <th>غرفه انتخابی</th>
                                    <th>مبلغ (تومان)</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>وضعیت</th>
                                    <th>رسید پرداخت</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$request->event->title}}</td>
                                        <td class="text-nowrap align-middle">{{$request->companyInfo->name}}</td>
                                        <td class="text-nowrap align-middle">{{$request->booth->name}}</td>
                                        <td class="text-nowrap align-middle">{{number_format($request->booth->plan->price)}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                        <td class="text-nowrap align-middle">
                                            @if($request->status === 1)
                                                <span class="badge bg-success">پرداخت شد</span>
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">
                                            {{$request->payment->tracking_code}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection

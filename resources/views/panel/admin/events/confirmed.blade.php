@extends('panel.admin.layout.master')

@section('title' , 'رویداد های تایید شده')

@section('main')

    <div class="page-header">
        <h1 class="page-title">داشبورد کارمندان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست رویداد های تایید شده</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
            {{-- <input type="text" class="form-control " placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-2 text-center">
                        <h5>{{Session::pull('success')}}</h5>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger mt-2 text-center">
                        <h5>{{Session::pull('error')}}</h5>
                    </div>
                @endif
                <div class="card-body">
                    @if(count($events)>0)
                    <div class="row">
                        @foreach ($events as $event)
                            <div class="col-12 col-md-6">
                                <div class="card shadow-lg">
                                    <div class="card-header bg-info br-te-3 br-ts-3 justify-content-between">
                                        <h4 class="card-title text-white">عنوان : {{$event->title}}</h4>
                                        <h4 class="card-title text-white">دانشگاه {{$event->university->name}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-6 col-md-3 px-1">
                                                <label class="" for="">عکس : </label>
                                                @if($event->photo)
                                                    <a href="{{url($event->photo->path)}}">
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->photo->path)}}">
                                                    </a>
                                                @else
                                                    <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/images/default-image.svg')}}">
                                                @endif
                                            </div>
                                            <div class="col-6 col-md-3 px-1">
                                                <label class="" for="">نقشه رویداد : </label>
                                                @if($event->map)
                                                    <a href="{{url($event->map->path)}}">
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->map->path)}}">
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-6 col-md-3 px-1">
                                                <label class="" for="">بنر عمودی : </label>
                                                @if($event->verticalBanner)
                                                    <a href="{{url($event->verticalBanner->path)}}">
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->verticalBanner->path)}}">
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-6 col-md-3 px-1">
                                                <label class="" for="">بنر افقی : </label>
                                                @if ($event->horizontalBanner)
                                                    <a href="{{url($event->horizontalBanner->path)}}">
                                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->horizontalBanner->path)}}">
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="border border-info mt-2 align-items-center">
                                            <div class="row justify-content-center">
                                                <span class="col-7 col-md-4">شروع و پایان رویداد : </span>
                                                <span class="col-5 col-md-3">{{verta($event->start_at)->formatDate()}}</span>
                                                <span class="col-7 col-md-2">لغایت</span>
                                                <span class="col-5 col-md-3" dir="ltr">{{verta($event->end_at)->formatDate()}}</span>
                                            </div>
                                        </div>
                                        <div class="border border-warning mt-2 align-items-center">
                                            <div class="row justify-content-center">
                                                <span class="col-7 col-md-4">شروع و پایان ثبت نام : </span>
                                                @if(!is_null($event->start_register_at))
                                                    <span class="col-5 col-md-3">{{verta($event->start_register_at)->formatDate()}}</span>
                                                    <span class="col-7 col-md-2">لغایت</span>
                                                    <span class="col-5 col-md-3" dir="ltr">{{verta($event->end_register_at)->formatDate()}}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2 px-2">
                                            <span class="col-5 col-md-2">توضیحات : </span>
                                            <div class="col-7 col-md-10">
                                                {{Str::limit(strip_tags($event->description),'50','...')}}
                                            </div>
                                        </div>
                                        <div class="row mt-2 px-2 align-items-center">
                                            <span class="col-5 col-md-2">وضعیت : </span>
                                            <div class="col-7 col-md-10">
                                                @if (is_null($event->status))
                                                    <span class="badge rounded-pill bg-primary">در انتظار تایید</span>

                                                @else
                                                    @if ($event->status == 1)
                                                        <span class="badge rounded-pill bg-success">تایید شده</span>
                                                    @else
                                                        <span class="badge rounded-pill bg-danger">رد شده</span>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-flex justify-content-center">
                                        @if(userIsProvincialEmployee())
                                            <a class="btn btn-sm btn-warning mx-1" onclick="cancellationEvent({{$event->id}})" href="javascript:void(0)">لغو تایید</a>
                                        @endif
                                        <a class="btn btn-sm btn-green mx-1" href="{{route('list.index.plan',$event->id)}}">پلن های ثبت نام</a>
                                        <a class="btn btn-sm btn-info mx-1" href="{{route('show.register.event',$event->id)}}">شرکت ها</a>
                                        <a class="btn btn-sm btn-secondary mx-1" href="{{route('show-students.register.event',$event->id)}}">دانشجویان</a>
                                        @if(userIsProvincialEmployee())
                                            <a class="btn btn-sm btn-danger mx-1" onclick="deleteEvent({{$event->id}})" href="javascript:void(0)">حذف</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @else
                        <div class="alert alert-warning text-center">
                            اطلاعاتی جهت نمایش وجود ندارد.
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="deletEventUrl" value="{{ route('delete.event') }}">
        <input type="hidden" id="cancellationEventUrl" value="{{ route('cancellation.event') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/events/js/event.js') }}"></script>
@endsection
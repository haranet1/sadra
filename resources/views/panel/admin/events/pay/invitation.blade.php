@extends('panel.admin.layout.master')
@section('title','رویدادها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/invitation-style.css')}}">
@endsection
@section('main')


    <div class="mt-1 row row-cards justify-content-center">
        <div class="col-lg-12 col-xl-12">
            <div class="mb-5 input-group">
            </div>
            @if(Session::has('error'))
                <div class="text-center alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="text-center alert alert-success">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="row">
                <div class="col-12 c-m-400">
                    <div class="card shadow-ld bg-400 div-card-invitation">
                        <div class="row align-items-center justify-content-center">
                            <div class="p-5 col-10 c-m-400-c">
                                <div class="mb-2 card mb-md-3 div-card-invitation-bg" >
                                    <div class="m-3 row justify-content-between align-items-center c-row-1">
                                        <div class="my-3 col-6 col-md-3">
                                            <img src="{{asset('assets/panel/images/logo/sadra-logo.svg')}}" alt="sadra-logo">
                                        </div>
                                        <div class="col-6 col-md-3">

                                        </div>
                                        <div class="col-6 col-md-2">
                                            <span class="span-invitation-date">تاریخ :</span>
                                            <span class="span-invitation-date">{{verta($companyEvent->updated_at)->formatJalaliDate()}}</span>
                                        </div>
                                    </div>
                                    <div class="m-3 mt-5 row justify-content-center">
                                        <div class="col-12 col-md-11">
                                            <div class="row align-items-center">
                                                <div class="col-3 col-md-1 c-m-400-l2">
                                                    <img class="img-invitation-style" src="{{asset($companyEvent->companyInfo->logo->path)}}" alt="company-logo">
                                                </div>
                                                <div class="col-9 col-md-10 c-m-400">
                                                    <h4 class="c-h4 h4-invitation-style" >
                                                        @if ($companyEvent->companyInfo->user->sex == 'male')
                                                            <strong>
                                                                <span>جناب آقای</span>
                                                                <span>{{$companyEvent->companyInfo->ceo}}</span>
                                                            </strong>
                                                        @elseif ($companyEvent->companyInfo->user->sex == 'female')
                                                            <strong>
                                                                <span>سرکار خانم</span>
                                                                <span>{{$companyEvent->companyInfo->ceo}}</span>
                                                            </strong>
                                                        @endif
                                                    </h4>
                                                    <h4 class="c-h4 h4-invitation-style">
                                                        <strong>
                                                            <span>مدیر عامل محترم شرکت</span>
                                                            <span>{{$companyEvent->companyInfo->name}}</span>
                                                        </strong>
                                                    </h4>
                                                </div>
                                            </div>
                                            <p class="mb-1 c-p-400 p-invitation-style" align="justify" >
                                                @php
                                                    echo $companyEvent->event->invitation;
                                                @endphp
                                            </p>
                                            {{-- @if ($company->inviter && ($company->inviter != "سامانه فن‌یار" || $company->inviter != "سایر موارد"))
                                                <p class="mt-1 c-p-400" align="justify" style="font-size: 1.1rem; line-height: 2rem">
                                                    همچنین
                                                    <strong>{{$company->inviter}}</strong>
                                                    جهت هماهنگی های لازم حضورتان معرفی می‌گردند.
                                                </p>
                                            @endif --}}

                                        </div>
                                    </div>
                                </div>
                                <div class="mb-2 card mb-md-3 div-card-invitation-bg">
                                    <div class="m-3 row justify-content-center align-items-center">
                                        <div class="col-12 col-md-11">
                                            <div class="row align-items-center">
                                                <div class="col-4 col-md-2">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 col-md-8">
                                                            <img src="{{asset('assets/panel/images/invitation/qr-code.svg')}}" alt="img">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-md-10">
                                                    <div class="mt-1 row align-items-center">
                                                        <div class="px-0 col-3 col-md-1">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/web-icon.svg')}}" alt="img">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h4 class="m-0 c-h4">www.sadra.iau.ir</h4>
                                                        </div>
                                                    </div>
                                                    <div class="mt-1 row align-items-center">
                                                        <div class="px-0 col-3 col-md-1">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/location-icon.svg')}}" alt="img">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h5 class="m-0 c-h4">{{$companyEvent->event->university->address}}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-2">
                            <a id="print" class="btn btn-primary d-print-none a-invitation-style" href="javascript:void(0)" >چاپ دعوتنامه</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{-- {{$events->links('pagination.panel')}} --}}
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{ asset('assets/events/js/pay.js') }}"></script>
@endsection

{{-- js is fixed --}}

@extends('panel.admin.layout.master')
@section('title','رویدادها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/invitation-style.css')}}">
@endsection
@section('main')


    <div class="mt-1 row row-cards justify-content-center">
        <div class="col-lg-12 col-xl-12">
            <div class="mb-5 input-group">
            </div>
            @if(Session::has('error'))
                <div class="text-center alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="text-center alert alert-success">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="row">
                <div class="col-12 c-m-400">
                    <div class="card shadow-ld bg-400 div-card-invitation">
                        <div class="row align-items-center justify-content-center">
                            <div class="p-5 col-10 c-m-400-c">
                                <div class="mb-2 card mb-md-3 div-card-invitation-bg" >
                                    <div class="m-3 mb-0 row justify-content-between align-items-center c-row-1">
                                        <div class="my-3 col-6 col-md-3">
                                            <img src="{{asset('assets/panel/images/logo/sadra-logo.svg')}}" alt="sadra-logo">
                                        </div>
                                        <div class="d-none d-md-block col-md-2 ">
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <span class="span-invitation-date">تاریخ :</span>
                                            <span class="span-invitation-date">{{verta($studentEvent->updated_at)->formatJalaliDate()}}</span>
                                        </div>
                                    </div>
                                    <div class="m-3 mt-0 row justify-content-center">
                                        <div class="mb-2 col-12 col-md-10 c-m-400">
                                            <p class="m-0 text-center c-p-400 p-invitation-style">کارت ورود دانشجو</p>
                                            <p class="m-0 text-center c-h4 h4-invitation-style" >
                                                @if ($studentEvent->studentInfo->user->sex == 'male')
                                                    <strong>
                                                        <span>جناب آقای</span>
                                                        <span>{{$studentEvent->studentInfo->user->name}} {{$studentEvent->studentInfo->user->family}}</span>
                                                    </strong>
                                                @elseif ($studentEvent->studentInfo->user->sex == 'female')
                                                    <strong>
                                                        <span>سرکار خانم</span>
                                                        <span>{{$studentEvent->studentInfo->user->name}} {{$studentEvent->studentInfo->user->family}}</span>
                                                    </strong>
                                                @endif
                                            </p>
                                            <p class="mb-1 text-center c-p-400 p-invitation-style" align="justify" >
                                                {{ $studentEvent->event->title }}
                                            </p>
                                            <p class="mb-1 text-center c-p-400 p-invitation-style" align="justify" >
                                                {{ verta($studentEvent->event->start_at)->formatJalaliDate() }} الی {{ verta($studentEvent->event->end_at)->formatJalaliDate() }}
                                            </p>
                                            <p class="mb-1 text-center c-p-400 p-invitation-style" align="justify">
                                                دانشگاه آزاد واحد {{ $studentEvent->event->university->name }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-2 card mb-md-3 div-card-invitation-bg">
                                    <div class="m-3 row justify-content-center align-items-center">
                                        <div class="col-12 col-md-11">
                                            <div class="row align-items-center">
                                                <div class="col-4 col-md-2">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 col-md-8">
                                                            <img src="{{asset('assets/panel/images/invitation/qr-code.svg')}}" alt="img">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-md-10">
                                                    <div class="mt-1 row align-items-center">
                                                        <div class="px-0 col-3 col-md-1">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/web-icon.svg')}}" alt="img">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h4 class="m-0 c-h4">www.sadra.iau.ir</h4>
                                                        </div>
                                                    </div>
                                                    <div class="mt-1 row align-items-center">
                                                        <div class="px-0 col-3 col-md-1">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/location-icon.svg')}}" alt="img">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h5 class="m-0 c-h4">{{$studentEvent->event->university->address}}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-2">
                            <a id="print" class="btn btn-primary d-print-none a-invitation-style" href="javascript:void(0)" >چاپ دعوتنامه</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{-- {{$events->links('pagination.panel')}} --}}
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{ asset('assets/events/js/pay.js') }}"></script>
@endsection

{{-- js is fixed --}}

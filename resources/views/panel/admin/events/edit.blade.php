@extends('panel.admin.layout.master')
@section('title' , 'ویرایش رویداد')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش رویداد </a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش رویداد: {{$event->title}}</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('update.event')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event" value="{{$event->id}}">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="title" value="{{$event->title}}" type="text" class="form-control">
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">برای شرکت در رویداد</label>
                            <div class="col-md-9">
                                <select name="type" id="type" class="form-control form-select select2">
                                    <option
                                    @if ($event->is_free == true)
                                        selected
                                    @endif
                                    value="price">از شرکت ها هزینه گرفته می‌شود</option>
                                    <option
                                    @if ($event->is_free == false)
                                        selected
                                    @endif
                                    value="free">از شرکت ها هزینه گرفته نمی‌شود</option>
                                </select>
                                @error('type') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">پوستر عمودی</label>
                            <div class="col-md-9">
                                <input name="photo" type="file" class="form-control" >
                                @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نقشه غرفه ها</label>
                            <div class="col-md-9">
                                <input name="map" type="file" class="form-control" >
                                @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        {{-- <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">بنر عمودی</label>
                            <div class="col-md-9">
                                <input name="vertical_banner" type="file" class="form-control" >
                                @error('vertical_banner') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div> --}}
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">بنر افقی</label>
                            <div class="col-md-9">
                                <input name="horizontal_banner" type="file" class="form-control" >
                                @error('horizontal_banner') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ شروع رویداد</label>
                            <div class="col-md-9">
                                <input name="start" value="{{str_replace('-','/',verta($event->start_at)->formatDate())}}" type="text" id="start" class="form-control" >
                                @error('start') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ خاتمه رویداد</label>
                            <div class="col-md-9">
                                <input name="end" value="{{str_replace('-','/',verta($event->end_at)->formatDate())}}" type="text" id="end" class="form-control" >
                                @error('end') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ شروع ثبت نام</label>
                            <div class="col-md-9">
                                <input name="start_register" value="{{str_replace('-','/',verta($event->start_register_at)->formatDate())}}" type="text" id="start_register" class="form-control" >
                                @error('start_register') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ پایان ثبت نام</label>
                            <div class="col-md-9">
                                <input name="end_register" value="{{str_replace('-','/',verta($event->end_register_at)->formatDate())}}" type="text" id="end_register" class="form-control" >
                                @error('end_register') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">توضیحات</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="editor" cols="30" rows="5">{{$event->description}}</textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">متن دعوتنامه</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="invitation" id="editor1" cols="30" rows="5">{{$event->description}}</textarea>
                                @error('invitation') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor/translations/fa.js') }}"></script>
    <script src="{{ asset('assets/events/js/event.js') }}"></script>
@endsection

{{-- js is fixed --}}
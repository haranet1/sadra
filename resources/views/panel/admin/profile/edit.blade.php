@extends('panel.admin.layout.master')
@section('title' , 'ویرایش پروفایل')
@section('css')
    
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش اطلاعات شخصی</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">اطلاعات شخصی</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('update.profile')}}" method="POST">
                        @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="name">نام</label>
                                <input name="name" type="text" value="{{$user->name}}" class="form-control" >
                                @error('name')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="family">نام خانوادگی</label>
                                <input name="family" type="text" value="{{$user->family}}" class="form-control" >
                                @error('family')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="mobile">موبایل</label>
                                <input name="mobile" type="text" value="{{$user->mobile}}" class="form-control" >
                                @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <label for="mobile">کلمه عبور</label>

                            <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                </a>
                                <input name="password" class="input100 border-start-0 form-control ms-0" type="password" placeholder="کلمه عبور">
                            </div>
                        </div>
                    </div>
                    <div class="form-footer mt-2">
                        <button type="submit" class="btn btn-primary">ثبت تغییرات</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/show-password.min.js')}}"></script>
@endsection
@extends('panel.admin.layout.master')
@section('title' , 'لیست دانشگاه های زیر مجموعه')
@section('main')

    <div class="page-header">
        <h1 class="page-title">لیست دانشگاه های زیر مجموعه</h1>
        <div class="">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد کارکنان</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
            </div>
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-end">
                    <div class="">
                        <a href="{{ route('list.p-uni.export') }}" class="btn btn-success">خروجی اکسل</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($universities) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>واحد دانشگاهی</th>
                                <th>تلفن</th>
                                <th>شهر</th>
                                <th>تعداد رویدادها</th>
                                <th>تعداد دانشجویان</th>
                                {{-- <th class="text-center">عملکردها</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($universities as $uni)
                            <tr>
                                <td class="text-nowrap align-middle">{{$uni->name}}</td>
                                <td class="text-nowrap align-middle">{{$uni->phone}}</td>
                                @if ($uni->city)
                                    <td class="text-nowrap align-middle">{{$uni->city->name}}</td>
                                @else
                                    <td class="text-nowrap align-middle"> -- </td>
                                @endif
                                @if ($uni->events->count() > 0)
                                    <td class="text-nowrap align-middle">{{$uni->events->count()}}</td>
                                @else
                                    <td class="text-nowrap align-middle"> رویدادی ثبت نشده </td>
                                @endif
                                @if ($uni->students->count() > 0)
                                    <td class="text-nowrap align-middle">{{$uni->students->count()}}</td>
                                @else
                                    <td class="text-nowrap align-middle"> دانشجویی ثبت نشده </td>
                                @endif
                                {{-- <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('student.single' , $std->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                        @if (is_null($std->user->status))
                                            <a href="javascript:void(0)" onclick="confirmUser({{$std->user->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                        @endif
                                    </div>
                                </td> --}}
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$universities->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
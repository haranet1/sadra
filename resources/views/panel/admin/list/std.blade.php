@extends('panel.admin.layout.master')
@section('title','دانشجویان تایید نشده')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                @if (Route::is('un-std.list'))
                    <li class="breadcrumb-item"><a href="javascript:void(0)">دانشجویان تایید نشده</a></li>
                @else
                    <li class="breadcrumb-item"><a href="javascript:void(0)">دانشجویان تایید شده</a></li>
                @endif
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
            </div>
            <div class="card">
                <div class="card-header border-bottom-0 justify-content-between">
                   
                    <div class="">
                        @if (Route::is('std.list'))
                            <a href="{{ route('sub-students.p-uni.export') }}" class="btn btn-success" >خروجی اکسل</a>
                        @else
                            <a href="{{ route('un.sub-students.p-uni.export') }}" class="btn btn-success" >خروجی اکسل</a>
                        @endif

                        {{-- <select class="form-control select2 w-100">
                            <option value="asc">آخرین</option>
                            <option value="desc">قدیمی ترین</option>
                        </select> --}}
                    </div>
                    @if (Route::is('std.list'))
                        <a href="{{route('un-std.list')}}" class="btn btn-info">دانشجویان تایید نشده</a>
                    @elseif (Route::is('un-std.list'))
                        <a href="{{route('std.list')}}" class="btn btn-info">دانشجویان تایید شده</a>
                    @endif
                    
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($students) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>نام و نام خانوادگی </th>
                                <th>تلفن</th>
                                <th>سن</th>
                                <th>دانشگاه</th>
                                <th>رشته</th>
                                <th>شهر</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $std)
                            <tr>
                                <td class="text-nowrap align-middle">{{$std->user->name." ".$std->user->family}}</td>
                                <td class="text-nowrap align-middle">{{$std->user->mobile}}</td>
                                <td class="text-nowrap align-middle">{{verta($std->user->birth)->diffYears(verta()->now())}}</td>
                                <td class="text-nowrap align-middle">{{$std->university->name}}</td>
                                <td class="text-nowrap align-middle">{{$std->major}}</td>
                                @if ($std->user->city)
                                    <td class="text-nowrap align-middle">{{$std->user->city->name}}</td>
                                @endif
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('student.single' , $std->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                        @if (is_null($std->user->status))
                                            <a href="javascript:void(0)" onclick="confirmUser({{$std->user->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$students->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="confirmUserUrl" value="{{ route('confirm.user') }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/auth/js/user.js')}}"></script>
@endsection

{{-- js is fixed --}}

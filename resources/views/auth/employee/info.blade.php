@extends('auth.layouts.master')
@section('title' , 'اطلاعات کارمندان')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/auth/css/auth-company-info-in-style.css')}}">
@endsection
@section('main')
    <div class="">
        {{-- logo --}}
        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}"  class="header-brand-img m-0 img-auth-company-info"
                    alt="logo">
            </div>
        </div>
        {{-- form --}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-7">
                    <div class="wrap-login100 p-6">
                        <span class="login100-form-title pb-3">
                            تکمیل اطلاعات کارمندان
                        </span>
                        <div class="panel panel-primary">
                            <form action="{{route('employee.store.info')}}" method="POST" class="login100-form validate-form" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        {{-- province --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-map-marker-radius" aria-hidden="true"></i>
                                            </span>
                                            <select id="province" onchange="getCities()" name="province" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="" label="انتخاب استان">انتخاب استان</option>
                                                @foreach($provinces as $province)
                                                    <option value="{{$province->id}}"
                                                    @if (old('province'))
                                                        {{old('province') == $province->id ? 'selected' : ''}}
                                                    @endif
                                                    >{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- city --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-city" aria-hidden="true"></i>
                                            </span>
                                            <select  id="city" name="city" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="">انتخاب شهر</option>

                                            </select>
                                        </div>
                                        @error('city') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- employee_code --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-account" aria-hidden="true"></i>
                                            </span>
                                            <input name="employee_code" value="{{old('employee_code')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="کد کارمندی ">
                                        </div>
                                        @error('employee_code') <small class="text-danger">{{$message}}</small> @enderror
                                        {{-- university_position --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-account" aria-hidden="true"></i>
                                            </span>
                                            <input name="university_position" value="{{old('university_position')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="سمت دانشگاهی ">
                                        </div>
                                        @error('university_position') <small class="text-danger">{{$message}}</small> @enderror

                                    </div>
                                </div>
                                <div class="row mt-3">
                                    {{-- header --}}
                                    <div class="col-12 col-lg-6">
                                        <label for="header">تصویر سربرگ: </label>
                                        <div class="wrap-input100 validate-input input-group ">
                                            <input name="header" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('header') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    {{-- logo --}}
                                    <div class="col-12 col-lg-6">
                                        <label for="logo">تصویر پروفایل: </label>
                                        <div class="wrap-input100 validate-input input-group">
                                            <input name="logo" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('logo') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <button type="submit" class="btn btn-primary w-100">ثبت اطلاعات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/auth/js/info.js')}}"></script>
@endsection

{{-- js is fixed --}}
@extends('auth.layouts.master')
@section('title' , 'اطلاعات شرکت')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/auth/css/auth-company-info-in-style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/auth/css/svg-icons.css') }}">
@endsection
@section('main')
    <div class="">
        {{-- logo --}}
        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}" class="header-brand-img m-0 img-auth-company-info"
                    alt="logo">
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="wrap-login100 p-6">
                        <span class="login100-form-title pb-3">
                            تکمیل اطلاعات شرکت
                        </span>
                        <div class="row justify-content-center">
                            <span class="text-danger text-center pb-3 span-auth-company-info">(تمام فیلد ها الزامی است)</span>
                        </div>
                        <div class="panel panel-primary">
                            <form action="{{route('company.store.info')}}" method="POST" class="login100-form validate-form" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        {{-- name --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/company.svg') }}" alt="نام شرکت">
                                            </span>
                                            <input name="name" value="{{old('name')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="نام شرکت ">
                                        </div>
                                        @error('name') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- n_code --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="کد ملی">
                                            </span>
                                            <input name="n_code" value="{{old('n_code')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="کد ملی شرکت ">
                                        </div>
                                        @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- registration_number --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/info-card.svg') }}" alt="شماره ثبت">
                                            </span>
                                            <input name="registration_number" value="{{old('registration_number')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="شماره ثبت ">
                                        </div>
                                        @error('registration_number') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- co_phone --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/mobile.svg') }}" alt="تلفن">
                                            </span>
                                            <input name="co_phone" value="{{old('co_phone')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="تلفن شرکت ">
                                        </div>
                                        @error('co_phone') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- activity_field --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/sitemap.svg') }}" alt="حوزه فعالیت">
                                            </span>
                                            <input name="activity_field" value="{{old('activity_field')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="حوزه فعالیت ">
                                        </div>
                                        @error('activity_field') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- website --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/web.svg') }}" alt="وب سایت">
                                            </span>
                                            <input name="website" value="{{old('website')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="وب سایت شرکت ">
                                        </div>
                                        @error('website') <small class="text-danger ">{{$message}}</small> @enderror


                                    </div>
                                    <div class="col-12 col-md-6">
                                        {{-- year --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/date.svg') }}" alt="تاریخ">
                                            </span>
                                            <input name="year" id="year" value="{{old('year')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="سال تاسیس ">
                                        </div>
                                        @error('year') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- ceo --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/ceo.svg') }}" alt="مدیرعامل">
                                            </span>
                                            <input name="ceo" value="{{old('ceo')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="مدیر عامل ">
                                        </div>
                                        @error('ceo') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- province --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/location.svg') }}" alt="استان">
                                            </span>
                                            <select id="province" onchange="getCities()" name="province" class="select-auth-company-info form-control form-select select2" >
                                                <option value="" label="انتخاب استان">انتخاب استان</option>
                                                @foreach($provinces as $province)
                                                    <option value="{{$province->id}}"
                                                    @if (old('province'))
                                                        {{old('province') == $province->id ? 'selected' : ''}}
                                                    @endif
                                                    >{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- city --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/location.svg') }}" alt="شهر">
                                            </span>
                                            <select id="city" name="city" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="">انتخاب شهر</option>

                                            </select>
                                        </div>
                                        @error('city') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- address --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/location.svg') }}" alt="آدرس">
                                            </span>
                                            <input name="address" value="{{old('address')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="آدرس شرکت ">
                                        </div>
                                        @error('address') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- co_email --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/email.svg') }}" alt="ایمیل">
                                            </span>
                                            <input name="co_email" value="{{old('co_email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                   placeholder="ایمیل شرکت ">
                                        </div>
                                        @error('co_email') <small class="text-danger ">{{$message}}</small> @enderror


                                    </div>
                                </div>
                                <div class="row mt-3">
                                    {{-- header --}}
                                    <div class="col-12 col-lg-6">
                                        <label for="header">تصویر سربرگ: </label>
                                        <div class="wrap-input100 validate-input input-group ">
                                            <input name="header" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('header') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    {{-- logo --}}
                                    <div class="col-12 col-lg-6">
                                        <label for="logo">لوگو شرکت: </label>
                                        <div class="wrap-input100 validate-input input-group">
                                            <input name="logo" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('logo') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                </div>
                                {{-- about --}}
                                <div class="row justify-content-center">
                                    <div class="col-12 ">
                                        <label for="desc">درباره شرکت: </label>
                                        <div class="wrap-input100 validate-input input-group justify-content-center">
                                            <textarea class="form-control" name="about" id="editor" cols="30" rows="5">{{old('about')}}</textarea>
                                        </div>
                                        @error('about') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>

                                <div class="row justify-content-center mt-3">
                                    <button type="submit" class="btn btn-primary w-100">ثبت اطلاعات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor/translations/fa.js') }}"></script>
    <script src="{{ asset('assets/auth/js/info.js') }}"></script>
@endsection

{{-- js is fixed --}}
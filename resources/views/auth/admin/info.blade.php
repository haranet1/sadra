@extends('auth.layouts.master')


@section('css')
    <link rel="stylesheet" href="{{asset('assets/auth/css/info-in-style.css')}}">
@endsection


@section('main')
    <div class="">
        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}" class="header-brand-img m-0 img-info-in-style"
                    alt="logo">
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    @if(Session::has('error'))
                    <div class="row">
                        <div class="alert alert-success">
                            {{Session::pull('error')}}
                        </div>
                    </div>
                @endif
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="wrap-login100 p-6">
                        <span class="login100-form-title pb-3">
                            تکمیل اطلاعات مدیر
                        </span>
                        <div class="panel panel-primary">
                            <form action="{{route('admin.store.info')}}" method="POST" class="login100-form validate-form">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                        <span class="input-group-text bg-white text-muted">
                                            <i class="mdi mdi-account" aria-hidden="true"></i>
                                        </span>
                                        <input name="mobile" value="{{old('mobile')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                               placeholder="شماره موبایل ">
                                    </div>
                                    @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                                    <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                        <span class="input-group-text bg-white text-muted">
                                            <i class="mdi mdi-account" aria-hidden="true"></i>
                                        </span>
                                        <input name="password" value="{{old('password')}}" class="input100 border-start-0 ms-0 form-control" type="password" placeholder="رمز عبور">

                                    </div>
                                    @error('password') <small class="text-danger ">{{$message}}</small> @enderror
                                    <div class="row justify-content-center mt-3">
                                        <button type="submit" class="btn btn-primary w-100">ثبت اطلاعات</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('auth.layouts.master')
@section('title' , 'ثبت نام')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/persianDatepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/auth/css/info-in-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/auth/css/svg-icons.css')}}">
@endsection
@section('main')
    <div class="">
        {{-- logo --}}
        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}"  class="header-brand-img m-0 img-info-in-style"
                    alt="logo">
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="wrap-login100 p-6">
                        <span class="login100-form-title">ثبت نام</span>
                        <div class="panel panel-primary">
                            <form action="{{route('register')}}" method="POST"  autocomplete="off" class="login100-form validate-form">
                                @csrf
                                <div class="row">
                                    {{-- right col --}}
                                    <div class="col-12 col-md-6">
                                        {{-- name --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-12" src="{{ asset('assets/auth/icons/user.svg') }}" alt="نام">
                                            </span>
                                            <input name="name" value="{{old('name')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder="نام">

                                        </div>
                                        @error('name') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- family --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-12" src="{{ asset('assets/auth/icons/user.svg') }}" alt="نام خانوادگی">
                                            </span>
                                            <input name="family" value="{{old('family')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder="نام خانوادگی">
                                        </div>
                                        @error('family') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- n_code --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="کد ملی">
                                            </span>
                                            <input name="n_code" value="{{old('n_code')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder=" کد ملی">
                                        </div>
                                        @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- sex --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/gender.svg') }}" alt="جنسیت">
                                            </span>
                                            <select name="sex" class="form-control form-select select2" >
                                                <option value="" label="جنسیت">جنسیت</option>
                                                <option value="male">مرد</option>
                                                <option value="female">زن</option>
                                            </select>
                                        </div>
                                        @error('sex') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    {{-- left col --}}
                                    <div class="col-12 col-md-6">
                                        {{-- birth --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/date.svg') }}" alt="تاریخ تولد">
                                            </span>
                                            <input name="birth" id="birth" value="{{old('birth')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder=" تاریخ تولد">
                                        </div>
                                        @error('birth') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- email --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/email.svg') }}" alt="ایمیل">
                                            </span>
                                            <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                placeholder=" ایمیل">
                                        </div>
                                        @error('email') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- mobile --}}
                                        <div class="wrap-input100 validate-input input-group">
                                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/mobile.svg') }}" alt="موبایل">
                                            </span>
                                            <input name="mobile" value="{{old('mobile')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder=" موبایل">
                                        </div>
                                        @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- password --}}
                                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/password.svg') }}" alt="رمز عبور">
                                            </a>
                                            <input name="password" class="input100 border-start-0 ms-0 form-control" type="password"
                                                placeholder="کلمه عبور">
                                        </div>
                                        @error('password') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>
                                {{-- role --}}
                                <div class="wrap-input100 validate-input input-group">
                                    <div class="product-variants">
                                        <span>نوع کاربری: </span>
                                        <ul class="js-product-variants">
                                            <li class="ui-variant">
                                                <label class="ui-variant ui-variant--color">
                                                    <span class="ui-variant-shape">
                                                        <img class="c-svg-14" src="{{ asset('assets/auth/icons/company.svg') }}" alt="جنسیت">
                                                    </span>
                                                    <input type="radio" value="company" name="role"
                                                        class="variant-selector">
                                                    <span class="ui-variant--check">شرکت</span>
                                                </label>
                                            </li>
                                            <li class="ui-variant">
                                                <label class="ui-variant ui-variant--color">
                                                    <span class="ui-variant-shape">
                                                        <img class="c-svg-14" src="{{ asset('assets/auth/icons/student.svg') }}" alt="جنسیت">
                                                    </span>
                                                    <input type="radio" value="student" name="role"
                                                        class="variant-selector">
                                                    <span class="ui-variant--check">دانشجو</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @error('role') <small class="text-danger ">{{$message}}</small> @enderror
                                {{-- submit --}}
                                <div class="container-login100-form-btn">
                                    <button type="submit" class="login100-form-btn btn-primary">ثبت نام</button>
                                </div>

                                <div class="text-center pt-3">
                                    <p class="text-dark mb-0">
                                        از قبل حساب دارید؟<a href="{{route('login')}}" class="text-primary ms-1">ورود به سیستم</a>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('script')
<script src="{{asset('assets/panel/plugins/persianDatepicker/js/persianDatepicker.min.js')}}"></script>
    <script>
        $('#birth').persianDatepicker({
            altField: '#birth',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
    </script>
@endsection

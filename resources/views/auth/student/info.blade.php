@extends('auth.layouts.master')
@section('title' , 'اطلاعات دانشجویی')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/auth/css/auth-company-info-in-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/auth/css/svg-icons.css')}}">
@endsection

@section('main')
    <div class="">
        {{-- logo --}}
        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}" class="header-brand-img m-0 img-auth-company-info"
                    alt="logo">
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="wrap-login100 p-6">
                        <span class="login100-form-title pb-3">
                            تکمیل اطلاعات دانشجویی
                        </span>
                        <div class="row justify-content-center">
                            <span class="text-danger text-center pb-3 span-auth-company-info">(تمام فیلد ها الزامی است)</span>
                        </div>
                        <div class="panel panel-primary">
                            <form action="{{route('student.store.info')}}" method="POST" class="login100-form validate-form">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        {{-- province --}}
                                        <div class="wrap-input100 mb-0 mt-2 mt-md-0 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/location.svg') }}" alt="استان">
                                            </span>
                                            <select id="province" onchange="getCities()" name="province" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="" label="انتخاب استان">انتخاب استان</option>
                                                @foreach($provinces as $province)
                                                    <option value="{{$province->id}}"
                                                    @if (old('province'))
                                                        {{old('province') == $province->id ? 'selected' : ''}}
                                                    @endif
                                                    >{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- city --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/location.svg') }}" alt="شهر">
                                            </span>
                                            <select  id="city" name="city" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="">انتخاب شهر</option>

                                            </select>
                                        </div>
                                        @error('city') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- university --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/university.svg') }}" alt="دانشگاه">
                                            </span>
                                            <select  id="university_id" name="university_id" class="form-control form-select select2 select-auth-company-info" >
                                                <option value="" label="انتخاب واحد دانشگاهی">انتخاب واحد دانشگاهی</option>
                                                @foreach($universities as $uni)
                                                    <option value="{{$uni->id}}"
                                                        @if(old('university_id'))
                                                            {{old('university_id') == $uni->id ? 'selected' : ''}}
                                                        @endif
                                                        >{{$uni->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('university_id') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- student_code --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span  class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="شماره دانشجویی">
                                            </span>
                                            <input name="student_code" inputmode="numeric" value="{{old('student_code')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="شماره دانشجویی">
                                        </div>
                                        @error('student_code') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                    <div class="col-12 col-md-6">

                                        {{-- grade --}}
                                        <div class="wrap-input100 mb-0 mt-2 mt-md-0 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-book-open" aria-hidden="true"></i>
                                                {{-- <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="شماره دانشجویی"> --}}
                                            </span>
                                            <select  name="grade" id="grade" class="form-control form-select select2 select-auth-company-info">
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == '' ? 'selected' : ''}}
                                                @endif
                                                value="">انتخاب مقطع تحصیلی</option>
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == 'دکترا' ? 'selected' : ''}}
                                                @endif
                                                value="دکترا">دکترا</option>
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == 'کارشناسی ارشد' ? 'selected' : ''}}
                                                @endif
                                                value="کارشناسی ارشد">کارشناسی ارشد</option>
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == 'کارشناسی' ? 'selected' : ''}}
                                                @endif
                                                value="کارشناسی">کارشناسی</option>
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == 'کاردانی' ? 'selected' : ''}}
                                                @endif
                                                value="کاردانی">کاردانی</option>
                                                <option
                                                @if (old('grade'))
                                                    {{old('grade') == 'دیپلم' ? 'selected' : ''}}
                                                @endif
                                                value="دیپلم">دیپلم</option>
                                            </select>
                                        </div>
                                        @error('grade') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- major --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-book" aria-hidden="true"></i>
                                                {{-- <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="شماره دانشجویی"> --}}
                                            </span>
                                            <input name="major" value="{{old('major')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="رشته تحصیلی">
                                        </div>
                                        @error('major') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- avg --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-counter" aria-hidden="true"></i>
                                                {{-- <img class="c-svg-15" src="{{ asset('assets/auth/icons/n_code.svg') }}" alt="شماره دانشجویی"> --}}
                                            </span>
                                            <input name="avg" inputmode="numeric" value="{{old('avg')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="معدل">
                                        </div>
                                        @error('avg') <small class="text-danger ">{{$message}}</small> @enderror
                                        {{-- end_date --}}
                                        <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <img class="c-svg-15" src="{{ asset('assets/auth/icons/date.svg') }}" alt="تاریخ">
                                            </span>
                                            <input name="end_date" id="end_date" value="{{old('end_date')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                placeholder=" تاریخ اخذ مدرک">
                                        </div>
                                        @error('end_date') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <button type="submit" class="btn btn-primary w-100">ثبت اطلاعات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="getCitiesUrl" value="{{ route('auth.ajax.get-cities') }}">
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/auth/js/info.js')}}"></script>
@endsection

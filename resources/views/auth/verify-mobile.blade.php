<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="صدرا">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{'assets/front/images/favicon3.svg'}}" />

    <title>صدرا | تایید شماره تلفن</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet" />

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}" />
    <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{'assets/auth/css/info-in-style.css'}}">
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{'assets/panel/images/loader.svg'}}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">
            <div class="col col-login mx-auto mt-7 mb-3">
                <div class="text-center">
                    <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}"  class="header-brand-img m-0 img-info-in-style"
                        alt="logo">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{Session::pull('error')}}</div>
                    @endif
                        <span class="login100-form-title pb-5">
                        تایید شماره تلفن
                        </span>
                        <div class="panel panel-primary">

                            <form action="{{route('verify-mobile')}}" method="POST">
                                @csrf
                            <div  class="wrap-input100 validate-input input-group mb-4">
                                <input name="code" type="text" class="input100 border-start-0 form-control ms-0">

                            </div>
                                @if(Session::has('error'))
                                    <div>
                                        <span class="text-danger">
                                            {{Session::pull('error')}}
                                        </span>
                                    </div>
                                @endif
                            <span>کد ارسال شده به موبایل خود را اینجا وارد کنید</span>

                            <div class="container-login100-form-btn ">
                                <button type="submit" class="login100-form-btn btn-primary">ادامه</button>
                            </div>
                            </form>
                        </div>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

<script src="{{asset('assets/panel/js/jquery.sparkline.min.js')}}"></script>

<script src="{{asset('assets/panel/js/sticky.js')}}"></script>
{{--<script src="{{'assets/panel/js/show-password.min.js'}}"></script>--}}

{{--<script src="{{'assets/panel/js/generate-otp.js'}}"></script>--}}

<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll-1.js')}}"></script>
<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>
</body>
</html>

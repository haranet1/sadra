@extends('auth.layouts.master')
@section('title' , 'ثبت نام شرکت')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/auth/css/info-in-style.css')}}">
@endsection

@section('main')
    <div class="">

        <div class="col col-login mx-auto mt-7 mb-3">
            <div class="text-center">
                <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}"  class="header-brand-img m-0 img-info-in-style"
                    alt="logo">
            </div>
        </div>

        <div class="container-login100">

            <div class="wrap-login100 p-6">
                <form action="{{route('register')}}" method="POST" class="login100-form validate-form">
                @csrf
                    <span class="login100-form-title">ثبت نام</span>

                    <div class="wrap-input100 validate-input input-group">
                        <span class="input-group-text bg-white text-muted">
                            <i class="mdi mdi-account" aria-hidden="true"></i>
                        </span>
                        <input name="name" value="{{old('name')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                            placeholder="نام">

                    </div>
                    @error('name') <small class="text-danger ">{{$message}}</small> @enderror


                    <div class="wrap-input100 validate-input input-group">
                        <span class="input-group-text bg-white text-muted">
                            <i class="mdi mdi-account" aria-hidden="true"></i>
                        </span>
                        <input name="family" value="{{old('family')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                            placeholder="نام خانوادگی">
                    </div>
                    @error('family') <small class="text-danger ">{{$message}}</small> @enderror



                    <div class="wrap-input100 validate-input input-group">
                        <span class="input-group-text bg-white text-muted">
                            <i class="mdi mdi-account" aria-hidden="true"></i>
                        </span>
                        <select name="sex" class="form-control form-select select2" >
                            <option value="" label="جنسیت">جنسیت</option>
                            <option value="male">مرد</option>
                            <option value="female">زن</option>
                        </select>
                    </div>
                    @error('sex') <small class="text-danger ">{{$message}}</small> @enderror

                    <div class="wrap-input100 validate-input input-group">
                        <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                            <i class="mdi mdi-phone" aria-hidden="true"></i>
                        </span>
                        <input name="mobile" value="{{old('mobile')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                            placeholder=" موبایل">
                    </div>
                    @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror

                    <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                        <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                            <i class="zmdi zmdi-eye" aria-hidden="true"></i>
                        </span>
                        <input name="password" class="input100 border-start-0 ms-0 form-control" type="password"
                            placeholder="کلمه عبور">
                    </div>
                    @error('password') <small class="text-danger ">{{$message}}</small> @enderror
                    <div class="wrap-input100 validate-input input-group">
                        <div class="product-variants">
                            <span>نوع کاربری: </span>
                            <ul class="js-product-variants">
                                <li class="ui-variant">
                                    <label class="ui-variant ui-variant--color">
                                        <span class="ui-variant-shape">
                                        <i class="fa fa-building"></i>
                                        </span>
                                        <input type="radio" value="company" name="role"
                                            class="variant-selector">
                                        <span class="ui-variant--check">شرکت</span>
                                    </label>
                                </li>
                                <li class="ui-variant">
                                    <label class="ui-variant ui-variant--color">
                                        <span class="ui-variant-shape">
                                        <i class="fa fa-user"></i>
                                        </span>
                                        <input type="radio" value="applicant" name="role"
                                            class="variant-selector">
                                        <span class="ui-variant--check">کارجو</span>
                                    </label>
                                </li>
                                <li class="ui-variant">
                                    <label class="ui-variant ui-variant--color">
                                        <span class="ui-variant-shape">
                                        <i class="fa fa-user"></i>
                                        </span>
                                        <input type="radio" value="student" name="role"
                                            class="variant-selector">
                                        <span class="ui-variant--check">دانشجو</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @error('role') <small class="text-danger ">{{$message}}</small> @enderror


                        {{--                        <label class="custom-control custom-checkbox mt-4">--}}
                        {{--                            <input type="checkbox" class="custom-control-input">--}}
                        {{--                            <span class="custom-control-label">موافقت با <a href="#">شرایط و خط مشی</a></span>--}}
                        {{--                        </label>--}}
                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn btn-primary">ثبت نام</button>
                    </div>
                    <div class="text-center pt-3">
                        <p class="text-dark mb-0">
                            از قبل حساب دارید؟<a href="{{route('login')}}" class="text-primary ms-1">ورود به سیستم</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection

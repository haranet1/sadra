<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="فن یار">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/panel/images/logo/sadra-logo-min.svg')}}"/>

    <title>صدرا | @yield('title')</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    
    <link href="{{'assets/panel/css/style.css'}}" rel="stylesheet"/>
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet"/>
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet"/>

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet"/>

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}"/>

    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('assets/panel/css/auth.css')}}">
    <link rel="stylesheet" href="{{asset('assets/panel/css/persianDatepicker.css')}}">
    @yield('css')
</head>
<body class="app sidebar-mini rtl login-img">
    
    <div class="">
        <div id="global-loader">
            <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
        </div>

        <div class="page">
            @yield('main')
        </div>
    </div>

    <input type="hidden" id="CSRF" value="{{csrf_token()}}">

    <script src="{{'assets/panel/js/jquery.min.js'}}"></script>

    <script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
    <script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

    <script src="{{'assets/panel/js/show-password.min.js'}}"></script>


    <script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>

    <script src="{{'assets/panel/js/themeColors.js'}}"></script>

    <script src="{{'assets/panel/js/custom.js'}}"></script>
    <script src="{{'assets/panel/js/custom1.js'}}"></script>
    <script src="{{asset('assets/panel/plugins/persianDatepicker/js/persianDatepicker.min.js')}}"></script>

    @yield('script')
</body>
</html>
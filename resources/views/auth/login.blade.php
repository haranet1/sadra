<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="صدرا ">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{'assets/front/images/favicon3.svg'}}" />

    <title>صدرا | ورود</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet" />

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}" />
    {{--<link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />--}}
    {{--<link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />--}}
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{'assets/auth/css/info-in-style.css'}}">
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{'assets/panel/images/loader.svg'}}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">

            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{asset('assets/panel/images/logo/sadra-logo-w.svg')}}"  class="header-brand-img img-info-in-style" alt="">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6">
                    <form action="{{route('login')}}" method="POST" autocomplete="off" class="login100-form validate-form">
                        @csrf
                        <span class="login100-form-title pb-5">
                        ورود
                        </span>
                        <div class="panel panel-primary">
                            <div class="tab-menu-heading">
                                @if(Session::has('success'))
                                   <div class="alert alert-success">
                                       <span class="text-success">{{Session::pull('success')}}</span>
                                   </div>
                                @endif
                            </div>
                            <div class="panel-body tabs-menu-body p-0 pt-5">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab5">
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="zmdi zmdi-smartphone zmdi-hc-lg text-muted" aria-hidden="true"></i>
                                            </a>
                                            <input inputmode="numeric" name="mobile" class="input100 border-start-0 form-control ms-0" type="text" placeholder="شماره تلفن">
                                        </div>
                                        @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                            </a>
                                            <input name="password" class="input100 border-start-0 form-control ms-0" type="password" placeholder="کلمه عبور">
                                        </div>
                                        @error('password')<span class="text-danger">{{$message}}</span> @enderror
                                        <div class="text-end pt-4">
                                            <p class="mb-0"><a href="{{route('request.password')}}" class="text-primary ms-1">رمز خود را فراموش کرده‌اید؟</a></p>
                                        </div>
                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">ورود</button>
                                        </div>
                                        <div class="text-center pt-3">
                                            <p class="text-dark mb-0">عضو نیستید؟<a href="{{route('register')}}" class="text-primary ms-1">ثبت نام کنید.</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

<script src="{{'assets/panel/js/show-password.min.js'}}"></script>

<script src="{{'assets/panel/js/generate-otp.js'}}"></script>

<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>

<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

</body>
</html>

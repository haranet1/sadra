let myData = document.getElementById('myData').value;
let currentDate = document.getElementById('currentDate').value
myData = JSON.parse(myData);

Highcharts.chart('container', {
    chart: {
        type: 'column',
        style: {
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های صدرا کشور در سال جاری' + ' ('+ currentDate +') '
    },
    subtitle: {
        text: 'تعداد رویداد های ثبت شده به تفکیک استان'
    },
    xAxis: {
        type: 'category',
        labels: {
            autoRotation: [-45, -90],
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد رویداد های برگزار شده'
        },
        labels: {
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'رویداد برگزار شده : <b>{point.y:.1f}</b>'
    },
    series: [{
        name: 'Population',
        colors: [
            '#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
            '#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
            '#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
            '#03c69b', '#00f194', '#00f894', '#00ff9e',
            '#00ffb4',
            '#00ffc9',
            '#00ffdf',
            '#00fff4',
            '#00f4ff',
            '#00d9ff',
            '#00c3ff',
            '#00a8ff',
            '#008eff',
            '#0078ff',
            '#0063ff',
        ],
        colorByPoint: true,
        groupPadding: 0,
        data: myData,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
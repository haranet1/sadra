

$(document).ready(function () {

    $('.OwlSliderIndex').owlCarousel({
        items:1,
        loop: true,
        autoplay: true,
        rtl: true,
        nav: true,
        // dots: true,
        autoplayTimeout: 7000,
        responsive: {
            1200: {
                items: 1
            },
            992: {
                items:1
            },
            768: {
                items: 1
            },
            576: {
                items: 1
            },
            400: {
                items: 1
            }
        }
    });

    let gallery = $('.OwlSlider1');
    if(gallery.length && $.fn.owlCarousel) {
        gallery.owlCarousel({
            loop : true,
            rtl: true,
            nav : false,
            items : 3,
            dots : true,
            center : true,
            autoplay : true,
            smartSpeed: 1000,
            autoplayTimeout: 6000,
            responsive : {
                0 : {
                    items : 1
                },
                480:{
                    items : 1
                },
                576:{
                    items :2,
                },
                768:{
                    items :2,
                    margin:0
                },
                992:{
                    items : 3,
                    margin:0
                },
                1000:{
                    items:3
                }
            }
        });
    }

    let gallery2 = $('.OwlSlider2');
    if(gallery2.length && $.fn.owlCarousel) {
        gallery2.owlCarousel({
            loop : true,
            rtl: true,
            nav : false,
            items : 3,
            dots : true,
            center : true,
            autoplay : true,
            smartSpeed: 1000,
            autoplayTimeout: 6000,
            responsive : {
                0 : {
                    items : 1
                },
                480:{
                    items : 1
                },
                768:{
                    items :2
                },
                992:{
                    items : 3,
                    margin: 0
                },
                1000:{
                    items:3
                }
            }
        });
    }

    $('.OwlSliderIndex2').owlCarousel({
        items:1,
        margin:10,
        loop: true,
        autoplay: true,
        rtl: true,
        nav: true,
        dots: false,
        autoplayTimeout: 7000,
        responsive: {
            1200: {
                items: 1
            },
            992: {
                items:1
            },
            768: {
                items: 1
            },
            576: {
                items: 1
            },
            575: {
                items: 1
            },
            400: {
                items: 1
            },

        }
    });
    $('.OwlSliderNews').owlCarousel({
        items:1,
        loop: true,
        autoplay: true,
        rtl: true,
        nav: true,
        // dots: true,
        autoplayTimeout: 7000,
        responsive: {
            1200: {
                items: 1
            },
            992: {
                items:1
            },
            768: {
                items: 1
            },
            576: {
                items: 1
            },
            400: {
                items: 1
            }
        }
    });
/*
    var module_slider_1 = new Swiper("#module-1 .module-slider .swiper", {
        slidesPerView: 1,
        spaceBetween: 0,
        grabCursor: true,
        autoplay: {
            delay: 5000,
        },
        effect: "slide",
        fadeEffect: {
            crossFade: 1,
        },
        on: {
            slideChange: function () {
                $(`#module-1 .module-controls a[data-slide="${this.realIndex}"]`)
                    .addClass("active")
                    .siblings()
                    .removeClass("active");
            },
            init: function () {
                $(`#module-1 .module-controls a[data-slide="${this.realIndex}"]`)
                    .addClass("active")
                    .siblings()
                    .removeClass("active");
            },
        },
    });
    $(document).on("click", "#module-1 .module-controls a", (e) => {
        let index = $(e.currentTarget).attr("data-slide");
        $(e.currentTarget).addClass("active").siblings().removeClass("active");
        module_slider_1.slideTo(index);
    });*/

});



const chosenContainer = document.querySelectorAll('.have-child1');

chosenContainer.forEach(item=>{
    item.addEventListener('click',function (){
        item.classList.toggle('is-active-drop')
    })
})





//اسکرول هدر
let header = document.querySelector('header');
let logo = document.querySelector('.logo');
let headerContainer = document.querySelector('.header-container');
let sticky = header.offsetTop;
window.addEventListener('scroll', () =>{
    if (window.pageYOffset > sticky){
        header.classList.add('stickyHeaderFixed');
        headerContainer.classList.add('sticky-header');
        headerContainer.classList.remove('container-lg');
        logo.style.width="45px";
        logo.style.height="45px";
        logo.style.borderRadius= "50%";
        logo.style.top= "50%";
        logo.style.left= "10px";
        logo.style.transform= "translateY(-50%)";
        logo.style.padding="2px";
        logo.style.background=" rgb(243, 242, 242)";
    }else{
        header.classList.remove('stickyHeaderFixed');
        headerContainer.classList.remove('sticky-header');
        headerContainer.classList.add('container-lg');
        logo.style.width="100px";
        logo.style.height= "auto";
        logo.style.borderRadius=" 0px";
        logo.style.top= "120%";
        logo.style.bottom= "10px";
        logo.style.left= "45%";
        logo.style.transform= "translateY(0px)";
        logo.style.padding="0px";
        logo.style.background=" transparent";
    }
});
//اسکرول هدر

// function menuToggle(){
//     let nav = document.querySelector('.parent-nav-responsive ');
//     let toggle = document.querySelector('#toggle');
//     nav.classList.toggle('activeResponsive');
//     toggle.classList.toggle('active');
// }


//counter up

const counters = document.querySelectorAll('.counter');

const options = {
  threshold: 1.0, // وقتی المان به صورت کامل داخل ناحیه دید کاربر وارد شود
};

const observer = new IntersectionObserver((entries, observer) => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      // وقتی المان به ناحیه دید کاربر وارد می‌شود
      const counter = entry.target;
      counter.innerText = '0';
      const target = +counter.getAttribute('data-target');
      const duration = 3000;
      const steps = 100;
      const increment = target / steps;
      let currentCount = 0;

      const updateCounter = () => {
        if (currentCount < target) {
          counter.innerText = Math.ceil(currentCount);
          currentCount += increment;
        } else {
          counter.innerText = target;
          clearInterval(counterInterval);
        }
      };

      const counterInterval = setInterval(updateCounter, duration / steps);
      observer.unobserve(counter); // پس از اجرا، دیگر نیازی به مشاهده نمی‌شود
    }
  });
}, options);

counters.forEach(counter => {
  observer.observe(counter);
});

// const counters = document.querySelectorAll('.counter');
// counters.forEach(counter =>{
//     counter.innerText = '0';
//     const updateCounter = () =>{
//         const target = +counter.getAttribute('data-target');
//         const c = +counter.innerText;
//         const increment = target / 700;
//         if(c<target){
//             counter.innerText = Math.ceil( c + increment);
//             setTimeout(updateCounter,1)
//         }else {
//             counter.innText = target;
//         }
//     }

//     updateCounter()

// })

//counter up end


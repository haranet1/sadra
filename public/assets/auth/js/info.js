const CSRF = document.getElementById('CSRF').value;

const getCitiesUrl = document.getElementById('getCitiesUrl').value;
function getCities()
{
    if($('#province').val() != 0)
    {
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': CSRF
            },
            url: getCitiesUrl,
            data: {
                province:$('#province').val() ,
            },
            dataType: 'json',
            beforeSend: function(){
                $('#global-loader').removeClass('d-none');
                $('#global-loader').addClass('d-block');
                $('#global-loader').css('opacity', 0.6);
            },
            success: function (response) {
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);
                var city = document.getElementById('city')
                city.innerHTML ='<option value="" >انتخاب شهر</option>'
                response.forEach(function (res){
                    city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                })
            },
            error: function (response) {
                console.log(response)
                console.log('error')
            }
        });
    }
}

/**
 * js from employee->edit
 */
if($('#birth').length > 0){
    $('#birth').persianDatepicker({
        altField: '#birth',
        altFormat: "YYYY/MM/DD",
        observer: true,
        format: 'YYYY/MM/DD',
        initialValue: false,
        initialValueType: 'persian',
        autoClose: true,
    
    });
}

/**
 * js from company->info
 */
if($('#year').length > 0){
    $('#year').persianDatepicker({
        altField: '#year',
        altFormat: "YYYY/MM/DD",
        observer: true,
        format: 'YYYY/MM/DD',
        initialValue: false,
        initialValueType: 'persian',
        autoClose: true,

    });
}

if($('#editor').length > 0){
    ClassicEditor.create( document.querySelector( '#editor' ),{language: 'fa'} ).catch( error => {console.error( error );} );
}

/**
 * js from student->info
 */
if($('#end_date').length > 0){
    $('#end_date').persianDatepicker({
        altField: '#end_date',
        altFormat: "YYYY/MM/DD",
        observer: true,
        format: 'YYYY/MM/DD',
        initialValue: false,
        initialValueType: 'persian',
        autoClose: true,

    });
}
const CSRF = document.getElementById('CSRF').value;

function confirmUser(id)
{
    const confirmUserUrl = document.getElementById('confirmUserUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این کاربر تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                type: 'POST',
                url: confirmUserUrl,
                data: {
                    id: id
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire({
                        icon: 'success',
                        title: 'کاربر با موفقیت تایید شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}
const CSRF = document.getElementById('CSRF').value;

const acceptRequestInternship = document.getElementById('acceptRequestInternship').value;
const declineRequestInternship = document.getElementById('declineRequestInternship').value;

let accept = document.getElementById("accept");
id = document.getElementById("request_id").value;
accept.addEventListener('click',function(){
    $.ajax({
        headers:{
            'X-CSRF-TOKEN': CSRF
        },
        url: acceptRequestInternship,
        type:'post',
        data:{
            id:id
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success:function(response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            swal.fire('درخواست پویش تایید شد')
            location.reload()
        },
        error:function (response){
            console.log(response)
        }
    })
});
let decline = document.getElementById('decline');
decline.addEventListener('click', function(){
    $.ajax({
        headers:{
            'X-CSRF-TOKEN':CSRF
        },
        url: declineRequestInternship,
        type:'post',
        data:{
            id:id
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success:function(response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            swal.fire('درخواست پویش رد شد')
            location.reload()
        },
        error:function (response){
            console.log(response)
        }
    })
})
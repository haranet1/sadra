const CSRF = document.getElementById('CSRF').value;

function deleteEvent(event) {
    const deletEventUrl = document.getElementById('deletEventUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این رویداد حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: deletEventUrl,
                data: {
                    event: event,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'رویداد حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function confirmeEvent(event)
{
    
    const confirmeEventUrl = document.getElementById('confirmeEventUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این رویداد تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: confirmeEventUrl,
                data: {
                    event: event,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'رویداد تایید شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function cancellationEvent(event)
{
    const cancellationEventUrl = document.getElementById('cancellationEventUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این رویداد لغو تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: cancellationEventUrl,
                data: {
                    event: event,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'رویداد لغو تایید شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}


/**
 * js from create-event
 */

ClassicEditor.create( document.querySelector( '#editor' ),{language: 'fa'} ).catch( error => {console.error( error );} );


ClassicEditor.create( document.querySelector( '#editor1' ),{language: 'fa'} ).catch( error => {console.error( error );} );


$('#start').persianDatepicker({
    altField: '#start',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,

});
$('#end').persianDatepicker({
    altField: '#end',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,
});
$('#end_register').persianDatepicker({
    altField: '#end_register',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,
});
$('#start_register').persianDatepicker({
    altField: '#start_register',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,
});
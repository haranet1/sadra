const CSRF = document.getElementById('CSRF').value;

function deleteNews(news) {
    const deleteNewsUrl = document.getElementById('deleteNewsUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این خبر حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: deleteNewsUrl,
                data: {
                    news: news,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'خبر حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function confirmeNews(news){
    const confirmeNewsUrl = document.getElementById('confirmeNewsUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این خبر تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: confirmeNewsUrl,
                data: {
                    news: news,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'خبر تایید شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function rejectNews(news){
    const rejectNewsUrl = document.getElementById('rejectNewsUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این خبر رد خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: rejectNewsUrl,
                data: {
                    news: news,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'خبر رد شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

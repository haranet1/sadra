const CSRF = document.getElementById('CSRF').value;

function addEventBooth(plan_id)
{
    const addBoothUrl = document.getElementById('addBoothUrl').value;
    const {value: text} = Swal.fire({
        title: 'نام غرفه را وارد کنید',
        text: 'برای ثبت چند غرفه هم‌زمان ، بین نام های غرفه نقطه (.) بگذارید. مثال : غرفه1.غرفه2.غرفه3',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'ذخیره',
        cancelButtonText: 'انصراف',
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.isConfirmed) {
            const name = Swal.getInput()
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: addBoothUrl,
                data: {
                    name: name.value,
                    plan_id: plan_id
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق!',
                        'غرفه اضافه شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });
        }
    })
}

function deletePlan(id)
{
    const deletePlanUrl = document.getElementById('deletePlanUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این پلن حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: deletePlanUrl,
                data: {
                    id: id,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    if(response = 'success'){
                        $('#global-loader').removeClass('d-block');
                        $('#global-loader').addClass('d-none');
                        $('#global-loader').css('opacity', 1);
                        Swal.fire(
                            'موفق!',
                            'رویداد حذف شد',
                            'success'
                        )
                        location.reload()
                    }else if(response = 'error'){
                        Swal.fire(
                            'ناموفق',
                            'پلن مورد نظر غرفه رزرو شده دارد',
                            'error'
                        )
                    }
                    
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}


/**
 * js from create.blade.php 
 */
$('#start').persianDatepicker({
    altField: '#start',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,

});
$('#end').persianDatepicker({
    altField: '#end',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,
});
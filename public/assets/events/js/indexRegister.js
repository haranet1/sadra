const CSRF = document.getElementById('CSRF').value;

let plan = document.getElementById('plan');
plan.addEventListener('change', function(){
    const getBoothByPlanIdUrl = document.getElementById('getBoothByPlanIdUrl').value;
    let imageSrc = document.getElementById('imageSrc').value;
    let planValue = document.getElementById('plan').value;
    let booth = document.getElementById('booth');
    booth.innerHTML = "";

    if(planValue){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': CSRF
            },
            url: getBoothByPlanIdUrl,
            data: {
                id : planValue
            },
            dataType: "json",
            beforeSend: function(){
                $('#global-loader').removeClass('d-none');
                $('#global-loader').addClass('d-block');
                $('#global-loader').css('opacity', 0.6);
            },
            success: function (response) {
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);
                let price = document.getElementById('price');
                price.innerHTML = "";
                let img = document.querySelector('img[name="map"]');
                let link = document.querySelector('a[name="map"]');
                img.src = "";
                link.href = "";

                if(response){
                    let data = ""
                    response['booths'].forEach(function(resp){
                        data += '<option class="form-control" value="' + resp['id'] + '">' + resp['name'] + '</option>'
                    });
                    booth.innerHTML = data;

                    let imgsrc = imageSrc;
                    imgsrc = imgsrc.replace(':url', response['map']);
                    img.src = imgsrc
                    link.href = imgsrc;

                    if(response['price'] = '0'){
                        price.innerHTML = 'رایگان';

                    }else{

                        price.innerHTML = response['price'];
                    }
                }
            },
            error: function (response) {
                console.log(response)
            }
        });
    }
});



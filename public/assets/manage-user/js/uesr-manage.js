/**
 * js from uesr create
 */
$('#birth').persianDatepicker({
    altField: '#birth',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,

});
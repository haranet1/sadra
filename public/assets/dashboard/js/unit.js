let currentDate = document.getElementById('currentDate').value;

let freeEvents = document.getElementById('freeEvents').value; 
let priceEvents = document.getElementById('priceEvents').value; 
let freeDoneEvents = document.getElementById('freeDoneEvents').value; 
let priceDoneEvents = document.getElementById('priceDoneEvents').value; 
let freeUnDoneEvents = document.getElementById('freeUnDoneEvents').value; 
let priceUnDoneEvents = document.getElementById('priceUnDoneEvents').value; 

Highcharts.chart('container-u-event', {
    colors: ['#FFD700', '#C0C0C0', '#CD7F32'],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
    },
    title: {
        text: 'رویداد ها',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'همه <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
            'در دست اقدام <span class="f16"><span id="flag" class="flag us">' +
              '</span></span>',
            'برگزار شده <span class="f16"><span id="flag" class="flag us">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'غیر رایگان',
        data: [
            parseInt(priceEvents),
            parseInt(priceUnDoneEvents),
            parseInt(priceDoneEvents)
        ]
    }, {
        name: 'رایگان',
        data: [
            parseInt(freeEvents),
            parseInt(freeUnDoneEvents),
            parseInt(freeDoneEvents)
        ]
    }]
});

let u_allCompanies = document.getElementById('u_allCompanies').value;
let u_companyHasEvent = document.getElementById('u_companyHasEvent').value;
let u_companyNoHasEvent = parseInt(u_allCompanies) - parseInt(u_companyHasEvent);

Highcharts.chart('container-u-company', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'شرکت های عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد شرکت ها : <b>{point.y}</b><br/>' 
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(u_companyHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: u_companyNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let u_allStudents = document.getElementById('u_allStudents').value;
let u_studentsHasEvent = document.getElementById('u_studentsHasEvent').value;
let u_studentNoHasEvent = parseInt(u_allStudents) - parseInt(u_studentsHasEvent);

Highcharts.chart('container-u-student', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'دانشجویان عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد دانشجویان : <b>{point.y}</b><br/>' 
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(u_studentsHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: u_studentNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let u_acceptInternRequests = document.getElementById('u_acceptInternRequests').value;
let u_declinedInternRequests = document.getElementById('u_declinedInternRequests').value;
let u_pendingInternRequests = document.getElementById('u_pendingInternRequests').value;

Highcharts.chart('container-u-pooyesh', {
    colors: ['#C0C0C0','#f71c41','#1ba00e',],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'درخواست های پویش',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'درخواست ها <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'باز',
        data: [parseInt(u_pendingInternRequests)]
    },{
        name: 'رد شده',
        data: [parseInt(u_declinedInternRequests)]
    },{
        name: 'تایید شده',
        data: [parseInt(u_acceptInternRequests)]
    },]
});

let price_events = document.getElementById('price_events').value;

Highcharts.chart('container-u-price-event', {
    chart: {
        type: 'column',
        style: {
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های ویژه برگزار شده در استان'
    },
    subtitle: {
        text: 'تعداد رویداد های برگزار شده به تفکیک واحد ها در سال جاری' + ' ('+ currentDate +') '
    },
    xAxis: {
        type: 'category',
        labels: {
            autoRotation: [-45, -90],
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد رویداد های ویژه برگزار شده'
        },
        labels: {
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'رویداد برگزار شده : <b>{point.y}</b>'
    },
    series: [{
        name: 'Population',
        colors: [
            '#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
            '#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
            '#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
            '#03c69b',  '#00f194', '#00f894' , '#00ff9e',
                            '#00ffb4',
                            '#00ffc9',
                            '#00ffdf',
                            '#00fff4',
                            '#00f4ff',
                            '#00d9ff',
                            '#00c3ff',
                            '#00a8ff',
                            '#008eff',
                            '#0078ff',
                            '#0063ff',
        ],
        colorByPoint: true,
        groupPadding: 0,
        data: JSON.parse(price_events),
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

let free_events = document.getElementById('free_events').value;

Highcharts.chart('container-u-free-event', {
    chart: {
        type: 'column',
        style: {
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های رایگان برگزار شده در استان'
    },
    subtitle: {
        text: 'تعداد رویداد های برگزار شده به تفکیک واحد ها در سال جاری' + ' ('+ currentDate +') '
    },
    xAxis: {
        type: 'category',
        labels: {
            autoRotation: [-45, -90],
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد رویداد های رایگان برگزار شده'
        },
        labels: {
            style: {
                fontSize: '13px',
                fontFamily: 'B Yekan'
            }
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'رویداد برگزار شده : <b>{point.y}</b>'
    },
    series: [{
        name: 'Population',
        colors: [
            '#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
            '#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
            '#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
            '#03c69b',  '#00f194', '#00f894' , '#00ff9e',
                            '#00ffb4',
                            '#00ffc9',
                            '#00ffdf',
                            '#00fff4',
                            '#00f4ff',
                            '#00d9ff',
                            '#00c3ff',
                            '#00a8ff',
                            '#008eff',
                            '#0078ff',
                            '#0063ff',
        ],
        colorByPoint: true,
        groupPadding: 0,
        data: JSON.parse(free_events),
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

let MixCompany = document.getElementById('MixCompany').value;
let MixEvent = document.getElementById('MixEvent').value;
let MixStudent = document.getElementById('MixStudent').value;

Highcharts.chart('container-mix', {
    chart: {
        type: 'column',
        style:{
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های برگزار شده به همراه شرکت ها و دانشجویان'
    },
    subtitle:{
        text: '« شرکت ها و دانشجویانی که در رویداد ها شرکت کرده اند به تفکیک ماه های سال نمایش داده می‌شوند »'
    },
    xAxis: {
        categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد','شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']
    },
    yAxis: {
        title: {
        text: 'تعداد '
        }
    },
    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}</b><br/>'
    },
    series: [{
        name: 'شرکت',
        type: 'area',
        color: '#e3cb12',
        data: JSON.parse(MixCompany),
    },{
        name: 'رویداد',
        type: 'column',
        color: '#1928ff',
        data: JSON.parse(MixEvent),
    },{
        name: 'دانشجو',
        type: 'line',
        color: '#fb04ff',
        data: JSON.parse(MixStudent),
    }]
});
/**
 * page header chart event
 */
let currentDate = document.getElementById('currentDate').value;


let freeEvents = document.getElementById('freeEvents').value;
let priceEvents = document.getElementById('priceEvents').value;
let freeDoneEvents = document.getElementById('freeDoneEvents').value;
let priceDoneEvents = document.getElementById('priceDoneEvents').value;
let freeUnDoneEvents = document.getElementById('freeUnDoneEvents').value;
let priceUnDoneEvents = document.getElementById('priceUnDoneEvents').value;

Highcharts.chart('container-event', {
    colors: ['#FFD700', '#C0C0C0', '#CD7F32'],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
    },
    title: {
        text: 'رویداد ها',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'همه <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
            'در دست اقدام <span class="f16"><span id="flag" class="flag us">' +
              '</span></span>',
            'برگزار شده <span class="f16"><span id="flag" class="flag us">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'غیر رایگان',
        data: [
            parseInt(priceEvents),
            parseInt(priceUnDoneEvents),
            parseInt(priceDoneEvents)
        ]
    }, {
        name: 'رایگان',
        data: [
            parseInt(freeEvents),
            parseInt(freeUnDoneEvents),
            parseInt(freeDoneEvents)
        ]
    }]
});

let allCompanies = document.getElementById('allCompanies').value;
let companyHasEvent = document.getElementById('companyHasEvent').value;
let companyNoHasEvent = parseInt(allCompanies) - parseInt(companyHasEvent);

Highcharts.chart('container-company', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'شرکت های عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد شرکت ها : <b>{point.y}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(companyHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: companyNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let allStudents = document.getElementById('allStudents').value;
let studentsHasEvent = document.getElementById('studentsHasEvent').value;
let studentNoHasEvent = parseInt(allStudents) - parseInt(studentsHasEvent);

Highcharts.chart('container-student', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'دانشجویان عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد دانشجویان : <b>{point.y}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(studentsHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: studentNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let acceptInternRequests = document.getElementById('acceptInternRequests').value;
let declinedInternRequests = document.getElementById('declinedInternRequests').value;
let pendingInternRequests = document.getElementById('pendingInternRequests').value;

Highcharts.chart('container-pooyesh', {
    colors: ['#C0C0C0','#f71c41','#1ba00e',],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'درخواست های پویش',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'درخواست ها <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'باز',
        data: [parseInt(pendingInternRequests)]
    },{
        name: 'رد شده',
        data: [parseInt(declinedInternRequests)]
    },{
        name: 'تایید شده',
        data: [parseInt(acceptInternRequests)]
    },]
});

let provinces = document.getElementById('c_provincesEvents').value;
let price_drilldown = document.getElementById('price_drilldown').value;

Highcharts.chart('container-price-event', {
    chart: {
        type: 'column',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        align: 'center',
        text: 'رویداد های برگزار شده ویژه (سود آور)'
    },
    subtitle: {
        align: 'center',
        text: 'تعداد رویداد های برگزار شده ویژه در سال جاری' + ' ('+ currentDate +') '
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',

    },
    yAxis: {
        title: {
            text: ' تعداد رویداد ها'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: 'استان ها',
            colorByPoint: true,
            data: JSON.parse(provinces),
        }
    ],
    drilldown: {
        breadcrumbs: {
            position: {
                align: 'right'
            }
        },
        series: JSON.parse(price_drilldown),
    }
});

let c_free_events = document.getElementById('c_free_events').value;
let free_drilldown = document.getElementById('free_drilldown').value;

Highcharts.chart('container-free-event', {
    chart: {
        type: 'column',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        align: 'center',
        text: 'رویداد های برگزار شده رایگان'
    },
    subtitle: {
        align: 'center',
        text: 'تعداد رویداد های برگزار شده رایگان در سال جاری' + ' ('+ currentDate +') '
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',

    },
    yAxis: {
        title: {
            text: 'تعداد رویداد ها'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: 'استان ها',
            colorByPoint: true,
            data: JSON.parse(c_free_events),
        }
    ],
    drilldown: {
        breadcrumbs: {
            position: {
                align: 'right'
            }
        },
        series: JSON.parse(free_drilldown),
    }
});

let future_price_events = document.getElementById('future_price_events').value;
let future_free_events = document.getElementById('future_free_events').value;
let future_dilldown = document.getElementById('future_dilldown').value;

Highcharts.chart('container-future-event', {
    chart: {
        type: 'column',
        style: {
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'رویداد های آتی'
    },
    subtitle: {
        align: 'center',
        text: 'تعداد رویداد های ویژه و رایگان آتی در سال جاری' + ' ('+ currentDate +') '
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
          text: 'تعداد رویداد ها'
        }
    },
    plotOptions: {
        column: {
          stacking: 'normal'
        }
    },
    series: [{
        name: 'ویژه',
        data: JSON.parse(future_price_events)
    }, {
        name: 'رایگان',
        data: JSON.parse(future_free_events)
    }],
    drilldown: {
        series: JSON.parse(future_dilldown)
    }
});

let c_MixCompany = document.getElementById('c_MixCompany').value;
let c_MixEvent = document.getElementById('c_MixEvent').value;
let c_MixStudent = document.getElementById('c_MixStudent').value;

Highcharts.chart('container-mix', {
    chart: {
        type: 'column',
        style:{
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های برگزار شده به همراه شرکت ها و دانشجویان'
    },
    subtitle:{
        text: '« شرکت ها و دانشجویانی که در رویداد ها شرکت کرده اند به تفکیک ماه های سال نمایش داده می‌شوند »'
    },
    xAxis: {
        categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد','شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']
    },
    yAxis: {
        title: {
        text: 'تعداد '
        }
    },
    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}</b><br/>'
    },
    series: [{
        name: 'شرکت',
        type: 'area',
        color: '#e3cb12',
        data: JSON.parse(c_MixCompany),
    },{
        name: 'رویداد',
        type: 'column',
        color: '#1928ff',
        data: JSON.parse(c_MixEvent),
    },{
        name: 'دانشجو',
        type: 'line',
        color: '#fb04ff',
        data: JSON.parse(c_MixStudent),
    }]
});

let c_allEvents = document.getElementById('c_allEvents').value;
let allEventDrilldown = document.getElementById('allEventDrilldown').value;

Highcharts.chart('container-all-events', {
    chart: {
        type: 'column',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        align: 'center',
        text: 'کل رویداد های ثبت و تایید شده به تفکیک استان ها'
    },
    subtitle: {
        align: 'center',
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',

    },
    yAxis: {
        title: {
            text: ' تعداد رویداد ها'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: 'استان ها',
            colorByPoint: true,
            data: JSON.parse(c_allEvents),
        }
    ],
    drilldown: {
        breadcrumbs: {
            position: {
                align: 'right'
            }
        },
        series: JSON.parse(allEventDrilldown),
    }
});
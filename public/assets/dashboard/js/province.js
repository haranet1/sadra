let currentDate = document.getElementById('currentDate').value;

let freeEvents = document.getElementById('freeEvents').value;
let priceEvents = document.getElementById('priceEvents').value;
let freeDoneEvents = document.getElementById('freeDoneEvents').value;
let priceDoneEvents = document.getElementById('priceDoneEvents').value;
let freeUnDoneEvents = document.getElementById('freeUnDoneEvents').value;
let priceUnDoneEvents = document.getElementById('priceUnDoneEvents').value;

Highcharts.chart('container-p-event', {
    colors: ['#FFD700', '#C0C0C0', '#CD7F32'],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
    },
    title: {
        text: 'رویداد ها',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'همه <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
            'در دست اقدام <span class="f16"><span id="flag" class="flag us">' +
              '</span></span>',
            'برگزار شده <span class="f16"><span id="flag" class="flag us">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'غیر رایگان',
        data: [parseInt(priceEvents),parseInt(priceUnDoneEvents),parseInt(priceDoneEvents)]
    }, {
        name: 'رایگان',
        data: [parseInt(freeEvents),parseInt(freeUnDoneEvents),parseInt(freeDoneEvents)]
    }]
});

let p_allCompanies = document.getElementById('p_allCompanies').value;
let p_companyHasEvent = document.getElementById('p_companyHasEvent').value;
let p_companyNoHasEvent = parseInt(p_allCompanies) - parseInt(p_companyHasEvent);

Highcharts.chart('container-p-company', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'شرکت های عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد شرکت ها : <b>{point.y}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(p_companyHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: p_companyNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let p_allStudents = document.getElementById('p_allStudents').value;
let p_studentsHasEvent = document.getElementById('p_studentsHasEvent').value;
let p_studentNoHasEvent = parseInt(p_allStudents) - parseInt(p_studentsHasEvent);

Highcharts.chart('container-p-student', {
    chart: {
        type: 'variablepie',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'دانشجویان عضو سامانه',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'تعداد دانشجویان : <b>{point.y}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '49%',
        zMin: 0,
        name: 'countries',
        borderRadius: 5,
        data: [{
            name: 'شرکت کرده در رویداد',
            y: parseInt(p_studentsHasEvent),
            z: 200
        }, {
            name: 'شرکت نکرده در رویداد',
            y: p_studentNoHasEvent,
            z: 119
        }, ],
        colors: [
          '#23e274',
          '#4caefe'
        ]
    }]
});

let p_acceptInternRequests = document.getElementById('p_acceptInternRequests').value;
let p_declinedInternRequests = document.getElementById('p_declinedInternRequests').value;
let p_pendingInternRequests = document.getElementById('p_pendingInternRequests').value;

Highcharts.chart('container-p-pooyesh', {
    colors: ['#C0C0C0','#f71c41','#1ba00e',],
    chart: {
        type: 'column',
        inverted: true,
        polar: true,
        height:'400px',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'درخواست های پویش',
        align: 'center',
        style: {
          fontFamily: "B Yekan",
        }
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '100%',
        innerSize: '50%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '16px',
                fontFamily: "B Yekan",
            }
        },
        lineWidth: 0,
        gridLineWidth: 0,
        categories: [
            'درخواست ها <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
        ]
    },
    yAxis: {
        lineWidth: 0,
        tickInterval: 10,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 0
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15,
            borderRadius: '50%'
        }
    },
    series: [{
        name: 'باز',
        data: [parseInt(p_pendingInternRequests)]
    },{
        name: 'رد شده',
        data: [parseInt(p_declinedInternRequests)]
    },{
        name: 'تایید شده',
        data: [parseInt(p_acceptInternRequests)]
    },]
});

let provinces = document.getElementById('provinces').value;
let p_thisPriceDrilldown = document.getElementById('p_thisPriceDrilldown').value;

Highcharts.chart('container-p-price-event', {
    chart: {
        type: 'column',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        align: 'center',
        text: 'رویداد های برگزار شده ویژه (سود آور)'
    },
    subtitle: {
        align: 'center',
        text: 'تعداد رویداد های برگزار شده ویژه در سال جاری' + ' ('+ currentDate +') '
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',

    },
    yAxis: {
        title: {
            text: ' تعداد رویداد ها'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: 'استان ها',
            colorByPoint: true,
            data: JSON.parse(provinces),
        }
    ],
    drilldown: {
        breadcrumbs: {
            position: {
                align: 'right'
            }
        },
        series: JSON.parse(p_thisPriceDrilldown),
    }
});

let c_free_events = document.getElementById('c_free_events').value;
let p_thisFreeDrilldown = document.getElementById('p_thisFreeDrilldown').value;

Highcharts.chart('container-p-free-event', {
    chart: {
        type: 'column',
        style: {
          fontFamily: "B Yekan",
        }
    },
    title: {
        align: 'center',
        text: 'رویداد های برگزار شده رایگان'
    },
    subtitle: {
        align: 'center',
        text: 'تعداد رویداد های برگزار شده رایگان در سال جاری' + ' ('+ currentDate +') '
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',

    },
    yAxis: {
        title: {
            text: 'تعداد رویداد ها'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: 'استان ها',
            colorByPoint: true,
            data: JSON.parse(c_free_events),
        }
    ],
    drilldown: {
        breadcrumbs: {
            position: {
                align: 'right'
            }
        },
        series: JSON.parse(p_thisFreeDrilldown),
    }
});

let MixCompany = document.getElementById('MixCompany').value;
let MixEvent = document.getElementById('MixEvent').value;
let MixStudent = document.getElementById('MixStudent').value;

Highcharts.chart('container-mix', {
    chart: {
        type: 'column',
        style:{
            fontFamily: "B Yekan",
        }
    },
    title: {
        text: 'نمودار رویداد های برگزار شده به همراه شرکت ها و دانشجویان'
    },
    subtitle:{
        text: '« شرکت ها و دانشجویانی که در رویداد ها شرکت کرده اند به تفکیک ماه های سال نمایش داده می‌شوند »'
    },
    xAxis: {
        categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد','شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']
    },
    yAxis: {
        title: {
        text: 'تعداد '
        }
    },
    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}</b><br/>'
    },
    series: [{
        name: 'شرکت',
        type: 'area',
        color: '#e3cb12',
        data: JSON.parse(MixCompany),
    },{
        name: 'رویداد',
        type: 'column',
        color: '#1928ff',
        data: JSON.parse(MixEvent),
    },{
        name: 'دانشجو',
        type: 'line',
        color: '#fb04ff',
        data: JSON.parse(MixStudent),
    }]
});
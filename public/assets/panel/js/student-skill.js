$('#start_date').persianDatepicker({
    altField: '#start_date',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,

});

$('#end_date').persianDatepicker({
    altField: '#end_date',
    altFormat: "YYYY/MM/DD",
    observer: true,
    format: 'YYYY/MM/DD',
    initialValue: false,
    initialValueType: 'persian',
    autoClose: true,

});


function acceptSkill(id)
{
    let cccsrf = document.getElementById('CSRF').value;
    const acceptSkillUrl = document.getElementById('acceptSkillUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این مهارت تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': cccsrf
                },
                type: 'POST',
                url: acceptSkillUrl,
                data: {
                    id: id
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire({
                        icon: 'success',
                        title: 'مهارت با موفقیت تایید شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

function deleteSkill(id)
{
    let ccsrf = document.getElementById('CSRF').value;
    
    
    const deleteSkillUrl = document.getElementById('deleteSkillUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این مهارت حذف خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': ccsrf
                },
                type: 'POST',
                url: deleteSkillUrl,
                data: {
                    id: id
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire({
                        icon: 'success',
                        title: 'مهارت با موفقیت حذف شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}


function getAcademies(e) {
    let cccsrf = document.getElementById('CSRF').value;

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': cccsrf
        },
        url: $('#getAcademySkillUrl').val(),
        data: {
            text: e.value
        },
        dataType: 'json',
        success: function(response) {
            autocomplete(e, response);
        },
        error: function(response) {
            console.log(response);
        }
    });
}



function autocomplete(e, response) {
    let dropdown = document.getElementById('academy-options');

    dropdown.innerHTML = '';

    if (response.length > 0) {
        let ul = document.createElement('ul');
        ul.className = 'list-unstyled';

        response.forEach(item => {
            let li = document.createElement('li');
            li.className = 'dropdown-item';
            li.textContent = 'دانشگاه آزاد'+' '+item['name'];

            li.onclick = function() {
                document.getElementById('dropdownMenuButton').textContent = 'دانشگاه آزاد'+' '+item['name'];
                document.getElementById('selectedAcademy').value = item['id'];
                dropdown.innerHTML = '';
                document.querySelector('.dropdown-menu').classList.remove('show');
            };

            ul.appendChild(li);
        });

        dropdown.appendChild(ul);
    } else {
        let p = document.createElement('p');
        p.className = 'ms-4 red fs-17';
        p.textContent = 'این محل وجود ندارد ... !';
        dropdown.appendChild(p);
        
    
    }

    if (!dropdown.classList.contains('show')) {
        dropdown.classList.add('show');
    }
}


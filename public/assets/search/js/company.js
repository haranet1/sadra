const CSRF = document.getElementById('CSRF').value;


function coSearch()
{
    const companySearchUrl = document.getElementById('companySearchUrl').value;
    let showCompanySingleUrl = document.getElementById('showCompanySingleUrl').value;

    let obj = {
        'name': '*',
        'family': '*',
        'mobile': '*',
        'co_name': '*',
    }

    let name = document.getElementById('name').value;
    let family = document.getElementById('family').value;
    let mobile = document.getElementById('mobile').value;
    let co_name = document.getElementById('co_name').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(co_name != ''){
        obj.co_name = co_name;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': CSRF
        },
        url: companySearchUrl,
        data: {
            obj:obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success : function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);

            let companies = response;    
            let table = document.getElementById('co_table');
            table.innerHTML = '';

            companies.forEach(function (company , index){
                let rowNumber = index + 1;

                let url = showCompanySingleUrl.replace(':id', company['id']);
                var rowHTML = 
                '<tr>' +
                    '<td class="text-nowrap align-middle">' + rowNumber + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['user']['name'] + ' ' + company['user']['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['user']['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['ceo'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['activity_field'] + '</td>' +
                    '<td class="text-center align-middle">' +
                        '<div class="btn-group align-top">' +
                            '<a href="'+ url +'" class="btn btn-sm btn-primary badge" type="button">ویرایش اطلاعات</a>'+
                            // '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button">تایید </a>'+
                        '</div>'+
                    '</td>'+
                '</tr>';

                table.innerHTML += rowHTML;

            });
        },
        error: function (response){
            console.log(response);
        }
    })
}

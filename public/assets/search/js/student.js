const CSRF = document.getElementById('CSRF').value;


function coSearch()
{
    const studentSearchUrl = document.getElementById('studentSearchUrl').value;
    let showStudentSingleUrl = document.getElementById('showStudentSingleUrl').value;

    let obj = {
        'name': '*',
        'family': '*',
        'mobile': '*',
        'grade': '*',
        'major': '*',
    }

    let name = document.getElementById('name').value;
    let family = document.getElementById('family').value;
    let mobile = document.getElementById('mobile').value;
    let grade = document.getElementById('grade').value;
    let major = document.getElementById('major').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(grade != ''){
        obj.grade = grade;
    }
    if(major != ''){
        obj.major = major;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': CSRF
        },
        url: studentSearchUrl,
        data: {
            obj:obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success : function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);

            let students = response;    
            let table = document.getElementById('std_table');
            table.innerHTML = '';

            

            students.forEach(function (student , index){
                let rowNumber = index + 1;

                let confirm = '';
                if(student['user']['status'] == null){
                    confirm = '<a href="javascript:void(0)" onclick="confirmUser('+ student['user']['id'] +')" class="btn btn-sm btn-success badge" type="button">تایید </a>'
                }

                let url = showStudentSingleUrl.replace(':id', student['id']);
                var rowHTML = 
                '<tr>' +
                    '<td class="text-nowrap align-middle">' + rowNumber + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['user']['name'] + ' ' + student['user']['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['user']['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['grade'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['major'] + '</td>' +
                    '<td class="text-center align-middle">' +
                        '<div class="btn-group align-top">' +
                            '<a href="'+ url +'" class="btn btn-sm btn-primary badge" type="button">مشاهده اطلاعات</a>'+
                            confirm+
                        '</div>'+
                    '</td>'+
                '</tr>';

                table.innerHTML += rowHTML;

            });
        },
        error: function (response){
            console.log(response);
        }
    })
}

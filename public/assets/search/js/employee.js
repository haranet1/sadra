const CSRF = document.getElementById('CSRF').value;


function empSearch()
{
    const employeeSearchUrl = document.getElementById('employeeSearchUrl').value;
    let editEmployeeInfoUrl = document.getElementById('editEmployeeInfoUrl').value;
    let deleteInfoWithUserUrl = document.getElementById('deleteInfoWithUserUrl').value;
    
    let obj = {
        'name': '*',
        'family': '*',
        'mobile': '*',
        'uni': '*',
    }

    let name = document.getElementById('name').value;
    let family = document.getElementById('family').value;
    let mobile = document.getElementById('mobile').value;
    let uni = document.getElementById('uni').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(uni != ''){
        obj.uni = uni;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': CSRF
        },
        url: employeeSearchUrl,
        data: {
            obj:obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success : function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);

            let employees = response;
            let table = document.getElementById('emp_table');
            table.innerHTML = '';

            employees.forEach(function (employee , index){
                let rowNumber = index + 1;

                let rand = ['success','dark','warning','primary', 'secondary' , 'info'];
                let role_names = '';
                role_names = '<div class="badge mx-1 bg-'+ rand[getRandomInt(0,5)]+'">'+ employee['role_names'] +'</div>'
                let url = editEmployeeInfoUrl.replace(':id', employee['id']);
                console.log(role_names);
                var rowHTML = 
                '<tr>' +
                    '<td class="text-nowrap align-middle">' + rowNumber + '</td>' +
                    '<td class="text-nowrap align-middle">' + employee['university']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + employee['user']['name'] + ' ' + employee['user']['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + employee['user']['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + role_names + '</td>' +
                    '<td class="text-nowrap align-middle">' + employee['university_position'] + '</td>' +
                    '<td class="text-center align-middle">' +
                        '<div class="btn-group align-top">' +
                            '<a href="'+ url +'" class="btn btn-sm btn-primary badge" type="button">ویرایش اطلاعات</a>'+
                            // '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button">تایید </a>'+
                        '</div>'+
                    '</td>'+
                '</tr>';

                table.innerHTML += rowHTML;

            });
        },
        error: function (response){
            console.log(response);
        }
    })
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function deleteInfoWithUser(id)
{
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این کاربر حذف خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                type: 'POST',
                url: deleteInfoWithUserUrl,
                data: {
                    id: id
                },
                success: function (data) {
                    console.log(data)
                    Swal.fire({
                        icon: 'success',
                        title: 'کاربر با موفقیت حذف شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

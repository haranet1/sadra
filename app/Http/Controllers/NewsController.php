<?php

namespace App\Http\Controllers;

use App\Models\EmployeeInfo;
use App\Models\Event;
use App\Models\News;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function index()
    {
        $News = News::PublicNews()
        ->NotDeleted()
        ->with('photo')
        ->where('university_id' , Auth::user()->groupable->university_id)
        ->orderByDesc('updated_at')
        ->paginate(20);
        
        return view('panel.admin.news.index' , compact('News'));
    }

    public function create()
    {
        return view('panel.admin.news.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photo' => 'required|image|max:3072',
            'description' => 'required',
        ],[
            'title.required' => 'عنوان خبر الزامی است',
            'photo.required' => 'تصویر خبر الزامی است',
            'photo.image' => 'فرمت تصویر انتخاب شده صحیح نمی‌باشد',
            'photo.max' => 'حجم تصویر انتخاب شده بیش از حد مجاز است',
            'description.required' => 'شرح خبر الزامی است',
        ]);

        $news = new News();
        $news->creator_id = Auth::user()->groupable_id;
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->university_id = Auth::user()->groupable->university_id;

        $name = time() . $request->photo->getClientOriginalName();
        $path = 'img/news';
        $file = $request->file('photo');
        $file->move( public_path($path) , $name);

        $photo = new Photo();
        $photo->name = $name;
        $photo->path = $path.'/'.$name;
        $photo->type = News::class;
        $photo->save();
        $news->photo_id = $photo->id;
        $news->save();
        
        return redirect()->route('index.news')->with('success' , 'خبر با موفقیت ایجاد شد');
    }

    public function edit($id)
    {
        $news = News::find($id);

        return view('panel.admin.news.edit' , compact('news'));
    }

    public function update(Request $request)
    {
        $news = News::with('photo')->find($request->news);
        if(isset($request->title) && !is_null($request->title)){
            $news->title = $request->title;
        }
        if(isset($request->description) && !is_null($request->description)){
            $news->description = $request->description;
        }
        if($request->file('photo') && !is_null($request->photo)){
            unlink($news->photo->path);
            $news->photo->delete();

            $name = time() . $request->photo->getClientOriginalName();
            $path = 'img/news';
            $file = $request->file('photo');
            $file->move( public_path($path) , $name);

            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = News::class;
            $photo->save();
            $news->photo_id = $photo->id;
        }
        $news->save();
        if(is_null($news->event_id)){
            return redirect()->route('index.news')->with('success' , 'خبر با موفقیت ویرایش یافت');
        }else{
            return redirect()->route('index.news.event',$news->event_id)->with('success' , 'خبر با موفقیت ویرایش یافت');
        }
    }

    public function delete(Request $request)
    {
        $news = News::with('photo')->find($request->news);
        if ($news->photo){
            unlink($news->photo->path);
            $news->photo->delete();
        }
        $news->deleted = 1;
        return response()->json($news->save());
    }

    public function confirme(Request $request)
    {
        $news = News::with('acceptor' , 'event')->find($request->news);
        $event = null;
        if(isset($news->event)){
            $event = Event::with('news')->whereRelation('news', 'status' , 1)->find($news->event_id);
            if(!is_null($event)){
                if(count($event->news)>= 5 && $event->has_five_news == false){
                    $event->has_five_news = true;
                    $event->save();
                }
            }
        }
        $news->acceptor_id = Auth::user()->groupable->id;
        $news->status = 1;

        
        return response()->json($news->save());
    }
    
    public function reject(Request $request)
    {
        $news = News::find($request->news);
        $news->acceptor_id = Auth::user()->groupable->id;
        $news->status = 0;
        return response()->json($news->save());
    }

    public function unConfirmed()
    {
        $News = News::NotDeleted()
            ->Unconfirmed()
            ->whereRelation('university' , 'parent' , Auth::user()->groupable->university_id)
            ->with('university.subUnits')
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('panel.admin.news.unconfirmed' , compact('News'));
    }

    public function indexE($id)
    {
        $News = News::EventNews()
        ->NotDeleted()
        ->with('photo' , 'event')
        ->where('event_id' , $id)
        ->orderByDesc('updated_at')
        ->paginate(20);
        
        $event = Event::find($id);
        return view('panel.admin.news.index' , compact('News' , 'event'));
    }

    public function createE($id)
    {
        $event = Event::find($id);
        return view('panel.admin.news.create' , compact('event'));
    }

    public function storeE(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photo' => 'required|image|max:3072',
            'description' => 'required',
        ],[
            'title.required' => 'عنوان خبر الزامی است',
            'photo.required' => 'تصویر خبر الزامی است',
            'photo.image' => 'فرمت تصویر انتخاب شده صحیح نمی‌باشد',
            'photo.max' => 'حجم تصویر انتخاب شده بیش از حد مجاز است',
            'description.required' => 'شرح خبر الزامی است',
        ]);

        $news = new News();
        $news->creator_id = Auth::user()->groupable_id;
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->university_id = Auth::user()->groupable->university_id;
        $news->event_id = $request->event;

        $name = time() . $request->photo->getClientOriginalName();
        $path = 'img/news';
        $file = $request->file('photo');
        $file->move( public_path($path) , $name);

        $photo = new Photo();
        $photo->name = $name;
        $photo->path = $path.'/'.$name;
        $photo->type = News::class;
        $photo->save();
        $news->photo_id = $photo->id;
        $news->save();
        
        return redirect()->route('index.news.event',$request->event)->with('success' , 'خبر با موفقیت ایجاد شد');
    }
}

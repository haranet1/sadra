<?php

namespace App\Http\Controllers;

use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function employee(Request $request)
    {
        $req = $request->obj;
        $query = EmployeeInfo::query();
        $query = $query->with('user' , 'university');

        if($req['name'] != '*'){
            $query->whereRelation('user' , 'name' , 'LIKE' , '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->whereRelation('user' , 'family' , 'LIKE' , '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->whereRelation('user' , 'mobile' , 'LIKE' , '%'.$req['mobile'].'%');
        }
        if($req['uni'] != '*'){
            $query->whereRelation('university' , 'name' , 'LIKE' , '%'.$req['uni'].'%');
        }

        $result = $query->get();
        foreach($result as $res){
            $res->role_names = getRoleNamesFa($res->user->getRoleNames());
        }
        return response()->json($result);
    }

    public function company(Request $request)
    {
        $req = $request->obj;
        $query = CompanyInfo::query();
        $query = $query->with('user');

        if($req['name'] != '*'){
            $query->whereRelation('user' , 'name' , 'LIKE' , '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->whereRelation('user' , 'family' , 'LIKE' , '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->whereRelation('user' , 'mobile' , 'LIKE' , '%'.$req['mobile'].'%');
        }
        if($req['co_name'] != '*'){
            $query->where('name' , 'LIKE' , '%'.$req['co_name'].'%');
        }

        $result = $query->get();
        return response()->json($result);
    }

    public function student(Request $request)
    {
        $req = $request->obj;
        $query = StudentInfo::query();
        $query = $query->with('user');

        if($req['name'] != '*'){
            $query->whereRelation('user' , 'name' , 'LIKE' , '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->whereRelation('user' , 'family' , 'LIKE' , '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->whereRelation('user' , 'mobile' , 'LIKE' , '%'.$req['mobile'].'%');
        }
        if($req['grade'] != '*'){
            $query->where('grade' , 'LIKE' , '%'.$req['grade'].'%');
        }
        if($req['major'] != '*'){
            $query->where('major' , 'LIKE' , '%'.$req['major'].'%');
        }

        $result = $query->get();
        return response()->json($result);
    }
}

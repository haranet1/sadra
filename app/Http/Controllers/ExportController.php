<?php

namespace App\Http\Controllers;

use App\Exports\acceptedListSkillExport;
use App\Exports\AllCompaniesExport;
use App\Exports\AllEmployeeInfosExport;
use App\Exports\AllEventsExport;
use App\Exports\AllInternPositionsExport;
use App\Exports\AllStudentHasSkill;
use App\Exports\AllStudentHasSkillExport;
use App\Exports\AllStudentsExport;
use App\Exports\EventsExport;
use App\Exports\ProvinceUniConfirmedSubStudentsExport;
use App\Exports\ProvinceUniEventsExport;
use App\Exports\ProvinceUniInternPositionsExport;
use App\Exports\ProvinceUniListExport;
use App\Exports\ProvinceUniUnConfirmedSubStudentsExport;
use App\Exports\RegisteredCompaniesInEventExport;
use App\Exports\RegisteredStudentsInEventExport;
use App\Exports\unConfirmListSkillExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function allEvents()
    {
        return Excel::download(new AllEventsExport , 'all-events.xlsx');
    }

    public function allCompanies()
    {
        return Excel::download(new AllCompaniesExport , 'all-companies.xlsx');
    }

    public function allStudents()
    {
        return Excel::download(new AllStudentsExport , 'all-students.xlsx');
    }

    public function allInternPositions()
    {
        return Excel::download(new AllInternPositionsExport , 'all-intern-positions.xlsx');
    }

    public function allEmployees()
    {
        return Excel::download(new AllEmployeeInfosExport , 'all-employee.xlsx');
    }

    public function provinceUniEvents()
    {
        return Excel::download(new ProvinceUniEventsExport , 'university-events.xlsx');
    }

    public function provinceUniInternPositions()
    {
        return Excel::download(new ProvinceUniInternPositionsExport , 'university-intern-positions.xlsx');
    }

    public function provinceUniList()
    {
        return Excel::download(new ProvinceUniListExport , 'universitt-list.xlsx');
    }

    public function registeredCompaniesInEvent($event)
    {
        return Excel::download(new RegisteredCompaniesInEventExport($event) , 'companies.xlsx');
    }

    public function registeredStudentsInEvent($event)
    {
        return Excel::download(new RegisteredStudentsInEventExport($event) , 'students.xlsx');
    }

    public function provinceUniConfirmedSubStudents()
    {
        return Excel::download(new ProvinceUniConfirmedSubStudentsExport , 'confirmed-students.xlsx');
    }

    public function provinceUniUnConfirmedSubStudents()
    {
        return Excel::download(new ProvinceUniUnConfirmedSubStudentsExport , 'unconfirmed-students.xlsx');
    }

    public function AllStudentHasSkill()
    {
        return Excel::download(new AllStudentHasSkillExport , 'all-student-has-skill.xlsx');
    }

    public function unConfirmListSkill()
    {
        return Excel::download(new unConfirmListSkillExport , 'unconfirm-skill-list.xlsx');
    }

    public function acceptedListSkill()
    {
        return Excel::download(new acceptedListSkillExport , 'accepted-skill-list.xlsx');
    }
}

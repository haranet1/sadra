<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventBooth;
use App\Models\EventPlan;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
    public function index()
    {
        $plans = EventPlan::NotDeleted()
        ->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->with('event')
        ->paginate(20);
        return view('panel.admin.events.plans.index' , compact('plans'));
    }

    public function list($id)
    {
        $event = Event::find($id);
        $plans = EventPlan::NotDeleted()
        ->whereRelation('event' , 'id' , $id)
        ->with('event')
        ->paginate(20);
        
        return view('panel.admin.events.plans.index' , compact('plans' , 'event'));
    }

    public function create($id)
    {
        $event = Event::find($id);
        return view('panel.admin.events.plans.create' , compact('event'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'price'=>'numeric',
            'dimensions'=>'required|numeric',
            'tables_qty'=>'required|numeric',
            'chairs_qty'=>'required|numeric',
            'map'=>'nullable|image',
        ]);
        $price = persian_to_eng_num($request->price);
        $request->merge(['price'=>$price]);

        $plan = new EventPlan();
        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->dimensions = $request->dimensions;
        $plan->tables_qty = $request->tables_qty;
        $plan->chairs_qty = $request->chairs_qty;
        $plan->event_id = $request->event_id;
        $plan->save();

        if ($request->file('map')) {
            $name = time() . $request->map->getClientOriginalName();
            $path = 'img/events/plans';
            $file = $request->file('map');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = EventPlan::class;
            $photo->save();
            $plan->map_id = $photo->id;
            $plan->save();
        }

        return redirect()->route('list.index.plan', $request->event_id)->with('success','پلن با موفقیت اضافه شد، لطفا برای این پلن حتما غرفه تعریف کنید.');
    }

    public function edit($id)
    {
        $plan = EventPlan::with('event' , 'map')->find($id);

        return view('panel.admin.events.plans.edit' , compact('plan'));
    }

    public function update(Request $request)
    {
        $price = persian_to_eng_num($request->price);
        $request->merge(['price'=>$price]);

        $plan = EventPlan::find($request->plan_id);
        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->dimensions = $request->dimensions;
        $plan->tables_qty = $request->tables_qty;
        $plan->chairs_qty = $request->chairs_qty;
        $plan->save();
        
        if ($request->file('map')) {
            unlink($plan->map);
            $plan->map->delete();

            $name = time() . $request->map->getClientOriginalName();
            $path = 'img/events/plans';
            $file = $request->file('map');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = EventPlan::class;
            $photo->save();
            $plan->map_id = $photo->id;
            $plan->save();
        }

        return redirect()->route('list.index.plan',$plan->event->id)->with('success' , 'پلن مورد نظر شما با موفقیت ویرایش شد');
    }

    public function delete(Request $request)
    {
        $plan = EventPlan::find($request->id);
        $booths = EventBooth::where('plan_id' , $request->id)->get();
        $delete = false;
        if(count($booths)>0){
            foreach($booths as $booth){
                if($booth->reserved == false){
                    $delete = true;
                }else{
                    $delete = false;
                }
            }
        }
        if($delete == true){
            $plan->deleted = true;
            $plan->save();
            return response()->json('success');
        }else{
            return response()->json('error');
        }
    }

    public function deleteBooth($id)
    {
        $booth = EventBooth::find($id);
        if($booth->reserved == false){
            $booth->delete();
            return redirect()->back()->with('success','غرفه مورد نظر حذف شد');
        }

        return redirect()->back()->with('error' , 'غرفه مورد نظر رزور شده و غیرقابل حذف شدن است');
    }
}

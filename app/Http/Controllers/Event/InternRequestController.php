<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\InternRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InternRequestController extends Controller
{
    public function empIndex()
    {
        $requests = InternRequest::whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id)
        ->with('internPosition.event' , 'companyInfo' , 'studentInfo.user')
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('panel.admin.pooyesh.emp-index' , compact('requests'));
    }

    public function stdIndex()
    {
        $requests = InternRequest::where('student_id' , Auth::user()->groupable_id)
        ->with('internPosition.event' , 'companyInfo')
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('panel.admin.pooyesh.std-index' , compact('requests'));
    }

    public function coIndex()
    {
        $requests = InternRequest::Unconfirmed()
        ->where('company_id' , Auth::user()->groupable_id)
        ->with('internPosition.event' , 'studentInfo.user')->orderByDesc('updated_at')->paginate(20);

        $confirmedRequests = InternRequest::Confirmed()
        ->where('company_id' , Auth::user()->groupable_id)
        ->with('internPosition.event' , 'studentInfo.user')->orderByDesc('updated_at')->paginate(20);

        
        return view('panel.admin.pooyesh.co-index' , compact('requests' , 'confirmedRequests'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $internRequest = InternRequest::create([
            'intern_position_id' => $request->input('internPosition'),
            'student_id' => Auth::user()->groupable_id,
            'company_id' => $request->input('company'),
        ]);

        return redirect()->route('std-index.request.internship')->with('success' , 'درخواست پویش شما با موفقیت ثبت شد');
    }

    public function accept(Request $request)
    {
        $internRequest = InternRequest::find($request->id);
        $internRequest->status = 1;
        $internRequest->save();

        return response()->json(true);
    }

    public function decline(Request $request)
    {
        $internRequest = InternRequest::find($request->id);
        $internRequest->status = 0;
        $internRequest->save();

        return response()->json(true);
    }
}

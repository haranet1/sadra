<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\InternPosition;
use App\Models\InternRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InternShipController extends Controller
{
    public function index()
    {
        $internPositions = InternPosition::NotDeleted()
        ->with('event.university' , 'internRequests')
        ->where('company_id' , Auth::user()->groupable_id)
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('panel.admin.events.internship.index' , compact('internPositions'));
    }

    public function StdIndex()
    {
        $internPositions = InternPosition::NotDeleted()
        ->with('event.university' , 'companyInfo')
        ->whereRelation('event' , 'university_id' , Auth::user()->groupable->university_id)
        ->orderByDesc('updated_at')
        ->paginate(20);

        return view('panel.admin.events.internship.std-index' , compact('internPositions'));
    }

    public function eventIndex($id)
    {
        $event = Event::find($id);
        $internPositions = InternPosition::NotDeleted()
        ->where('event_id' , $id)
        ->where('company_id' , Auth::user()->groupable_id)
        ->with('event')
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('panel.admin.events.internship.index' , compact('internPositions' , 'event'));
    }

    public function eventStdIndex($id)
    {
        $event = Event::find($id);
        $internRequests = InternRequest::where('student_id' , Auth::user()->groupable_id)->get();
        $internPositions = InternPosition::NotDeleted()
        ->where('event_id' , $id)
        ->with('event' , 'companyInfo' , 'internRequests')
        ->orderByDesc('updated_at')
        ->paginate(20);

        return view('panel.admin.events.internship.std-index' , compact('internPositions' , 'event' , 'internRequests'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:30',
            'description' => 'required',
        ],[
            'title.required' => 'عنوان موقعیت پویش الزامی است',
            'title.max' => 'برای عنوان بیشتر از 30 کاراکتر نمی‌توانید وارد کنید',
            'description.required' => 'توضیحات الزامی است',
        ]);

        $intern = InternPosition::create([
            'company_id' => Auth::user()->groupable_id,
            'event_id' => $request->input('event'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ]);

        return redirect()->back()->with('success', 'موقعیت پویش شما با موفقیت ثبت شد');
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:30',
            'description' => 'required',
        ],[
            'title.required' => 'عنوان موقعیت پویش الزامی است',
            'title.max' => 'برای عنوان بیشتر از 30 کاراکتر نمی‌توانید وارد کنید',
            'description.required' => 'توضیحات الزامی است',
        ]);

        $internPosition = InternPosition::find($request->input('internPosition'));
        $internPosition->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ]);
        $internPosition->save();

        return redirect()->back()->with('success', 'موقعیت پویش شما با موفقیت به‌روز شد');
    }

    public function delete(Request $request)
    {
        $internPosition = InternPosition::find($request->input('internPosition'));
        $internPosition->update([
            'deleted' => 1,
        ]);
        $internPosition->save();
        return redirect()->back()->with('error', ' موقعیت پویش'. $internPosition->title .'حذف شد ');
    }

    public function list()
    {
        $internPositions = InternPosition::NotDeleted()
        ->with('companyInfo' , 'event')
        ->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->orderByDesc('created_at')
        ->paginate(20);
        
        return view('panel.admin.events.internship.list' , compact('internPositions'));
    }

    public function confirme($id)
    {
        $internPosition = InternPosition::find($id);
        $internPosition->status = 1;
        $internPosition->save();

        return redirect()->back()->with('success', 'موقعیت پویش مورد نظر تایید شد');
    }
}

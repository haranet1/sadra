<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\Event;
use App\Models\Photo;
use App\Models\StudentEvent;
use App\Models\University;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;
use PhpParser\Node\Expr\FuncCall;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::NotDeleted()
        ->whereRelation('university' ,'id' , Auth::user()->groupable->university_id)
        ->with('plans', 'photo' , 'map')
        ->orderByDesc('created_at')
        ->paginate(20);
        return view('panel.admin.events.index' , compact('events'));
    }

    public function create()
    {
        return view('panel.admin.events.create-event');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photo' => 'required|dimensions:width=1748,height=2480|max:1024',
            'map' => 'required|max:2048',
            'vertical_banner' => 'image',
            'horizontal_banner' => 'image|dimensions:width=1200,height=450|max:1024',
            'start' => 'required',
            'end' => 'required',
            'start_register' => 'required',
            'end_register' => 'required',
            'description' => 'required',
            'invitation' => 'required',
        ],[
            'title.required' => 'عنوان رویداد الزامی است',
            'photo.required' => 'تصویر الزامی است',
            'photo.dimensions' => 'تصویر انتخابی ابعاد مناسب ندارد',
            'photo.max' => 'حجم فایل انتخابی بیشتر از حد مجاز است',
            'map.required' => 'نفشه الزامی است',
            'vertical_banner.image' => 'بنر عمودی را با فرمت مناسب وارد کنید',
            'horizontal_banner.image' => 'بنر افقی را با فرمت مناسب وارد کنید',
            'horizontal_banner.dimensions' => 'بنر افقی انتخابی ابعاد مناسب ندارد',
            'horizontal_banner.max' => 'حجم فایل انتخابی بیشتر از حد مجاز است',
            'start.required' => 'تاریخ شروع رویداد الزامی است',
            'end.required' => 'تاریخ پایان رویداد الزامی است',
            'start_register.required' => 'تاریخ شروع ثبت نام الزامی است',
            'end_register.required' => 'تاریخ پایان ثبت نام الزمی است',
            'description.required' => 'توضیحات الزامی است',
            'invitation.required' => 'متن دعوتنامه الزامی است',
        ]);

        $event = new Event();
        $event->title = $request->title;
        $event->description = $request->description;
        $event->creator_id = Auth::user()->groupable->id;
        $event->university_id = Auth::user()->groupable->university->id;
        if($request->input('type') == 'free'){
            $event->is_free = true;
        }
        if (strpos($request->start, '/') !== false) {
            $startDate = explode('/', $request->start);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }

        if (strpos($request->start_register, '/') !== false) {
            $startRegDate = explode('/', $request->start_register);

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
        }

        if (strpos($request->end_register, '/') !== false) {
            $endRegDate = explode('/', $request->end_register);

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
        }

        $event->start_at = $startDateGre;
        $event->end_at = $endDateGre;
        $event->start_register_at = $startRegDateGre;
        $event->end_register_at = $endRegDateGre;
        $event->invitation = $request->invitation;
        $event->save();

        if ($request->file('photo')) {
            $name = time() . $request->photo->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('photo');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = Event::class;
            $photo->save();
            $event->photo_id = $photo->id;
            $event->save();
        }
        if ($request->file('map')) {
            $name = time() . $request->map->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('map');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = Event::class;
            $photo->save();
            $event->map_id = $photo->id;
            $event->save();
        }

        if ($request->file('vertical_banner')) {
            $name = time() . $request->vertical_banner->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('vertical_banner');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = event::class;
            $photo->save();
            $event->vertical_banner_id = $photo->id;
            $event->save();
        }
        if ($request->file('horizontal_banner')) {
            $name = time() . $request->horizontal_banner->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('horizontal_banner');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = event::class;
            $photo->save();
            $event->horizontal_banner_id = $photo->id;
            $event->save();
        }
        
        return redirect(route('create.plan',$event->id))->with('success','رویداد با موفقیت اضافه شد، لطفا برای این رویداد پلن ثبت نام تعریف کنید.');
    }

    public function single($id)
    {
        $event = Event::with(
            'photo',
            'map',
            'horizontalBanner',
            'plans',
            )->find($id);

        return view('panel.admin.events.single' , compact('event'));
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return view('panel.admin.events.edit' , compact('event'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $request->validate(['description' => 'required']);
        $event = Event::find($request->event);
        // dd($event);
        $event->title = $request->title;
        if($request->input('type') == 'free'){
            $event->is_free = true;
        }else{
            $event->is_free = false;
        }
        $event->description = $request->description;
        if (strpos($request->start, '/') !== false) {
            $startDate = explode('/', $request->start);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }
        if (strpos($request->start_register, '/') !== false) {
            $startRegDate = explode('/', $request->start_register);

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
        }

        if (strpos($request->end_register, '/') !== false) {
            $endRegDate = explode('/', $request->end_register);

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
        }
        $event->start_at = $startDateGre;
        $event->end_at = $endDateGre;
        $event->start_register_at = $startRegDateGre;
        $event->end_register_at = $endRegDateGre;
        $event->save();
        if ($request->file('photo')) {
            $name = time() . $request->photo->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('photo');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = Event::class;
            $photo->save();
            $event->photo_id = $photo->id;
            $event->save();
        }
        if ($request->file('map')) {
            $name = time() . $request->map->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('map');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = Event::class;
            $photo->save();
            $event->map_id = $photo->id;
            $event->save();
        }

        if ($request->file('vertical_banner')) {
            $name = time() . $request->vertical_banner->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('vertical_banner');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = event::class;
            $photo->save();
            $event->vertical_banner_id = $photo->id;
            $event->save();
        }
        if ($request->file('horizontal_banner')) {
            $name = time() . $request->horizontal_banner->getClientOriginalName();
            $path = 'img/events';
            $file = $request->file('horizontal_banner');
            $file->move( public_path($path) , $name);
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path.'/'.$name;
            $photo->type = event::class;
            $photo->save();
            $event->horizontal_banner_id = $photo->id;
            $event->save();
        }

        return redirect(route('index.event'))->with('success', 'رویداد با موفقیت ویرایش یافت');
    }

    public function delete(Request $request)
    {
        $event = Event::with('photo' , 'map' , 'verticalBanner' , 'horizontalBanner')
        ->find($request->event);

        if ($event->photo){
            unlink($event->photo->path);
            $event->photo->delete();
        }
        if($event->map){
            unlink($event->map->path);
            $event->map->delete();
        }
        if($event->verticalBanner){
            unlink($event->verticalBanner->path);
            $event->verticalBanner->delete();
        }
        if($event->horizontalBanner){
            unlink($event->horizontalBanner->path);
            $event->horizontalBanner->delete();
        }
        
        $event->deleted = 1;
        $event->save();
        return response()->json($event);
    }

    public function confirme(Request $request)
    {
        $event = Event::where('id' ,$request->event)->with('acceptor')->first();
        $event->acceptor_id = Auth::user()->groupable->id;
        $event->status = 1;
        $event->save();
        return response()->json('success');
    }

    public function unConfirmed()
    {
        $events = Event::Unconfirmed()
            ->whereRelation('university' , 'parent' , Auth::user()->groupable->university_id)
            ->with('university.subUnits' , 'plans.Booths')
            ->paginate(20);

        
        return view('panel.admin.events.unconfirmed' , compact(
            'events',
        ));
    }

    public function confirmed()
    {
        $events = Event::Confirmed()
            ->whereRelation('university' , 'parent' , Auth::user()->groupable->university_id)
            ->with('university.subUnits' , 'plans.Booths')
            ->paginate(20);

        return view('panel.admin.events.confirmed' , compact(
            'events',
        ));
    }

    public function cancellation(Request $request)
    {
        $event = Event::with('acceptor')->find($request->event);
        $event->acceptor_id = null;
        $event->status = null;
        $event->save();
        return response()->json('success');
    }

    // company methods 

    public function list()
    {
        $events = Event::Confirmed()
            ->with('plans.booths', 'photo' , 'map')
            ->orderByDesc('start_register_at')
            ->paginate(20);
        $companyEvent = CompanyEvent::where('company_id' , Auth::user()->groupable->id)->get();

        // dd($companyEvent);
        return view('panel.admin.events.list' , compact('events' ,'companyEvent'));
    }


    // student methods

    public function studentList()
    {
        $events = Event::Confirmed()
        ->whereRelation('university' , 'id' , Auth::user()->groupable->university->id)
        ->orderByDesc('start_register_at')
        ->with('photo' , 'map')->paginate(20);
        $studentEvent = StudentEvent::where('student_id' , Auth::user()->groupable->id)->get();
        return view('panel.admin.events.list-std' , compact('events' , 'studentEvent'));
    }
}

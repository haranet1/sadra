<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\Event;
use App\Models\Payment;
use App\Models\StudentEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    // company methods
    public function index()
    {
        $requests = CompanyEvent::where('company_id', Auth::user()->groupable->id)
        ->with('booth' , 'event' , 'payment')
        ->orderByDesc('updated_at')
        ->paginate(20);

        return view('panel.admin.events.register.company.index' , compact('requests'));
    }

    public function create(Request $request)
    {
        $event = Event::where('id' , $request->event)
        ->with('map' , 'photo')
        ->first();
        return view('panel.admin.events.register.company.create' , compact('event'));
    }

    public function store(Request $request)
    {
        // to be change

        $request->validate([
            'plan' => 'required',
            'booth' => 'required',
        ],[
            'plan.required' => 'پلن ثبت نام را انتخاب کنید',
            'booth.required' => 'غرفه مورد نظر خود را انتخاب کنید'
        ]);

        $companyEvent = CompanyEvent::create([
            'company_id' => Auth::user()->groupable->id,
            'event_id' => $request->event,
            'booth_id' => $request->booth,
        ]);
        $companyEvent->booth->update([
            'reserved' => true,
        ]);
        $companyEvent->booth->save();
        return redirect()->route('index.register.event')->with('success', 'ثبت نام با موفقیت ثبت شد.');
    }

    public function show($id)
    {
        $requests = CompanyEvent::with('event','companyInfo','companyInfo.user','booth')
        ->where('event_id' , $id)
        ->whereNull('status')
        ->orderByDesc('updated_at')
        ->paginate(20);
        
        return view('panel.admin.events.register.requests' , compact('requests' , 'id'));
    }

    public function done($id)
    {
        $requests = CompanyEvent::where('event_id', $id)
        ->whereHas('payment')->where('status' , true)
        ->with('companyInfo.logo' , 'payment' , 'event' , 'booth.plan')
        ->orderByDesc('updated_at')
        ->paginate(20);
        
        return view('panel.admin.events.register.done' , compact('requests'));
    }

    public function invitation($id)
    {
        $companyEvent = CompanyEvent::with('companyInfo.logo' , 'event.university')->find($id);
        return view('panel.admin.events.pay.invitation' , compact('companyEvent'));
    }

    // student methods

    public function studentIndex()
    {
        $requests = StudentEvent::where('student_id' , Auth::user()->groupable->id)
        ->with('event')
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('panel.admin.events.register.student.index' , compact('requests'));
    }

    public function studentStore(Request $request)
    {
        $studentEvent = StudentEvent::create([
            'student_id' => Auth::user()->groupable->id,
            'event_id' => $request->event,
        ]);
        
        return redirect()->route('index-std.event')->with('success' , 'ثبت نام با موفقیت انجام شد');
    }

    public function showStudents($id)
    {
        $requests = StudentEvent::with('event' , 'studentInfo.user' , 'studentInfo.internRequests')
        ->where('event_id' , $id)->orderByDesc('updated_at')->whereNull('status')->paginate(20);
        
        return view('panel.admin.events.register.employee.show-students' , compact('requests' , 'id'));
    }

    public function stdInvitation($id)
    {
        $studentEvent = StudentEvent::with('studentInfo' , 'event.university')->find($id);
        
        return view('panel.admin.events.pay.std-invitation' , compact('studentEvent'));
    }
}

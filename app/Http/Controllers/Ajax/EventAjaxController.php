<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\EventBooth;
use App\Models\EventPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventAjaxController extends Controller
{
    public function addBooth(Request $request)
    {
        $names = explode('.' , $request->name);
        $array = [];
        foreach($names as $item){
            array_push($array , ['plan_id' => $request->plan_id , 'name' => $item , 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()]);
        }
        return response()->json( EventBooth::insert($array));
    }

    public function getBoothByPlanId(Request $request)
    {
        $booths = EventBooth::where('plan_id', $request->id)->where('reserved', false)->get();
        $plan = EventPlan::with('map')->find($request->id);
        $map = null;
        if($plan->map){
            $map = $plan->map->path;
        }
        if(count($booths) > 0){
            return response()->json([
                'booths' => $booths,
                'map' => $map,
                'price' => $plan->price
            ], 200);
        }else return response()->json([false,]);
    }
}

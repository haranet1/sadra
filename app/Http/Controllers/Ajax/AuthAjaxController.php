<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthAjaxController extends Controller
{
    public function getCities(Request $request)
    {   
        $cities = DB::table('cities')->where('province', $request->province)->get(['name', 'id']);
        return response()->json($cities);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\AdminInfo;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function verifyMobileShow()
    {
        return view('auth.verify-mobile');
    }

    public function verifyMobile(Request $request) 
    {
        $user = Auth::user();
        if ($request->code == $user->mobile_code) {
            $user->update([
                'mobile_verified_at' => Carbon::now(),
            ]);
            if($user->groupable_type == StudentInfo::class){
                return redirect(route('set.std.info'));
            }elseif($user->groupable_type == CompanyInfo::class){
                return redirect(route('set.co.info'));
            }elseif($user->groupable_type == AdminInfo::class){
                return redirect(route('dashboard'));
            }elseif($user->groupable_type == EmployeeInfo::class){
                return redirect(route('set.emp.info'));
            }

        } else {
            Session::put('error', 'کد وارد شده صحیح نمی باشد');
            return response()->view('auth.verify-mobile');
        }
    }

    public function confirm(Request $request)
    {
        $user = User::find($request->id);
        $user->status = 1;
        return response()->json($user->save());
    }
}

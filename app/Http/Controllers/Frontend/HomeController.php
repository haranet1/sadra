<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\News;
use App\Models\StudentInfo;
use App\Models\University;
use App\Models\User;

class HomeController extends Controller
{

    protected $startDate;
    protected $endDate;

    public function setDate()
    {
        $startDate = verta();
        $startDate->year = verta()->now()->year;
        $startDate->month = 1;
        $startDate->day = 1;
        
        $endDate = verta();
        $endDate->year = verta()->now()->year;
        $endDate->month = 12;
        $endDate->day = 29;
        
        $this->startDate = $startDate->toCarbon();
        $this->endDate = $endDate->toCarbon();
    }

    public function index()
    {
        $events = $this->slider();
        $News = $this->newsSlider();
        $activeEvents = $this->activeExhibitions();
        $futureEvents = $this->futureExhibitions();
        $publicChart = $this->publicChart();

        $students=StudentInfo::all()->count();
        $coms=CompanyInfo::all()->count();
        $eventsCount=Event::confirmed()->notDeleted()->count();
        $pooyeshCount=Event::all()->count();
        // dd($publicChart);
        return view('frontend.home.index' , compact('events' , 'activeEvents' , 'News' , 'publicChart','students','coms','eventsCount','pooyeshCount','futureEvents'));
    }

    public function slider()
    {
        $events = Event::NotDeleted()
        ->Confirmed()
        ->with('horizontalBanner', 'photo' , 'university' , 'creator.user')
        ->orderByDesc('created_at')
        ->take(10)
        ->get();
        return $events;
    }

    public function activeExhibitions()
    {
        $startDate = verta()->startMonth()->toCarbon();
        $endDate = verta()->endMonth()->toCarbon();
        $events = Event::NotDeleted()
        ->Confirmed()
        ->with('horizontalBanner', 'photo' , 'university' , 'creator.user')
        ->whereBetween('start_at', [$startDate , $endDate])
        ->take(6)
        ->get();
        // dd($events);
        return $events;
    }

    public function futureExhibitions()
    {
        $endDate = verta()->endMonth()->toCarbon();
        $events = Event::NotDeleted()
            ->Confirmed()
            ->with('horizontalBanner', 'photo' , 'university' , 'creator.user')
            ->where('start_at' ,'>' , $endDate)
            ->orderByDesc('created_at')
            ->take(10)
            ->get();
        return $events;
    }

    public function newsSlider()
    {
        return News::NotDeleted()
        ->Confirmed()
        ->with('photo' , 'event' , 'university')
        ->orderByDesc('updated_at')
        ->take(9)
        ->get();
    }


    // news methods 

    public function news()
    {
        $events = $this->slider();
        $News = News::NotDeleted()
        ->Confirmed()
        ->with('photo', 'event' , 'university')
        ->orderByDesc('updated_at')
        ->paginate(12);
        
        return view('frontend.news' , compact('events' , 'News'));
    }

    public function events()
    {
        $events = Event::NotDeleted()
        ->Confirmed()
        ->orderByDesc('start_at')
        ->paginate(12);

        return view('frontend.events' , compact('events'));
    }

    public function serviceIncentives()
    {
        return view('frontend.service-incentives');
    }

    public function publicChart()
    {
        $this->setDate();
        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')->first();
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($University->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            
            $counter = 0;
            if(count($provinceal->events)>0){
                foreach($provinceal->events as $event){
                    if(($event->status == true && $event->deleted == false) && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
            }else{
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data =  $counter + provincealEvents($provinceal->subUnits,$this->endDate,$this->startDate);
            }
            array_push($array, [$p_name , $p_event_data]);
        }
        usort($array, function ($a, $b) {
            return $b[1] - $a[1];
        });
        return $array;
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Frontend\HomeController;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\InternPosition;
use App\Models\InternRequest;
use App\Models\News;
use App\Models\StudentInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SinglePageController extends Controller
{
    public function company($id)
    {
        $img = rand(1,7);
        $company = CompanyInfo::where('id',Auth::user()->groupable_id)->with('user','events','logo','header')->firstOrFail();

        $internPositions = InternPosition::NotDeleted()
        ->Confirmed()
        ->where('company_id' , $id)
        ->with('event')
        ->orderByDesc('updated_at')
        ->paginate(10);
        $studentCount = StudentInfo::count();
        $eventCount = Event::count();
        $internCount = InternPosition::count();
        $requestCount = InternRequest::where('company_id', $id)->count();
        $internRequests = InternRequest::where('company_id', $id)->get();

        return view('panel.admin.single.company-page' ,
        compact('company' , 'img' , 'internPositions' ,'studentCount','eventCount','internCount','requestCount' , 'internRequests'));
    }

    public function student($id)
    {
        $img = rand(1,4);
        $student = StudentInfo::where('id',Auth::user()->groupable_id)->with([
            'user',
            'university',
            'events',
            'skills' => function ($query) {
                $query->notDeleted()->with('academy')->orderByDesc('end_date');
            }
        ])->firstOrFail();

        $unpaidSkills = array();

        foreach($student->skills as $skill){
            if($skill->status == 0){
                array_push($unpaidSkills , $skill);
            }
        }

        $companyCount = CompanyInfo::count();
        $eventCount = Event::count();
        $internCount = InternPosition::count();
        $requestCount = InternRequest::where('student_id', $id)->count();

        return view('panel.admin.single.student-page',
        compact('student' ,'img' ,'companyCount' ,'eventCount','internCount','requestCount','unpaidSkills'));
    }

    public function event($id)
    {
        $event = Event::with('university' , 'photo' , 'map' , 'verticalBanner' , 'horizontalBanner' , 'creator.user' , 'news.photo')
        ->find($id);

        $events = new HomeController();
        $events = $events->slider();

        if($event->university->id == 35){
            $najafAbadEvent = true;
        }

        return view('frontend.event-single' , compact('event' , 'events'));
    }

    public function news($id) # get id
    {
        $news = News::NotDeleted()
        ->Confirmed()
        ->with('photo' , 'event' , 'university')
        ->find($id);

        if(!is_null($news)){
            $news->counter++;
            $news->save();
        }

        return view('frontend.news-single' , compact('news'));
    }
}

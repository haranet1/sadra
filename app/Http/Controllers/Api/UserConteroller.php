<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserConteroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $userExist = User::where('mobile' , $request->mobile)->first();

        if(! is_null($userExist)){
            
            $userExist->tokens()->delete();

            return response()->json([
                'message' => 'user is already exist',
                'token' => $userExist->createToken($userExist->mobile)->plainTextToken
            ]);

        }

        $validator = validator($request->all() , [
            'name' => ['required', 'string', 'max:30'],
            'family' => ['required', 'string', 'max:40'],
            'sex' => ['required'],
            'n_code' => ['required' , 'integer'],
            'birth' => 'required|date',
            'mobile' => ['required' ,'numeric', 'digits:11'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role' => ['required'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $birth = persian_to_eng_date($request->birth);
        $mobile = persian_to_eng_num($request->mobile);
        $n_code = persian_to_eng_num($request->n_code);

        $request->merge([
            'birth' => $birth,
            'mobile' => $mobile,
            'n_code' => $n_code,
        ]);

        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'sex' => $request->sex,
            'n_code' => $request->n_code,
            'birth' => $request->birth,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if($request->role == 'student'){
            $user->groupable_type = StudentInfo::class;
            $user->save();
            $token = $user->createToken($request->mobile);

            return response()->json([
                'message' => 'create student is Done!',
                'token' => $token->plainTextToken
            ] , 200);
        }elseif($request->role == 'company'){
            $user->groupable_type = CompanyInfo::class;
            $user->save();
            $token = $user->createToken($request->mobile);

            return response()->json([
                'message' => 'create company is Done!',
                'token' => $token->plainTextToken
            ] , 200);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function getToken(Request $request)
    {
        $validator = validator($request->all() , [
            'mobile' => ['required' ,'numeric', 'digits:11']
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $user = User::where('mobile' , $request->mobile)->where('password' , $request->password)->first();

        if(is_null($user)){
            return response()->json([
                'massage' => 'user not found'
            ], 404);
        }

        $user->tokens()->delete();

        return response()->json([
            'massage' => 'ok',
            'token' => $user->createToken($request->mobile)->plainTextToken
        ]);
        
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\EventBooth;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    public function getEventId()
    {
        return response()->json([
            'massage' => 'ok',
            'data' => [
                'event_id' => Event::whereRelation('university' , 'id' , 35)
                                    ->where('start_register_at' ,'<=', now())
                                    ->where('end_register_at' , '>=' , now())
                                    ->with('plans.booths')
                                    ->first()
            ],
        ] , 200);
    }

    public function companyEvent(Request $request)
    {
        if(Auth::user()->groupable_type != CompanyInfo::class){
            return response()->json([
                'message' => 'user not company!',
            ] , 404);
        }

        $validator = Validator::make($request->all() , [
            'company_id' => 'required',
            'event_id' => 'required',
            'booth_name' => 'required|exists:event_booths,name',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $company = CompanyInfo::with('user')
                        ->find($request->company_id);

        $event = Event::with('plans.booths')
                        ->find($request->event_id);

        $booth = EventBooth::where('name' , $request->booth_name)
                        ->with('plan.event')
                        ->first();

        if($booth->reserved == 1){
            return response()->json([
                'message' => 'your booth reserved!',
            ] , 404);
        }

        $companyEvent = CompanyEvent::create([
            'company_id' => $company->id,
            'event_id' => $event->id,
            'booth_id' => $booth->id
        ]);

        $companyEvent->booth->reserved = 1;
        $companyEvent->booth->save();

        return response()->json([
            'message' => 'OK',
            'data' => [
                'company_event_id' => $companyEvent->id
            ]
        ] , 200);
    }

    public function companyPay(Request $request)
    {
        $validator = validator($request->all() , [
            'company_event_id' => 'required',
            'price' => 'required',
            'status' => 'required',
            'tracking_code' => 'required',
            'receipt' => 'required',
            'card_number' => 'required',
            'payer_bank' => 'required',
            'payment_date_time' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $payment = Payment::updateOrCreate([
            'user_info_id' => Auth::user()->groupable->id , 'paymentable_id'=> $request->company_event_id
        ],[
            'user_info_id' => Auth::user()->groupable->id,
            'user_info_type' => Auth::user()->groupable_type,
            'paymentable_id' => $request->company_event_id,
            'paymentable_type' => CompanyEvent::class,
            'price' => $request->price,
            'getway' => 'iaun-getway',
            'status' => $request->status,
            'tracking_code' => $request->tracking_code,
            'receipt' => $request->receipt,
            'card_number' => $request->card_number,
            'payer_bank' => $request->payer_bank,
            'payment_date_time' => $request->payment_date_time,
        ]);

        $payment->paymentable->update([
            'status' => true,
        ]);
        $payment->paymentable->save();
        
        return response()->json([
            'message' => 'OK',
        ] , 200);
    }
}

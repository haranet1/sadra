<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\StudentEvent;
use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function store(Request $request)
    {
        if(Auth::user()->groupable_type != StudentInfo::class){
            return response()->json([
                'message' => 'user not student!',
            ] , 404);
        }

        $validator = validator($request->all() , [
            'province' => 'required',
            'city' => 'required',
            'university_id' => 'required',
            'student_code' => 'required|numeric',
            'grade' => 'required',
            'major' => 'required',
            'avg' => 'required',
            'end_date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $about = null;
        if($request->about){
            $about = $request->about;
        }

        $info = StudentInfo::create([
            'university_id' => $request->university_id,
            'student_code' => $request->student_code,
            'grade' => $request->grade,
            'major' => $request->major,
            'avg' => $request->avg,
            'end_date' => $request->end_date,
            'about' => $about,
        ]);

        $user = Auth::user();
        $user->groupable_id = $info->id;
        $user->province_id = $request->province;
        $user->city_id = $request->city;
        $user->save();

        return response()->json([
            'message' => 'OK'
        ] , 200);
    }

    public function studentEvent(Request $request)
    {
        
    }

    public function storeStudentFullProcess(Request $request)
    {
        $validator = validator($request->all() , [
            'user_name' => 'required|string|max:30',
            'user_family' => 'required|string|max:40',
            'user_sex' => 'required',
            'user_n_code' => 'required|integer',
            'user_birth' => 'required|date',
            'user_mobile' => 'required|numeric|digits:11',
            'user_email' => 'required|string|email|max:255',
            'user_role' => 'required',
            'user_password' => 'required|string|min:8',

            'std_province_id' => 'required',
            'std_city_id' => 'required',
            'std_student_code' => 'required|numeric',
            'std_grade' => 'required',
            'std_major' => 'required',
            'std_avg' => 'required',
            'std_end_date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $userExist = User::where('mobile' , $request->user_mobile)->first();

        if(! is_null($userExist)){

            $userCreated = $userExist;

        } else {

            $userCreated = $this->storeUser($request);

        }

        if($userCreated->groupable && $userCreated->groupable_type == StudentInfo::class){

            $studentCreated = $userCreated;

        } else {

            $studentCreated = $this->storeStudent($userCreated->id , $request);

        }

        $event = $this->getEvent();

        $studenEventExist = StudentEvent::where('student_id' , $studentCreated->groupable->id)
            ->where('event_id' , $event->id)->first();

        if(! is_null($studenEventExist)) {

            $studentEvent = $studenEventExist;

        } else {

            $studentEvent = StudentEvent::create([
                'student_id' => $studentCreated->groupable->id,
                'event_id' => $event->id,
            ]);

        }

        return response()->json([
            'message' => 'OK',
        ] , 200);

    }

    protected function storeUser($request)
    {
        $birth = persian_to_eng_date($request->user_birth);
        $mobile = persian_to_eng_num($request->user_mobile);
        $n_code = persian_to_eng_num($request->user_n_code);

        $request->merge([
            'user_birth' => $birth,
            'user_mobile' => $mobile,
            'user_n_code' => $n_code,
        ]);

        $user = User::create([
            'name' => $request->user_name,
            'family' => $request->user_family,
            'sex' => $request->user_sex,
            'n_code' => $request->user_n_code,
            'birth' => $request->user_birth,
            'mobile' => $request->user_mobile,
            'email' => $request->user_email,
            'password' => $request->user_password,
        ]);

        if ($request->user_role == 'student'){

            $user->groupable_type = StudentInfo::class;
            $user->save();

        } elseif ($request->user_role == 'company') {

            $user->groupable_type = CompanyInfo::class;
            $user->save();

        }

        return $user;
    }

    protected function storeStudent($suer_id , $request)
    {
        $info = StudentInfo::create([
            'university_id' => 35,
            'student_code' => $request->std_student_code,
            'grade' => $request->std_grade,
            'major' => $request->std_major,
            'avg' => $request->std_avg,
            'end_date' => $request->std_end_date,
        ]);

        $user = User::find($suer_id);
        $user->groupable_id = $info->id;
        $user->province_id = $request->std_province_id;
        $user->city_id = $request->std_city_id;
        $user->save();

        return $user;
    }

    protected function getEvent()
    {
        return Event::whereRelation('university' , 'id' , 35)
            ->where('start_register_at' ,'<=', now())
            ->where('end_register_at' , '>=' , now())
            ->with('plans.booths')
            ->first();
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\EventBooth;
use App\Models\Payment;
use App\Models\Photo;
use App\Models\StudentInfo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDO;

class CompanyController extends Controller
{
    public function store(Request $request)
    {
        if(Auth::user()->groupable_type != CompanyInfo::class){
            return response()->json([
                'message' => 'user not company!',
            ] , 404);
        }

        if(Auth::user()->groupable){
            return response()->json([
                'message' => 'company has info!',
                'data' => [
                    'company_id' => Auth::user()->groupable_id
                ]
            ] , 200);
        }

        $validator = validator($request->all() , [
            'name' => 'required|max:50',
            'n_code' => 'required|numeric',
            'registration_number' => 'required|numeric',
            'co_phone' => 'required|numeric|unique:'.CompanyInfo::class,
            'activity_field' => 'required',
            'year' => 'required|date',
            'ceo' => 'required',
            'co_email' => 'required|email',
            'about' => 'required',
            'logo' => 'required',
            'header' => 'mimes:jpeg,jpg,png|max:2048',
            'province' => 'required',
            'city' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $n_code = persian_to_eng_num($request->n_code);
        $registration_number = persian_to_eng_num($request->registration_number);
        $co_phone = persian_to_eng_num($request->co_phone);
        $year = persian_to_eng_date($request->year);

        $request->merge([
            'n-code' => $n_code,
            'registration_number' => $registration_number,
            'co_phone' => $co_phone,
            'year' => $year,

        ]);

        $website = null;
        if(isset($request->website)){
            $website = $request->website;
        }
        $address = null;
        if(isset($request->address)){
            $address = $request->address;
        }
        
        $logo = null;
        if($request->logo){
            $name = time() . $request->logo->getClientOriginalName();
            $path = 'img/company';
            $file = $request->file('logo');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);

            $logo = $photo->id;
        }

        $info = CompanyInfo::create([
            'name' => $request->name,
            'n_code' => $request->n_code,
            'registration_number' => $request->registration_number,
            'co_phone' => $request->co_phone,
            'activity_field' => $request->activity_field,
            'year' => $request->year,
            'ceo' => $request->ceo,
            'website' => $website,
            'co_email' => $request->co_email,
            'about' => $request->about,
            'address' => $address,
            'logo_id' => $logo,
            'province_id' => $request->province,
            'city_id' => $request->city,
        ]); 

        $user = Auth::user();
        $user->groupable_id = $info->id;
        $user->province_id = $request->province;
        $user->city_id = $request->city;
        $user->save();

        return response()->json([
            'message' => 'OK',
            'data' => [
                'company_id' => Auth::user()->groupable_id,
            ]
        ] , 200);
    }

    public function getCompany(Request $request)
    {
        return response()->json([
            'message' => 'OK',
            'data' => [
                'company_id' => Auth::user()->groupable_id,
            ]
        ] , 200);
    }
    
    public function storeCompanyFullProcess(Request $request)
    {
        $validator = validator($request->all() , [
            'user_name' => 'required|string|max:30',
            'user_family' => 'required|string|max:40',
            'user_sex' => 'required',
            'user_n_code' => 'required|integer',
            'user_birth' => 'required|date',
            'user_mobile' => 'required|numeric|digits:11',
            'user_email' => 'required|string|email|max:255',
            'user_role' => 'required',
            'user_password' => 'required|string|min:8',

            'co_name' => 'required|max:50',
            'co_n_code' => 'required|numeric',
            'co_registration_number' => 'required|numeric',
            'co_phone' => 'required|numeric',
            'co_activity_field' => 'required',
            'co_year' => 'required|date',
            'co_ceo' => 'required',
            'co_email' => 'required|email',
            'co_about' => 'required',
            'co_logo' => 'required',
            'co_province_id' => 'required',
            'co_city_id' => 'required',

            'booth_name' => 'required',

            'price' => 'required',
            'status' => 'required',
            'tracking_code' => 'required',
            'receipt' => 'required',
            'card_number' => 'required',
            'payer_bank' => 'required',
            'payment_date_time' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ] , 422);
        }

        $userExist = User::where('mobile' , $request->user_mobile)->first();

        if(! is_null($userExist)){

            $userCreated = $userExist;

        } else {

            $userCreated = $this->storeUser($request);

        }

        if($userCreated->groupable && $userCreated->groupable_type == CompanyInfo::class){
           
            $companyCreated = $userCreated;

        } else {

            $companyCreated = $this->storeCompany($userCreated->id , $request);

        }

        $event = $this->getEvent();

        $booth = EventBooth::where('name' , $request->booth_name)
            ->whereRelation('plan' , 'event_id' , $event->id)
            ->with('plan.event')
            ->first();

        if(is_null($booth)){

            return response()->json([
                'message' => 'booth_not_found'
            ], 404);

        }

        if($booth->reserved == 1){
            return response()->json([
                'error' => 'your_booth_reserved',
            ] , 404);
        }

        $companyEventExist = CompanyEvent::where('company_id' , $companyCreated->groupable->id)
            ->where('event_id' , $event->id)->first();

        if (! is_null($companyEventExist)) {

            $companyEvent = $companyEventExist;

        } else {

            $companyEvent = CompanyEvent::create([
                'company_id' => $companyCreated->groupable->id,
                'event_id' => $event->id,
                'booth_id' => $booth->id
            ]);

        }

        $companyEvent->booth->reserved = 1;
        $companyEvent->booth->save();

        $payment = Payment::updateOrCreate([
            'user_info_id' => $companyCreated->groupable->id , 'paymentable_id'=> $companyEvent->id
        ],[
            'user_info_id' => $companyCreated->groupable->id,
            'user_info_type' => $userCreated->groupable_type,
            'paymentable_id' => $companyEvent->id,
            'paymentable_type' => CompanyEvent::class,
            'price' => $request->price,
            'getway' => 'iaun-getway',
            'status' => $request->status,
            'tracking_code' => $request->tracking_code,
            'receipt' => $request->receipt,
            'card_number' => $request->card_number,
            'payer_bank' => $request->payer_bank,
            'payment_date_time' => $request->payment_date_time,
        ]);

        $payment->paymentable->update([
            'status' => true,
        ]);
        $payment->paymentable->save();

        return response()->json([
            'message' => 'OK',
        ] , 200);
    }

    protected function storeUser($request)
    {
        $birth = persian_to_eng_date($request->user_birth);
        $mobile = persian_to_eng_num($request->user_mobile);
        $n_code = persian_to_eng_num($request->user_n_code);

        $request->merge([
            'user_birth' => $birth,
            'user_mobile' => $mobile,
            'user_n_code' => $n_code,
        ]);

        $userExist = User::where('email' , $request->user_email)->first();

        if(! is_null($userExist)){

            $request->user_email = 'a'.$request->user_email;

        }

        $user = User::create([
            'name' => $request->user_name,
            'family' => $request->user_family,
            'sex' => $request->user_sex,
            'n_code' => $request->user_n_code,
            'birth' => $request->user_birth,
            'mobile' => $request->user_mobile,
            'email' => $request->user_email,
            'password' => $request->user_password,
        ]);

        if ($request->user_role == 'student'){

            $user->groupable_type = StudentInfo::class;
            $user->save();

        } elseif ($request->user_role == 'company') {

            $user->groupable_type = CompanyInfo::class;
            $user->save();

        }

        return $user;
    }

    protected function storeCompany($user_id , $request)
    {
        $n_code = persian_to_eng_num($request->co_n_code);
        $registration_number = persian_to_eng_num($request->co_registration_number);
        $co_phone = persian_to_eng_num($request->co_phone);
        $year = persian_to_eng_date($request->co_year);

        $request->merge([
            'co_n-code' => $n_code,
            'co_registration_number' => $registration_number,
            'co_phone' => $co_phone,
            'co_year' => $year,

        ]);

        $logo = null;
        if($request->co_logo){
            $name = time() . $request->co_logo->getClientOriginalName();
            $path = 'img/company';
            $file = $request->co_logo;
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);

            $logo = $photo->id;
        }

        $companyPhoneExist = CompanyInfo::where('co_phone' , $request->co_phone)->first();

        if (! is_null($companyPhoneExist)) {

            $request->co_phone = $request->co_phone.'0';

        }

        $companyEmailExist = CompanyInfo::where('co_email' , $request->co_email)->first();

        if (! is_null($companyEmailExist)) {

            $request->co_email = 'a'.$request->co_email;

        }

        $info = CompanyInfo::create([
            'name' => $request->co_name,
            'n_code' => $request->co_n_code,
            'registration_number' => $request->co_registration_number,
            'co_phone' => $request->co_phone,
            'activity_field' => $request->co_activity_field,
            'year' => $request->co_year,
            'ceo' => $request->co_ceo,
            'co_email' => $request->co_email,
            'about' => $request->co_about,
            'logo_id' => $logo,
            'province_id' => $request->co_province_id,
            'city_id' => $request->co_city_id,
        ]);

        $user = User::find($user_id);

        $user->groupable_id = $info->id;
        $user->province_id = $request->co_province_id;
        $user->city_id = $request->co_city_id;
        $user->save();


        return $user;
    }

    protected function getEvent()
    {
        return Event::whereRelation('university' , 'id' , 35)
            ->where('start_register_at' ,'<=', Carbon::now())
            ->where('end_register_at' , '>=' , Carbon::now())
            ->with('plans.booths')
            ->first();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\InternRequest;
use App\Models\StudentEvent;
use App\Models\StudentInfo;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnitController extends Controller
{
    
    public static $freeEvents;
    public static $priceEvents;

    public static $freeDoneEvents;
    public static $priceDoneEvents;

    public static $freeUnDoneEvents;
    public static $priceUnDoneEvents;

    public static $allCompanies;
    public static $companyHasEvent;

    public static $allStudents;
    public static $studentsHasEvent;
    
    public static $acceptInternRequests;
    public static $declinedInternRequests;
    public static $pendingInternRequests;

    public static $MixEvent;
    public static $MixCompany;
    public static $MixStudent;

    protected $startDate;
    protected $endDate;

    public function setDate()
    {
        $startDate = verta();
        $startDate->year = verta()->now()->year;
        $startDate->month = 1;
        $startDate->day = 1;
        
        $endDate = verta();
        $endDate->year = verta()->now()->year;
        $endDate->month = 12;
        $endDate->day = 29;
        
        $this->startDate = $startDate->toCarbon();
        $this->endDate = $endDate->toCarbon();
    }

    public function show()
    {
        $this->setDate();
        $this->pageHeader();
        $this->mixCart();
        $price_events = $this->price_events();
        $free_events = $this->free_events();
        $hamposhani = $this->hamposhani();
        $companiesAll = CompanyInfo::count();
        $internRequests = InternRequest::whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id)
        ->count();
        $internAcceptedRequest = InternRequest::Confirmed()
        ->whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id)
        ->count();
        $internDeclinedRequest = InternRequest::Declined()
        ->whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id)
        ->count();

        return view('panel.admin.unit-dashboard' , compact(
            'companiesAll', 'hamposhani','price_events','free_events',
            'internRequests','internAcceptedRequest','internDeclinedRequest',
        ));
    }

    public function pageHeader()
    {
        $this::$freeEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsFree()->get();
        $this::$priceEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsPrice()->get();

        $this::$freeDoneEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsFree()->HasFiveNews()->get();
        $this::$priceDoneEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsPrice()->HasFiveNews()->get();

        $this::$freeUnDoneEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsFree()->HasntFiveNews()->get();
        $this::$priceUnDoneEvents = Event::NotDeleted()->Confirmed()->ThisUniversity()->IsPrice()->HasntFiveNews()->get();

        $this::$allCompanies = CompanyInfo::ThisUniversityCity()->get();
        $this::$companyHasEvent = CompanyInfo::ThisUniversityCity()->HasEvents()->get();

        $this::$allStudents = StudentInfo::InThisUniversity()->get();
        $this::$studentsHasEvent = StudentInfo::InThisUniversity()->HasEvents()->get();

        $this::$acceptInternRequests = InternRequest::Confirmed()->InThisUniversity()->get();
        $this::$declinedInternRequests = InternRequest::Declined()->InThisUniversity()->get();
        $this::$pendingInternRequests = InternRequest::Unconfirmed()->InThisUniversity()->get();
    }

    public function price_events()
    {
        $pUniversity = University::with('subUnits.events')->find(Auth::user()->groupable->university->parent);
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($pUniversity->subUnits as $unit){
            $p_name = $unit->name;
            if(count($unit->events)>0){
                $counter = 0;
                foreach($unit->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_event_data =  $counter;
            }else{
                $p_event_data = 0;
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => $p_name,
            ];
        }
        
        usort($array, function ($a, $b) {
            return $b['y'] - $a['y'];
        });

        return $array;
    }

    public function free_events()
    {
        $pUniversity = University::with('subUnits.events')->find(Auth::user()->groupable->university->parent);
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($pUniversity->subUnits as $unit){
            $p_name = $unit->name;
            if(count($unit->events)>0){
                $counter = 0;
                foreach($unit->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_event_data =  $counter;
            }else{
                $p_event_data = 0;
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => $p_name,
            ];
        }
        
        usort($array, function ($a, $b) {
            return $b['y'] - $a['y'];
        });

        return $array;
    }

    public function hamposhani()
    {
        $companyEvents = CompanyEvent::whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->get();
        $hamposhani = 0;
        $companyId = [];
        foreach($companyEvents as $coevent){
            if(in_array($coevent->companyInfo->id,$companyId)){
                $hamposhani++;
            }else{
                array_push($companyId , $coevent->companyInfo->id);
            }
        }
        return $hamposhani;
    }

    public function mixCart()
    {
        $mix_events = array_fill(0,12,0);
        $mix_companies = array_fill(0,12,0);    
        $mix_students = array_fill(0,12,0);

        $thisYearEvents = Event::ThisUniversity()->whereBetween('start_at' , [$this->startDate , $this->endDate])->get();
        foreach($thisYearEvents as $event){
            switch(verta($event->start_at)->month){
                case 1: $mix_events[0]++; break;
                case 2: $mix_events[1]++; break;
                case 3: $mix_events[2]++; break;
                case 4: $mix_events[3]++; break;
                case 5: $mix_events[4]++; break;
                case 6: $mix_events[5]++; break;
                case 7: $mix_events[6]++; break;
                case 8: $mix_events[7]++; break;
                case 9: $mix_events[8]++; break;
                case 10: $mix_events[9]++; break;
                case 11: $mix_events[10]++; break;
                case 12: $mix_events[11]++; break;
            }
        }
        $thisYearCompanies = CompanyEvent::InThisUniversity()->with('event')->whereBetween('created_at' , [$this->startDate , $this->endDate])->get();
        foreach($thisYearCompanies as $company){
            if($company->event->start_at <= $this->endDate && $company->event->start_at >= $this->startDate){
                switch(verta($company->event->start_at)->month){
                    case 1: $mix_companies[0]++; break;
                    case 2: $mix_companies[1]++; break;
                    case 3: $mix_companies[2]++; break;
                    case 4: $mix_companies[3]++; break;
                    case 5: $mix_companies[4]++; break;
                    case 6: $mix_companies[5]++; break;
                    case 7: $mix_companies[6]++; break;
                    case 8: $mix_companies[7]++; break;
                    case 9: $mix_companies[8]++; break;
                    case 10: $mix_companies[9]++; break;
                    case 11: $mix_companies[10]++; break;
                    case 12: $mix_companies[11]++; break;
                }
            }
        }
        $thisYearStudents = StudentEvent::InThisUniversity()->with('event')->whereBetween('created_at' , [$this->startDate , $this->endDate])->get();
        foreach($thisYearStudents as $student){
            if($student->event->start_at <= $this->endDate && $student->event->start_at >= $this->startDate){
                switch(verta($student->event->start_at)->month){
                    case 1: $mix_students[0]++; break;
                    case 2: $mix_students[1]++; break;
                    case 3: $mix_students[2]++; break;
                    case 4: $mix_students[3]++; break;
                    case 5: $mix_students[4]++; break;
                    case 6: $mix_students[5]++; break;
                    case 7: $mix_students[6]++; break;
                    case 8: $mix_students[7]++; break;
                    case 9: $mix_students[8]++; break;
                    case 10: $mix_students[9]++; break;
                    case 11: $mix_students[10]++; break;
                    case 12: $mix_students[11]++; break;
                }
            }
        }
        $this::$MixEvent = $mix_events;
        $this::$MixCompany = $mix_companies;
        $this::$MixStudent = $mix_students;
    }
    
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanyEvent;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\Event;
use App\Models\EventPlan;
use App\Models\InternPosition;
use App\Models\InternRequest;
use App\Models\Payment;
use App\Models\Photo;
use App\Models\Province;
use App\Models\StudentEvent;
use App\Models\StudentInfo;
use App\Models\University;
use App\Models\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;
use PDO;

class AdminIndexController extends Controller
{
    public static $thisUniversity;

    public static $freeEvents;
    public static $priceEvents;

    public static $freeDoneEvents;
    public static $priceDoneEvents;

    public static $freeUnDoneEvents;
    public static $priceUnDoneEvents;

    public static $universityEvent;

    public static $allCompanies;
    public static $companyHasEvent;

    public static $allStudents;
    public static $studentsHasEvent;

    public static $internRequests;

    public static $acceptInternRequests;
    public static $declinedInternRequests;
    public static $pendingInternRequests;

    public static $c_MixEvent;
    public static $c_MixCompany;
    public static $c_MixStudent;


    protected $startDate;
    protected $endDate;

    public function setDate()
    {
        $startDate = verta();
        $startDate->year = verta()->now()->year;
        $startDate->month = 1;
        $startDate->day = 1;
        
        $endDate = verta();
        $endDate->year = verta()->now()->year;
        $endDate->month = 12;
        $endDate->day = 29;
        
        $this->startDate = $startDate->toCarbon();
        $this->endDate = $endDate->toCarbon();
    }

    public function show()
    {
        $this->setDate();
        $this::$thisUniversity = University::Country()->first();
        // page header 
        $this->pageHeader();
        $this->cMixChart();
        // the best 
        $bestUniEvent = $this->BestUniInEvents();
        $bestUniCompany = $this->bestUniInRegisteredCompany();
        $bestUniStudent = $this->bestUniInRegisteredStedent();
        $bestUniInternPosition = $this->bestUniInInternPosition();
        
        $c_provincesEvents = $this->priceEvent();
        $price_drilldown = $this->priceDrilldown();
        $c_free_events = $this->freeEvent();
        $free_drilldown = $this->freeDrilldown();

        $future_price_events = $this->futurePriceEvent();
        $future_price_drilldown = $this->futurePriceDrilldown();
        $future_free_events = $this->futureFreeEvent();
        $future_free_drilldown = $this->futureFreeDrilldown();
        $future_dilldown = array_merge($future_price_drilldown, $future_free_drilldown);
     
        $c_allEvents = $this->allEvent();
        $allEventDrilldown = $this->allEventDrilldown();

        // dd($c_allEvents,$allEventDrilldown);

        $companies = CompanyInfo::all();
        $students = StudentInfo::all();

        $internPositions = InternPosition::all();
        $internRequests = InternRequest::all();
        $internAcceptedRequest = InternRequest::Confirmed()->get();
        $internDeclinedRequest = InternRequest::Declined()->get();

        $companyEvents = CompanyEvent::all();
        $hamposhani = 0;
        $companyId = [];
        foreach($companyEvents as $coevent){
            if(in_array($coevent->companyInfo->id,$companyId)){
                $hamposhani++;
            }else{
                array_push($companyId , $coevent->companyInfo->id);
            }
        }
        
        
        

        return view('panel.admin.dashboard' , 
            compact('companies' ,'internRequests' ,'internPositions','internAcceptedRequest', 'internDeclinedRequest',
            'c_provincesEvents','future_free_events','future_free_drilldown','future_price_events','future_price_drilldown',
            'hamposhani' , 'price_drilldown', 'c_free_events', 'free_drilldown','future_dilldown',
            'c_allEvents','allEventDrilldown',
            )
        );
    }

    public function bestUniInEvents()
    {
        // return University::whereHas('events')
        // ->withCount('events')
        // ->orderByDesc('events_count')
        // ->first();
        $array = [];
        $events = Event::Held()->with('university')->get();
        foreach($events as $event){
            $universityName = $event->university->name;
            if (isset($array[$universityName])) {
                $array[$universityName] += 1;
            } else {
                $array[$universityName] = 1;
            }
        }
        uasort($array , function($a , $b){
            return $b - $a;
        });
        // $newArr = [];
        // $score = 0;
        // foreach($array as $key => $value){
        //     if($value >= $score){
        //         $value = $score;
        //         if (isset($newArr[$key])) {
        //             $newArr[$key] = $value;
        //         }else{
        //             $newArr[]
        //         }
        //     }
        // }
        reset($array);
        $arr = [key($array) => current($array)];

        return $arr;
    }

    public function bestUniInRegisteredCompany()
    {
        $array = [];
        $bestCompanyEvent = CompanyEvent::Paid()->with('event.university')->get();
        foreach($bestCompanyEvent as $company){
            $universityName = $company->event->university->name;
            if (isset($array[$universityName])) {
                $array[$universityName] += 1;
            } else {
                $array[$universityName] = 1;
            }
        }
        uasort($array , function($a , $b){
            return $b - $a;
        });
        reset($array);
        $arr = [key($array) => current($array)];
        
        return $arr;
    }

    public function bestUniInRegisteredStedent()
    {
        $array = [];
        $bestStudentEvent = StudentEvent::with('event.university')->get();
        foreach($bestStudentEvent as $std){
            $universityName = $std->event->university->name;
            if (isset($array[$universityName])) {
                $array[$universityName] += 1;
            } else {
                $array[$universityName] = 1;
            }
        }
        uasort($array , function($a , $b){
            return $b - $a;
        });
        // dd($array);
        reset($array);
        $arr = [key($array) => current($array)];
        
        return $arr;
    }

    public function bestUniInInternPosition()
    {
        // return University::whereHas('events' , function($query){
        //     $query->withCount('internPositions')->get();
        // })
        
        // ->first();
    }

    public function pageHeader()
    {

        $this::$freeEvents = Event::where('is_free' , true)->get();
        $this::$priceEvents = Event::where('is_free' , false)->get();

        $this::$freeDoneEvents = Event::where('is_free' , true)->where('has_five_news' , true)->get();
        $this::$priceDoneEvents = Event::where('is_free' , false)->where('has_five_news' , true)->get();

        $this::$freeUnDoneEvents = Event::where('is_free' , true)->where('has_five_news' , false)->get();
        $this::$priceUnDoneEvents = Event::where('is_free' , false)->where('has_five_news' , false)->get();
        
        $this::$universityEvent = University::whereHas('events')->get();

        $this::$allCompanies = CompanyInfo::all();
        $this::$companyHasEvent = CompanyInfo::whereHas('events')->get();

        $this::$allStudents = StudentInfo::all();
        $this::$studentsHasEvent = StudentInfo::whereHas('events')->get();

        $this::$internRequests = InternRequest::all();

        $this::$acceptInternRequests = InternRequest::Confirmed()->get();
        $this::$declinedInternRequests = InternRequest::Declined()->get();
        $this::$pendingInternRequests = InternRequest::Unconfirmed()->get();

    }

    public function priceEvent()
    {

        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($University->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            $counter = 0;
            if(count($provinceal->events)>0){
                foreach($provinceal->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                
            }else{
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data =  $counter + provincealDonePriceEvents($provinceal->subUnits,$this->endDate,$this->startDate);
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => $p_name,
            ];
        }
        
        usort($array, function ($a, $b) {
            return $b['y'] - $a['y'];
        });
        return $array;
    }

    public function priceDrilldown()
    {
        $array = [];

        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();

        foreach($University->subUnits as $province){
            $data = [];
            $p_events = 0; 
            
            if(count($province->events)>0){
                $counter = 0;
                foreach($province->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_events =  $counter;
            }else{
                $p_events = 0;
            }
            $data[]= [$province->name , $p_events];

            foreach($province->subUnits as $unit){
                if(count($unit->events)>0){
                    $u_events = 0;
                    foreach($unit->events as $event){
                        if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                            $u_events++;
                        }
                    }
                    $data[] = [$unit->name , $u_events];
                }else{
                    $data[] = [$unit->name , 0];
                }
                
            }

            usort($data, function ($a, $b) {
                return $b[1] - $a[1];
            });

            $array[]= [
                'name' => $province->name,
                'id' => $province->name, 
                'data' => $data,
            ];
        }
        return $array;
    }

    public function freeEvent()
    {
        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($University->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            $counter = 0;
            if(count($provinceal->events)>0){
                foreach($provinceal->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                
            }else{
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data =  $counter + provincealDoneFreeEvents($provinceal->subUnits,$this->endDate,$this->startDate);
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => $p_name,
            ];
        }
        
        usort($array, function ($a, $b) {
            return $b['y'] - $a['y'];
        });

        return $array;
    }

    public function freeDrilldown()
    {
        $array = [];

        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();

        foreach($University->subUnits as $province){
            $data = [];
            $p_events = 0; 
            
            if(count($province->events)>0){
                $counter = 0;
                foreach($province->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == true && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_events =  $counter;
            }else{
                $p_events = 0;
            }
            $data[]= [$province->name , $p_events];

            foreach($province->subUnits as $unit){
                if(count($unit->events)>0){
                    $u_events = 0;
                    foreach($unit->events as $event){
                        if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                            $u_events++;
                        }
                    }
                    $data[] = [$unit->name , $u_events];
                }else{
                    $data[] = [$unit->name , 0];
                }
                
            }

            usort($data, function ($a, $b) {
                return $b[1] - $a[1];
            });

            $array[]= [
                'name' => $province->name,
                'id' => $province->name, 
                'data' => $data,
            ];
        }

        return $array;
    }

    public function futureFreeEvent()
    {
        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($University->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            $counter = 0;
            if(count($provinceal->events)>0){
                foreach($provinceal->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                
            }else{
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data =  $counter + provincealfutureFreeEvents($provinceal->subUnits,$this->endDate,$this->startDate);
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => 'free_'.$p_name,
            ];
        }
        
        // usort($array, function ($a, $b) {
        //     return $b['y'] - $a['y'];
        // });

        return $array;
    }

    public function futureFreeDrilldown()
    {
        $array = [];

        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();

        foreach($University->subUnits as $province){
            $data = [];
            $p_events = 0; 
            
            if(count($province->events)>0){
                $counter = 0;
                foreach($province->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_events =  $counter;
            }else{
                $p_events = 0;
            }
            $data[]= [$province->name , $p_events];

            foreach($province->subUnits as $unit){
                if(count($unit->events)>0){
                    $u_events = 0;
                    foreach($unit->events as $event){
                        if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                            $u_events++;
                        }
                    }
                    $data[] = [$unit->name , $u_events];
                }else{
                    $data[] = [$unit->name , 0];
                }
                
            }

            // usort($data, function ($a, $b) {
            //     return $b[1] - $a[1];
            // });

            $array[]= [
                'name' => $province->name,
                'id' => 'free_'.$province->name, 
                'data' => $data,
            ];
        }

        return $array;
    }

    public function futurePriceEvent()
    {
        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();
        
        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($University->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            $counter = 0;
            if(count($provinceal->events)>0){
                foreach($provinceal->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                
            }else{
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data =  $counter + provincealfuturePriceEvents($provinceal->subUnits,$this->endDate,$this->startDate);
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data, 
                'drilldown' => 'price_'.$p_name,
            ];
        }
        
        // usort($array, function ($a, $b) {
        //     return $b['y'] - $a['y'];
        // });

        return $array;
    }

    public function futurePriceDrilldown()
    {
        $array = [];

        $University = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
        ->first();

        foreach($University->subUnits as $province){
            $data = [];
            $p_events = 0; 
            
            if(count($province->events)>0){
                $counter = 0;
                foreach($province->events as $event){
                    if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                        $counter++;
                    }
                }
                $p_events =  $counter;
            }else{
                $p_events = 0;
            }
            $data[]= [$province->name , $p_events];

            foreach($province->subUnits as $unit){
                if(count($unit->events)>0){
                    $u_events = 0;
                    foreach($unit->events as $event){
                        if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == false && ($event->start_at <= $this->endDate && $event->start_at >= $this->startDate)){
                            $u_events++;
                        }
                    }
                    $data[] = [$unit->name , $u_events];
                }else{
                    $data[] = [$unit->name , 0];
                }
                
            }

            // usort($data, function ($a, $b) {
            //     return $b[1] - $a[1];
            // });

            $array[]= [
                'name' => $province->name,
                'id' => 'price_'.$province->name, 
                'data' => $data,
            ];
        }

        return $array;
    }

    public function cMixChart()
    {
        $mix_events = array_fill(0,12,0);
        $mix_companies = array_fill(0,12,0);    
        $mix_students = array_fill(0,12,0);
        

        $thisYearEvents = Event::NotDeleted()->Confirmed()->HasFiveNews()->whereBetween('start_at' , [$this->startDate , $this->endDate])->get();
        foreach($thisYearEvents as $event){
            switch(verta($event->start_at)->month){
                case 1: $mix_events[0]++; break;
                case 2: $mix_events[1]++; break;
                case 3: $mix_events[2]++; break;
                case 4: $mix_events[3]++; break;
                case 5: $mix_events[4]++; break;
                case 6: $mix_events[5]++; break;
                case 7: $mix_events[6]++; break;
                case 8: $mix_events[7]++; break;
                case 9: $mix_events[8]++; break;
                case 10: $mix_events[9]++; break;
                case 11: $mix_events[10]++; break;
                case 12: $mix_events[11]++; break;
            }
        }
        // whereBetween('created_at' , [$this->startDate , $this->endDate])->
        $thisYearCompanies = CompanyEvent::with('event')->get();
        foreach($thisYearCompanies as $company){
            if($company->event->start_at <= $this->endDate && $company->event->start_at >= $this->startDate){
                switch(verta($company->event->start_at)->month){
                    case 1: $mix_companies[0]++; break;
                    case 2: $mix_companies[1]++; break;
                    case 3: $mix_companies[2]++; break;
                    case 4: $mix_companies[3]++; break;
                    case 5: $mix_companies[4]++; break;
                    case 6: $mix_companies[5]++; break;
                    case 7: $mix_companies[6]++; break;
                    case 8: $mix_companies[7]++; break;
                    case 9: $mix_companies[8]++; break;
                    case 10: $mix_companies[9]++; break;
                    case 11: $mix_companies[10]++; break;
                    case 12: $mix_companies[11]++; break;
                }
            }
        }
        $thisYearStudents = StudentEvent::whereBetween('created_at' , [$this->startDate , $this->endDate])->with('event')->get();
        foreach($thisYearStudents as $student){
            if($student->event->start_at <= $this->endDate && $student->event->start_at >= $this->startDate){
                switch(verta($student->event->start_at)->month){
                    case 1: $mix_students[0]++; break;
                    case 2: $mix_students[1]++; break;
                    case 3: $mix_students[2]++; break;
                    case 4: $mix_students[3]++; break;
                    case 5: $mix_students[4]++; break;
                    case 6: $mix_students[5]++; break;
                    case 7: $mix_students[6]++; break;
                    case 8: $mix_students[7]++; break;
                    case 9: $mix_students[8]++; break;
                    case 10: $mix_students[9]++; break;
                    case 11: $mix_students[10]++; break;
                    case 12: $mix_students[11]++; break;
                }
            }
        }
        $this::$c_MixEvent = $mix_events;
        $this::$c_MixCompany = $mix_companies;
        $this::$c_MixStudent = $mix_students;
    }

    public function allEvent()
    {
        $university = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
            ->first();

        $array = [];
        $p_name = '';
        $p_event_data = 0;

        foreach($university->subUnits as $provinceal){
            $p_name = showNameOfProvinceOfUniversities($provinceal->name);
            $counter = 0;

            if(count($provinceal->events) > 0){
                foreach($provinceal->events as $event){
                    if($event->status == true && $event->deleted == false){
                        $counter++;
                    }
                }
            } else {
                $p_event_data = 0;
            }
            if($provinceal->subUnits){
                $p_event_data = $counter + provincealAllEvents($provinceal->subUnits);
            }
            $array[]= [
                'name' => $p_name,
                'y' => $p_event_data,
                'drilldown' => $p_name,
            ];
        }

        usort($array , function ($a, $b){
            return $b['y'] - $a['y'];
        });
        return $array;
    }

    public function allEventDrilldown()
    {
        $array = [];

        $university = University::Country()->with('subUnits.events' , 'subUnits.subUnits.events')
            ->first();

        foreach($university->subUnits as $province){
            $data = [];

            $p_events = 0;

            if(count($province->events)>0){
                $counter = 0;
                foreach($province->events as $event){
                    if($event->status == true && $event->deleted == false){
                        $counter++;
                    }
                }
                $p_events = $counter;
            }else{
                $p_events = 0;
            }
            $data[]= [showNameOfProvinceOfUniversities($province->name) , $p_events];

            foreach($province->subUnits as $unit){
                if(count($unit->events)>0){
                    $u_events = 0;
                    foreach($unit->events as $event){
                        if($event->status == true && $event->deleted == false){
                            $u_events++;
                        }
                    }
                    $data[] = [$unit->name , $u_events];
                }else{
                    $data[] = [$unit->name , 0];
                }
            }

            usort($data , function ($a , $b){
                return $b[1] - $a[1];
            });

            $array[] = [
                'name' => showNameOfProvinceOfUniversities($province->name),
                'id' => showNameOfProvinceOfUniversities($province->name),
                'data' => $data,
            ];
        }
        return $array;
    }
}

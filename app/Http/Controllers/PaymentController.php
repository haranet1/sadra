<?php

namespace App\Http\Controllers;

use App\Models\CompanyEvent;
use App\Models\CompanyInfo;
use App\Models\Event;
use App\Models\Payment;
use App\Models\StudentSkill;
use App\Models\StudentSkillPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    public function eventPrepay($id)
    {
        $request = CompanyEvent::where('id' , $id)
        ->where('company_id' , Auth::user()->groupable->id)
        ->with('booth' , 'event')->first();
        $price = (float)$request->booth->plan->price;
        $response= Http::post('http://api.iaun.ac.ir/api/login',[
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,

        ]);

        $dataArray = $response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withHeaders([
            'accept' => 'application/json',
        ])->withToken($token)
            ->accept('application/json')
            ->post( 'http://api.iaun.ac.ir/api/payments/authorizedpayer' , [
                'uid' => ''.Auth::id(),
                'paymentkind' => 50,
                'name' => Auth::user()->name,
                'lastname' => Auth::user()->family,
                'mobile' => Auth::user()->mobile
            ]);
        $dataArray = $response->json();

        $payment = Payment::updateOrCreate([
            'user_info_id' => Auth::user()->groupable->id , 'paymentable_id'=> $id
        ],[
            'user_info_id' => Auth::user()->groupable->id,
            'user_info_type' => Auth::user()->groupable_type,
            'paymentable_id' => $id,
            'paymentable_type' => CompanyEvent::class,
            'price' => $price,
            'getway' => 'iaun-getway'
        ]);

        if($dataArray['success']== true){
            $response = Http::withHeaders([
                'accept' => 'application/json',
            ])->withToken($token)
                ->accept('application/json')
                ->post('http://api.iaun.ac.ir/api/payments/startpayment', [
                    'Accept' => 'application/json',
                    'uid' => ''.Auth::id(),
                    'amount' => $price * 10,
                    'appid' => 4,    //  3 = Code Application: RahiNoo
                    'kinduser' => 3,
                    'paymentkind' => 50,
                    'paymenttitle' => 'اجاره غرفه رویداد صدرا',
                    'returnurl' => route('return.show.pay.event',$payment->id),
            ]);
            $dataArray = $response->json();

            if ($dataArray['success'] == true) {
                $payment->update([
                    'payment_key' => $dataArray['data']['payment_key'],
                ]);
                return redirect('http://api.iaun.ac.ir/payment-gateway/'.$dataArray['data']['payment_key']);
            }
            dd($dataArray) ;
        }

    }

    public function eventReturnShow($id)
    {
        $payment = Payment::with('paymentable.event')->find($id);
        $companyEvent = CompanyEvent::where('event_id' , $payment->paymentable->event->id)->whereHas('payment')->Paid()->get();

        $response = Http::post('http://api.iaun.ac.ir/api/login',[
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,
        ]);

        $dataArray = $response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withHeaders([
            'accept' => 'application/json',
        ])->withToken($token)
            ->accept('application/json')
            ->post('http://api.iaun.ac.ir/api/payments/verify',[
            'Accept' => 'application/json',
            'payment_key' => $payment->payment_key,
        ]);

        if(isset($response['success']) && $response['success'] == true){
            if($response['data']['pay_status'] == "2"){
                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                    'tracking_code' => $response['data']['pay_tracenumber'],
                    'receipt' => $response['data']['pay_rrn'],
                    'card_number' => $response['data']['pay_cardnumber'],
                    'payer_bank' => $response['data']['pay_issuerbank'],
                    'payment_date_time' => $response['data']['pay_paymentdatetime'],
                ]);
                $payment->paymentable->update([
                    'status' => true,
                ]);
                $payment->paymentable->save();
                if($payment->paymentable->event->held != true){
                    if(count($companyEvent) >= 5){
                        $payment->paymentable->event->held = true;
                        $payment->paymentable->event->save();
                    }
                }
                return redirect()->route('result.show.pay.event',['id' => $payment]);
            }elseif($response['data']['pay_status'] == "3"){
                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با مشکل روبرو شده است.'
                ]);
                $payment->save();
                return redirect()->route('index.register.event')->with('error', $payment->status);
            }elseif($response['data']['pay_status'] == "4"){
                $payment->update([
                    'status' => 'تراکنش تکراری است.',
                ]);
                $payment->save();
                return redirect()->route('index.register.event')->with('error', $payment->status);
            }elseif($response['data']['pay_status'] == "5"){
                $payment->update([
                    'status' => 'تراکنش توسط کاربر لغو شده است.',
                ]);
                $payment->save();
                return redirect()->route('index.register.event')->with('error', $payment->status);
            }
        }
        dd($response->json());

    }

    public function eventResultShow($id)
    {
        $payment = Payment::where('id', $id)->firstOrFail();
        // dd($payment);
        return view('panel.admin.events.pay.result' , compact('payment'));
    }

    public function studentPaySkills()
    {
        $skills = StudentSkill::where('student_id' , Auth::user()->groupable_id)
            ->NotConfirm()
            ->NotDeleted()
            ->get();

        $amount = $skills->count() * 50000;

        $studentSkillPayment = StudentSkillPayment::create([
            'student_id' => Auth::user()->groupable_id,
            'quantity' => $skills->count(),
            'amount' => $amount,
        ]);

        foreach($skills as $skill){
            $skill->update([
                'payment_id' => $studentSkillPayment->id
            ]);
        }

        $response= Http::withOptions([
            'verify' => false
        ])->post('https://api.iaun.ac.ir/api/login',[
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,
        ]);

        $dataArray = $response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withOptions([
            'verify' => false
        ])->withHeaders([
            'accept' => 'application/json',
        ])->withToken($token)
            ->accept('application/json')
            ->post( 'https://api.iaun.ac.ir/api/payments/authorizedpayer' , [
                'uid' => ''.Auth::id(),
                'paymentkind' => 50,
                'name' => Auth::user()->name,
                'lastname' => Auth::user()->family,
                'mobile' => Auth::user()->mobile
            ]);
        $dataArray = $response->json();

        $payment = Payment::create([
            'user_info_id' => Auth::user()->groupable->id,
            'user_info_type' => Auth::user()->groupable_type,
            'paymentable_id' => $studentSkillPayment->id,
            'paymentable_type' => StudentSkillPayment::class,
            'price' => $amount,
            'getway' => 'iaun-getway'
        ]);

        if($dataArray['success']== true){
            $response = Http::withOptions([
                'verify' => false
            ])->acceptJson()
                ->withToken($token)
                ->post('https://api.iaun.ac.ir/api/payments/startpayment', [
                    'Accept' => 'application/json',
                    'uid' => ''.Auth::id(),
                    'amount' => $amount * 10,
                    'appid' => 4,    //  3 = Code Application: RahiNoo
                    'kinduser' => 3,
                    'paymentkind' => 50,
                    'paymenttitle' => 'سرباز ماهر',
                    'returnurl' => route('return.show.pay.skill',$payment->id),
            ]);
            $dataArray = $response->json();

            if ($dataArray['success'] == true) {
                $payment->update([
                    'payment_key' => $dataArray['data']['payment_key'],
                ]);
                return redirect('https://api.iaun.ac.ir/payment-gateway/'.$dataArray['data']['payment_key']);
            }
            dd($dataArray) ;
        }

    }

    public function studentSkillReturnShow($id)
    {
        $payment = Payment::where('user_info_id' , Auth::user()->groupable_id)->where('id',$id)->with('paymentable.skills')->firstOrFail();

        $response= Http::withOptions([
            'verify' => false
        ])->post('https://api.iaun.ac.ir/api/login',[
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,
        ]);

        $dataArray = $response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withOptions([
            'verify' => false
        ])->withToken($token)
            ->acceptJson()
            ->post('https://api.iaun.ac.ir/api/payments/verify',[
            'Accept' => 'application/json',
            'payment_key' => $payment->payment_key,
        ]);


        if(isset($response['success']) && $response['success'] == true){
            if($response['data']['pay_status']  == "2"){

                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                    'tracking_code' => $response['data']['pay_tracenumber'],
                    'receipt' => $response['data']['pay_rrn'],
                    'card_number' => $response['data']['pay_cardnumber'],
                    'payer_bank' => $response['data']['pay_issuerbank'],
                    'payment_date_time' => $response['data']['pay_paymentdatetime'],
                ]);
                $payment->paymentable->update([
                    'receipt' => $response['data']['pay_rrn'],
                ]);
                foreach($payment->paymentable->skills as $skill){
                    $skill->update([
                        'status' => 1
                    ]);
                }
                return redirect()->route('result.show.pay.skill',['id' => $payment]);

            } elseif ($response['data']['pay_status'] == "3"){

                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با مشکل روبرو شده است.'
                ]);
                return redirect()->route('student.single' , Auth::user()->groupable_id)
                    ->with('error' , $payment->status);

            } elseif($response['data']['pay_status'] == "4"){

                $payment->update([
                    'status' => 'تراکنش تکراری است.',
                ]);
                return redirect()->route('student.single' , Auth::user()->groupable_id)
                    ->with('error' , $payment->status);

            } elseif($response['data']['pay_status'] == "5"){

                $payment->update([
                    'status' => 'تراکنش توسط کاربر لغو شده است.',
                ]);
                return redirect()->route('student.single' , Auth::user()->groupable_id)
                    ->with('error' , $payment->status);

            }
        }
        dd($response->json());
    }

    public function studentSkillResultShow($id)
    {
        $payment = Payment::where('user_info_id' , Auth::user()->groupable_id)->where('id',$id)->with('paymentable.skills')->firstOrFail();

        return view('panel.admin.skill.pay.result' , compact(
            'payment'
        ));
    }
}

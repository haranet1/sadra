<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Photo;
use App\Models\Province;
use App\Models\StudentInfo;
use App\Models\StudentSkill;
use App\Models\StudentSkillPayment;
use App\Models\University;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;

class StudentSkillController extends Controller
{
    public function create()
    {
        return view('panel.admin.skill.create');
    }

    public function store(Request $request)
    {
       $this->skillValidation($request);

       if (strpos($request->input('end_date'), '/') !== false) {
            $date = explode('/', $request->input('end_date'));

            $month = (int)persian_to_eng_num($date[1]);
            $day = (int)persian_to_eng_num($date[2]);
            $year = (int)persian_to_eng_num($date[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->input('end_date'))->datetime());
        }

       $studentSkill = StudentSkill::create([
            'student_id' => Auth::user()->groupable_id,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'end_date' => $startDateGre,
            'hours_passed' => $request->input('hours_passed'),
            'academy_id' => $request->input('academy'),
            'type' => $request->input('type'),
       ]);

       if($request->file('photo')){
            $name = time() . $request->photo->getClientOriginalName();
            $path = 'img/skills';
            $file = $request->file('photo');
            $file->move(public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => StudentSkill::class,
            ]);

            $studentSkill->update([
                'photo_id' => $photo->id
            ]);
       }

       return redirect()->route('student.single', Auth::user()->groupable_id)->with('success' , 'مهارت جدید برای شما ثبت شد و در انتظار تایید توسط دانشگاه است.');

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $skill = StudentSkill::where('student_id',Auth::user()->groupable_id)->where('id',$id)->NotDeleted()->firstOrFail();

        return view('panel.admin.skill.edit' , compact(
            'skill'
        ));
    }

    public function update(Request $request)
    {
        $this->skillValidation($request);

        if (strpos($request->input('end_date'), '/') !== false) {
            $date = explode('/', $request->input('end_date'));

            $month = (int)persian_to_eng_num($date[1]);
            $day = (int)persian_to_eng_num($date[2]);
            $year = (int)persian_to_eng_num($date[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->input('end_date'))->datetime());
        }

       $studentSkill = StudentSkill::where('student_id',Auth::user()->groupable_id)->where('id',$request->input('skill_id'))->NotDeleted()->firstOrFail();

       $studentSkill->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'end_date' => $startDateGre,
            'hours_passed' => $request->input('hours_passed'),
            'academy_id' => $request->input('academy'),
            'type' => $request->input('type'),
       ]);

       if($request->file('photo')){
            if($studentSkill->photo){
                unlink($studentSkill->photo->path);
            }

            $name = time() . $request->photo->getClientOriginalName();
            $path = 'img/skills';
            $file = $request->file('photo');
            $file->move(public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => StudentSkill::class,
            ]);

            $studentSkill->update([
                'photo_id' => $photo->id
            ]);
       }

       return redirect()->route('student.single', Auth::user()->groupable_id)->with('success' , 'مهارت جدید برای شما ثبت شد و در انتظار تایید توسط دانشگاه است.');
    }

    public function delete(Request $request)
    {
        $skill = StudentSkill::where('student_id',Auth::user()->groupable_id)->where('id',$request->id)->NotDeleted()->firstOrFail();

        if($skill->photo){
            unlink($skill->photo->path);
        }

        return response()->json($skill->update([
            'deleted' => true
        ]));
    }

    public function uniList()
    {
        // $studentSkills = StudentSkill::whereRelation('student' , 'university_id' , Auth::user()->groupable->university_id)
        //     ->NotDeleted()
        //     ->NotConfirm()
        //     ->with('student.user', 'photo')
        //     ->orderByDesc('created_at')
        //     ->paginate(20);
        $studentSkills = StudentSkill::where('academy_id' , Auth::user()->groupable->university_id)
            ->NotDeleted()
            ->Payed()
            ->with('student.user', 'photo')
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('panel.admin.skill.uni-list' , compact(
            'studentSkills'
        ));
    }

    public function acceptedList()
    {
        $studentSkills = StudentSkill::where('academy_id' , Auth::user()->groupable->university_id)
            ->NotDeleted()
            ->Confirm()
            ->with('student.user', 'photo')
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('panel.admin.skill.accepted-uni-list' , compact(
            'studentSkills'
        ));
    }

    public function armedList()
    {

        $studentsHasSkill = StudentInfo::whereHas('skills')->get();

        $ids = [];
        foreach($studentsHasSkill as $std){
            foreach($std->skills as $skill){
                if($skill->status == 2){
                    array_push($ids, $std->id);
                }
            }
        }

        $studentSkills = StudentInfo::whereIn('id' , $ids)
            ->with([
                'skills' => function ($query) {
                    $query->where('status' , 2);
                }
            ])
            ->orderByDesc('updated_at')
            ->paginate(20);

        // $universities = University::all();
        $provinces = Province::all();


        return view('panel.admin.skill.list' , compact(
            'studentSkills',
            // 'universities',
            'provinces'
        ));
    }

    public function armedSearch(Request $request)
    {
        // dd($request->all());
        $provinces = Province::all();

        $oldData = [
            'n_code' => $request->input('n_code'),
            'type' => $request->input('type'),
            'mobile' => $request->input('mobile'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'province' => $request->input('province'),
            'city' => $request->input('city'),
        ];

        $studentsHasSkill = StudentInfo::whereHas('skills')->get();

        $ids = [];
        foreach($studentsHasSkill as $std){
            foreach($std->skills as $skill){
                if($skill->status == true){
                    array_push($ids, $std->id);
                }
            }
        }

        $query = StudentInfo::query();
        $query = $query->NotDeleted();
        $query = $query->whereIn('id' , $ids);
        $query = $query->with([
            'skills' => function ($q) {
                $q->status = true;
            }
        ]);

        if($request->input('n_code') != null || $request->input('n_code') != ''){
            $query->where('n_code' , 'LIKE' , '%'. $request->input('n_code') .'%');
        }

        if($request->input('type') != null || $request->input('type') != ''){
            $query->whereRelation('skills' ,'type' , $request->input('type'));
        }

        if($request->input('mobile') != null || $request->input('mobile') != ''){
            $query->where('mobile' , 'LIKE' , '%'. $request->input('mobile') .'%');
        }

        if ($request->input('start_date') != null && $request->input('end_date') != null) {
            $start_date = Verta::parse($request->input('start_date'))->toCarbon()->startOfDay();
            $end_date = Verta::parse($request->input('end_date'))->toCarbon()->endOfDay();

            $query->whereHas('skills', function ($q) use ($start_date, $end_date) {
                $q->whereBetween('end_date', [$start_date, $end_date]);

            });
        }

        if($request->input('province') != null || $request->input('province') != ''){
            $query->whereRelation('user.province' , 'id' , $request->input('province'));
        }

        if($request->input('city') != null || $request->input('city') != ''){
            $query->whereRelation('user.city' , 'id' , $request->input('city'));
        }

        $studentSkills = $query->with('skills')
            ->orderByDesc('updated_at')
            ->paginate(20)
            ->appends($oldData);


        return view('panel.admin.skill.list' , compact(
            'studentSkills',
            'oldData',
            'provinces'
        ));

    }

    public function accept(Request $request)
    {
        $skill = StudentSkill::find($request->id);
        return response()->json($skill->update([
            'status' => 2
        ]));
    }

    public function getAcademyByText(Request $request)
    {
        $universities = University::where('name', 'LIKE' , '%'. $request->text .'%')
            ->get();

        return response()->json($universities);
    }

    public function transactionsList()
    {
        $studentSkills = StudentSkillPayment::where('student_id' , Auth::user()->groupable_id)
            ->with('skills' , 'payment')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('panel.admin.skill.pay.std-transactions-list' , compact(
            'studentSkills'
        ));

    }

    public function skillTransactionListForAdmin()
    {
        $studentSkills = StudentSkillPayment::with('skills' , 'payment')
            ->whereNotNull('receipt')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('panel.admin.transactions.skill-list' , compact(
            'studentSkills'
        ));
    }

    protected function skillValidation(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'end_date' => 'required',
            'photo' => 'image|max:1024',
            'hours_passed' => 'required',
            'academy' => 'required'
        ],[
            'title.required' => 'عنوان مهارت الزامی می‌باشد',
            'end_date.required' => 'تاریخ اخذ مدرک الزامی می‌باشد',
            'photo.image' => 'تصویر مدرک را با فرمت مناسب آپلود کنید',
            'photo.max' => 'حجم فایل انتخابی نباید بیشتر از 1 مگابایت باشد',
            'type.required' => 'انتخاب نوع مهارت الزامی می‌باشد',
            'hours_passed.required' => 'ساعت گذرانده الزامی می‌باشد',
            'academy.required' => 'انتخاب محل آموزش الزامی می باشد',
        ]);
    }
}

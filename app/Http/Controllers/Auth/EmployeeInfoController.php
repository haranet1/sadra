<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\EmployeeInfo;
use App\Models\Photo;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeInfoController extends Controller
{
    public function show()
    {   
        $provinces = Province::all();
        return view('auth.employee.info' , compact('provinces'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'province' => 'required',
            'city' => 'required',
            'employee_code' => 'required|numeric',
            'university_position' => 'required',
            'logo' => 'mimes:jpeg,jpg,png|max:1024',
            'header' => 'mimes:jpeg,jpg,png|max:2048',
        ],[
            'province.required' => 'استان الزامی است',
            'city.required' => 'شهر الزامی است',
            'employee_code.required' => 'شماره کارمندی الزامی است',
            'employee_code.numeric' => 'شماره کارمندی را به صورت صحیح وارد کنید',
            'university_position.required' => 'سمت دانشگاهی الزامی است',
            'logo.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
            'logo.max' => 'حجم لوگو نباید بیشتر از 1 مگابایت باشد',
            'header.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
            'header.max' => 'حجم تصویر انتخاب شده نباید بیشتر از 2 مگابایت باشد',
        ]); 

        $employee_code = persian_to_eng_num($request->employee_code);
        $header = null;
        $logo = null;

        if($request->file('header')){
            $name = time() . $request->header->getClientOriginalName();
            $path = 'img/employee';
            $file = $request->file('header');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => EmployeeInfo::class,
            ]);

            $header = $photo->id;
        }

        if($request->file('logo')){
            $name = time() . $request->logo->getClientOriginalName();
            $path = 'img/employee';
            $file = $request->file('logo');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => EmployeeInfo::class,
            ]);

            $logo = $photo->id;
        }

        Auth::user()->province_id = $request->province;
        Auth::user()->city_id = $request->city;
        Auth::user()->save();
        
        Auth::user()->groupable->update([
            'employee_code' => $employee_code,
            'university_position' => $request->university_position,
            'header_id' => $header,
            'logo_id' => $logo,
        ]);

        return redirect(route('dashboard'));
    }
}

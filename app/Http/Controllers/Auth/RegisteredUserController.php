<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AdminInfo;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'name' => ['required', 'string', 'max:30'],
            'family' => ['required', 'string', 'max:40'],
            'sex' => ['required'],
            'n_code' => ['required' , 'numeric','digits:10'],
            'birth' => 'required|date',
            'mobile' => ['required' ,'numeric', 'digits:11', 'unique:'.User::class],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'role' => ['required'],
            'password' => ['required', 'string', 'min:8'],
        ],[
            'name.required' => 'نام الزامی است',
            'name.string' => 'نام را به صورت صحیح وارد کنید',
            'name.max' => 'طول نام بیش از حد مجاز است',
            'family.required' => 'نام خانوادگی الزامی است',
            'family.string' => 'نام خانوادگی را به صورت صحیح وارد کنید',
            'family.max' => 'طول نام خانوادگی بیش از حد مجاز است',
            'sex.required' => 'جنسیت خود را انتخاب کنید',
            'n_code.required' => 'کد ملی الزامی است',
            'n_code.numeric' => 'کد ملی را به صورت عدد وارد کنید',
            'n_code.digits' => 'کد ملی باید 10 رقم باشد.',
            'birth.required' => 'تاریخ تولد الزامی است',
            'birth.date' => 'تاریخ تولد را به صورت صحیح وارد کنید',
            'mobile.required' => 'شماره همراه الزامی است',
            'mobile.numeric' => 'شماره همراه را به صورت صحیح وارد کنید',
            'mobile.digits' => 'شماره همراه باید 11 رقم باشد',
            'mobile.unique' => 'شماره همراه تکراری است',
            'email.required' => 'ایمیل الزامی است',
            'email.email' => 'ایمیل را به صورت صحیح وارد کنید',
            'email.unique' => 'ایمیل تکراری است',
            'role.required' => 'نوع کاربری الزامی است',
            'password.required'=>'کلمه عبور الزامی است',
            'password.min'=>'کلمه عبور حداقل باید 8 کاراکتر باشد',
        ]);

        $birth = persian_to_eng_date($request->birth);
        $mobile = persian_to_eng_num($request->mobile);
        $n_code = persian_to_eng_num($request->n_code);

        $request->merge([
            'birth' => $birth,
            'mobile' => $mobile,
            'n_code' => $n_code,
        ]);


        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'sex' => $request->sex,
            'n_code' => $request->n_code,
            'birth' => $request->birth,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        if($request->role == 'student'){
            Auth::user()->groupable_type = StudentInfo::class;
            Auth::user()->save();
            return redirect()->route('set.std.info');
        }elseif($request->role == 'company'){
            Auth::user()->groupable_type = CompanyInfo::class;
            Auth::user()->save();
            return redirect()->route('set.co.info');
        }elseif($request->role == 'admin'){
            $info = AdminInfo::create([
                'status' => 1,
            ]);
            Auth::user()->groupable_id = $info->id;
            Auth::user()->groupable_type = AdminInfo::class;
            Auth::user()->save();
            // dd(Auth::user()->groupable);
            return redirect()->route('dashboard');
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class ForgotPasswordController extends Controller
{

    public function showLinkRequestForm()
    {
        return view('auth.passwords.sms');
    }

    public function sendVerifyCode(Request $request)
    {
        $request->validate([
            'mobile' => 'required|numeric|digits:11|exists:users'
        ], [
            'mobile.exists' => 'شماره تلفن وارد شده در سیستم وجود ندارد.'
        ]);
        $code = rand(10000, 99999);
        $user = User::where('mobile', $request->mobile)->first();

        $user->mobile_code = $code;
        $user->mobile_code_created_at = now();
        $user->save();
        Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
            'receptor' => $user->mobile,
            'token' => $code,
            'template' => 'sadraMobileVerify',
        ]);

        return view('auth.passwords.verify-sms')->with(['id' => $user->id]);
    }

    public function VerifyCode(Request $request)
    {
        $user = User::findorfail($request->id);
        if ($user->mobile_code == $request->code) {
            return view('auth.passwords.change-password')->with(['id' => $user->id]);
        } else {
            return redirect(route('request.password'))->with('error', 'کد وارد شده صحیح نمی باشد.');
        }

    }

    public function passwordReset(Request $request)
    {
        $request->validate([
            'password' =>'required|min:8'
        ]);

        $user = User::find($request->id);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect(route('login'))->with('success','کلمه عبور با موفقیت تغییر کرد');
    }
}

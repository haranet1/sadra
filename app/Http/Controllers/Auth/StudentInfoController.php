<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\StudentInfo;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentInfoController extends Controller
{
    public function show()
    {
        $provinces = Province::all();
        $universities = University::all();
        return view('auth.student.info' , compact('provinces' , 'universities'));
    }

    public function store(Request $request)
    {   
        $end_date = persian_to_eng_date($request->end_date);
        $student_code = persian_to_eng_num($request->student_code);
        
        $request->merge([
            'end_date' => $end_date,
            'student_code' => $student_code,
        ]);

        $request->validate([
            'province' => 'required',
            'city' => 'required',
            'university_id' => 'required',
            'student_code' => 'required|numeric',
            'grade' => 'required',
            'major' => 'required',
            'avg' => 'required',
            'end_date' => 'required|date',
            ],[
            'province.required' => 'استان الزامی است',
            'city.required' => 'شهر الزامی است',
            'university.required' => 'دانشگاه خود را انتخاب کنید',
            'student_code.required' => 'شماره دانشجویی الزامی است',
            'student_code.numeric' => 'شماره دانشجویی را به صورت صحیح وارد کنید',
            'grade.required' => 'مقطع تحصیلی الزامی است',
            'major.required' => 'رشته تحصیلی الزامی است',
            'avg.required' => 'معدل الزامی است',
            'end_date.required' => 'تاریخ اخذ مدرک تحصیلی الزامی است',
            'end_date.date' => 'تاریخ اخذ مدرک تحصیلی را به صورت صحیح وارد کنید',
        ]);

        $about = null;
        if($request->about){
            $about = $request->about;
        }

        $info = StudentInfo::create([
            'university_id' => $request->university_id,
            'student_code' => $request->student_code,
            'grade' => $request->grade,
            'major' => $request->major,
            'avg' => $request->avg,
            'end_date' => $request->end_date,
            'about' => $about,
        ]);

        Auth::user()->groupable_id = $info->id;
        Auth::user()->province_id = $request->province;
        Auth::user()->city_id = $request->city;
        Auth::user()->save();

        return redirect()->route('dashboard');
    }
}

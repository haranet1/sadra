<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\Photo;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyInfoController extends Controller
{
    public function show()
    {
        $provinces = Province::all();
        return view('auth.company.info' , compact('provinces'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        

        $request->validate([
                'name' => 'required|max:50',
                'n_code' => 'required|numeric',
                'registration_number' => 'required|numeric',
                'co_phone' => 'required|numeric|unique:'.CompanyInfo::class,
                'activity_field' => 'required',
                'year' => 'required|date',
                'ceo' => 'required',
                'co_email' => 'required|email',
                'about' => 'required',
                'logo' => 'required|mimes:jpeg,jpg,png|max:1024',
                'header' => 'mimes:jpeg,jpg,png|max:2048',
                'province' => 'required',
                'city' => 'required',
                
            
            ],[
                'name.required' => 'نام شرکت الزامی است',
                'name.max' => 'طول نام شرکت بیشتر از حد مجاز است',
                'n_code.required' => 'کد ملی شرکت الزامی است',
                'n_code.numeric' => 'کد ملی شرکت را به صورت صحیح وارد کنید',
                'registration_number.required' => 'شماره ثبت شرکت الزامی است',
                'registration_number.numeric' => 'شماره ثبت شرکت را به صورت صحیح وارد کنید',
                'co_phone.required' => 'شماره تلفن شرکت الزامی است',
                'co_phone.numeric' => 'شماره تلفن شرکت را به صورت صحیح وارد کنید',
                'co_phone.unique' => 'شماره تلفن شرکت تکراری است',
                'activity_field.required' => 'حوزه فعالیت شرکت الزامی است',
                'year.required' => 'سال تاسیس شرکت الزامی است',
                'year.date' => 'سال تاسیس شرکت را به صورت صحیح وارد کنید',
                'ceo.required' => 'نام و نام خانوادگی مدیر عامل الزامی است',
                'co_email.required' => 'ایمیل شرکت الزامی است',
                'co_email.email' => 'ایمیل شرکت را به صورت صحیح وارد کنید',
                'about.required' => 'درباره شرکت الزامی است',
                'logo.required' => 'لوگو الزامی است',
                'logo.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
                'logo.max' => 'حجم لوگو نباید بیشتر از 1 مگابایت باشد',
                'header.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
                'header.max' => 'حجم تصویر انتخاب شده نباید بیشتر از 2 مگابایت باشد',
                'province.required' => 'انتخاب استان الزامی است',
                'city.required' => 'انتخاب شهر الزامی است',

        ]);

        $n_code = persian_to_eng_num($request->n_code);
        $registration_number = persian_to_eng_num($request->registration_number);
        $co_phone = persian_to_eng_num($request->co_phone);
        $year = persian_to_eng_date($request->year);

        $request->merge([
            'n-code' => $n_code,
            'registration_number' => $registration_number,
            'co_phone' => $co_phone,
            'year' => $year,

        ]);

        $website = null;
        if(isset($request->website)){
            $website = $request->website;
        }
        $address = null;
        if(isset($request->address)){
            $address = $request->address;
        }
        $header = null;
        if($request->file('header')){
            $name = time() . $request->header->getClientOriginalName();
            $path = 'img/company';
            $file = $request->file('header');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);

            $header = $photo->id;
        }
        $logo = null;
        if($request->file('logo')){
            $name = time() . $request->logo->getClientOriginalName();
            $path = 'img/company';
            $file = $request->file('logo');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);

            $logo = $photo->id;
        }

        $info = CompanyInfo::create([
            'name' => $request->name,
            'n_code' => $request->n_code,
            'registration_number' => $request->registration_number,
            'co_phone' => $request->co_phone,
            'activity_field' => $request->activity_field,
            'year' => $request->year,
            'ceo' => $request->ceo,
            'website' => $website,
            'co_email' => $request->co_email,
            'about' => $request->about,
            'address' => $address,
            'logo_id' => $logo,
            'header_id' => $header,
            'province_id' => $request->province,
            'city_id' => $request->city,

        ]); 

        Auth::user()->groupable_id = $info->id;
        Auth::user()->province_id = $request->province;
        Auth::user()->city_id = $request->city;
        Auth::user()->save();

        return redirect(route('dashboard'));

    }
    
    
}

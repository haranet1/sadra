<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\AdminInfo;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use App\Models\University;
use App\Models\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserManagementController extends Controller
{
    public function index()
    {
        return 'لیست کل کاربران';
    }

    public function employee()
    {
        $employees = EmployeeInfo::whereHas('user')
        ->with('user' , 'university')
        ->orderByDesc('created_at')
        ->paginate(20);

        return view('panel.admin.manage-users.employee' , compact('employees'));
    }

    public function company()
    {
        $companies = CompanyInfo::whereHas('user')
        ->with('user')
        ->orderByDesc('created_at')
        ->paginate(20);

        return view('panel.admin.manage-users.company' , compact('companies'));
    }

    public function student()
    {
        $students = StudentInfo::whereHas('user')
        ->with('user')
        ->orderByDesc('created_at')
        ->paginate(20);

        return view('panel.admin.manage-users.student' , compact('students'));
    }

    public function unConfirmed()
    {
        $coimg = rand(1,7);
        $companies = User::UnConfirmed()
        ->where('groupable_type', CompanyInfo::class)
        ->whereHas('groupable')
        ->with('groupable.logo')
        ->orderByDesc('created_at')
        ->paginate(20);

        $students = User::UnConfirmed()
        ->where('groupable_type', StudentInfo::class)
        ->whereHas('groupable')
        ->with('groupable')
        ->orderByDesc('created_at')
        ->paginate(20);

        $employees = User::unConfirmed()
        ->where('groupable_type', EmployeeInfo::class)
        ->whereHas('groupable')
        ->with('groupable.university')
        ->orderByDesc('created_at')
        ->paginate(20);

        // dd($companies);
        return view('panel.admin.manage-users.unconfirmed' , compact('companies', 'students', 'employees' ,  'coimg'));
    }

    public function confirmed()
    {
        $companies = User::Confirmed()
        ->where('groupable_type', CompanyInfo::class)
        ->whereHas('groupable')
        ->with('groupable.logo')
        ->orderByDesc('created_at')
        ->paginate(20);

        $students = User::Confirmed()
        ->where('groupable_type', StudentInfo::class)
        ->whereHas('groupable')
        ->with('groupable')
        ->orderByDesc('created_at')
        ->paginate(20);
    }

    public function provincialUnConfirmed()
    {
        $coimg = rand(1,7);
        $companies = User::UnConfirmed()
        ->where('groupable_type', CompanyInfo::class)
        ->whereRelation('groupable.university' , 'id' , Auth::user()->groupable->university_id)
        ->whereHas('groupable')
        ->with('groupable.logo')
        ->orderByDesc('created_at')
        ->paginate(20);

        $students = User::UnConfirmed()
        ->where('groupable_type', StudentInfo::class)
        ->whereRelation('groupable.university' , 'id' , Auth::user()->groupable->university_id)
        ->whereHas('groupable')
        ->with('groupable')
        ->orderByDesc('created_at')
        ->paginate(20);
        dd($companies);

        return view('panel.admin.manage-users.unconfirmed' , compact('companies', 'students', 'coimg'));
    }

    public function provincialConfirmed() 
    {
        $companies = User::Confirmed()
        ->where('groupable_type', CompanyInfo::class)
        ->whereHas('groupable')
        ->whereRelation('groupable.university' , 'id' , Auth::user()->groupable->university_id)
        ->with('groupable.logo')
        ->orderByDesc('created_at')
        ->paginate(20);

        $students = User::Confirmed()
        ->where('groupable_type', StudentInfo::class)
        ->whereRelation('groupable.university' , 'id' , Auth::user()->groupable->university_id)
        ->whereHas('groupable')
        ->with('groupable')
        ->orderByDesc('created_at')
        ->paginate(20);
    }

    public function unitUnConfirmed()
    {

    }

    public function unitConfirmed()
    {

    }

    public function create()
    {
        $universities = University::all();

        return view('panel.admin.manage-users.create' , compact('universities'));
    }

    public function createArmedForces()
    {
        $universities = University::all();

        return view('panel.admin.manage-users.create-armed' , compact('universities'));
    }

    public function storeArmedForces(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'n_code' => 'required',
            'sex' => 'required',
            'birth' => 'required',
            'email' => 'required',
            'mobile' => 'required|unique:\app\models\User,mobile',
            'role' => 'required',
        ],[
            'name.required' => 'نام الزامی است',
            'family.required' => 'نام خانوادگی الزامی است',
            'n_code.required' => 'کد ملی الزامی است',
            'sex.required' => 'جنسیت الزامی است',
            'birth.required' => 'تاریخ تولد الزامی است',
            'email.required' => 'ایمیل الزامی است',
            'mobile.required' => 'موبایل الزامی است',
            'mobile.unique'=> 'موبایل وارد شده پیش از این ثبت شده است',
            'role.required' => 'انتخاب دسترسی کاربری الزامی است',
        ]);

        $vertaDate = Verta::parse($request->birth);
        $miladiDate = $vertaDate->toCarbon();

        $info = AdminInfo::create([
            'status' => 0
        ]);

        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'n_code' => $request->n_code,
            'sex' => $request->sex,
            'birth' => $miladiDate,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'groupable_id' => $info->id,
            'groupable_type' => AdminInfo::class,
            'password' => $request->mobile,
        ]);

        $user->assignRole($request->role);

        return redirect()->back()->with('success','کاربر با موفقیت ثبت شد');
    }

    public function storeEmployee(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'n_code' => 'required',
            'sex' => 'required',
            'birth' => 'required|date_format:Y/m/d',
            'email' => 'required',
            'mobile' => 'required|unique:\app\models\User,mobile',
            'role' => 'required',
            'university_id' => 'required',
        ],[
            'name.required' => 'نام الزامی است',
            'family.required' => 'نام خانوادگی الزامی است',
            'n_code.required' => 'کد ملی الزامی است',
            'sex.required' => 'جنسیت الزامی است',
            'birth.required' => 'تاریخ تولد الزامی است',
            'birth.date_format' => 'تاریخ تولد را به صورت صحیح وارد کنید مثال: (1350/07/21)',
            'email.required' => 'ایمیل الزامی است',
            'mobile.required' => 'موبایل الزامی است',
            'mobile.unique'=> 'موبایل وارد شده پیش از این ثبت شده است',
            'role.required' => 'انتخاب دسترسی کاربری الزامی است',
            'university_id.required' => 'انتخاب دانشگاه الزامی است',
        ]);

        $info = EmployeeInfo::create([
            'university_id' => $request->university_id,
        ]);

        $vertaDate = Verta::parse($request->birth);
        $miladiDate = $vertaDate->toCarbon();
        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'n_code' => $request->n_code,
            'sex' => $request->sex,
            'birth' => $miladiDate,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'groupable_id' => $info->id,
            'groupable_type' => EmployeeInfo::class,
            'password' => $request->mobile,
        ]);

        if(isset($request->role)){
            if($request->role == 'unit'){
                $user->assignRole('unit');
            }elseif($request->role == 'provincial'){
                $user->assignRole('provincial');
            }elseif($request->role == 'country'){
                $user->assignRole('country');
            }
        }
        
        return redirect()->back()->with('success','کاربر با موفقیت ثبت شد');
    }

    public function confirmStudent($id)
    {
        
    }

    
}

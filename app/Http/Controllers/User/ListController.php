<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\StudentInfo;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListController extends Controller
{
    public function students()
    {
        $students = StudentInfo::UserConfirmed()
        ->whereRelation('university' , 'id' , Auth::user()->groupable->university_id)
        ->with('user' , 'university')
        ->orderByDesc('created_at')
        ->paginate(20);
        
        return view('panel.admin.list.std' , compact('students'));
    }

    public function unStudents()
    {
        $students = StudentInfo::UserUnConfirmed()
        ->whereRelation('university' , 'id' , Auth::user()->groupable->university_id)
        ->with('user' , 'university')
        ->orderByDesc('created_at')
        ->paginate(20);

        return view('panel.admin.list.std' , compact('students'));
    }

    public function universities()
    {
        $universities = University::SubUnitsOfTheProvince()
            ->with('events' , 'students' , 'city')
            ->paginate(20);
        
        return view('panel.admin.list.uni' , compact(
            'universities'
        ));
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\Photo;
use App\Models\Province;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class InfoController extends Controller
{
    public function showCo()
    {
        $info = Auth::user()->groupable;
        return view('panel.admin.information.company.show' , compact('info'));
    }

    public function editCo()
    {
        $info = Auth::user()->groupable;
        return view('panel.admin.information.company.edit' , compact('info'));
    }

    public function updateCo(Request $request)
    {
        $request->validate([
            'header' => 'mimes:jpeg,jpg,png|max:2048',
            'logo' => 'mimes:jpeg,jpg,png|max:1024',
            'co_phone' => 'min:8|max:11',
            'co_email' => 'required|email',
        ],[
            'header.mimes' => 'فرمت تصویر سربرگ نادرست است',
            'logo.mimes' => 'فرمت تصویر لوگو نادرست است',
            'co_phone.min' => 'تلفن شرکت نباید کم تر از 8 رقم باشد',
            'co_phone.max' => 'تلفن شرکت نباید بیشتر از 11 رقم باشد',
            'co_email.required' => 'ایمیل شرکت الزامی است',
            'co_email.email' => 'ایمیل شرکت را به صورت صحیح وارد کنید',
        ]);

        $info = Auth::user()->groupable;
        if ($request->about != ""){
            $info->about = $request->input('about');
        }
        $info->name = $request->input('name');
        $info->website = $request->input('website');
        $info->co_phone = $request->input('co_phone');
        $info->co_email = $request->input('co_email');
        $info->address = $request->input('address');
        $info->save();
        if($request->file('header')){
            $name = time() . $request->header->getClientOriginalName();
            $path = 'img/company';
            $file = $request->file('header');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);
            $info->header_id = $photo->id;
            $info->save();
        }
        if($request->file('logo')){
            $name = time() . $request->logo->getClientOriginalName();
            $path = 'img/company';
            $file = $request->file('logo');
            $file->move( public_path($path) , $name);

            $photo = Photo::create([
                'name' => $name,
                'path' => $path.'/'.$name,
                'type' => CompanyInfo::class,
            ]);
            $info->logo_id = $photo->id;
            $info->save();
        }

        return redirect()->route('co.show.info')->with('success' , 'اطلاعات شما ویرایش یافت');
    }

    public function showStd()
    {
        return view('panel.admin.information.student.show');
    }

    public function editStd()
    {
        $info = Auth::user()->groupable;
        $universities = University::all();
        $provincies = Province::all();
        return view('panel.admin.information.student.edit' , compact('info' , 'universities' , 'provincies'));
    }

    public function updateStd(Request $request)
    {
        $request->validate([
            'city' => 'required',
            'birth' => 'date',
            'email' => 'email'
        ],[
            'city.required' => 'انتخاب شهر الزامی است',
            'birth.date' => 'تاریخ تولد را به صورت صحیح وارد کنید',
            'email.email' => 'ایمیل را به صورت صحیح وارد کنید',
        ]);

        $info = Auth::user()->groupable;
        if(!is_null($request->input('name'))){
            $info->user->name = $request->input('name');
        }
        if(!is_null($request->input('family'))){
            $info->user->family = $request->input('family');
        }
        if(!is_null($request->input('province'))){
            $info->user->province_id = $request->input('province');
        }
        if(!is_null($request->input('city'))){
            $info->user->city_id = $request->input('city');
        }
        if(!is_null($request->input('birth'))){
            $info->user->birth = persian_to_eng_date($request->input('birth'));
        }
        if(!is_null($request->input('grade'))){
            $info->grade = $request->input('grade');
        }
        if(!is_null($request->input('major'))){
            $info->major = $request->input('major');
        }
        if(!is_null($request->input('email'))){
            $info->user->email = $request->input('email');
        }

        $info->user->save();
        $info->save();

        return redirect()->route('std.show.info')->with('success' , 'اطلاعات با موفقیت ویرایش یافت');
    }

    public function showEmp($id)
    {
        $info = EmployeeInfo::with('user')->find($id);
        return view('panel.admin.information.employee.show' , compact('info'));
    }

    public function editEmp($id)
    {
        $info = EmployeeInfo::with('user')->find($id);
        $universities = University::all();
        $provincies = Province::all();
        return view('panel.admin.information.employee.edit' , compact('info', 'universities' , 'provincies'));
    }

    public function updateEmp(Request $request)
    {
        $request->validate([
            // 'city' => 'required',
            'birth' => 'date',
            'email' => 'email'
        ],[
            // 'city.required' => 'انتخاب شهر الزامی است',
            'birth.date' => 'تاریخ تولد را به صورت صحیح وارد کنید',
            'email.email' => 'ایمیل را به صورت صحیح وارد کنید',
        ]);

        $info = EmployeeInfo::with('user')->find($request->user);
        if(!is_null($request->input('name'))){
            $info->user->name = $request->input('name');
        }
        if(!is_null($request->input('family'))){
            $info->user->family = $request->input('family');
        }
        if(!is_null($request->input('province'))){
            $info->user->province_id = $request->input('province');
        }
        if(!is_null($request->input('city'))){
            $info->user->city_id = $request->input('city');
        }
        if(!is_null($request->input('birth'))){
            $info->user->birth = persian_to_eng_date($request->input('birth'));
        }
        if(!is_null($request->input('email'))){
            $info->user->email = $request->input('email');
        }
        if(!is_null($request->input('university'))){
            $info->university_id = $request->input('university');
        }
        if(!is_null($request->input('mobile'))){
            $info->user->mobile = $request->input('mobile');
        }
        if(!is_null($request->input('password'))){
            $request->validate( [
                'password' => [ Password::defaults()],
            ],[
                'password' => 'رمز عبور حداقل باید 8 کاراکتر باشد.',
            ]);
            $info->user->password = Hash::make($request->input('password'));
        }

        if(isset($request->role)){
            if($request->role == 'unit'){
                $info->user->syncRoles('unit');
            }elseif($request->role == 'provincial'){
                $info->user->syncRoles('provincial');
            }elseif($request->role == 'country'){
                $info->user->syncRoles('country');
            }
        }

        $info->user->save();
        $info->save();
        
        if(Auth::user()->isAdmin()){
            return redirect()->route('employee.user-managment')->with('success' , 'اطلاعات با موفقیت ویرایش یافت');
        }
        return redirect()->route('emp.show.info')->with('success' , 'اطلاعات با موفقیت ویرایش یافت');

    }

    public function deleteEmp(Request $request)
    {
        $employeeInfo = EmployeeInfo::find($request->id);

        if ($employeeInfo) {
            $employeeInfo->delete();
        }

        return response()->json($employeeInfo);
    }
}


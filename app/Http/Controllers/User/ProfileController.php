<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function edit() : View
    {
        $user = Auth::user();
        return view('panel.admin.profile.edit' , compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'mobile' => 'unique:users,mobile,'. Auth::user()->id,
        ],[
            'mobile.unique' => 'موبایل وارد شده تکراری است',
        ]);

        if(filled($request->password)){
            Auth::user()->password = Hash::make($request->password);
        }
        if($request->mobile != Auth::user()->mobile){
            Auth::user()->update(['mobile_verified_at'=>null]);
        }

        Auth::user()->name = $request->name;
        Auth::user()->family = $request->family;
        Auth::user()->mobile = $request->mobile;
        Auth::user()->save();

        return redirect()->route('dashboard')->with('success' , 'پروفایل شما ویرایش شد');
    }
    
}
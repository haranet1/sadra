<?php

namespace App\Http\Middleware;

use App\Models\AdminInfo;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(!Auth::user()->groupable){
            if(Auth::user()->groupable_type == StudentInfo::class) return redirect(route('set.std.info'));
            
            if(Auth::user()->groupable_type == CompanyInfo::class) return redirect(route('set.co.info'));

            if(Auth::user()->groupable_type == EmployeeInfo::class) return redirect(route('set.emp.info'));

        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::user()->IsAdmin() && Auth::user()->hasRole(4)) {
            return redirect()->route('armed.list.skill');
        }
        if (Auth::user()->IsStudent()) {
            return redirect()->route('student.single',Auth::user()->groupable_id);
        }
        if(Auth::user()->IsCompany()){
            return redirect()->route('company.single' , Auth::user()->groupable_id);
        }

        return $next($request);
        
    }
}

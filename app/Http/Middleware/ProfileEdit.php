<?php

namespace App\Http\Middleware;

use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ProfileEdit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::user()->groupable_type == CompanyInfo::class){
            return redirect()->route('co-edit.profile');
        }
        elseif(Auth::user()->groupable_type == StudentInfo::class){
            return redirect()->route('std-edit.profile');
        }
        return $next($request);
    }
}

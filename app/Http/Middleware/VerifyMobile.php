<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Services\Notification\Notification;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class VerifyMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Route::CurrentRouteName()=='verifyMobile' || Route::CurrentRouteName()=='change_mobile' || Route::CurrentRouteName()=='update_mobile'){
            return $next($request);
        }else{
            if (Auth::check() && Auth::user()->mobile_verified_at == null){
                if (Auth::user()->mobile_code_created_at < now()->subMinute(2) || Auth::user()->mobile_code_created_at == null){
                    if (has_internet()){
                        $rand = rand(10000,99999);
                        Auth::user()->mobile_code = $rand;
                        Auth::user()->mobile_code_created_at = now();
                        Auth::user()->save();

                        Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                            'receptor' => Auth::user()->mobile,
                            'token' => str_replace(' ','-',trim($rand)),
                            'template' => 'sadraMobileVerify',
                        ]);

                        return redirect(\route('verify-mobile.show'));
                    }else{
                        return redirect(\route('verify-mobile.show'))->with('error','اتصال اینترنت خود را بررسی کنید');
                    }

                }else{
                    return redirect(\route('verify-mobile.show'));
                }
            }else{
                return $next($request);
            }
        }
    }
}

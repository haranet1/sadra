<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EmpDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(userIsProvincialEmployee()) return redirect()->route('provincial.dashboard');
        if(userIsUnitEmployee()) return redirect()->route('unit.dashboard');
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContentSecurityPolicy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $response = $next($request);

        // اضافه کردن سیاست CSP به هدر
        $csp = "default-src 'self'; ";
        $csp .= "style-src 'self' code.highcharts.com; ";
        // $csp .= "style-src 'self' code.highcharts.com; ";
        $csp .= "script-src 'self' code.highcharts.com;";
        // $csp .= "script-src 'self' aparat.com;";

        $response->headers->set('Content-Security-Policy', $csp);

        return $response;
    }
}

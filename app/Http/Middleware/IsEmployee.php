<?php

namespace App\Http\Middleware;

use App\Models\AdminInfo;
use App\Models\EmployeeInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        
        if(Auth::user()->groupable && Auth::user()->groupable_type == EmployeeInfo::class){
            return $next($request);
        }elseif(Auth::user()->groupable && Auth::user()->groupable_type == AdminInfo::class){
            return $next($request);
        }else{
            return redirect(404); 
        }
    }
}

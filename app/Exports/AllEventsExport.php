<?php

namespace App\Exports;

use App\Models\Event;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AllEventsExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Event::NotDeleted()
        ->with('creator.user', 'university' , 'acceptor.user' , 'companies' , 'students' , 'internPositions')
        ->get();
    }

    public function map($event): array
    {
        $acceptor = '-';
        $creator = '-';
        $companiesCount = 0;
        $studentsCount = 0;
        $internPositionsCount = 0;
        
        if($event->acceptor && !is_null($event->acceptor)){
            $acceptor = $event->acceptor->user->name. ' ' . $event->acceptor->user->family;
        }
        if(!is_null($event->creator)){
            $creator = $event->creator->user->name . ' ' . $event->creator->user->family;
        }
        if($event->companies){
            $companiesCount = count($event->companies);
        }
        if($event->students){
            $studentsCount = count($event->students);
        }
        if($event->internPositions){
            $internPositionsCount = count($event->internPositions);
        }

        return [
            $event->title,
            $creator,
            $event->university->name,
            $acceptor,
            (string)$companiesCount,
            (string)$studentsCount,
            (string)$internPositionsCount,
            verta($event->start_register_at)->formatDate(),
            verta($event->end_register_at)->formatDate(),
            verta($event->start_at)->formatDate(),
            verta($event->end_at)->formatDate(),
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان رویداد',
            'مجری',
            'نام دانشگاه',
            'تایید کننده',
            'تعداد شرکت ها',
            'تعداد دانشجو ها',
            'تعداد موقعیت ها',
            'تاریخ شروع ثبت نام',
            'تاریخ پایان ثبت نام',
            'تاریخ شروع رویداد',
            'تاریخ پایان رویداد',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

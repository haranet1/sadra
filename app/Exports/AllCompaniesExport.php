<?php

namespace App\Exports;

use App\Models\CompanyInfo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AllCompaniesExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return CompanyInfo::whereHas('user')
        ->with('user' , 'events' , 'internPositions' , 'internRequests')
        ->get();
    }

    public function map($company): array
    {
        
        $eventsCount = 0;
        $internPositionsCount = 0;
        $internRequestsCount = 0;
        if($company->events){
            $eventsCount = count($company->events);
        }
        if($company->internPositions){
            $internPositionsCount = count($company->internPositions);
        }
        if($company->internRequests){
            $internRequestsCount = count($company->internRequests);
        }

        
        return [
            $company->name,
            $company->activity_field,
            $company->year,
            $company->co_phone,
            $company->ceo,
            $company->co_email,
            (string) $eventsCount,
            (string) $internPositionsCount,
            (string) $internRequestsCount,
        ];
    }

    public function headings(): array
    {
        return [
            'نام شرکت',
            'حوزه فعالیت',
            'سال تاسیس',
            'تلفن شرکت',
            'مدیر عامل',
            'ایمیل',
            'تعداد رویداد ها',
            'تعداد موقعیت های پویش',
            'تعداد درخواست های پویش',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

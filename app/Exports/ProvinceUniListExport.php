<?php

namespace App\Exports;

use App\Models\University;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProvinceUniListExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        return University::SubUnitsOfTheProvince()
            ->with('events' , 'students' , 'city')
            ->get();
    }

    public function map($uni): array
    {
        $phone = '--'; 
        if($uni->phone) $phone = $uni->phone;
        $city = '--';
        if($uni->city) $city = $uni->city->name;
        $events = '--';
        if($uni->events->count() > 0) $events = $uni->events->count();
        $students = '--';
        if($uni->students->count() > 0) $students = $uni->students->count();
        return [
            $uni->name,
            $phone,
            $city,
            $events,
            $students,
        ];
    }

    public function headings(): array
    {
        return [
            'واحد دانشگاهی',
            'تلفن',
            'شهر',
            'تعداد رویداد ها',
            'تعداد دانشجویان',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

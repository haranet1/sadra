<?php

namespace App\Exports;

use App\Models\Event;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProvinceUniEventsExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Event::NotDeleted()
        ->Confirmed()
        ->whereRelation('university' , 'id' , Auth::user()->groupable->university_id)
        ->get();
    }

    public function map($event): array
    {
        $acceptor = '-';
        $creator = '-';
        $companiesCount = 0;
        $studentsCount = 0;
        $internPositionsCount = 0;
        
        if($event->acceptor){
            $acceptor = $event->acceptor->user->name. ' ' . $event->acceptor->user->family;
        }
        if(!is_null($event->creator)){
            $creator = $event->creator->user->name . ' ' . $event->creator->user->family;
        }
        if($event->companies){
            $companiesCount = count($event->companies);
        }
        if($event->students){
            $studentsCount = count($event->students);
        }
        if($event->internPositions){
            $internPositionsCount = count($event->internPositions);
        }

        return [
            $event->title,
            $creator,
            $acceptor,
            (string)$companiesCount,
            (string)$studentsCount,
            (string)$internPositionsCount,
            verta($event->start_register_at)->formatDate(),
            verta($event->end_register_at)->formatDate(),
            verta($event->start_at)->formatDate(),
            verta($event->end_at)->formatDate(),
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان رویداد',
            'مجری',
            'تایید کننده',
            'تعداد شرکت ها',
            'تعداد دانشجو ها',
            'تعداد موقعیت ها',
            'تاریخ شروع ثبت نام',
            'تاریخ پایان ثبت نام',
            'تاریخ شروع رویداد',
            'تاریخ پایان رویداد',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

<?php

namespace App\Exports;

use App\Models\StudentInfo;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProvinceUniUnConfirmedSubStudentsExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StudentInfo::UserUnConfirmed()
            ->whereRelation('university' , 'id' , Auth::user()->groupable->university_id)
            ->with('user' , 'university')
            ->get();
    }

    public function map($std): array
    {
        $uni = '--';
        if($std->university) $uni = $std->university->name;
        $major = '--';
        if($std->major) $major = $std->major;
        $city = '--';
        if($std->city) $city = $std->city->name;

        return [
            $std->user->name. ' ' .$std->user->family,
            $std->user->mobile,
            verta($std->user->birth)->diffYears(verta()->now()),
            $uni,
            $major,
            $city
        ];
    }

    public function headings(): array
    {
        return [
            'نام و نام خانوادگی',
            'تلفن',
            'سن',
            'دانشگاه',
            'رشته',
            'شهر',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

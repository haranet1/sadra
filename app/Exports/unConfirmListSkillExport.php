<?php

namespace App\Exports;

use App\Models\StudentSkill;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class unConfirmListSkillExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StudentSkill::whereRelation('student' , 'university_id' , Auth::user()->groupable->university_id)
            ->NotDeleted()
            ->NotConfirm()
            ->with('student.user', 'photo')
            ->orderByDesc('created_at')
            ->get();
    }

    public function map($skill): array
    {
        $academyAndHours = 'نا مشخص | '. $skill->hours_passed;

        if(! is_null($skill->academy)){
            $academyAndHours = $skill->academy->name. ' | '. $skill->hours_passed;
        }

        return [
            $skill->title,
            $skill->student->user->name.' '.$skill->student->user->family,
            $skill->student->user->mobile,
            $skill->student->user->n_code,
            $academyAndHours,
            $skill->end_date,
            $skill->type,
            asset($skill->photo->path)
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان مهارت',
            'نام و نام خانوادگی',
            'تلفن',
            'کد ملی',
            'محل آموزش | ساعت گذرانده',
            'تاریخ اخذ مدرک',
            'نوع مهارت',
            'لینک تصویر مدرک',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

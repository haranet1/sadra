<?php

namespace App\Exports;

use App\Models\StudentInfo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AllStudentHasSkillExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize,WithStyles
{
    private $rowCount = 1;
    private $mergeRows = [];

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $studentsHasSkill = StudentInfo::whereHas('skills')->get();

        $ids = [];
        foreach($studentsHasSkill as $std){
            foreach($std->skills as $skill){
                if($skill->status == true){
                    array_push($ids, $std->id);
                }
            }
        }

        return StudentInfo::whereIn('id' , $ids)
            ->NotDeleted()
            ->with([
                'skills' => function ($query) {
                    $query->where('status' , true);
                } 
            ])
            ->orderByDesc('updated_at')
            ->get();
    }

    public function map($std): array
    {
        $studentData = [
            $std->user->name.' '.$std->user->family,
            $std->user->n_code,
            $std->user->mobile,
            verta($std->user->birth)->diffYears(verta()->now()),
            $std->university->name,
            $std->user->province->name.'|'.$std->user->city->name,
        ];

        $rows = [];
        foreach ($std->skills as $skill) {
            $rows[] = array_merge($studentData, [
                $skill->title,
                $skill->type,
                $skill->academy->name ?? 'ثبت نشده',
                $skill->hours_passed,
                $skill->end_date,
                asset($skill->photo->path),
            ]);
        }

        // Add row count and merge info for styling
        $this->mergeRows[] = ['start' => $this->rowCount + 1, 'end' => $this->rowCount + count($rows)];
        $this->rowCount += count($rows);

        return $rows;
    }

    public function headings(): array
    {
        return [
            'نام و نام خانوادگی',
            'کد ملی',
            'تلفن',
            'سن',
            'دانشگاه',
            'استان و شهر',
            'نام مهارت',
            'نوع مهارت',
            'محل آموزش',
            'ساعت گذرانده',
            'تاریخ اخذ مدرک',
            'لینک تصویر مدرک',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        // Style for the header row
        $headerStyleArray = [
            'font' => [
                'bold' => true,
                // 'color' => 'ffffff'
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '0096FF', // Blue color
                ],
            ],
        ];

        // Apply the header style
        $sheet->getStyle('A1:L1')->applyFromArray($headerStyleArray);

        // Merge cells for student info
        foreach ($this->mergeRows as $range) {
            for ($col = 'A'; $col <= 'F'; $col++) {
                $sheet->mergeCells("{$col}{$range['start']}:{$col}{$range['end']}");
                $sheet->getStyle("{$col}{$range['start']}:{$col}{$range['end']}")->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
            }
        }

        return [];
    }

}

<?php

namespace App\Exports;

use App\Models\StudentEvent;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RegisteredStudentsInEventExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function collection()
    {
        return StudentEvent::with('event' , 'studentInfo.user', 'studentInfo.internRequests.internPosition')
        ->where('event_id' , $this->event)->get();
    }

    public function map($student): array
    {
        $internRequests = 0;
        if($student->studentInfo->internRequests){
            foreach($student->studentInfo->internRequests as $req){
                if($req->internPosition->event_id == $this->event){
                    $internRequests++;
                } 
            }
        }

        return [
            $student->event->title,
            $student->studentInfo->user->name. ' ' .$student->studentInfo->user->family,
            (string)$internRequests,
            verta($student->created_at)->formatDate(),
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان رویداد',
            'نام و نام خانوادگی',
            'تعداد درخواست های پویش',
            'تاریخ ثبت نام',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

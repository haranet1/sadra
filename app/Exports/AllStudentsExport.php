<?php

namespace App\Exports;

use App\Models\StudentInfo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AllStudentsExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StudentInfo::UserConfirmed()
        ->with('user.province', 'user.city' , 'university' , 'events' , 'internRequests')
        ->get();
    }

    public function map($student): array
    {
        $eventsCount = 0;
        $internRequestsCount = 0;

        if($student->events){
            $eventsCount = count($student->events);
        }
        if($student->internRequests){
            $internRequestsCount = count($student->internRequests);
        }

        return [
            $student->user->name. ' ' .$student->user->family,
            $student->user->n_code,
            $student->user->province->name,
            $student->user->city->name,
            $student->university->name,
            $student->student_code,
            $student->grade,
            $student->major,
            $student->user->birth,
            $student->user->mobile,
            $student->user->email,
            (string) $eventsCount,
            (string) $internRequestsCount,
        ];
    }

    public function headings(): array
    {
        return [
            'نام و نام خانوادگی',
            'کد ملی',
            'استان',
            'شهر',
            'دانشگاه',
            'شماره دانشجویی',
            'مقطع تحصیلی',
            'رشته',
            'تاریخ تولد',
            'موبایل',
            'ایمیل',
            'تعداد رویداد های شرکت کرده',
            'تعداد درخواست های پویش',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

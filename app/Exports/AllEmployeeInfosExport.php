<?php

namespace App\Exports;

use App\Models\EmployeeInfo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AllEmployeeInfosExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return EmployeeInfo::whereHas('user')->with('user' , 'university')->get();
    }

    public function map($employee): array
    {

        return [
            $employee->user->name,
            $employee->user->family,
            $employee->university->name,
            $employee->user->mobile,
            getRoleNamesFa($employee->user->getRoleNames()),
            $employee->university_position
        ];
    }

    public function headings(): array
    {
        return [
            'نام',
            'نام خانوادگی',
            'نام دانشگاه',
            'شماره تلفن همراه',
            'نوع کاربری',
            'سمت دانشگاهی',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}



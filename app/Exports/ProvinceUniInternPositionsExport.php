<?php

namespace App\Exports;

use App\Models\InternPosition;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProvinceUniInternPositionsExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return InternPosition::NotDeleted()
        ->with('companyInfo' , 'event' , 'internRequests')
        ->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->get();
    }

    public function map($position): array
    {
        $requests = 0;
        if($position->internRequests){
            $requests = count($position->internRequests);
        }
        return [
            $position->event->title,
            $position->title,
            $position->companyInfo->name,
            (string)$requests,
            $position->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان رویداد',
            'عنوان موقعیت پویش',
            'نام شرکت',
            'تعداد درخواست های پویش',
            'تاریخ ایجاد',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}

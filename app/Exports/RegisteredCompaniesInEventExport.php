<?php

namespace App\Exports;

use App\Models\CompanyEvent;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RegisteredCompaniesInEventExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function collection()
    {
        return CompanyEvent::with('event' , 'booth.plan' , 'companyInfo.user' , 'companyInfo.internPositions')
        ->where('event_id' , $this->event)->get();
    }

    public function map($company): array
    {
        $status = 'پرداخت نشده';
        if(!is_null($company->status)){
            $status = 'پرداخت شده';
        }
        $internPositionsCount = 0;
        if(isset($company->companyInfo->internPositions)){
            $internPositionsCount = count($company->companyInfo->internPositions);
        }
        return [
            $company->event->title,
            $company->companyInfo->name,
            $company->companyInfo->user->name. ' ' .$company->companyInfo->user->family,
            $company->booth->name,
            number_format($company->booth->plan->price), 
            (string)$internPositionsCount,
            verta($company->created_at)->formatDate(),
            $status,
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان رویداد',
            'نام شرکت',
            'نام و نام خانوادگی',
            'غرفه انتخابی',
            'هزینه ثبت نام (تومان)',
            'تعداد موقعیت های پویش',
            'تاریخ ثبت نام',
            'وضعیت',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }


}

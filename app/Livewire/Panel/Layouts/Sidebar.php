<?php

namespace App\Livewire\Panel\Layouts;

use Livewire\Component;

class Sidebar extends Component
{
    public function render()
    {
        return view('livewire.panel.layouts.sidebar');
    }
}

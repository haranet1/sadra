<?php

use App\Models\EventBooth;
use App\Models\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;
use Spatie\Permission\Traits\HasRoles;

function has_internet()
{
    $connected = @fsockopen("www.google.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function persian_to_eng_num($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    $output = str_replace($persian, $english, $string);
    return $output;
}

function persian_to_eng_date($date)
{

    if (strpos($date, '/') !== false) {
        $startDate = explode('/', $date);
    
        $month = (int)persian_to_eng_num($startDate[1]);
        $day = (int)persian_to_eng_num($startDate[2]);
        $year = (int)persian_to_eng_num($startDate[0]);
    
        $jalaliDate = \verta();
        $jalaliDate->year($year);
        $jalaliDate->month($month);
        $jalaliDate->day($day);
        $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
        $DateGre = $jalaliDate->toCarbon('Asia/Tehran');
    } else {
        $DateGre = Carbon::instance(Verta::parse($date)->datetime());
    }

    return $DateGre;
}

function getBuyPlanBooths($id)
{
    $booths = EventBooth::where('plan_id', $id)->get();
    if (count($booths) > 0) return $booths;
    else return false;
}

function planBoothExist($id)
{
    $booths = EventBooth::where('plan_id', $id)->get();
    if (count($booths) > 0) return true;
    else return false;
}

function userIsUnitEmployee()
{
    if(Auth::user()->hasRole(1)) return true;
    else return false;
}

function userIsProvincialEmployee()
{
    if(Auth::user()->hasRole(2)) return true;
    else return false;
}

function userIsCountryEmployee()
{
    if(Auth::user()->hasRole(3)) return true;
    else return false;
}

function userIsArmedForces()
{
    if(Auth::user()->hasRole(4)) return true;
    else return false;
}


function getRoleNamesFromUser($roles)
{
    $rand = ['success','dark','warning','primary', 'secondary' , 'info'];
    foreach($roles as $role){
        echo '<div class="badge mx-1 bg-'.$rand[rand(0,5)].'">'.getRoleNameFa($role).'</div>';
    }
}

function getRoleNameFa($role)
{
    switch($role){
        case'unit': return 'واحد'; break;
        case'provincial': return 'استانی'; break;
        case'country': return 'کشوری'; break;
    }
}

function getRoleNamesFa($roles)
{
    
    foreach($roles as $role){
        switch($role){
            case'unit': return 'واحد'; break;
            case'provincial': return 'استانی'; break;
            case'country': return 'کشوری'; break;
        }
    }
}

function provincealEvent($subs)
{
    $sum = 0;
    foreach($subs as $sub){
        if($sub->has('events')){
            $sum += count($sub->events);
        }
    }
    return $sum;
}

function provincealCompany($subs)
{
    $sum = 0;
    foreach($subs as $sub){
        if($sub->events->has('companies')){
            foreach($sub->events as $event){
                $sum += count($event->companies);
            }
        }
    }
    return $sum;
}

function provincealStudent($subs)
{
    $sum = 0;
    foreach($subs as $sub){
        if($sub->events->has('students')){
            foreach($sub->events as $event){
                $sum += count($event->students);
            }
        }
    }
    return $sum;
}

function Percent($Component , $all)
{
    if($all != 0){
        $pecent = ($Component * 100) / $all;
        return number_format($pecent , 2).'%';
    }
    
    return 0 .'%';
}

function showAllNameOfProvinceOfUniversities($unis)
{
    $array = [];

    foreach($unis as $uni){
        switch($uni){
            case 'تبريز' :
                array_push($array , 'آذربایجان شرقی');
            case 'ارومیه' :
                array_push($array , 'آذربایجان غربی');
            case 'اردبیل' :
                array_push($array , 'اردبیل');
            case 'اصفهان' :
                array_push($array , 'اصفهان');
            case 'کرج' :
                array_push($array , 'البرز');
            case 'ایلام' :
                array_push($array , 'ایلام');
            case 'بوشهر' :
                array_push($array , 'بوشهر');
            case 'استان تهران' :
                array_push($array , 'تهران');
            case 'شهرکرد' :
                array_push($array , 'چهارمحال بختیاری');
            case 'بیرجند' :
                array_push($array , 'خراسان جنوبی');
            case 'مشهد' :
                array_push($array , 'خراسان رضوی');
            case 'بجنورد' :
                array_push($array , 'خراسان شمالی');
            case 'اهواز' :
                array_push($array , 'خوزستان');
            case 'زنجان' :
                array_push($array , 'زنجان');
            case 'سمنان' :
                array_push($array , 'سمنان');
            case 'زاهدان' :
                array_push($array , 'سیستان و بلوچستان');
            case 'شیراز' :
                array_push($array , 'فارس');
            case 'قزوین' :
                array_push($array , 'قزوین');
            case 'قم' :
                array_push($array , 'قم');
            case 'سنندج' :
                array_push($array , 'كردستان');
            case 'کرمان' :
                array_push($array , 'کرمان');
            case 'کرمانشاه' :
                array_push($array , 'کرمانشاه');
            case 'ياسوج' :
                array_push($array , 'کهگیلویه و بویراحمد');
            case 'گرگان' :
                array_push($array , 'گلستان');
            case 'رشت' :
                array_push($array , 'گیلان');
            case 'خرم آباد' :
                array_push($array , 'لرستان');
            case 'ساری' :
                array_push($array , 'مازندران');
            case 'اراک' :
                array_push($array , 'مرکزی');
            case 'بندرعباس' :
                array_push($array , 'هرمزگان');
            case 'همدان' :
                array_push($array , 'همدان');
            case 'یزد' :
                array_push($array , 'یزد');
            case 'سازمان سما' :
                array_push($array , 'سازمان سما');
            case 'مستقل علوم تحقیقات' :
                array_push($array , 'علوم تحقیقات');
            case 'مستقل نجف آباد' :
                array_push($array , 'نجف آباد');
        }
    }

    return $array;
}

function showNameOfProvinceOfUniversities($provinceName)
{
    switch($provinceName){
        case 'تبريز' :
             return 'آذربایجان شرقی';
        case 'ارومیه' :
             return 'آذربایجان غربی';
        case 'اردبیل' :
             return 'اردبیل';
        case 'اصفهان' :
             return 'اصفهان';
        case 'کرج' :
             return 'البرز';
        case 'ایلام' :
             return 'ایلام';
        case 'بوشهر' :
             return 'بوشهر';
        case 'استان تهران' :
             return 'تهران';
        case 'شهرکرد' :
             return 'چهارمحال بختیاری';
        case 'بیرجند' :
             return 'خراسان جنوبی';
        case 'مشهد' :
             return 'خراسان رضوی';
        case 'بجنورد' :
             return 'خراسان شمالی';
        case 'اهواز' :
             return 'خوزستان';
        case 'زنجان' :
             return 'زنجان';
        case 'سمنان' :
             return 'سمنان';
        case 'زاهدان' :
             return 'سیستان و بلوچستان';
        case 'شیراز' :
             return 'فارس';
        case 'قزوین' :
             return 'قزوین';
        case 'قم' :
             return 'قم';
        case 'سنندج' :
             return 'كردستان';
        case 'کرمان' :
             return 'کرمان';
        case 'کرمانشاه' :
             return 'کرمانشاه';
        case 'ياسوج' :
             return 'کهگیلویه و بویراحمد';
        case 'گرگان' :
             return 'گلستان';
        case 'رشت' :
             return 'گیلان';
        case 'خرم آباد' :
             return 'لرستان';
        case 'ساری' :
             return 'مازندران';
        case 'اراک' :
             return 'مرکزی';
        case 'بندرعباس' :
             return 'هرمزگان';
        case 'همدان' :
             return 'همدان';
        case 'یزد' :
             return 'یزد';
        case 'سازمان سما' :
             return 'سازمان سما';
        case 'مستقل علوم تحقیقات' :
            return 'مستقل علوم تحقیقات';
        case 'مستقل نجف آباد' :
             return 'مستقل نجف آباد';
    }
}

function provincealDoneEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && $event->has_five_news == true && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealDonePriceEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealDoneFreeEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealfutureFreeEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && $event->is_free == true && $event->has_five_news == false && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealfuturePriceEvents($subs,$end,$start)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if(($event->status == true && $event->deleted == false) && $event->is_free == false && $event->has_five_news == false && ($event->start_at <= $end && $event->start_at >= $start)){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function provincealAllEvents($subs)
{
    $sum = 0;
    foreach($subs as $sub){
        if(count($sub->events)>0){
            foreach($sub->events as $event){
                if($event->status == true && $event->deleted == false){
                    $sum++;
                }
            }
        }
    }
    return $sum;
}

function change_sex_to_fa($sex)
{
    if($sex == 'male'){
        return 'مرد';
    }else{
        return 'زن';
    }
}

function get_student_skill_status($status){

    switch($status){
        case 0:
            return '<button style="cursor: auto;" type="button" class="btn btn-warning">در انتظار پرداخت</button>';
            break;
        case 1:
            return '<button style="cursor: auto;" type="button" class="btn btn-primary">پرداخت شده و در انتظار بررسی دانشگاه</button>';
            break;
        case 2:
            return '<button style="cursor: auto;" type="button" class="btn btn-success">تایید توسط دانشگاه و در انتظار بررسی نیروهای مسلح</button>';
            break;
        case 3:
            return '<button style="cursor: auto;" type="button" class="btn btn-danger">رد توسط دانشگاه</button>';
            break;
    }

}


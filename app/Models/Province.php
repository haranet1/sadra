<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    
    // relations

    public function universities()
    {
        return $this->hasMany(University::class);
    }

    public function companies()
    {
        return $this->hasMany(CompanyInfo::class);
    }
}

<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'family',
        'sex',
        'birth',
        'n_code',
        'province_id',
        'city_id',
        'mobile',
        'mobile_verified_at',
        'mobile_code',
        'mobile_code_created_at',
        'email',
        'email_verified_at',
        'password',
        'status',
        'groupable_id',
        'groupable_type',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    // 

    public function isAdmin() :bool
    {
        return $this->groupable_type === AdminInfo::class;
    }

    public function isEmployee() :bool
    {
        return $this->groupable_type === EmployeeInfo::class;
    }

    public function isCompany() :bool
    {
        return $this->groupable_type === CompanyInfo::class;
    }

    public function isStudent() :bool
    {
        return $this->groupable_type === StudentInfo::class;
    }

    // scopes 

    public function scopeConfirmed($query)
    {
        return $query->where('status' , 1);
    }

    public function scopeUnConfirmed($query)
    {
        return $query->whereNull('status');
    }

    public function scopeBaned($query)
    {
        return $query->where('status' , 0);
    }
    
    // relations 

    public function groupable()
    {
        return $this->morphTo();
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InternRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'intern_position_id','student_id','company_id','status',
    ];

    // scopes

    public function scopeInThisUniversityAndSubUnits($query)
    {
        return $query->whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id)
        ->orWhereRelation('internPosition.event.university' , 'parent' , Auth::user()->groupable->university_id);
    }

    public function scopeInThisUniversity($query)
    {
        return $query->whereRelation('internPosition.event.university' , 'id' , Auth::user()->groupable->university_id);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status' , 1);
    }

    public function scopeUnconfirmed($query)
    {
        return $query->whereNull('status');
    }

    public function scopeDeclined($query)
    {
        return $query->where('status' , 0);
    }

    // relations

    public function internPosition()
    {
        return $this->belongsTo(InternPosition::class , 'intern_position_id');
    }

    public function studentInfo()
    {
        return $this->belongsTo(StudentInfo::class , 'student_id');
    }

    public function companyInfo()
    {
        return $this->belongsTo(CompanyInfo::class , 'company_id');
    }
}

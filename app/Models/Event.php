<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'creator_id',
        'description',
        'start_register_at',
        'end_register_at',
        'start_at',
        'end_at',
        'photo_id',
        'map_id',
        'vertical_banner_id',
        'horizontal_banner_id',
        'university_id',
        'acceptor_id',
        'is_free',
        'status',
        'held',
        'has_five_news',
        'deleted',
    ];

    // scopes

    // public function scopeBestEvent($query)
    // {
    //     return $query->wherePivot('status' , 1)->withCount('companies');
    // }

    public function scopeThisUniversity($query)
    {
        return $query->whereRelation('university' , 'id' ,Auth::user()->groupable->university_id);
    }

    public function scopeThisUniversityAndSubUnits($query)
    {
        return $query->whereRelation('university' , 'id' , Auth::user()->groupable->university_id)
        ->orWhereRelation('university' , 'parent' , Auth::user()->groupable->university_id);
    }

    public function scopeIsFree($query)
    {
        return $query->where('is_free' , true);
    }

    public function scopeIsPrice($query)
    {
        return $query->where('is_free' , false);
    }

    public function scopeHasFiveNews($query)
    {
        return $query->where('has_five_news' , true);
    }

    public function scopeHasntFiveNews($query)
    {
        return $query->where('has_five_news' , false);
    }

    public function scopeHeld($query)
    {
        return $query->where('held' , true);
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted', false);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', true);
    }

    public function scopeUnconfirmed($query)
    {
        return $query->whereNull('status');
    }

    // realtions

    public function creator()
    {
        return $this->belongsTo(EmployeeInfo::class ,'creator_id');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function acceptor()
    {
        return $this->belongsTo(EmployeeInfo::class , 'acceptor_id');
    }

    public function photo()
    {
        return $this->hasOne(Photo::class , 'id' , 'photo_id');
    }

    public function map()
    {
        return $this->hasOne(Photo::class , 'id' , 'map_id');
    }

    public function verticalBanner()
    {
        return $this->hasOne(Photo::class , 'id' , 'vertical_banner_id');
    }

    public function horizontalBanner()
    {
        return $this->hasOne(Photo::class , 'id' , 'horizontal_banner_id');
    }

    public function plans()
    {
        return $this->hasMany(EventPlan::class , 'event_id' , 'id');
    }

    public function companies()
    {
        return $this->belongsToMany(CompanyInfo::class , 'company_events' , 'event_id' , 'company_id');
    }

    public function students()
    {
        return $this->belongsToMany(StudentInfo::class  , 'student_events' , 'event_id' , 'student_id');
    }

    public function internPositions()
    {
        return $this->hasMany(InternPosition::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }
}

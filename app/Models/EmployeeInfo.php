<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
/**
 * @method user();
 */
class EmployeeInfo extends Model
{
    use HasFactory,HasRoles;

    protected $fillable = [
        'employee_code',
        'university_id',
        'university_position',
        'about',
        'status',
        'logo_id',
        'header_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($employeeInfo) {
            if ($employeeInfo->user) {
                $employeeInfo->user->delete();
            }
        });
    }

    // realations
    
    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function acceptor()
    {
        return $this->hasMany(Event::class , 'acceptor_id');
    }
    
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'title',
        'description',
        'end_date',
        'hours_passed',
        'academy_id',
        'type',
        'course',
        'photo_id',
        'payment_id',
        'status',
        'seen',
        'deleted',
    ];



    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , 0);
    }

    /**
     * status = [
     *  0 => create || defult
     *  1 => payed 
     *  2 => accept by that university
     *  3 => reject bt that university
     * ]
     */

    public function scopeNotConfirm($query)
    {
        return $query->where('status' , 0);
    }

    public function scopePayed($query)
    {
        return $query->where('status' , 1);
    }

    public function scopeConfirm($query)
    {
        return $query->where('status' , 2);
    }

    public function student()
    {
        return $this->belongsTo(StudentInfo::class , 'student_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class , 'photo_id');
    }

    public function pay()
    {
        return $this->belongsTo(StudentSkillPayment::class , 'payment_id' , 'id');
    }

    public function academy()
    {
        return $this->belongsTo(University::class , 'academy_id' , 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class University extends Model
{
    use HasFactory;

    // scope

    public function scopeCountry($query)
    {
        return $query->whereNull('parent');
    }

    public function scopeSubUnitsOfTheProvince($query)
    {
        return $query->where('parent' , Auth::user()->groupable->university_id);
    }

    // realtions 

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function parent()
    {
        return $this->belongsTo(University::class , 'parent');
    }

    public function subUnits()
    {
        return $this->hasMany(University::class , 'parent');
    }

    public function students()
    {
        return $this->hasMany(StudentInfo::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    
}

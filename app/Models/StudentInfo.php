<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StudentInfo extends Model
{
    use HasFactory;

    protected $fillable = ['university_id' , 'student_code' , 'grade' ,'major' ,'avg' ,'end_date' ,'about', 'logo_id' , 'header_id' , 'status'];

    // scopes 

    public function scopeInThisUniversity($query)
    {
        return $query->where('university_id' , Auth::user()->groupable->university_id);
    }

    public function scopeInThisOrSubUnitsUniversity($query)
    {
        return $query->whereRelation('university' , 'parent' , Auth::user()->groupable->university_id)
        ->orWhere('university_id' , Auth::user()->groupable->university_id);
    }

    public function scopeHasEvents($query)
    {
        return $query->whereHas('events');
    }

    public function scopeUserConfirmed($query)
    {
        return $query->whereRelation('user' , 'status' , '1');
    }

    public function scopeUserUnConfirmed($query)
    {
        return $query->whereRelation('user' , 'status' , null);
    }

    // relations 

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class , 'student_events' , 'student_id' , 'event_id');
    }

    public function internRequests()
    {
        return $this->hasMany(InternRequest::class , 'student_id');
    }

    public function skills()
    {
        return $this->hasMany(StudentSkill::class , 'student_id');
    }

    
}

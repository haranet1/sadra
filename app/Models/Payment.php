<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_info_id',
        'user_info_type',
        'paymentable_id',
        'paymentable_type',
        'price',
        'payment_key',
        'gateway',
        'receipt' ,
        'tracking_code',
        'status',
        'payment_date_time',
        'payer_bank',
        'card_number',
    ];

    // relations

    public function userInfo()
    {
        return $this->morphTo();
    }

    public function paymentable()
    {
        return $this->morphTo();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternPosition extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id', 'event_id', 'title', 'description', 'status', 'deleted',
    ];

    // scopes 

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted', null);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnconfirmed($query)
    {
        return $query->whereNull('status');
    }

    // relations

    public function companyInfo()
    {
        return $this->belongsTo(CompanyInfo::class , 'company_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }

    public function internRequests()
    {
        return $this->hasMany(InternRequest::class , 'intern_position_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSkillPayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'quantity',
        'amount',
        'receipt'
    ];

    public function student()
    {
        return $this->belongsTo(StudentInfo::class , 'student_id');
    }

    public function skills()
    {
        return $this->hasMany(StudentSkill::class , 'payment_id' , 'id');
    }

    public function payment()
    {
        return $this->morphOne(Payment::class , 'paymentable');
    }
}

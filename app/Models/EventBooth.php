<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventBooth extends Model
{
    use HasFactory;

    protected $fillable = [
        'plan_id' , 'name', 'reserved'
    ];

    // relations

    public function plan()
    {
        return $this->belongsTo(EventPlan::class , 'plan_id' , 'id');
    }
}

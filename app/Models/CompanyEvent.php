<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CompanyEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id' , 'event_id', 'booth_id', 'status', 'manpower_require_id',
    ];

    public function scopePaid($query)
    {
        return $query->where('status' , 1);
    }

    public function scopeInThisUniversityAndSubUnits($query)
    {
        return $query->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->orWhereRelation('event.university' , 'parent' , Auth::user()->groupable->university_id);
    }

    public function scopeInThisUniversity($query)
    {
        return $query->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id);
    }

    // relations 

    public function booth()
    {
        return $this->hasOne(EventBooth::class , 'id' ,'booth_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }

    public function companyInfo()
    {
        return $this->belongsTo(CompanyInfo::class , 'company_id');
    }

    public function payment()
    {
        return $this->morphOne(Payment::class , 'paymentable');
    }

}

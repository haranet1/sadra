<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CompanyInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'n_code' , 'registration_number', 'co_phone', 'activity_field', 'year', 'ceo', 'website', 'co_email', 'about', 'address', 'logo_id', 'header_id','province_id','city_id'
    ];

    // scopes 

    public function scopeThisUniversityProvince($query)
    {
        return $query->where('province_id' , Auth::user()->groupable->university->province_id);
    }

    public function scopeThisUniversityCity($query)
    {
        return $query->where('city_id' , Auth::user()->groupable->university->city_id);
    }

    public function scopeHasEvents($query)
    {
        return $query->whereHas('events');
    }

    public function scopeUserConfirmed($query)
    {
        return $query->whereRelation('user' , 'status' , '1');
    }

    // realations

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function logo()
    {
        return $this->hasOne(Photo::class , 'id' , 'logo_id');
    }

    public function header()
    {
        return $this->hasOne(Photo::class , 'id' ,'header_id');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class , 'company_events' ,'company_id' ,'event_id');
    }

    public function payments()
    {
        return $this->morphMany(Payment::class , 'userInfo' , 'user_info_type' , 'user_info_id');
    }

    public function internPositions()
    {
        return $this->hasMany(InternPosition::class , 'company_id');
    }

    public function internRequests()
    {
        return $this->hasMany(InternRequest::class , 'company_id');
    }
}

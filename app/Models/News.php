<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'creator_id',
        'acceptor_id',
        'title',
        'description',
        'counter',
        'photo_id',
        'event_id',
        'university_id',
        'deleted',
        'status',
    ];

    // scopes 

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted', 0);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnconfirmed($query)
    {
        return $query->whereNull('status');
    }

    public function scopePublicNews($query)
    {
        return $query->whereNull('event_id');
    }

    public function scopeEventNews($query)
    {
        return $query->whereNotNull('event_id');
    }

    // relations

    public function creator()
    {
        return $this->belongsTo(EmployeeInfo::class , 'creator_id');
    }

    public function acceptor()
    {
        return $this->belongsTo(EmployeeInfo::class , 'acceptor_id');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function photo()
    {
        return $this->hasOne(Photo::class , 'id' , 'photo_id');
    }
}

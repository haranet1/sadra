<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        'name' , 'price', 'dimensions', 'tables_qty', 'chairs_qty', 'deleted', 'event_id', 'map_id'
    ];

    // scopes

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted', 0);
    }

    // relations

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }

    public function map()
    {
        return $this->hasOne(Photo::class ,'id' , 'map_id');
    }

    public function Booths()
    {
        return $this->hasMany(EventBooth::class , 'plan_id' , 'id');
    }


}

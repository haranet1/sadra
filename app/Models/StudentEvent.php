<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StudentEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id' , 'event_id' , 'status'
    ];

    // scopes
    
    public function scopeInThisUniversityAndSubUnits($query)
    {
        return $query->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id)
        ->orWhereRelation('event.university' , 'parent' , Auth::user()->groupable->university_id);
    }

    public function scopeInThisUniversity($query)
    {
        return $query->whereRelation('event.university' , 'id' , Auth::user()->groupable->university_id);
    }


    // relations

    public function studentInfo()
    {
        return $this->belongsTo(StudentInfo::class , 'student_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }
}

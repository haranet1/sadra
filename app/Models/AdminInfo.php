<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminInfo extends Model
{
    use HasFactory;

    protected $fillable = ['status'];

    // relations 

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    
}

<?php

namespace App\Services\Notification\Providers;

use App\Models\User;
use App\Services\Notification\Providers\Contracts\Provider;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class EmailProvider implements Provider
{
    private $user;
    private $mailale;

    public function __construct(User $user, Mailable $mailable)
    {
        $this->user = $user;
        $this->mailale = $mailable;
    }

    public function send()
    {
        return Mail::to($this->user)->send($this->mailale);
    }
}
